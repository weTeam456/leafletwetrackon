var wertrackon = angular.module('wertrackon', [
    'ngAnimate',
    'ngResource',
    'mm.acl',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'ngTable',
    'nvd3',
    'uiGmapgoogle-maps',
    'angularjs-dropdown-multiselect',
    'ui.bootstrap.modal',
    'LocalStorageModule',
    'ngCsv',
    'ngSanitize',
    'ui.select2',
    'angularMoment',
    'ui.bootstrap.datetimepicker',
    'ui-leaflet',
    'angucomplete-alt'
])

.service('apppdfService', function() {
            var curdate = new Date();

            this.pdfDownload = function(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align) {
                
                function buildTableBody(data, columns) {
                    var body = [colHeadings];
                    body.push();
                    data.forEach(function(row) {
                        var dataRow = [];
                        columns.forEach(function(column) {
                            if (row[column] == null) {
                                row[column] = 'NA';
                            }                            
                            dataRow.push(row[column].toString());
                        });
                        body.push(dataRow);
                    });
                    return body;
                }
                
                function table(data, columns) {
                    return {
                        table: {
                            alignment: 'center',
                            body: buildTableBody(data, columns)
                        },
                        alignment: "center",
                        wordspacing: "10px"
                    };
                }
                
                var docDefinition = {
                      content: [
                            {table: {
                                widths: ['auto', 'auto'],
                                body: [
                                // url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAUEBAQEAwUEBAQGBQUGCA0ICAcHCBALDAkNExAUExIQEhIUFx0ZFBYcFhISGiMaHB4fISEhFBkkJyQgJh0gISD/2wBDAQUGBggHCA8ICA8gFRIVICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICD/wAARCACAAIADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7LooooAKKK4vVPGFxHqDQ6akTQxnaXcE7z3xz0rysyzXC5ZTVXEysm7Lq2dFDD1MRLlpo7GWVIIXmkbaiKWY+gFchc+N8S4s7HdGD96VsE/gOlb2j6vb6xZllAWVeJYjzj/EGuM8SaIdLu/PgU/ZJT8v+wf7v+FfNcRZjjY4KGNyua9m92ld26PXp0el0d+BoUnVdLEL3uh3un30eo6fFeRDCyDlT/CehH51HHBHa3N1qDXkjwyKGKu+UjAzkj0Fc54JvMpdWDN0IlT8eD/T86yvE0dzZarNarPKLSf8AfLHuO3nrx065qqnECjlNHM5U+d7OztaVmr+l7/ehRwV8TLDp2/y3OstbyPxForiG4e1kLYfy2w6YbP6j+dasLRi3URy+YiDbvLbiccHJ9a8hSSSMnynZWIx8pxn2r0PVHXRfB3kIQH8tYVxxlj1P8zXPkfEbxlGriK8LOlD3pX3tdqy6X1LxeB9lOMIP4nojNl8bFb1lislktgcBi+GYevSul0zU7fVbP7TbbgA21lYYKn0/WvMtN0+fUr5LS3GCeWbsi9ya768vbHwxpMcESbnxiOLPLnux/qa5uHc4zCvGrjcfNKhHq117K3b59FuaY7C0YONKivfZuUVyOj+LZLrUFtdQjijWU4R0yMHsDn1rrq+2y7M8NmVL22GldJ27NHk16FShLlqIKKKK9MwCiiub8T67LpqR2tmwW5kG4sRnYv8Aif6Vw4/HUcBh5Ymu/dj9/ojajRlWmqcN2b91Cbizmt1kaIyIU3r1XIxmvNr/AMOapp4LND50Q/5aRfNx7jqKntPFer2z/vJVuU7rIvP4EV0Vj4x064wt2j2j+p+ZfzH+Ffn+LxuR8RKMa1R05ra+n+cfxTPZpUsXgW3GPMvL+rnEWV9cafeJdWr7ZF/Jh3B9q9Htbqx8RaO6suVcbZIyeUb/ADyDUdzpGi61GZ1EbM3/AC2gYA/jjg/jVDTPDV5pespcQXytbYIcEYZh6Y6fjW2U5XmOU1fYq1bDVNHZrS/Wz/G17ojE4ihiY8/wzRT0XQdS07xNvK/6NFuBlzxIpHAA9c4/Kunv9KsNSMZvYPMMedp3EYz16fSmatqsGk2JuJRvc/LHGDy5rzrUNY1DUpGa5uG2HpEpwg/Dv+Na43GZXw7QlgeV1OZ83I7NK+17rRaaaN9SaNLEY6arX5baXO9j8P6CkqtHapvQhh+8Y8j8apeLdP1C/trb7HH5qRsS6A856A/z/OvPxwcjg+orodH8T3djKsV5I1xang7jlk9we/0rxKPEeW42lPA1qHsIVLXcbfjov1OyeAxFKSrRnztdH/w5v26WfhLRPMuCJLuXqB1dv7o9h61xF7ez39291dPukb8lHYD2rrb3w9fa1qrXkuoRmzbBhZMkhOwA6fjmrBg8NeHADNiW5HI3/PJ+XQfpXRmWW4rGRVDShhKeicmtf73nfpe33meHxFOl7+s6ku3TyOW0/wAP6pqOGjgMMR/5ay/KPw7mvTYY2it442kMjKoUuerYHWuF1DxleTZSwiFsn99vmc/0H61iQ6rqMF4LtbyVpQcne5Ib2PtXPl2c5RkUnTwvNVcrc0tlp2X9epdfC4rGLmqWjbZHrFFQWd0l7Yw3cYwsqBgD2qev1uE41IqcXdPU+baadmFc/rXhmPVrxbtbpoZNoQgruBA/lXQE4BJ6CuOvPGyrKVsbPzEHR5Wxu+grws9r5bTw6p5m/dlstbu3a2uh2YSFdz5sPug/4Qde+pH/AL9f/Xo/4QcdtSP/AH6/+vVZfGOq3EqxW1jCzt0VQzE/rW/Y/wDCSXGHvWtbNP7qoWf+eB+tfK4PB8PY6XLhcPKfmuZL73JI9CrVxtJXqTS+4xV8MpYXA2+IltZevHyNj/vquohvrGOBI31KGVlUAu0i5Y+vFeDfFa0+3fFa2stwVriK3hDkZxuYrn9a2f8AhQ8n/QzR/wDgF/8AZ16+BgsFVq0suw2idn77/KX6HqVMBhpUKVbG4nlc1dLkv+KPS9T0i1168WUasCI02rHHtYD1PX6VgyeEppFdtN1C3vAjFWAOCCO3GRn8q8713wBcfD6wl15NeSaQxtbwBIzDIJH+XI5PRC5rnfAfiWTwj4otryRmTT7n93crzhkzjf7lTz+Y715OYYXL6+ISx+H5Zy3ak9Oifb/JHbhsrqTw86uBr88Y7LltdrVrXX/M9etfCusXErK8K2yqcFpW4P0x1rRj8GxmYwPrEfnAbjGqDcB64zW74m8Q2vhzwxda1KVcRp+5TP8ArXP3VH1P6ZNfMFprWqWPie28UTSStdm4+0NKcjzhu+cZ9CMrj8KzxGRZNljhTqU3Ub3vJqy76WRz5bh8bmcJ1IzUFHbTd2vb/M+ptLsho1gLWa+EqBiULgLtB7dfWo7vTdBvbg3FysDSnqwkxn64NeQf8Ks8S+JNZv8AUNQ1YW9o87m3knZp3kjJypAzwMEd/wAKsf8ACirrH/I0Rf8AgIf/AIuvpP3kqKw6wfNTjspST22+K/yONYLAxlz1Mbab3tF9fNHqR0Xw1HGZGt7cIvVmk4H60Jp/hfjbHZH/AIGD/Wuf8F/DxPC9lqVpfX0epRXxQ4EJj2bc/wC0fWk1TwfcW4abTmNzH/zzP3x9PWuLHQrYWlGtRwEJK2qVuZfctflqcDjRdaVOOIbXR6pP73od1D5IhVbfZ5ajChMYA9sU+uD8Hx30OsSxGKWOHyz5gdSADkY/Gu8r3cmzF5jhFXdPkd2ren3HnYqj7Co4J3CuM8QL4I8ONHc68xsYJ2OJGEnkg+hZRhc54BIz2rs64/xjoPi/XYmsNE8RWenaddL5Vystn5kqqfvFGzjJHGCO/WvTqYLC4uUY4qEZJd1e34M5JVq1GDdFu/l/w6/MyL34k+ENLhgsvB8UfiLVrzi3sdLIZmPrIw+4B3Jrt9Fl1ifRrebXrS3s9RcEywW0hkSPk4G4gZOMZ7ZzivL5G8I/BHSZILCMXmo3CbwjkCRo16ySvgkLk4AAwTgKucmud0f4j/FXxhNbS6Nb6NpNjcXBt7ea8jcC5kALFF5JOFU5wMDHXNevHBQ9nbDxUYLq9PuS/wAjyvrzhUtXd5v7MdbfMg+LUcs3xQjhhOJpIIEjOcYYsQOe3NWx8P8A4qYx/a0o/wC4m/8AjVrSNR0rxF4r01viFo8cGqXmF03VrK4k+x3rRsRsAJ+RwwI2nr+WfYdS8QaJo4c6pqttabAGYSSAEZOF468ngDv2r4+XD8nias67fvO65X0+4+/hxVFYSjTw0V7itLmit/LU+ZNa03W7PxJBo/i/UbmDay/v55GuFSNjy65PI+npXq3jvwLYSfDWxk0FfNOjReZG4IYzwnmQ5HBJ+/8Agcda7G903wl8QNMikm8nUoYHO2SJyrxNjlTjBU9MqfbitbR9FsdC0pdL09ZBaITtSWQybQewz29qyo5KqUqtOesZrRv4l5fqbYjiJ1o0K1P3Zwd2lblfn31WlvP7/mcatr/jG38P+DhIHW3fyoSM5bPRn/3Ez+Ga9I+KHgpbbwTpMuj27PFoqGKRVGWMTAZc+pDDJ/3ia7/SPBPhjQdVl1TS9LSC6kyN25iEB6hQThfwroHOI2OwvgfdGMn254ow+TP2NSGJlzSlZX7JbBiuII/WaVTBw5YQbdu7l8W33I8a8G/F3SbHw5a6Zr8Vwk9pGsKTQp5iyIBhc85Bx1/P2ro/+FyeC/8Anre/+AxqTUPBXga+iGqXnhm5tWmk2lIY5EbdnAJSM8ZPfHuar6h8O/hzpZsvt1jLELy4W1iJuZMGRgdoPzd8Y+pFaQo5vTSpxlBpdXe5lUxOQ1pOrONSLerS5bK/a51Hhnxfo3iyK5k0hpmW2ZVk82MpyemPyry/xH8brHSvira6XCrSaNZM9rfSjoZGKDeOM/u8MD65b2rpfF2gaf4I+FHim48LiawmltstIszMw5xwSTjgnpXy/wCGvB3ibxrftFolhJd/N++uZDtiQnqWc9/bk+1fa5Lg/a0HVxzV1o7beuvkfn2fY32WIVHL07PVX3t208/wPuiGaO4t47iFt8Uih1YdwRkGn1xvw3019B8Ip4fufEUet3thIyzOj7vJLHcI+TnAzxn9MYrsq86pFQm4p3PTpTc4KTVmwqjrGrWehaHe6xqEmy1s4WmkPfAGcD3PQe9XqwvGGgt4n8HajoKyLEb2MR726L8wOf0pQUXJKWwVHJQbhvbQ+TLy/v8A4g+MrC2urlY7vX7xZJ23jEEW4rFGPZEDNjuWHevoOHwtrdtd6Nb29gY7HSNfaW2RXQLHZfZ2RcDP94njrkk1za/Ama1RGsdYtEljIdM2xXDDkHdkkHPet+11r4h+EXC+JtNbXNMX713a4eSMevABP/AgPrX0eKdOsoxws07X0ej+V/LQ+YwlOpQcpYuDV3utfvtfrqUB4C1q/wDDmn+HdUtZE05BqHnbJULRSNP5ltKnzfeXk5GMZI71y/gvSPFHijUdR12+t1TV7FPs0XmRtARM0JU3ZDAfvTtjHTgZxXo9z8RJNXnXT/Aumvq92yhmnlUxwwA/3s45/L8aibwX411geZr3jiW2Lf8ALCwQqi+2QVz+Irjjzwi1Xajfve/notdfOx2ShTnKLoJzt2tbbTV6aeVytqWl+OtP+0alpNqsl9cyfZnaOQF2iaVyrvkjJiUpj2Lr6ET3uieOtL0u+n0J45L6RAoJumkkkYnBdg4Clh1yMcDaOMYrS/DjxTY/v9E8dXhmHIS4Zwp+p3Efoafp/jjxB4b1CLSviBYeXFI22PUol+Q/72OD+GCO471HsOdXoSU7dNU/ue/yNPacjtXjKF+t00vmtvmbNjpPjjTJIYodVhvIZSFne7kaVo8AkuvA5J4x05BAGOdv+0tVsNMubnVrKN5oy3lR2rEiRR0JZsBc+/A9a5S58Za94m1CXTPAdmpgibbLqtyMRKf9kY5/X6Y5py/DS41BfM8TeLdU1CVvvJE/lxj2AOf6Vi6CjriJKL7bv7unzaN41m9MNFyXe9l973+VyWx+IStqVxHd6D4iTzNgji/s0yBDjBw0ZYEHg5zXP/Ee91zxENDh0bw5rXkWOoRX00jWZBby2BCqCRz164Fas3wb8O/estR1K0kHRllVsH8v61jXfhn4keEVN1oOvTataR8+QcswH/XN85/4Cc11UqeGc1KjNX7STS+/U5as8UoONaDt3jZv9Dt5NW0fxL4J1GfxBo97YabtcT2uox+VI8a852g5wSOOea1NLj0PUfCkEekxRw6TcQYjS2/dBEI6DbjaR7cg14P4p+IureJdDi0a9tYrMpJuuDGSPNI6KVP3cHkjJ5A9Kd4V8H+Mtbga3tZrvS9JmOZXkd0jcd8Jkb/5e9dDyrko89afJrturf5mCzXnrclGHPpvazv/AJHXfCHTmtvEPiSW3dnsoXFskmciQh2IOe5xj/vqvYKyfDvh+x8M6HDpNhkxplmkb70jHqx/z0AFa1eRja6xFeVRbf5aHsYLD/V6Eab3/wA9QooorjO0gup5Le3aSG1kupP4YoyoLfixAH51w2reOvE2gu1xq3w7v20xOXubG6juWQepQYOK9AorSEox+KN/vMqkJS+GVvuPMrz4u/DfQ9DTUtNu4rp77Mq2ljEPOdzwd442n/ewfrWFL40+NHiKH7R4Z8CQ6VaMPke/ceaR2OHZf/QTXoNt8PvCNp4um8VQaLCupyj72PkRu7qvQMe7dfzOejuLq1s4jLd3MVvGP4pXCj8zXZ7ajC3s4cz/AL2v4L+vI4vYV539pU5V/d0/F3/rqeAX3i79oDw6ftWqeHoLu2QZYRWyyrj38psiuj8J/F7wv4+jPhvxRp8enXlwNgjmbdBO3orHBVvQHBz0JNd9N478HwMVk8RWZI/uSbv5ZrifFXgn4d/E1Xk0rU7K11sjK3NqV3P/ANdI8jePfr711qUJr99ScP70U1b5HG4VIP8AcVuf+7Jp3+ZZ1b4s/DrwParo2nSm9e2Hli205Q4UjsXJC5z15Jrnj8W/iNq7FvDfwxuvJOdslysjA++cKPwya6PQfDHwz+GUCefdWb6oo+e6uiJJyf8AZUZKD2UfXPWtGb4ueDY32pPdTD+8luQD+eKI04y1o0ZVPN31+S/zHKpOKSrVo0/JW0+b/wAjz2b4mfGnTXMmo/D0PCOSI7SY4H+8rNVrSf2i7D7SLTxP4cudNkB2u8DeZt+qMFYfrXdW3xb8F3EgR72e3z3lgbH6ZrXuLDwT46sD59vputw9NwCuyfiPmU/kaVTkh/vGH5fNXX56CpqpJ/7NieZ9nZ/lqedeIPizY6hq9tpnw30GHxHrdygcXLwELCPfIBJHfJUDue1dz4Mh+IsaPJ42vNJlVwSsVpEwkjPYFshSPbH4mrfhLwL4c8EwXUWg2bRG6k3ySSNvfHZNx52jsPc101cVatSt7OjHTu9/+B8jvoUK1/aVpa9lt/wfmFFFFcR3hRVa9v7PToVmvrlLeNnEYZzgFj0H1NRnV9LXT21BtQgW0RtjTGQBVbOME9jk4xSuilGT1SLtFVp9Qsra7t7S4uo457kkQxscNIR1x6097u1jvIrN7iNbmVWeOIsNzAYyQPbIouhcr7EkiCRCpZgD3U4P51zl/wCGvCnm/aNS0VLk95pkabH1JyR9TWmNf0U2ZvBqlsYBN9nL+YMCT+5/ve1WjfWa3b2huY/tEcfnNFu+ZUzjdjrjINXCq46xdvRkzo82k4/ejlV8I/DvVFeO307TpSB832eTay59dpyK5zW/hHoSQteaTq8mkyR/MDPJujX8Thl+uTXZ3lx4N1iKE339m3yTI0sRlRZN6r95lyO3qK5DVPA/wg1iHz7iC0hQKrCSG6eJQGOFOA23BPGcV3UMfOEruq1+P5s4K+WqcdKKb+a/JCeHPhT4aa2W91DUjrbv95oZcRZ78qcn8T+Fbd9b/DnwtIlvcaZYLdOBst0t/Pnf0woBb8ab4Z8P+Bvh415a6TdG1e4kjSZJ7hny+CUAB4BIJ6DJ/Culk1XQbd4rlru1R7rOxxjdLtHIyOSR39MUq2OlUqNzqOUfW34apDo5eqdNKFNRl6X/AB0bM3Sbi11NwYvB01jb9pruCKLI9kyW/MCt2LT7GCbzoLKCKTGN6RhTj6gUh1LTxFaym9g8u7YJA3mDErEZAU9+AagXXdHYyBdRhJjmFu+G+7IeAh9/auOdRN6afM7YUZJaq/yNGiqMmsaVDa3N1JqECwWshinkLjbE/Hysex5H50q6tpr2c94t9D5FvnznLACLAyd3pxzzWd0ackuxdoqodU04XltZm9hFxdIZIY94zKoGSV9eKdZX9lqMDT2N1HcxK5jLxtkBh1GaLoOWSV7GV4q0+81LSre3s4WldbyCZgsgQhEkDMQcjnA496wk8PatcaZe6HqFoFs3kuZPtELoz3BYnYXDfxEN19VBzXd0VDppu5tCvKEeVepxemaf4mutQ8PTazZxQf2WjeZKswcykxbDwO+efp70/V9L1q61a21q2tXNzZXy+VF5kYD2+0q/zdRkM3GeuK1vEWk3mqxWX2GZLee2nEyysxBXAIxgDnIJFYtn4Ru7ez+zyJaSA20cTAyMcOnmd9vOd65PXjvWbi17p0RqJ+/dJ7Wt569TNm8K6yZZrqG1ws91b3RtjIuVkSXLvnOOY1UfU1qtpmtDxLp3iCO0czGSaK6gMkY2wNjaAw6kbUOM/wB71qq3gzVl8tI76FxEJESZ3YS+WURVRiF+bG1l3cHaQeuanvPCN1c35lhis7aB/JyiyFim2VnfAKEchgAOnGMYqVFroauonvJfd8u5R0vw7rtlFonm20jm0sbmCVDPGVR3xtC9ODt/UVPpug6lH4bg0XUtKe7i8i3hm86eNsBdodEx0TClvXJ/GppvC2rrcfuJreW2Jt8pPO+5RBKCvO05JTIPTk961bnSNRvryG6umt9/kBMJIwFvIH3b4+OT0HOPujsSKFDyJlWv1X9a9/NmCvhzX7OGC2J/tKO01OCaGQuqubaNeA2SMsM498U6bwzrEmoW1wqtCJb+6vHMUwVrYSQ+WmCOpzhjjjk9e7rzwdqd1bQRLNbReWqhgJGIdxE6mQ/L1ZmXPfC9ScVZh8J3guJr2aSHznuvNNush8qSIhcox25PIzzkce5o5fIr2q35l93/AATOm0DxDeeFo7OSwisrzT4YjZpbvGY2mVg27nleVHTHBNag0nWLfxBe6tb2qGG6t0uTamQZ+2KpTGemMEEn1UVXh8La3a6nFqMFxayTRIqKJJHAHEg/u5+XeoHqF5HSkg8GXIubf7YLW5igeYB2lfe0bcxjG3jaxPfp60KL7CdSOvvK3p/wfJFDUfDOvJb67ZWFs1zBqlpES8kkaEXCnDcDHBXGT6j8a07fRtY+2X1hcWg/s+8ufOa68xXlZNgwrhs7sEBe/Aplv4T1SPTIbWSa2eRIkRpPMbOBB5bRD5fuM3zE/wC0eCQDW74f0i40e3uYLi5FyGkHlSHl/LCgKrn+IjBGe4A704w12JqVrR0afy9PPyOTh8N+IZtL0i1mhNtdaSQsF00qNlBIAOAevlEg+4rqfCun3WmaPLa3Vv8AZz9rnkRNwb5GkLL09jj8K3aK1jTUXdHNUxEqkeVrzP/Z')
                                    [{image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAUEBAQEAwUEBAQGBQUGCA0ICAcHCBALDAkNExAUExIQEhIUFx0ZFBYcFhISGiMaHB4fISEhFBkkJyQgJh0gISD/2wBDAQUGBggHCA8ICA8gFRIVICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICD/wAARCACAAIADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7LooooAKKK4vVPGFxHqDQ6akTQxnaXcE7z3xz0rysyzXC5ZTVXEysm7Lq2dFDD1MRLlpo7GWVIIXmkbaiKWY+gFchc+N8S4s7HdGD96VsE/gOlb2j6vb6xZllAWVeJYjzj/EGuM8SaIdLu/PgU/ZJT8v+wf7v+FfNcRZjjY4KGNyua9m92ld26PXp0el0d+BoUnVdLEL3uh3un30eo6fFeRDCyDlT/CehH51HHBHa3N1qDXkjwyKGKu+UjAzkj0Fc54JvMpdWDN0IlT8eD/T86yvE0dzZarNarPKLSf8AfLHuO3nrx065qqnECjlNHM5U+d7OztaVmr+l7/ehRwV8TLDp2/y3OstbyPxForiG4e1kLYfy2w6YbP6j+dasLRi3URy+YiDbvLbiccHJ9a8hSSSMnynZWIx8pxn2r0PVHXRfB3kIQH8tYVxxlj1P8zXPkfEbxlGriK8LOlD3pX3tdqy6X1LxeB9lOMIP4nojNl8bFb1lislktgcBi+GYevSul0zU7fVbP7TbbgA21lYYKn0/WvMtN0+fUr5LS3GCeWbsi9ya768vbHwxpMcESbnxiOLPLnux/qa5uHc4zCvGrjcfNKhHq117K3b59FuaY7C0YONKivfZuUVyOj+LZLrUFtdQjijWU4R0yMHsDn1rrq+2y7M8NmVL22GldJ27NHk16FShLlqIKKKK9MwCiiub8T67LpqR2tmwW5kG4sRnYv8Aif6Vw4/HUcBh5Ymu/dj9/ojajRlWmqcN2b91Cbizmt1kaIyIU3r1XIxmvNr/AMOapp4LND50Q/5aRfNx7jqKntPFer2z/vJVuU7rIvP4EV0Vj4x064wt2j2j+p+ZfzH+Ffn+LxuR8RKMa1R05ra+n+cfxTPZpUsXgW3GPMvL+rnEWV9cafeJdWr7ZF/Jh3B9q9Htbqx8RaO6suVcbZIyeUb/ADyDUdzpGi61GZ1EbM3/AC2gYA/jjg/jVDTPDV5pespcQXytbYIcEYZh6Y6fjW2U5XmOU1fYq1bDVNHZrS/Wz/G17ojE4ihiY8/wzRT0XQdS07xNvK/6NFuBlzxIpHAA9c4/Kunv9KsNSMZvYPMMedp3EYz16fSmatqsGk2JuJRvc/LHGDy5rzrUNY1DUpGa5uG2HpEpwg/Dv+Na43GZXw7QlgeV1OZ83I7NK+17rRaaaN9SaNLEY6arX5baXO9j8P6CkqtHapvQhh+8Y8j8apeLdP1C/trb7HH5qRsS6A856A/z/OvPxwcjg+orodH8T3djKsV5I1xang7jlk9we/0rxKPEeW42lPA1qHsIVLXcbfjov1OyeAxFKSrRnztdH/w5v26WfhLRPMuCJLuXqB1dv7o9h61xF7ez39291dPukb8lHYD2rrb3w9fa1qrXkuoRmzbBhZMkhOwA6fjmrBg8NeHADNiW5HI3/PJ+XQfpXRmWW4rGRVDShhKeicmtf73nfpe33meHxFOl7+s6ku3TyOW0/wAP6pqOGjgMMR/5ay/KPw7mvTYY2it442kMjKoUuerYHWuF1DxleTZSwiFsn99vmc/0H61iQ6rqMF4LtbyVpQcne5Ib2PtXPl2c5RkUnTwvNVcrc0tlp2X9epdfC4rGLmqWjbZHrFFQWd0l7Yw3cYwsqBgD2qev1uE41IqcXdPU+baadmFc/rXhmPVrxbtbpoZNoQgruBA/lXQE4BJ6CuOvPGyrKVsbPzEHR5Wxu+grws9r5bTw6p5m/dlstbu3a2uh2YSFdz5sPug/4Qde+pH/AL9f/Xo/4QcdtSP/AH6/+vVZfGOq3EqxW1jCzt0VQzE/rW/Y/wDCSXGHvWtbNP7qoWf+eB+tfK4PB8PY6XLhcPKfmuZL73JI9CrVxtJXqTS+4xV8MpYXA2+IltZevHyNj/vquohvrGOBI31KGVlUAu0i5Y+vFeDfFa0+3fFa2stwVriK3hDkZxuYrn9a2f8AhQ8n/QzR/wDgF/8AZ16+BgsFVq0suw2idn77/KX6HqVMBhpUKVbG4nlc1dLkv+KPS9T0i1168WUasCI02rHHtYD1PX6VgyeEppFdtN1C3vAjFWAOCCO3GRn8q8713wBcfD6wl15NeSaQxtbwBIzDIJH+XI5PRC5rnfAfiWTwj4otryRmTT7n93crzhkzjf7lTz+Y715OYYXL6+ISx+H5Zy3ak9Oifb/JHbhsrqTw86uBr88Y7LltdrVrXX/M9etfCusXErK8K2yqcFpW4P0x1rRj8GxmYwPrEfnAbjGqDcB64zW74m8Q2vhzwxda1KVcRp+5TP8ArXP3VH1P6ZNfMFprWqWPie28UTSStdm4+0NKcjzhu+cZ9CMrj8KzxGRZNljhTqU3Ub3vJqy76WRz5bh8bmcJ1IzUFHbTd2vb/M+ptLsho1gLWa+EqBiULgLtB7dfWo7vTdBvbg3FysDSnqwkxn64NeQf8Ks8S+JNZv8AUNQ1YW9o87m3knZp3kjJypAzwMEd/wAKsf8ACirrH/I0Rf8AgIf/AIuvpP3kqKw6wfNTjspST22+K/yONYLAxlz1Mbab3tF9fNHqR0Xw1HGZGt7cIvVmk4H60Jp/hfjbHZH/AIGD/Wuf8F/DxPC9lqVpfX0epRXxQ4EJj2bc/wC0fWk1TwfcW4abTmNzH/zzP3x9PWuLHQrYWlGtRwEJK2qVuZfctflqcDjRdaVOOIbXR6pP73od1D5IhVbfZ5ajChMYA9sU+uD8Hx30OsSxGKWOHyz5gdSADkY/Gu8r3cmzF5jhFXdPkd2ren3HnYqj7Co4J3CuM8QL4I8ONHc68xsYJ2OJGEnkg+hZRhc54BIz2rs64/xjoPi/XYmsNE8RWenaddL5Vystn5kqqfvFGzjJHGCO/WvTqYLC4uUY4qEZJd1e34M5JVq1GDdFu/l/w6/MyL34k+ENLhgsvB8UfiLVrzi3sdLIZmPrIw+4B3Jrt9Fl1ifRrebXrS3s9RcEywW0hkSPk4G4gZOMZ7ZzivL5G8I/BHSZILCMXmo3CbwjkCRo16ySvgkLk4AAwTgKucmud0f4j/FXxhNbS6Nb6NpNjcXBt7ea8jcC5kALFF5JOFU5wMDHXNevHBQ9nbDxUYLq9PuS/wAjyvrzhUtXd5v7MdbfMg+LUcs3xQjhhOJpIIEjOcYYsQOe3NWx8P8A4qYx/a0o/wC4m/8AjVrSNR0rxF4r01viFo8cGqXmF03VrK4k+x3rRsRsAJ+RwwI2nr+WfYdS8QaJo4c6pqttabAGYSSAEZOF468ngDv2r4+XD8nias67fvO65X0+4+/hxVFYSjTw0V7itLmit/LU+ZNa03W7PxJBo/i/UbmDay/v55GuFSNjy65PI+npXq3jvwLYSfDWxk0FfNOjReZG4IYzwnmQ5HBJ+/8Agcda7G903wl8QNMikm8nUoYHO2SJyrxNjlTjBU9MqfbitbR9FsdC0pdL09ZBaITtSWQybQewz29qyo5KqUqtOesZrRv4l5fqbYjiJ1o0K1P3Zwd2lblfn31WlvP7/mcatr/jG38P+DhIHW3fyoSM5bPRn/3Ez+Ga9I+KHgpbbwTpMuj27PFoqGKRVGWMTAZc+pDDJ/3ia7/SPBPhjQdVl1TS9LSC6kyN25iEB6hQThfwroHOI2OwvgfdGMn254ow+TP2NSGJlzSlZX7JbBiuII/WaVTBw5YQbdu7l8W33I8a8G/F3SbHw5a6Zr8Vwk9pGsKTQp5iyIBhc85Bx1/P2ro/+FyeC/8Anre/+AxqTUPBXga+iGqXnhm5tWmk2lIY5EbdnAJSM8ZPfHuar6h8O/hzpZsvt1jLELy4W1iJuZMGRgdoPzd8Y+pFaQo5vTSpxlBpdXe5lUxOQ1pOrONSLerS5bK/a51Hhnxfo3iyK5k0hpmW2ZVk82MpyemPyry/xH8brHSvira6XCrSaNZM9rfSjoZGKDeOM/u8MD65b2rpfF2gaf4I+FHim48LiawmltstIszMw5xwSTjgnpXy/wCGvB3ibxrftFolhJd/N++uZDtiQnqWc9/bk+1fa5Lg/a0HVxzV1o7beuvkfn2fY32WIVHL07PVX3t208/wPuiGaO4t47iFt8Uih1YdwRkGn1xvw3019B8Ip4fufEUet3thIyzOj7vJLHcI+TnAzxn9MYrsq86pFQm4p3PTpTc4KTVmwqjrGrWehaHe6xqEmy1s4WmkPfAGcD3PQe9XqwvGGgt4n8HajoKyLEb2MR726L8wOf0pQUXJKWwVHJQbhvbQ+TLy/v8A4g+MrC2urlY7vX7xZJ23jEEW4rFGPZEDNjuWHevoOHwtrdtd6Nb29gY7HSNfaW2RXQLHZfZ2RcDP94njrkk1za/Ama1RGsdYtEljIdM2xXDDkHdkkHPet+11r4h+EXC+JtNbXNMX713a4eSMevABP/AgPrX0eKdOsoxws07X0ej+V/LQ+YwlOpQcpYuDV3utfvtfrqUB4C1q/wDDmn+HdUtZE05BqHnbJULRSNP5ltKnzfeXk5GMZI71y/gvSPFHijUdR12+t1TV7FPs0XmRtARM0JU3ZDAfvTtjHTgZxXo9z8RJNXnXT/Aumvq92yhmnlUxwwA/3s45/L8aibwX411geZr3jiW2Lf8ALCwQqi+2QVz+Irjjzwi1Xajfve/notdfOx2ShTnKLoJzt2tbbTV6aeVytqWl+OtP+0alpNqsl9cyfZnaOQF2iaVyrvkjJiUpj2Lr6ET3uieOtL0u+n0J45L6RAoJumkkkYnBdg4Clh1yMcDaOMYrS/DjxTY/v9E8dXhmHIS4Zwp+p3Efoafp/jjxB4b1CLSviBYeXFI22PUol+Q/72OD+GCO471HsOdXoSU7dNU/ue/yNPacjtXjKF+t00vmtvmbNjpPjjTJIYodVhvIZSFne7kaVo8AkuvA5J4x05BAGOdv+0tVsNMubnVrKN5oy3lR2rEiRR0JZsBc+/A9a5S58Za94m1CXTPAdmpgibbLqtyMRKf9kY5/X6Y5py/DS41BfM8TeLdU1CVvvJE/lxj2AOf6Vi6CjriJKL7bv7unzaN41m9MNFyXe9l973+VyWx+IStqVxHd6D4iTzNgji/s0yBDjBw0ZYEHg5zXP/Ee91zxENDh0bw5rXkWOoRX00jWZBby2BCqCRz164Fas3wb8O/estR1K0kHRllVsH8v61jXfhn4keEVN1oOvTataR8+QcswH/XN85/4Cc11UqeGc1KjNX7STS+/U5as8UoONaDt3jZv9Dt5NW0fxL4J1GfxBo97YabtcT2uox+VI8a852g5wSOOea1NLj0PUfCkEekxRw6TcQYjS2/dBEI6DbjaR7cg14P4p+IureJdDi0a9tYrMpJuuDGSPNI6KVP3cHkjJ5A9Kd4V8H+Mtbga3tZrvS9JmOZXkd0jcd8Jkb/5e9dDyrko89afJrturf5mCzXnrclGHPpvazv/AJHXfCHTmtvEPiSW3dnsoXFskmciQh2IOe5xj/vqvYKyfDvh+x8M6HDpNhkxplmkb70jHqx/z0AFa1eRja6xFeVRbf5aHsYLD/V6Eab3/wA9QooorjO0gup5Le3aSG1kupP4YoyoLfixAH51w2reOvE2gu1xq3w7v20xOXubG6juWQepQYOK9AorSEox+KN/vMqkJS+GVvuPMrz4u/DfQ9DTUtNu4rp77Mq2ljEPOdzwd442n/ewfrWFL40+NHiKH7R4Z8CQ6VaMPke/ceaR2OHZf/QTXoNt8PvCNp4um8VQaLCupyj72PkRu7qvQMe7dfzOejuLq1s4jLd3MVvGP4pXCj8zXZ7ajC3s4cz/AL2v4L+vI4vYV539pU5V/d0/F3/rqeAX3i79oDw6ftWqeHoLu2QZYRWyyrj38psiuj8J/F7wv4+jPhvxRp8enXlwNgjmbdBO3orHBVvQHBz0JNd9N478HwMVk8RWZI/uSbv5ZrifFXgn4d/E1Xk0rU7K11sjK3NqV3P/ANdI8jePfr711qUJr99ScP70U1b5HG4VIP8AcVuf+7Jp3+ZZ1b4s/DrwParo2nSm9e2Hli205Q4UjsXJC5z15Jrnj8W/iNq7FvDfwxuvJOdslysjA++cKPwya6PQfDHwz+GUCefdWb6oo+e6uiJJyf8AZUZKD2UfXPWtGb4ueDY32pPdTD+8luQD+eKI04y1o0ZVPN31+S/zHKpOKSrVo0/JW0+b/wAjz2b4mfGnTXMmo/D0PCOSI7SY4H+8rNVrSf2i7D7SLTxP4cudNkB2u8DeZt+qMFYfrXdW3xb8F3EgR72e3z3lgbH6ZrXuLDwT46sD59vputw9NwCuyfiPmU/kaVTkh/vGH5fNXX56CpqpJ/7NieZ9nZ/lqedeIPizY6hq9tpnw30GHxHrdygcXLwELCPfIBJHfJUDue1dz4Mh+IsaPJ42vNJlVwSsVpEwkjPYFshSPbH4mrfhLwL4c8EwXUWg2bRG6k3ySSNvfHZNx52jsPc101cVatSt7OjHTu9/+B8jvoUK1/aVpa9lt/wfmFFFFcR3hRVa9v7PToVmvrlLeNnEYZzgFj0H1NRnV9LXT21BtQgW0RtjTGQBVbOME9jk4xSuilGT1SLtFVp9Qsra7t7S4uo457kkQxscNIR1x6097u1jvIrN7iNbmVWeOIsNzAYyQPbIouhcr7EkiCRCpZgD3U4P51zl/wCGvCnm/aNS0VLk95pkabH1JyR9TWmNf0U2ZvBqlsYBN9nL+YMCT+5/ve1WjfWa3b2huY/tEcfnNFu+ZUzjdjrjINXCq46xdvRkzo82k4/ejlV8I/DvVFeO307TpSB832eTay59dpyK5zW/hHoSQteaTq8mkyR/MDPJujX8Thl+uTXZ3lx4N1iKE339m3yTI0sRlRZN6r95lyO3qK5DVPA/wg1iHz7iC0hQKrCSG6eJQGOFOA23BPGcV3UMfOEruq1+P5s4K+WqcdKKb+a/JCeHPhT4aa2W91DUjrbv95oZcRZ78qcn8T+Fbd9b/DnwtIlvcaZYLdOBst0t/Pnf0woBb8ab4Z8P+Bvh415a6TdG1e4kjSZJ7hny+CUAB4BIJ6DJ/Culk1XQbd4rlru1R7rOxxjdLtHIyOSR39MUq2OlUqNzqOUfW34apDo5eqdNKFNRl6X/AB0bM3Sbi11NwYvB01jb9pruCKLI9kyW/MCt2LT7GCbzoLKCKTGN6RhTj6gUh1LTxFaym9g8u7YJA3mDErEZAU9+AagXXdHYyBdRhJjmFu+G+7IeAh9/auOdRN6afM7YUZJaq/yNGiqMmsaVDa3N1JqECwWshinkLjbE/Hysex5H50q6tpr2c94t9D5FvnznLACLAyd3pxzzWd0ackuxdoqodU04XltZm9hFxdIZIY94zKoGSV9eKdZX9lqMDT2N1HcxK5jLxtkBh1GaLoOWSV7GV4q0+81LSre3s4WldbyCZgsgQhEkDMQcjnA496wk8PatcaZe6HqFoFs3kuZPtELoz3BYnYXDfxEN19VBzXd0VDppu5tCvKEeVepxemaf4mutQ8PTazZxQf2WjeZKswcykxbDwO+efp70/V9L1q61a21q2tXNzZXy+VF5kYD2+0q/zdRkM3GeuK1vEWk3mqxWX2GZLee2nEyysxBXAIxgDnIJFYtn4Ru7ez+zyJaSA20cTAyMcOnmd9vOd65PXjvWbi17p0RqJ+/dJ7Wt569TNm8K6yZZrqG1ws91b3RtjIuVkSXLvnOOY1UfU1qtpmtDxLp3iCO0czGSaK6gMkY2wNjaAw6kbUOM/wB71qq3gzVl8tI76FxEJESZ3YS+WURVRiF+bG1l3cHaQeuanvPCN1c35lhis7aB/JyiyFim2VnfAKEchgAOnGMYqVFroauonvJfd8u5R0vw7rtlFonm20jm0sbmCVDPGVR3xtC9ODt/UVPpug6lH4bg0XUtKe7i8i3hm86eNsBdodEx0TClvXJ/GppvC2rrcfuJreW2Jt8pPO+5RBKCvO05JTIPTk961bnSNRvryG6umt9/kBMJIwFvIH3b4+OT0HOPujsSKFDyJlWv1X9a9/NmCvhzX7OGC2J/tKO01OCaGQuqubaNeA2SMsM498U6bwzrEmoW1wqtCJb+6vHMUwVrYSQ+WmCOpzhjjjk9e7rzwdqd1bQRLNbReWqhgJGIdxE6mQ/L1ZmXPfC9ScVZh8J3guJr2aSHznuvNNush8qSIhcox25PIzzkce5o5fIr2q35l93/AATOm0DxDeeFo7OSwisrzT4YjZpbvGY2mVg27nleVHTHBNag0nWLfxBe6tb2qGG6t0uTamQZ+2KpTGemMEEn1UVXh8La3a6nFqMFxayTRIqKJJHAHEg/u5+XeoHqF5HSkg8GXIubf7YLW5igeYB2lfe0bcxjG3jaxPfp60KL7CdSOvvK3p/wfJFDUfDOvJb67ZWFs1zBqlpES8kkaEXCnDcDHBXGT6j8a07fRtY+2X1hcWg/s+8ufOa68xXlZNgwrhs7sEBe/Aplv4T1SPTIbWSa2eRIkRpPMbOBB5bRD5fuM3zE/wC0eCQDW74f0i40e3uYLi5FyGkHlSHl/LCgKrn+IjBGe4A704w12JqVrR0afy9PPyOTh8N+IZtL0i1mhNtdaSQsF00qNlBIAOAevlEg+4rqfCun3WmaPLa3Vv8AZz9rnkRNwb5GkLL09jj8K3aK1jTUXdHNUxEqkeVrzP/Z'},
                                        {width: '*',
                                            alignment: 'center',
                                            stack: [
                                                {style: 'h1', text: ['Rajamahendravaram Municipal Corporation'], fontSize: 25, color: '#800000'},
                                                {style: 'h2', text: 'Rajamahendravaram - 533101,Andhra Pradesh,India'},
                                                {style: 'h2', text: 'Homepage: http://www.rjymuncipal.in'},
                                                {style: 'h1', text: [rptHeading], color: '#800080'}                                                
                                            ]
                                        }
                                    ]
                                ]
                            },
                            layout: {
                                hLineWidth: function() {
                                    return 0;
                                },
                                vLineWidth: function() {
                                    return 0;
                                },
                                paddingBottom: function() {
                                    return 5;
                                }
                            }
                        },
                      ],
                        pageSize: 'A4',
                        pageOrientation: align,
                        styles: {
                            table: {alignment: 'center' , paddingTop: 555},
                            header: {fontSize: 22, bold: true, alignment: 'center'},
                            h1: {margin: [0, 20, 0, 0], fontSize: 12, bold: true},
                            h2: {margin: [0, 5, 0, 0], fontSize: 10, bold: true},
                            h3: {margin: [0, 25, 0, 0], fontSize: 11, bold: true},
                            layout: {padding: [0, 55, 0, 0]}
                        }
                    };
                
                docDefinition.content.push(table(tbldata, jsonColumns));
                pdfMake.createPdf(docDefinition).download(fileName + '.pdf');
            }
})


.service('fuelpdfService', function() {
            var curdate = new Date();

            this.pdfDownload = function(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align,cardsHeadings,cardsJsonColumns,cardsData) {
                
                function buildTableBody(data, columns, colHeadings) {
                    var body = [colHeadings];
                    body.push();
                    data.forEach(function(row) {
                        var dataRow = [];
                        columns.forEach(function(column) {
                            if (row[column] == null) {
                                row[column] = 'NA';
                            }                            
                            dataRow.push(row[column].toString());
                        });
                        body.push(dataRow);
                    });
                    return body;
                }
                
                function table(data, columns, colHeadings) {
                    return {
                        table: {
                            // headerRows: 5,
                            widths: '*',
                            alignment: 'center',
                            body: buildTableBody(data, columns, colHeadings)
                        },
                        alignment: "center",
                        wordspacing: "10px"
                    };
                }
                
                var docDefinition = {
                      content: [
                            {table: {
                                widths: ['auto', '*'],
                                body: [
                                    [{image:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAUEBAQEAwUEBAQGBQUGCA0ICAcHCBALDAkNExAUExIQEhIUFx0ZFBYcFhISGiMaHB4fISEhFBkkJyQgJh0gISD/2wBDAQUGBggHCA8ICA8gFRIVICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICD/wAARCACAAIADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7LooooAKKK4vVPGFxHqDQ6akTQxnaXcE7z3xz0rysyzXC5ZTVXEysm7Lq2dFDD1MRLlpo7GWVIIXmkbaiKWY+gFchc+N8S4s7HdGD96VsE/gOlb2j6vb6xZllAWVeJYjzj/EGuM8SaIdLu/PgU/ZJT8v+wf7v+FfNcRZjjY4KGNyua9m92ld26PXp0el0d+BoUnVdLEL3uh3un30eo6fFeRDCyDlT/CehH51HHBHa3N1qDXkjwyKGKu+UjAzkj0Fc54JvMpdWDN0IlT8eD/T86yvE0dzZarNarPKLSf8AfLHuO3nrx065qqnECjlNHM5U+d7OztaVmr+l7/ehRwV8TLDp2/y3OstbyPxForiG4e1kLYfy2w6YbP6j+dasLRi3URy+YiDbvLbiccHJ9a8hSSSMnynZWIx8pxn2r0PVHXRfB3kIQH8tYVxxlj1P8zXPkfEbxlGriK8LOlD3pX3tdqy6X1LxeB9lOMIP4nojNl8bFb1lislktgcBi+GYevSul0zU7fVbP7TbbgA21lYYKn0/WvMtN0+fUr5LS3GCeWbsi9ya768vbHwxpMcESbnxiOLPLnux/qa5uHc4zCvGrjcfNKhHq117K3b59FuaY7C0YONKivfZuUVyOj+LZLrUFtdQjijWU4R0yMHsDn1rrq+2y7M8NmVL22GldJ27NHk16FShLlqIKKKK9MwCiiub8T67LpqR2tmwW5kG4sRnYv8Aif6Vw4/HUcBh5Ymu/dj9/ojajRlWmqcN2b91Cbizmt1kaIyIU3r1XIxmvNr/AMOapp4LND50Q/5aRfNx7jqKntPFer2z/vJVuU7rIvP4EV0Vj4x064wt2j2j+p+ZfzH+Ffn+LxuR8RKMa1R05ra+n+cfxTPZpUsXgW3GPMvL+rnEWV9cafeJdWr7ZF/Jh3B9q9Htbqx8RaO6suVcbZIyeUb/ADyDUdzpGi61GZ1EbM3/AC2gYA/jjg/jVDTPDV5pespcQXytbYIcEYZh6Y6fjW2U5XmOU1fYq1bDVNHZrS/Wz/G17ojE4ihiY8/wzRT0XQdS07xNvK/6NFuBlzxIpHAA9c4/Kunv9KsNSMZvYPMMedp3EYz16fSmatqsGk2JuJRvc/LHGDy5rzrUNY1DUpGa5uG2HpEpwg/Dv+Na43GZXw7QlgeV1OZ83I7NK+17rRaaaN9SaNLEY6arX5baXO9j8P6CkqtHapvQhh+8Y8j8apeLdP1C/trb7HH5qRsS6A856A/z/OvPxwcjg+orodH8T3djKsV5I1xang7jlk9we/0rxKPEeW42lPA1qHsIVLXcbfjov1OyeAxFKSrRnztdH/w5v26WfhLRPMuCJLuXqB1dv7o9h61xF7ez39291dPukb8lHYD2rrb3w9fa1qrXkuoRmzbBhZMkhOwA6fjmrBg8NeHADNiW5HI3/PJ+XQfpXRmWW4rGRVDShhKeicmtf73nfpe33meHxFOl7+s6ku3TyOW0/wAP6pqOGjgMMR/5ay/KPw7mvTYY2it442kMjKoUuerYHWuF1DxleTZSwiFsn99vmc/0H61iQ6rqMF4LtbyVpQcne5Ib2PtXPl2c5RkUnTwvNVcrc0tlp2X9epdfC4rGLmqWjbZHrFFQWd0l7Yw3cYwsqBgD2qev1uE41IqcXdPU+baadmFc/rXhmPVrxbtbpoZNoQgruBA/lXQE4BJ6CuOvPGyrKVsbPzEHR5Wxu+grws9r5bTw6p5m/dlstbu3a2uh2YSFdz5sPug/4Qde+pH/AL9f/Xo/4QcdtSP/AH6/+vVZfGOq3EqxW1jCzt0VQzE/rW/Y/wDCSXGHvWtbNP7qoWf+eB+tfK4PB8PY6XLhcPKfmuZL73JI9CrVxtJXqTS+4xV8MpYXA2+IltZevHyNj/vquohvrGOBI31KGVlUAu0i5Y+vFeDfFa0+3fFa2stwVriK3hDkZxuYrn9a2f8AhQ8n/QzR/wDgF/8AZ16+BgsFVq0suw2idn77/KX6HqVMBhpUKVbG4nlc1dLkv+KPS9T0i1168WUasCI02rHHtYD1PX6VgyeEppFdtN1C3vAjFWAOCCO3GRn8q8713wBcfD6wl15NeSaQxtbwBIzDIJH+XI5PRC5rnfAfiWTwj4otryRmTT7n93crzhkzjf7lTz+Y715OYYXL6+ISx+H5Zy3ak9Oifb/JHbhsrqTw86uBr88Y7LltdrVrXX/M9etfCusXErK8K2yqcFpW4P0x1rRj8GxmYwPrEfnAbjGqDcB64zW74m8Q2vhzwxda1KVcRp+5TP8ArXP3VH1P6ZNfMFprWqWPie28UTSStdm4+0NKcjzhu+cZ9CMrj8KzxGRZNljhTqU3Ub3vJqy76WRz5bh8bmcJ1IzUFHbTd2vb/M+ptLsho1gLWa+EqBiULgLtB7dfWo7vTdBvbg3FysDSnqwkxn64NeQf8Ks8S+JNZv8AUNQ1YW9o87m3knZp3kjJypAzwMEd/wAKsf8ACirrH/I0Rf8AgIf/AIuvpP3kqKw6wfNTjspST22+K/yONYLAxlz1Mbab3tF9fNHqR0Xw1HGZGt7cIvVmk4H60Jp/hfjbHZH/AIGD/Wuf8F/DxPC9lqVpfX0epRXxQ4EJj2bc/wC0fWk1TwfcW4abTmNzH/zzP3x9PWuLHQrYWlGtRwEJK2qVuZfctflqcDjRdaVOOIbXR6pP73od1D5IhVbfZ5ajChMYA9sU+uD8Hx30OsSxGKWOHyz5gdSADkY/Gu8r3cmzF5jhFXdPkd2ren3HnYqj7Co4J3CuM8QL4I8ONHc68xsYJ2OJGEnkg+hZRhc54BIz2rs64/xjoPi/XYmsNE8RWenaddL5Vystn5kqqfvFGzjJHGCO/WvTqYLC4uUY4qEZJd1e34M5JVq1GDdFu/l/w6/MyL34k+ENLhgsvB8UfiLVrzi3sdLIZmPrIw+4B3Jrt9Fl1ifRrebXrS3s9RcEywW0hkSPk4G4gZOMZ7ZzivL5G8I/BHSZILCMXmo3CbwjkCRo16ySvgkLk4AAwTgKucmud0f4j/FXxhNbS6Nb6NpNjcXBt7ea8jcC5kALFF5JOFU5wMDHXNevHBQ9nbDxUYLq9PuS/wAjyvrzhUtXd5v7MdbfMg+LUcs3xQjhhOJpIIEjOcYYsQOe3NWx8P8A4qYx/a0o/wC4m/8AjVrSNR0rxF4r01viFo8cGqXmF03VrK4k+x3rRsRsAJ+RwwI2nr+WfYdS8QaJo4c6pqttabAGYSSAEZOF468ngDv2r4+XD8nias67fvO65X0+4+/hxVFYSjTw0V7itLmit/LU+ZNa03W7PxJBo/i/UbmDay/v55GuFSNjy65PI+npXq3jvwLYSfDWxk0FfNOjReZG4IYzwnmQ5HBJ+/8Agcda7G903wl8QNMikm8nUoYHO2SJyrxNjlTjBU9MqfbitbR9FsdC0pdL09ZBaITtSWQybQewz29qyo5KqUqtOesZrRv4l5fqbYjiJ1o0K1P3Zwd2lblfn31WlvP7/mcatr/jG38P+DhIHW3fyoSM5bPRn/3Ez+Ga9I+KHgpbbwTpMuj27PFoqGKRVGWMTAZc+pDDJ/3ia7/SPBPhjQdVl1TS9LSC6kyN25iEB6hQThfwroHOI2OwvgfdGMn254ow+TP2NSGJlzSlZX7JbBiuII/WaVTBw5YQbdu7l8W33I8a8G/F3SbHw5a6Zr8Vwk9pGsKTQp5iyIBhc85Bx1/P2ro/+FyeC/8Anre/+AxqTUPBXga+iGqXnhm5tWmk2lIY5EbdnAJSM8ZPfHuar6h8O/hzpZsvt1jLELy4W1iJuZMGRgdoPzd8Y+pFaQo5vTSpxlBpdXe5lUxOQ1pOrONSLerS5bK/a51Hhnxfo3iyK5k0hpmW2ZVk82MpyemPyry/xH8brHSvira6XCrSaNZM9rfSjoZGKDeOM/u8MD65b2rpfF2gaf4I+FHim48LiawmltstIszMw5xwSTjgnpXy/wCGvB3ibxrftFolhJd/N++uZDtiQnqWc9/bk+1fa5Lg/a0HVxzV1o7beuvkfn2fY32WIVHL07PVX3t208/wPuiGaO4t47iFt8Uih1YdwRkGn1xvw3019B8Ip4fufEUet3thIyzOj7vJLHcI+TnAzxn9MYrsq86pFQm4p3PTpTc4KTVmwqjrGrWehaHe6xqEmy1s4WmkPfAGcD3PQe9XqwvGGgt4n8HajoKyLEb2MR726L8wOf0pQUXJKWwVHJQbhvbQ+TLy/v8A4g+MrC2urlY7vX7xZJ23jEEW4rFGPZEDNjuWHevoOHwtrdtd6Nb29gY7HSNfaW2RXQLHZfZ2RcDP94njrkk1za/Ama1RGsdYtEljIdM2xXDDkHdkkHPet+11r4h+EXC+JtNbXNMX713a4eSMevABP/AgPrX0eKdOsoxws07X0ej+V/LQ+YwlOpQcpYuDV3utfvtfrqUB4C1q/wDDmn+HdUtZE05BqHnbJULRSNP5ltKnzfeXk5GMZI71y/gvSPFHijUdR12+t1TV7FPs0XmRtARM0JU3ZDAfvTtjHTgZxXo9z8RJNXnXT/Aumvq92yhmnlUxwwA/3s45/L8aibwX411geZr3jiW2Lf8ALCwQqi+2QVz+Irjjzwi1Xajfve/notdfOx2ShTnKLoJzt2tbbTV6aeVytqWl+OtP+0alpNqsl9cyfZnaOQF2iaVyrvkjJiUpj2Lr6ET3uieOtL0u+n0J45L6RAoJumkkkYnBdg4Clh1yMcDaOMYrS/DjxTY/v9E8dXhmHIS4Zwp+p3Efoafp/jjxB4b1CLSviBYeXFI22PUol+Q/72OD+GCO471HsOdXoSU7dNU/ue/yNPacjtXjKF+t00vmtvmbNjpPjjTJIYodVhvIZSFne7kaVo8AkuvA5J4x05BAGOdv+0tVsNMubnVrKN5oy3lR2rEiRR0JZsBc+/A9a5S58Za94m1CXTPAdmpgibbLqtyMRKf9kY5/X6Y5py/DS41BfM8TeLdU1CVvvJE/lxj2AOf6Vi6CjriJKL7bv7unzaN41m9MNFyXe9l973+VyWx+IStqVxHd6D4iTzNgji/s0yBDjBw0ZYEHg5zXP/Ee91zxENDh0bw5rXkWOoRX00jWZBby2BCqCRz164Fas3wb8O/estR1K0kHRllVsH8v61jXfhn4keEVN1oOvTataR8+QcswH/XN85/4Cc11UqeGc1KjNX7STS+/U5as8UoONaDt3jZv9Dt5NW0fxL4J1GfxBo97YabtcT2uox+VI8a852g5wSOOea1NLj0PUfCkEekxRw6TcQYjS2/dBEI6DbjaR7cg14P4p+IureJdDi0a9tYrMpJuuDGSPNI6KVP3cHkjJ5A9Kd4V8H+Mtbga3tZrvS9JmOZXkd0jcd8Jkb/5e9dDyrko89afJrturf5mCzXnrclGHPpvazv/AJHXfCHTmtvEPiSW3dnsoXFskmciQh2IOe5xj/vqvYKyfDvh+x8M6HDpNhkxplmkb70jHqx/z0AFa1eRja6xFeVRbf5aHsYLD/V6Eab3/wA9QooorjO0gup5Le3aSG1kupP4YoyoLfixAH51w2reOvE2gu1xq3w7v20xOXubG6juWQepQYOK9AorSEox+KN/vMqkJS+GVvuPMrz4u/DfQ9DTUtNu4rp77Mq2ljEPOdzwd442n/ewfrWFL40+NHiKH7R4Z8CQ6VaMPke/ceaR2OHZf/QTXoNt8PvCNp4um8VQaLCupyj72PkRu7qvQMe7dfzOejuLq1s4jLd3MVvGP4pXCj8zXZ7ajC3s4cz/AL2v4L+vI4vYV539pU5V/d0/F3/rqeAX3i79oDw6ftWqeHoLu2QZYRWyyrj38psiuj8J/F7wv4+jPhvxRp8enXlwNgjmbdBO3orHBVvQHBz0JNd9N478HwMVk8RWZI/uSbv5ZrifFXgn4d/E1Xk0rU7K11sjK3NqV3P/ANdI8jePfr711qUJr99ScP70U1b5HG4VIP8AcVuf+7Jp3+ZZ1b4s/DrwParo2nSm9e2Hli205Q4UjsXJC5z15Jrnj8W/iNq7FvDfwxuvJOdslysjA++cKPwya6PQfDHwz+GUCefdWb6oo+e6uiJJyf8AZUZKD2UfXPWtGb4ueDY32pPdTD+8luQD+eKI04y1o0ZVPN31+S/zHKpOKSrVo0/JW0+b/wAjz2b4mfGnTXMmo/D0PCOSI7SY4H+8rNVrSf2i7D7SLTxP4cudNkB2u8DeZt+qMFYfrXdW3xb8F3EgR72e3z3lgbH6ZrXuLDwT46sD59vputw9NwCuyfiPmU/kaVTkh/vGH5fNXX56CpqpJ/7NieZ9nZ/lqedeIPizY6hq9tpnw30GHxHrdygcXLwELCPfIBJHfJUDue1dz4Mh+IsaPJ42vNJlVwSsVpEwkjPYFshSPbH4mrfhLwL4c8EwXUWg2bRG6k3ySSNvfHZNx52jsPc101cVatSt7OjHTu9/+B8jvoUK1/aVpa9lt/wfmFFFFcR3hRVa9v7PToVmvrlLeNnEYZzgFj0H1NRnV9LXT21BtQgW0RtjTGQBVbOME9jk4xSuilGT1SLtFVp9Qsra7t7S4uo457kkQxscNIR1x6097u1jvIrN7iNbmVWeOIsNzAYyQPbIouhcr7EkiCRCpZgD3U4P51zl/wCGvCnm/aNS0VLk95pkabH1JyR9TWmNf0U2ZvBqlsYBN9nL+YMCT+5/ve1WjfWa3b2huY/tEcfnNFu+ZUzjdjrjINXCq46xdvRkzo82k4/ejlV8I/DvVFeO307TpSB832eTay59dpyK5zW/hHoSQteaTq8mkyR/MDPJujX8Thl+uTXZ3lx4N1iKE339m3yTI0sRlRZN6r95lyO3qK5DVPA/wg1iHz7iC0hQKrCSG6eJQGOFOA23BPGcV3UMfOEruq1+P5s4K+WqcdKKb+a/JCeHPhT4aa2W91DUjrbv95oZcRZ78qcn8T+Fbd9b/DnwtIlvcaZYLdOBst0t/Pnf0woBb8ab4Z8P+Bvh415a6TdG1e4kjSZJ7hny+CUAB4BIJ6DJ/Culk1XQbd4rlru1R7rOxxjdLtHIyOSR39MUq2OlUqNzqOUfW34apDo5eqdNKFNRl6X/AB0bM3Sbi11NwYvB01jb9pruCKLI9kyW/MCt2LT7GCbzoLKCKTGN6RhTj6gUh1LTxFaym9g8u7YJA3mDErEZAU9+AagXXdHYyBdRhJjmFu+G+7IeAh9/auOdRN6afM7YUZJaq/yNGiqMmsaVDa3N1JqECwWshinkLjbE/Hysex5H50q6tpr2c94t9D5FvnznLACLAyd3pxzzWd0ackuxdoqodU04XltZm9hFxdIZIY94zKoGSV9eKdZX9lqMDT2N1HcxK5jLxtkBh1GaLoOWSV7GV4q0+81LSre3s4WldbyCZgsgQhEkDMQcjnA496wk8PatcaZe6HqFoFs3kuZPtELoz3BYnYXDfxEN19VBzXd0VDppu5tCvKEeVepxemaf4mutQ8PTazZxQf2WjeZKswcykxbDwO+efp70/V9L1q61a21q2tXNzZXy+VF5kYD2+0q/zdRkM3GeuK1vEWk3mqxWX2GZLee2nEyysxBXAIxgDnIJFYtn4Ru7ez+zyJaSA20cTAyMcOnmd9vOd65PXjvWbi17p0RqJ+/dJ7Wt569TNm8K6yZZrqG1ws91b3RtjIuVkSXLvnOOY1UfU1qtpmtDxLp3iCO0czGSaK6gMkY2wNjaAw6kbUOM/wB71qq3gzVl8tI76FxEJESZ3YS+WURVRiF+bG1l3cHaQeuanvPCN1c35lhis7aB/JyiyFim2VnfAKEchgAOnGMYqVFroauonvJfd8u5R0vw7rtlFonm20jm0sbmCVDPGVR3xtC9ODt/UVPpug6lH4bg0XUtKe7i8i3hm86eNsBdodEx0TClvXJ/GppvC2rrcfuJreW2Jt8pPO+5RBKCvO05JTIPTk961bnSNRvryG6umt9/kBMJIwFvIH3b4+OT0HOPujsSKFDyJlWv1X9a9/NmCvhzX7OGC2J/tKO01OCaGQuqubaNeA2SMsM498U6bwzrEmoW1wqtCJb+6vHMUwVrYSQ+WmCOpzhjjjk9e7rzwdqd1bQRLNbReWqhgJGIdxE6mQ/L1ZmXPfC9ScVZh8J3guJr2aSHznuvNNush8qSIhcox25PIzzkce5o5fIr2q35l93/AATOm0DxDeeFo7OSwisrzT4YjZpbvGY2mVg27nleVHTHBNag0nWLfxBe6tb2qGG6t0uTamQZ+2KpTGemMEEn1UVXh8La3a6nFqMFxayTRIqKJJHAHEg/u5+XeoHqF5HSkg8GXIubf7YLW5igeYB2lfe0bcxjG3jaxPfp60KL7CdSOvvK3p/wfJFDUfDOvJb67ZWFs1zBqlpES8kkaEXCnDcDHBXGT6j8a07fRtY+2X1hcWg/s+8ufOa68xXlZNgwrhs7sEBe/Aplv4T1SPTIbWSa2eRIkRpPMbOBB5bRD5fuM3zE/wC0eCQDW74f0i40e3uYLi5FyGkHlSHl/LCgKrn+IjBGe4A704w12JqVrR0afy9PPyOTh8N+IZtL0i1mhNtdaSQsF00qNlBIAOAevlEg+4rqfCun3WmaPLa3Vv8AZz9rnkRNwb5GkLL09jj8K3aK1jTUXdHNUxEqkeVrzP/Z',width : '100'},
                                        {width: '*',
                                            alignment: 'center',
                                            stack: [
                                                {style: 'h1', text: ['Rajamahendravaram Municipal Corporation'], fontSize: 18, color: '#800000'},
                                                {style: 'h2', text: 'Rajamahendravaram - 533101,Andhra Pradesh,India'},
                                                {style: 'h2', text: 'Homepage: http://www.rjymuncipal.in'},
                                                {style: 'h1', text: [rptHeading], color: '#800080'}                                                
                                            ]
                                        }
                                    ]
                                ]
                            },
                            layout: {
                                hLineWidth: function() {
                                    return 0;
                                },
                                vLineWidth: function() {
                                    return 0;
                                },
                                paddingBottom: function() {
                                    return 15;
                                }
                            }
                        },
                      ],
                        pageSize: 'A4',
                        pageOrientation: align,
                        styles: {
                            table: {alignment: 'center' , paddingTop: 555},
                            header: {fontSize: 22, bold: true, alignment: 'center'},
                            h1: {margin: [0, 20, 0, 0], fontSize: 12, bold: true},
                            h2: {margin: [0, 5, 0, 0], fontSize: 10, bold: true},
                            h3: {margin: [0, 25, 0, 0], fontSize: 11, bold: true},
                            layout: {padding: [0, 55, 0, 0]}
                        }
                    };

                docDefinition.content.push(table(cardsData, cardsJsonColumns, cardsHeadings));
                docDefinition.content.push("\n\n\n");
                docDefinition.content.push(table(tbldata, jsonColumns, colHeadings));

                pdfMake.createPdf(docDefinition).download(fileName + '.pdf');
            }
})