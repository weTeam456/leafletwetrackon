var wetrackonlogin = angular.module('wetrackonlogin', [
    'ui.router',
    'angular-loading-bar',
    'LocalStorageModule'
])

.config(function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/login");

            $stateProvider

            .state('login', {
                url: '/login',
                templateUrl: 'client/src/login.html'
            })
            .state('resetpwd', {
                url: '/resetpwd',
                templateUrl: 'client/src/resetpwd.html'
            })
})

.service('growlService', function(){
        var gs = {};
        gs.growl = function(message, type, align) {
            $.growl({
                icon: '',
                title: '',
                message: message
            },{
                type: type,
                allow_dismiss: true,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: align
                },
                delay: 5000,
                animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                },
                icon_type: 'image',
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });
        }
        
        return gs;
    })

//====================================================================
// LOGIN Controller - Authentication against Cognito User Pool in AWS
//====================================================================
.controller('loginCtrl', function($scope, $rootScope,$location, $http, $window,$timeout,growlService,localStorageService) {

    $rootScope.BaseUrl = "http://localhost:4801/";

    $scope.showme = false;
    $scope.showVerify = false;
    $rootScope.loginDtls="";
    localStorageService.clearAll();

    //Status
    this.login = 1;
    this.register = 0;
    this.forgot = 0;
               
           

            $http.get($rootScope.BaseUrl+"apiv1/getOrgnsLst")
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.allOrgnsDtlLst= resdata.data;
                console.log($scope.allOrgnsDtlLst);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            }); 

        
            $scope.getLgnId = function(){
                $scope.login="#login";
            }

            $scope.logOrgn = function(){
            $http.get($rootScope.BaseUrl+'apiv1/goToLgn/'+$scope.tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                // console.log(resdata.data);
                $scope.goToLgnDtls= resdata.data;
               console.log($scope.goToLgnDtls);
               
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }

        // Perform the login action when the user submits the login form
        $scope.doLogin = function() {
            $http.post($rootScope.BaseUrl+"apiv1/login",{username:$scope.loginData.username,password:$scope.loginData.password})
            .success(function(resdata, status, headers, config) {
                // console.log(resdata);
                
                if(resdata.data.length > 0){
                    localStorageService.set('userDtls', resdata.data[0]);
                    $scope.setAccessUrls(resdata.data[0].usr_id);
                    
                }else{
                    growlService.growl('Incorrect username or password. Please check', 'danger','right');
                }                    
            })
            .error(function(data, status, headers, config) {
                console.log(status);
                growlService.growl("Unable to connect to server. Please check your internet connectivity.");
            });
        };

        //***********To get user access urls***********//

        $scope.setAccessUrls = function(usr_id){
            $http.get($rootScope.BaseUrl+'apiv1/acl/usrAccess/'+usr_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                // console.log(resdata.data);
                $scope.accessUrls= resdata.data;
                aclSetUp(usr_id);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }
        function aclSetUp(usr_id){
            var accessUrls = $scope.accessUrls;
            var menuItems = [],appIems  = [], mdls =[],aclData ={};;
            for(var k=0;k<accessUrls.length;k++){
                var flag = false;
                for (var j = 0; j < appIems.length; j++) {
                    if (appIems[j].app_url_tx == accessUrls[k].app_url_tx)
                        flag = true;
                }
                if (!flag){
                    appIems.push({app_url_tx:accessUrls[k].app_url_tx})
                }
            }
            // console.log(appIems);
            angular.forEach(accessUrls,function(data){
                menuItems.push({mnu_itm_url_tx:data.mnu_itm_url_tx})
            })
            // console.log(menuItems);

            for(var i=0;i<appIems.length;i++) {
                mdls.push(appIems[i].app_url_tx)
                if(appIems[i].app_url_tx == mdls[i]){
                    for(var j=0;j<menuItems.length;j++){
                        mdls.push(menuItems[j].mnu_itm_url_tx)
                    }
                }
            }
            aclData['User_Access_Control'] = mdls;
            localStorageService.set('aclData',aclData);
            $scope.JSONapp_Lst(usr_id)
        }

        $scope.JSONapp_Lst = function(usr_id){
            $http.get('../../apiv1/user/appsLst/'+usr_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                localStorageService.set('appsLst',resdata.data);
                $window.location.href = $rootScope.BaseUrl+'ds';
            })
            .error(function(data, status, headers, config) {
                console.log(data);            
            });
        }

        $scope.pwdPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
        $scope.showcnfrmPwd = false;

        // Perform the forgetPwd event when user submits forgot password form
        $scope.forgetPwd=function() {
            // console.log($scope.frgtPwdData);
            if($scope.frgtPwdData && $scope.frgtPwdData.phonenu && $scope.frgtPwdData.username){
                $http.post($rootScope.BaseUrl+"apiv1/forgetPwd",$scope.frgtPwdData)
                .success(function(resdata, status, headers, config) {
                    // console.log(status);
                    if( resdata.data[0] && status == 200){
                            $scope.showcnfrmPwd = true;
                    }else{ growlService.growl('Incorrect username or phone number. Please check', 'danger','right') };
                    
                })
                .error(function(data, status, headers, config) {
                    // console.log(status);
                    growlService.growl("Unable to connect to server. Please check your internet connectivity.");
                });
            }else{
                $scope.showcnfrmPwd = false;
                growlService.growl("Please fill the form to continue..");
            }
        }
    
        $scope.confrmPwd = function(){
            // console.log($scope.frgtPwdData);
            if($scope.showcnfrmPwd == true && $scope.frgtPwdData.password){
                $http.post($rootScope.BaseUrl+"apiv1/changePwd",$scope.frgtPwdData)
                .success(function(resdata, status, headers, config) {
                    $window.location.href = $rootScope.BaseUrl;
                })
                .error(function(data, status, headers, config) {
                    console.log(status);
                    growlService.growl("Unable to connect to server. Please check your internet connectivity.");
                });
            }else{
                growlService.growl("Password must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)");
            }
            
        }
})