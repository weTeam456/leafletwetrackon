wertrackon
    // =========================================================================
    // Geo Coding related controllers
    // =========================================================================
    .controller('sidebarGCCtrl', function($state, $http, sideBarMenuService, localStorageService, $rootScope, checkPermService, $scope) {
        var app_id = 2;

        $http.get('../../apiv1/user/menuLst/' + $rootScope.userCmpltDtls.usr_id + '/' + app_id)
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                // console.log(resdata.data)
                $scope.appMenu(resdata.data)
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

        $scope.appMenu = function(responseData) {
            $scope.menuitem = sideBarMenuService(responseData)
            // console.log($scope.menuitem)
        }
        $rootScope.canRead = function(menuNm){
            return checkPermService.canRead(menuNm,$scope.menuitem);
        }
        $rootScope.canCreate = function(menuNm){
            return checkPermService.canCreate(menuNm,$scope.menuitem);
        }
        $rootScope.canUpdate = function(menuNm){
            return checkPermService.canUpdate(menuNm,$scope.menuitem);
        }
        $rootScope.canDelete = function(menuNm){
            return checkPermService.canDelete(menuNm,$scope.menuitem);
        }
        $rootScope.canEdit = function(menuNm){
            return checkPermService.canEdit(menuNm,$scope.menuitem);
        }
    })

    .controller('geoHomeCtrl', function(recentpostService, $scope, $http, $rootScope) {
      
      $http.get($rootScope.BaseUrl + "/dashBoards")
          .success(function(resdata, status, headers, config) {
              if (status == 200) {
                  $scope.dashBoards = resdata.data;
                  $scope.landMrkStsCntDtl();
              } else {
                  console.log("Data = " + resdata);
              }
          }).error(function(data, status, headers, config) {
              $scope.message = "Unable retrive data from Server.";
          });

      $scope.landMrkStsCntDtl = function() {
          var landMarkTypeChart1 = [];
          var landMarkTypeChart2 = [];
          for (var i = 0; i < $scope.dashBoards.length - 1; i++) {
              landMarkTypeChart1.push($scope.dashBoards[i].tl_ct);
              landMarkTypeChart2.push($scope.dashBoards[i].tf_ct);
          }
          // console.log("Landmarks Count:" + $scope.dashBoards[i].tl_ct, $scope.dashBoards[i].tf_ct);
         
          $scope.options1={
            "chart": {
                "type": "multiBarChart",
                "height": 450,
                "useInteractiveGuideline": true,
                "clipEdge": true,
                "duration": 500,
                "xAxis": {
                      "axisLabel": "Land Marks Month wise Count",
                      "showMaxMin": false,
                      tickFormat: function(d) {
                          return landMarkTypeChart1[d]
                      }
                  },
                  "yAxis": {
                      "axisLabel": "Land Marks Status Counts",
                      "axisLabelDistance": -20,
                      tickFormat: function(d) {
                          return d3.format(',0f')(d);
                      }
                  },
            }
        };

        $scope.options2={
            "chart": {
                "type": "multiBarChart",
                "height": 450,
                "useInteractiveGuideline": true,
                "clipEdge": true,
                "duration": 500,
                "xAxis": {
                      "axisLabel": "Fences Month wise Count",
                      "showMaxMin": false,
                      tickFormat: function(d) {
                          return landMarkTypeChart2[d]
                      }
                  },
                  "yAxis": {
                      "axisLabel": "Fences Status Counts",
                      "axisLabelDistance": -20,
                      tickFormat: function(d) {
                          return d3.format(',0f')(d);
                      }
                  },
            }
        };

        $scope.dataset1 = generatedata1();
        function generatedata1(){
            var currentMonthLandMarkCount=[]; var previousMonthLandMarkCount=[];
            for(var i=0;i<=$scope.dashBoards.length-1; i++){
                currentMonthLandMarkCount.push({x:i,y:$scope.dashBoards[i].cur_mnth_l_ct});
                previousMonthLandMarkCount.push({x:i,y:$scope.dashBoards[i].lst_mnth_l_ct});
            }
            var dataArray = [
                {
                    values: currentMonthLandMarkCount,
                    key: "Current LandMark Count",
                    color: '#3399ff'
                },
                {
                    values: previousMonthLandMarkCount,
                    key: "Previous LandMark Count",
                    color: 'linear-gradient(60deg, #ef5350, #e53935)'
                }
            ];
            return dataArray;
        }

        $scope.dataset2 = generatedata2();
        function generatedata2(){
            var currentMonthFenceCount=[];  var previousMonthFenceCount=[];
            for(var i=0;i<=$scope.dashBoards.length-1; i++){
                currentMonthFenceCount.push({x:i,y:$scope.dashBoards[i].cur_mnth_f_ct});
                previousMonthFenceCount.push({x:i,y:$scope.dashBoards[i].lst_mnth_f_ct});
            }
            var dataArray = [
                {
                    values: currentMonthFenceCount,
                    key: "Current Fence Count",
                    color: '#3399ff'
                },
                {
                    values: previousMonthFenceCount,
                    key: "Previous Fence Count",
                    color: 'linear-gradient(60deg, #ef5350, #e53935)'
                }
            ];
            return dataArray;
        }
      }
  })
    // =========================================================================
    // Geo Fence related controllers
    // ========================================================================= 
    .controller('geoFenceCtrl', function(recentpostService, $http, $scope, $log, $timeout, $window, uiGmapGoogleMapApi) {
        console.log("geoFence....");

        var geocoder;
        var map;
        var myPoly;
        var shapeID = 1;
        // var infoWindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();
        var baseURL = 'http://localhost:4801/apiv1';

        $scope.map = {
            center: {
                latitude: 17.006760,
                longitude: 81.783209
            },
            zoom: 12,
            polygons: [],
            isDrawingModeEnabled: true,
            zoomControl: true,
            scaleControl: true,
            navigationControl: true,
            mapTypeControl: true,     
            draggable: true,
            bounds: {}
        };
        console.log($scope.map.polygons);

        $scope.options = {
            scrollwheel: false
        };

        $http.get(baseURL + "/getCategories")
            .success(function(resdata, status, headers, config) {
                if (status == 200) {
                    $scope.categoryNames = resdata.data;
                    // console.log($scope.categoryNames);
                } else {
                    console.log("Data = " + resdata);
                }
            }).
            error(function(data, status, headers, config) {
                swal("Please connect to internet and restart App");
                $scope.message = "Unable retrive data from Server.";
        });

        $scope.getGroups = function() {
            $http.get(baseURL + "/getCategories/" + $scope.fnce_ctgry_id)

                .success(function(resdata, status, headers, config) {
                    if (status == 200) {
                        $scope.categoryGroups = resdata.data;
                    } else {
                        console.log("Data = " + resdata);
                    }
                }).
                error(function(data, status, headers, config) {
                    swal("Please connect to internet and restart App");
                    $scope.message = "Unable retrive data from Server.";
            });
        }

        $scope.getFences = function() {
            $http.get(baseURL + "/getFences/" + $scope.fnce_grp_id)
                .success(function(resdata, status, headers, config) {
                    if (status == 200) {
                        $scope.geoFencesCords = resdata.data;
                    } else {
                        console.log("Data = " + resdata);
                    }
                }).
                error(function(data, status, headers, config) {
                    $scope.message = "Unable retrive data from Server.";
            });
        }


        // $scope.FillColors = ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082'];
        $scope.FillColors = ['rgba(244, 67, 54, 0.42)', 'rgba(174, 83, 189, 0.76)', 'rgb(203, 184, 237)', 'rgba(0, 150, 136, 0.69)', 'rgba(255, 152, 0, 0.38)', 'rgba(86, 76, 175, 0.86)'];
        $scope.setMapMode = function(mode) {
            //alert('I click');
            if (mode == "POLYGON") {
                console.log("POLY");
                $scope.drawingManager.drawingMode = google.maps.drawing.OverlayType.POLYGON;
                console.log("Draw Polygon::" + $scope.drawingManager.drawingMode);
            }

        }
        $scope.selColor = "#FF1493";

        $scope.setMapColor = function(FillColor) {
            var MapMode = $scope.drawingManager.drawingMode;
            console.log("change Fille color :: " + FillColor);
            $scope.selColor = FillColor;
            polyOptions.fillColor = FillColor;
            $scope.drawingManager.drawingMode = MapMode;
        }

        var polyOptions = {
            fillColor: '#BCDCF9',
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true,
            clickable: true,
            draggable: true,
            // geodesic: true,
            // strokeOpacity: 0.8,
            fillColor: $scope.selColor,
            zIndex: 1
        };

        // uiGmapGoogleMapApi.then(function (maps) {
        $scope.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            editable: true,
            draggable: true,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                editable: true,
                drawingModes: [google.maps.drawing.OverlayType.POLYGON]
            },
            polygonOptions: polyOptions
        });
        $scope.drawingManager.setMap(map);

        google.maps.event.addListener($scope.drawingManager, 'polygoncomplete', function(polygon) {

            $scope.drawingManager.setDrawingMode(null);

            polygon.setOptions({
                id: shapeID,
                editable: true,
                draggable: true
            });

            google.maps.event.addListener(polygon, 'click', function() {
                $scope.getGeoFnceDtls();
                console.log("Click");
                console.log(this.id + ' ' + this.getPath().getArray().toString());
            });

            google.maps.event.addListener(polygon, 'dragend', function() {
                $scope.getGeoFnceDtls();
                console.log(this.id + ' ' + this.getPath().getArray().toString());
                console.log("Draggend");
            });

            google.maps.event.addListener(polygon.getPath(), "insert_at", getPath);
            google.maps.event.addListener(polygon.getPath(), "remove_at", getPath);
            google.maps.event.addListener(polygon.getPath(), "set_at", getPath);

            function getPath() {
                var path = polygon.getPath();
                var coordStr = 'id: ' + polygon.id + '\n';
                
                for (var i = 0; i < polygon.getPath().getLength(); i++) {
                
                    $scope.coordStr += polygon.getPath().getAt(i).toUrlValue(6) + "<br>";
                    var cords_t = polygon.getPath().getAt(i).toUrlValue(6);
                    var clatlng = cords_t.split(',');
                    var final_cords = (final_cords) ? final_cords + ',' + clatlng[1] + ' ' + clatlng[0] : clatlng[1] + ' ' + clatlng[0];
                }
                // document.getElementById('coordStr').innerHTML
                $scope.coordStr += polygon.getPath().getAt(0).toUrlValue(6) + "<br>";
                var clatlng2 = polygon.getPath().getAt(0).toUrlValue(6).split(',');
                console.log('POLYGON((' + final_cords + ',' + clatlng2[1] + ' ' + clatlng2[0] + '))');
                $scope.poly_txt = 'POLYGON((' + final_cords + ',' + clatlng2[1] + ' ' + clatlng2[0] + '))';
                console.log($scope.poly_txt);
            }
            shapeID++;
        });


            $scope.getGeoFnceDtls = function(){
                console.log($scope.fnce_id);
                for(i=0;i< $scope.geoFencesCords.length;i++){
                if($scope.geoFencesCords[i].fnce_id == $scope.fnce_id){
                        var geoFenceCordDtls=$scope.geoFencesCords[i];
                }
            }

            var polyCoords = geoFenceCordDtls.fncs_dtl_tx;
            var x = polyCoords.replace("POLYGON((","").replace("))","");
            var buildStr='';
            var strArr = x.split(',');

            for(i=0;i<strArr.length;i++){
              var tmpStr = strArr[i].trim(" ");
              var cords = tmpStr.split(' ');
              (buildStr==='') ? buildStr = buildStr+'{"latitude":'+cords[1]+',"longitude":'+cords[0]+'}' : buildStr = buildStr+',{"latitude":'+cords[1]+',"longitude":'+cords[0]+'}';
            }
            var finalStr = JSON.parse('['+buildStr+']') ;
            console.log(finalStr);
            var polyCoordsColor = geoFenceCordDtls.fncs_clr_cd;
            var polyCoordsName = geoFenceCordDtls.fnce_nm;

                $scope.geoFences = [
                {    
                    id:$scope.fnce_id,            
                    path:finalStr,
                    stroke: {
                        color: polyCoordsColor,
                        weight: 3
                    },
                    editable: false,
                    draggable: false,
                    geodesic: false,
                    visible: true,
                    fill: {
                        color: '#ff0000', 
                        opacity: 0.8 
                    },
                }
            ];

            $scope.geo = [
                {                
                    path:finalStr,
                    editable: false,
                    draggable: false,
                    geodesic: false,
                    visible: true,
                }
            ];

            var geoPoly = $scope.geoFences[0].path;
            console.log(geoPoly);

             console.log($scope.geoFences[0].path);
             $scope.buildStr = '';
             for(i=0;i<$scope.geoFences[0].path.length;i++){
               
               ($scope.buildStr=='') ? $scope.buildStr = $scope.geoFences[0].path[i].longitude+' '+$scope.geoFences[0].path[i].latitude : $scope.buildStr = $scope.buildStr+','+$scope.geoFences[0].path[i].longitude+' '+$scope.geoFences[0].path[i].latitude;
             }
             
             $scope.geo = 'POLYGON((' + $scope.buildStr + '))';
            console.log($scope.geo);
        }

        $scope.geoFenceSubmit = function(mode, fncs_clr_cd, fnce_ctgry_id, fnce_grp_id, coordStr, fnce_nm) {
            alert("Successfully Added..");
            var data = {
                "fncs_clr_cd": $scope.selColor,
                "fnce_ctgry_id": fnce_ctgry_id,
                "fnce_grp_id": fnce_grp_id,
                "coordStr": $scope.poly_txt,
                "fnce_nm": fnce_nm,
                "fnce_type_id": 2,
                "clnt_id": 1,
                "tnt_id": 2,
                "a_in": 1
            }
            console.log(data);
            // console.log($scope.poly_txt);
            $http.post(baseURL + "/postGeoFences", data)
                .success(function(resdata, status, headers, config) {
                    if (status == 200) {
                        $scope.geoFences = resdata.data;
                        // console.log($scope.geoFences);
                    } else {
                        console.log("Data = " + resdata);
                    }
                }).
            error(function(data, status, headers, config) {
                //   alert("Please connect to internet and restart App");
                $scope.message = "Unable retrive data from Server.";
            });
        }
    })


    // =========================================================================
    // Geo Fence Landmarks related controllers
    // =========================================================================
  .controller('landMarksCtrl', function ($rootScope, recentpostService, $http, $scope, $log, $timeout, $window) {
        var centerLat = "17.031032";
        var centerLon = "81.78976";
        var zoomLevel = 11;
        var basePath = 'http://localhost:4801/';
        var iconBase = basePath + 'client/src/public/static/img/markers/';
        var group = L.featureGroup();
        var routerControl;
        var routeBuilder;
        var routeMapLayer;
        var totalDistance;
        var distMeterToKms;
        var totalTimeTaken;
        var secondsToHm;
        var polyline;
        var route;
        var routes;
        var timer;
        var waypoints = [];
        var wayFixedPoints = [];
        var LatLngObjects = [];
        var startData = [];
        $scope.fencesList = [];
        $scope.selected = "";

        var inTtPosition = new L.Icon({
                iconUrl: iconBase + 'red_flag.png',
                iconSize: [30, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41],
                radius: 7,
        });

        $scope.choices = [{ id: 'choice1', name: 'choice1' }];

        $scope.addNewChoice = function () {
            var newItemNo = $scope.choices.length + 1;
            $scope.choices.push({ 'id': 'choice' + newItemNo, 'name': 'choice' + newItemNo });
        };

        $scope.removeNewChoice = function () {
            var newItemNo = $scope.choices.length - 1;
            if (newItemNo !== 0) {
                $scope.choices.pop();
            }
        };

        $scope.showAddChoice = function (choice) {
            return choice.id === $scope.choices[$scope.choices.length - 1].id;
        };

        // Select Landmark to push into wapypoints
        $scope.addLandMarks = function (KeyboardEvent, fromobj, index) {
             console.log(KeyboardEvent,fromobj,index);
            // clearTimeout(timer);
            // timer = setTimeout( function() {
            // console.log(KeyboardEvent);
           LatLngObjects.push(L.latLng(KeyboardEvent.originalObject.fnce_lat, KeyboardEvent.originalObject.fnce_lng));
           // console.log(LatLngObjects);
            $scope.routeApply(LatLngObjects);
            // }, 1000);
        }

        // Populating Collection Spot Landmarks to anguAutoComplete
        $scope.getFence = function () {
            console.log("fence names %^*&");
            $http.get($rootScope.BaseUrl + 'getFences/' + 1)
                .success(function (resdata, status) {
                    $scope.getFencesList = resdata.data;

                    for (var k = 0; k < $scope.getFencesList.length; k++) {
                        $scope.fencesList.push({
                            fnce_id: $scope.getFencesList[k].fnce_id,
                            fnce_nm: $scope.getFencesList[k].fnce_nm,
                            fnce_lat: $scope.getFencesList[k].lat,
                            fnce_lng: $scope.getFencesList[k].lng,
                        });
                    }
                    console.log($scope.fencesList);
                })
                .error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getFence();

        // Initialize Leaflet Map with Google Layer
        $scope.initialize = function () {
            try {
                routeBuilder = L.map('routeBuilderMap').setView([centerLat, centerLon], zoomLevel);
                // routeMapLayer = new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                var osmLayer = new L.tileLayer('https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i349018013!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0', {
                    attribution: 'Dreamstep Software Innovations Pvt Ltd',
                    draggable: true,
                }).addTo(routeBuilder);
            } catch (e) {
                console.log(e);
            }
        }
        $scope.initialize();

        // Applying Route by recieving waypoints
        $scope.routeApply = function(resultData) {
            console.log(resultData);
            var routeCtrl = L.Routing.control({
                waypoints: resultData,
                createMarker: function(i, wp) {
                     return L.marker(wp.latLng, {
                        draggable: false,
                        icon: inTtPosition,
                        radius: 40
                     });
                },
                waypointMode: 'connect',
                // createMarker: function() {},
                draggableWaypoints: false,//to set draggable option to false
                addWaypoints: false, //disable adding new waypoints to the existing path 
                routeWhileDragging: false,
                geocoder: L.Control.Geocoder.nominatim(),
                // geocoder: L.Control.Geocoder.photon(),
                // geocoder: L.Control.Geocoder.google(googleAPIKey),
                reverseWaypoints: false,
                fitSelectedRoutes: true,
                showAlternatives: false,
                lineOptions: {
                    styles: [{ color: 'red', opacity: 1, weight: 5 }]
                }
            })
            .on('routesfound', function (e) {
                routes = e.routes;
                console.log(routes);
                if (routes !== undefined) {
                    totalDistance = routes[0].summary.totalDistance;
                    distMeterToKms = Math.round(totalDistance / 1000);
                    $scope.totalDistInKms = distMeterToKms.toFixed(1) + "Km"; //Total Distance route wise
                    totalTimeTaken = routes[0].summary.totalTime;
                    // console.log(totalTimeTaken);
                    $scope.tolTimeHm = secondsToHm(totalTimeTaken);
                    console.log("Distance:" + $scope.totalDistInKms + "," + "ETA:" + $scope.tolTimeHm);
                }
                // swal('Found ' + routes.length + ' route(s).');
                // alert('Showing route between waypoints:\n' + JSON.stringify(routes["0"].inputWaypoints, null, 2));
            }).addTo(routeBuilder);

        // .on('routingstart', showSpinner)
        // .on('routesfound routingerror', hideSpinner)
        // var resultError = L.Routing.errorControl(routeCtrl).addTo(routeBuilder);



        //show original point-to-point path for comparison
        polyline = L.polyline(resultData, {
            color: '#44C',
            weight: 10,
            opacity: 0.2
        }).addTo(group);
        routeBuilder.fitBounds(group.getBounds());
        group.addTo(routeBuilder);
    }

    secondsToHm = function (d) {
        console.log(d);
        d = Number(d);
        const h = Math.floor(d / 3600);
        const m = Math.floor(d % 3600 / 60);
        return ((h > 0 ? h + " h " + (m < 10 ? "0" : "") : "") + m + " min"); // eslint-disable-line
    };
})


// .on('routingerror', function(e) {
//             try {
//             routeBuilder.getCenter();
//         } catch (e) {
//             routeBuilder.fitBounds(L.latLngBounds(waypoints));
//             }
//             handleError(e);
//         });

//     L.Routing.errorControl(control).addTo(routeBuilder);

// control.on('routeselected', function(e) {
//   L.polyline(e.route.coordinates).addTo(group);
//   map.removeControl(control);
// });

   .controller('landCateCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService, apppdfService) {
        $scope.searchVehData = '';
        $scope.updtLandMarkCate = function(v) {
            $scope.showModal = true;
            console.log(v);
            $rootScope.landMarkCate = v;
            window.location.href = '#geoCoding/landmark/addUpdtLandMarkCate';
        };

    $scope.cancel = function() {
        $scope.showModal = false;
    };

    $scope.deleteLandMarkCate = function(fnce_ctgry_id) {
        var data = {
            fnce_ctgry_id: fnce_ctgry_id
        }
        console.log(data);

        swal({
            title: "Are you sure you want to delete this Category ?",
            text: "Once you delete this it wont be displayed again. Make sure you want to do this",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, Delete It",
            closeOnConfirm: true
        }, function() {

            $http.post($rootScope.BaseUrl + 'delLandMarkCateNme', data)
                .success(function(resdata, status) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if (resdata.status == 601) {
                        // console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                    } else {
                        growlService.growl(' Successfully Deleted!', 'inverse');
                        $scope.getLandMarkCate();
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        });
    }

    $scope.getLandMarkCate = function() {
        $http.get($rootScope.BaseUrl + 'getCategories')
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.getCateData = resdata.data;
                // console.log($scope.getCateData);
                $scope.loadfilter($scope.getCateData);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getLandMarkCate();
    $scope.loadfilter = function(data) {
        $scope.tableFilter = new NgTableParams({
            page: 1,
            count: 10
        }, {
            data: data
        });
    };
    $scope.loadfilter($scope.vehiclesLst);
  
   $scope.pdfDownload = function() {
         var jsonColumns = ['fnce_ctgry_id', 'fnce_ctgry_nm'];
         var colHeadings = ['SNO', 'FENCE CATEGORY NAME'];
         var tbldata = $scope.getCateData;
         console.log(tbldata);

         var rptHeading = 'LANDMARK_CATEGORY';
         var fileName = 'LANDMARK_CATEGORY';
         var align = 'portrait';
         apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align);
     }

     $scope.exceldownlad = function() {
         $scope.geoFenceCategory = [];
         $scope.geoFenceCategory.push({
             'fnce_ctrgy_id': '',
             'fnce_ctrgy_nm': ''
         });
         $scope.geoFenceCategory.push({
             'fnce_ctrgy_id': 'SNO',
             'fnce_ctrgy_nm': 'LANDMARK CATEGORY NAME'
         });
         for (var i = 0; i < $scope.getCateData.length; i++) {
             // console.log($scope.getCateData);
             $scope.geoFenceCategory.push({
                 'fnce_ctrgy_id': $scope.geoFenceCategory[i].fnce_ctrgy_id,
                 'fnce_ctrgy_nm': $scope.geoFenceCategory[i].fnce_ctrgy_nm
             });
         }
         return $scope.geoFenceCategory;
     }
})
    .controller('landMarkCateAddCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService) {

        if ($rootScope.landMarkCate) {
            $scope.fnce_ctgry_nm = $rootScope.landMarkCate.fnce_ctgry_nm;
            // console.log($scope.fnce_ctgry_nm);
        }

        $scope.addNewCategoryName = function() {

            if ($rootScope.landMarkCate) {
                console.log("Land Mark Category Add Controler");

                var upddata = {
                    "fnce_ctgry_nm": $scope.fnce_ctgry_nm,
                    "fnce_ctgry_id": $rootScope.landMarkCate.fnce_ctgry_id,
                    //"fnce_ctgry_id": 1,
                    //"fnce_type_id": 1,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in": 1
                }
                // console.log(upddata);
                var url = $rootScope.BaseUrl + 'updateLandMarkCate';
                $scope.addUpdtCat(upddata, url);
            } else {
                var data = {
                    "fnce_ctgry_nm": $scope.fnce_ctgry_nm,
                    // "fnce_ctgry_id": 1,
                    // "fnce_type_id": 1,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in": 1
                }
                // console.log(data);
                var url = $rootScope.BaseUrl + 'postLandMarkCat';
                $scope.addUpdtCat(data, url);
            }
        }

        $scope.addUpdtCat = function(data, url) {
            $http.post(url, data)
                .success(function(resdata, status) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if (resdata.status == 601) {
                        // console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                        window.location.href = '#//geoCoding/landmark/Category';
                    } else {
                        growlService.growl(' Successfully Updated!', 'inverse');
                        window.location.href = '#/geoCoding/landmark/Category';
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
    })
    .controller('landMarkGrpAddCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService) {

        if ($rootScope.landMarkGrp) {
            $scope.fnce_grp_nm = $rootScope.landMarkGrp.fnce_grp_nm;
            console.log($scope.fnce_grp_nm);
        }

        $scope.addNewGroupName = function() {
            if ($rootScope.landMarkGrp) {
                console.log("Land Mark Group Add Controller");
                var upddata = {
                    "fnce_grp_nm": $scope.fnce_grp_nm,
                    "fnce_grp_id": $rootScope.landMarkGrp.fnce_grp_id,
                    "fnce_ctgry_id": 1,
                    "fnce_type_id": 1,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in": 1
                }
                // console.log(upddata);
                var url = $rootScope.BaseUrl + 'updateLandMarkGrps';
                $scope.addUpdtGrp(upddata, url);
            } else {
                var data = {
                    "fnce_grp_nm": $scope.fnce_grp_nm,
                    "fnce_ctgry_id": 1,
                    "fnce_type_id": 1,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in": 1
                }
                console.log(data);
                var url = $rootScope.BaseUrl + 'postLandMarkGrp';
                $scope.addUpdtGrp(data, url);
            }
        }

        $scope.addUpdtGrp = function(data, url) {
            $http.post(url, data)
                .success(function(resdata, status) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if (resdata.status == 601) {
                        // console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                        window.location.href = '#/geoCoding/landmark/Groups';
                    } else {
                        growlService.growl(' Successfully Updated!', 'inverse');
                        window.location.href = '#geoCoding/landmark/Groups';
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
    })

.controller('landGrpCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService, apppdfService) {
    console.log("Land Mark Group Controller");

    $scope.searchVehData = '';
    $scope.getLandMarkGrp = function() {
        $http.get($rootScope.BaseUrl + 'getLandMarkGrps')
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.getLandMarkGrps = resdata.data;
                // console.log($scope.getLandMarkGrps);
                $scope.loadfilter($scope.getLandMarkGrps);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getLandMarkGrp();

    $scope.updtLandMarkGrp = function(v) {
        $scope.showModal = true;
        console.log(v);
        $rootScope.landMarkGrp = v;
        window.location.href = '#geoCoding/landmark/addUpdtLandMarkGrp';
    };

    $scope.deleteLandMarkGrp = function(fnce_grp_id) {
        var data = {
            fnce_grp_id: fnce_grp_id
        }
        // console.log(data);

        swal({
            title: "Are you sure you want to delete this Group ?",
            text: "Once you delete this it wont be displayed again. Make sure you want to do this",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, Delete It",
            closeOnConfirm: true
        }, function() {

            $http.post($rootScope.BaseUrl + 'delLandMarkGrpNme', data)
                .success(function(resdata, status) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if (resdata.status == 601) {
                        // console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                    } else {
                        growlService.growl(' Successfully Deleted!', 'inverse');
                        $scope.getLandMarkGrp();
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        });
    }

    $scope.cancel = function() {
        $scope.showModal = false;
    };

    $scope.loadfilter = function(data) {
        $scope.tableFilter = new NgTableParams({
            page: 1,
            count: 10
        }, {
            data: data
        });
    };

    $scope.loadfilter($scope.vehiclesLst);

       $scope.pdfDownload = function() {
         var jsonColumns = ['fnce_grp_id', 'fnce_grp_nm'];
         var colHeadings = ['SNO', 'FENCE GROUP NAME'];
         var tbldata = $scope.getGeoFenceGrps;

         var rptHeading = 'GEOFENCE_GROUP';
         var fileName = 'GEOFENCE_GROUP';
         var align = 'portrait';
         apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align);
     }

     $scope.exceldownload = function() {
         $scope.geoFenceGroup = [];
         $scope.geoFenceGroup.push({
             'fnce_grp_id': '',
             'fnce_grp_nm': ''
         });
         $scope.geoFenceGroup.push({
             'fnce_grp_id': 'SNO',
             'fnce_grp_nm': 'LANDMARK GROUP NAME'
         });
         for (var i = 0; i < $scope.getGeoFenceGrps.length; i++) {
             // console.log($scope.getGeoFenceGrps);
             $scope.geoFenceGroup.push({
                 'fnce_grp_id': $scope.getGeoFenceGrps[i].fnce_grp_id,
                 'fnce_grp_nm': $scope.getGeoFenceGrps[i].fnce_grp_nm
             });
         }
         return $scope.geoFenceGroup;
     }
})


    .controller('geoFenceCategoryCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService, apppdfService) {

        $scope.searchCateData = '';

        $scope.updtGeoFenceCate = function(v) {
            $scope.showModal = true;
            console.log(v);

            $rootScope.geoFenceCate = v;
            window.location.href = '#/geoCoding/geoFence/updtGeoFenceCate';
        };

        $scope.cancel = function() {
            $scope.showModal = false;
        };

        $scope.deleteGeoFenceCategory = function(fnce_ctgry_id) {
            var data = {
                "fnce_ctgry_id": fnce_ctgry_id
            }
            // console.log(data);

            swal({
                title: "Are you sure you want to delete this Category ?",
                text: "Once you delete this it wont be displayed again. Make sure you want to do this",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Delete It",
                closeOnConfirm: true
            }, function() {
                $http.post($rootScope.BaseUrl + 'delLandMarkCateNme', data)
                    .success(function(resdata, status) {
                        if (status == 257) $rootScope.sessionCheck();
                        $scope.resMesg = resdata;
                        if (resdata.status == 601) {
                            // console.log(resdata);
                            growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                        } else {
                            growlService.growl(' Successfully Deleted!', 'inverse');
                            $scope.getGeoFenceCate();
                        }
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                    });
            });
        }

        $scope.getGeoFenceCate = function() {
            $http.get($rootScope.BaseUrl + 'getCategories')
                .success(function(resdata, status) {
                    if (status == 257)
                        $rootScope.sessionCheck();
                    $rootScope.getCateData = resdata.data;
                    $scope.loadfilter($scope.getCateData);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getGeoFenceCate();


     $scope.pdfDownload = function() {
         var jsonColumns = ['fnce_ctgry_id', 'fnce_ctgry_nm'];
         var colHeadings = ['SNO', 'FENCE CATEGORY NAME'];
         var tbldata = $scope.getCateData;
         // console.log(tbldata);

         var rptHeading = 'GEOFENCE_CATEGORY';
         var fileName = 'GEOFENCE_CATEGORY';
         var align = 'portrait';
         apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align);
     }

     $scope.exceldownlad = function() {
         $scope.geoFenceCategory = [];
         $scope.geoFenceCategory.push({
             'fnce_ctrgy_id': '',
             'fnce_ctrgy_nm': ''
         });
         $scope.geoFenceCategory.push({
             'fnce_ctrgy_id': 'SNO',
             'fnce_ctrgy_nm': 'FENCE CATEGORY NAME'
         });
         for (var i = 0; i < $scope.getCateData.length; i++) {
             // console.log($scope.getCateData);
             $scope.geoFenceCategory.push({
                 'fnce_ctrgy_id': $scope.geoFenceCategory[i].fnce_ctrgy_id,
                 'fnce_ctrgy_nm': $scope.geoFenceCategory[i].fnce_ctrgy_nm
             });
         }
         return $scope.geoFenceCategory;
     }


        $scope.loadfilter = function(data) {
            $scope.tableFilter = new NgTableParams({
                page: 1,
                count: 10
            }, {
                data: data
            });
        };
        $scope.loadfilter($scope.vehiclesLst);
    })

    .controller('geoFenceCateAddCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService) {

     if ($rootScope.geoFenceCate) {
         $scope.fnce_ctgry_nm = $rootScope.geoFenceCate.fnce_ctgry_nm;

     }
     $scope.addNewCategoryName = function() {
         if ($rootScope.geoFenceCate) {

             var upddata = {
                 "fnce_ctgry_nm": $scope.fnce_ctgry_nm,
                 "fnce_ctgry_id": $rootScope.geoFenceCate.fnce_ctgry_id,
                 "clnt_id": 1,
                 "tnt_id": 2,
                 "a_in": 1
             }
             var url = $rootScope.BaseUrl + 'updateLandMarkCate';
             $scope.addUpdtCat(upddata, url);
         } else {
             var data = {
                 "fnce_ctgry_nm": $scope.fnce_ctgry_nm,
                 "clnt_id": 1,
                 "tnt_id": 2,
                 "a_in": 1
             }
             // console.log(data);
             var url = $rootScope.BaseUrl + 'postLandMarkCat';
             $scope.addUpdtCat(data, url);
         }

     }

     $scope.addUpdtCat = function(data, url) {
         $http.post(url, data)
             .success(function(resdata, status) {
                 if (status == 257) $rootScope.sessionCheck();
                 $scope.resMesg = resdata;
                 if (resdata.status == 601) {
                     // console.log(resdata);
                     growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                     window.location.href = '#/geoCoding/geoFence/Category';
                 } else {
                     growlService.growl(' Successfully Updated!', 'inverse');
                     window.location.href = '#/geoCoding/geoFence/Category';
                 }
             })
             .error(function(data, status, headers, config) {
                 console.log(data);
             });
     }
 })

 .controller('geoFenceGroupCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService, apppdfService) {

     $scope.searchGrpData = '';

     $scope.addUpdtgeoFenceGrp = function(v) {
         console.log(v);
         $scope.showModal = true;
         $rootScope.geoFenceGrp = v;
         console.log($rootScope.geoFenceGrp);
         window.location.href = '#/geoCoding/geoFence/updtGeoFenceGrp';
     }

     $scope.getGeoFenceGrp = function() {
         $http.get($rootScope.BaseUrl + 'getLandMarkGrps')
             .success(function(resdata, status) {
                 if (status == 257) $rootScope.sessionCheck();
                 $scope.getGeoFenceGrps = resdata.data;
                 // console.log($scope.getGefoFenceGrps);
                 $scope.loadfilter($scope.getGeoFenceGrps);
             })
             .error(function(data, status, headers, config) {
                 console.log(data);
             });
     }
     $scope.getGeoFenceGrp();

     $scope.deletegeoFenceGrp = function(fnce_grp_id) {
         var data = {
             fnce_grp_id: fnce_grp_id
         }
         // console.log(data);

         swal({
             title: "Are you sure you want to delete this Group ?",
             text: "Once you delete this it wont be displayed again. Make sure you want to do this",
             type: "warning",
             showCancelButton: true,
             confirmButtonText: "Yes, Delete It",
             closeOnConfirm: true
         }, function() {

             $http.post($rootScope.BaseUrl + 'delLandMarkGrpNme', data)
                 .success(function(resdata, status) {
                     if (status == 257) $rootScope.sessionCheck();
                     $scope.resMesg = resdata;
                     if (resdata.status == 601) {
                         // console.log(resdata);
                         growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                     } else {
                         growlService.growl(' Successfully Deleted!', 'inverse');
                         $scope.getGeoFenceGrp();
                     }
                 })
                 .error(function(data, status, headers, config) {
                     console.log(data);
                 });
         });
     }

     $scope.cancel = function() {
         $scope.showModal = false;
     };

     $scope.pdfDownload = function() {
         var jsonColumns = ['fnce_grp_id', 'fnce_grp_nm'];
         var colHeadings = ['SNO', 'FENCE GROUP NAME'];
         var tbldata = $scope.getGeoFenceGrps;

         var rptHeading = 'GEOFENCE_GROUP';
         var fileName = 'GEOFENCE_GROUP';
         var align = 'portrait';
         apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align);
     }

     $scope.exceldownlad = function() {
         $scope.geoFenceGroup = [];
         $scope.geoFenceGroup.push({
             'fnce_grp_id': '',
             'fnce_grp_nm': ''
         });
         $scope.geoFenceGroup.push({
             'fnce_grp_id': 'SNO',
             'fnce_grp_nm': 'FENCE GROUP NAME'
         });
         for (var i = 0; i < $scope.getGeoFenceGrps.length; i++) {
             // console.log($scope.getGeoFenceGrps);
             $scope.geoFenceGroup.push({
                 'fnce_grp_id': $scope.getGeoFenceGrps[i].fnce_grp_id,
                 'fnce_grp_nm': $scope.getGeoFenceGrps[i].fnce_grp_nm
             });
         }
         return $scope.geoFenceGroup;
     }

     $scope.loadfilter = function(data) {
         $scope.tableFilter = new NgTableParams({
             page: 1,
             count: 10
         }, {
             data: data
         });
     };
     $scope.loadfilter($scope.getGeoFenceGrps);
 })

.controller('geoFenceAddCtrl', function(recentpostService, $rootScope, $http, $scope, $log, $timeout, $window, NgTableParams, growlService) {
     if ($rootScope.geoFenceGrp) {
         $scope.fnce_grp_nm = $rootScope.geoFenceGrp.fnce_grp_nm;
         // console.log($scope.fnce_grp_nm);
     }

     $scope.addNewGroupName = function() {
         if ($rootScope.geoFenceGrp) {
             console.log("Land Mark Group Add Controller");
             var upddata = {
                 "fnce_grp_nm": $scope.fnce_grp_nm,
                 "fnce_grp_id": $rootScope.geoFenceGrp.fnce_grp_id,
                 "fnce_ctgry_id": 1,
                 "fnce_type_id": 1,
                 "clnt_id": 1,
                 "tnt_id": 2,
                 "a_in": 1
             }
             // console.log(upddata);
             var url = $rootScope.BaseUrl + 'updateLandMarkGrps';
             $scope.addUpdtGrp(upddata, url);
         } else {
             var data = {
                 "fnce_grp_nm": $scope.fnce_grp_nm,
                 "fnce_ctgry_id": 1,
                 "fnce_type_id": 1,
                 "clnt_id": 1,
                 "tnt_id": 2,
                 "a_in": 1
             }
             // console.log(data);
             var url = $rootScope.BaseUrl + 'postLandMarkGrp';
             $scope.addUpdtGrp(data, url);
         }
     }

     $scope.addUpdtGrp = function(data, url) {
         $http.post(url, data)
             .success(function(resdata, status) {
                 if (status == 257) $rootScope.sessionCheck();
                 $scope.resMesg = resdata;
                 if (resdata.status == 601) {
                     // console.log(resdata);
                     growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                     window.location.href = '#/geoCoding/geoFence/Group';
                 } else {
                     growlService.growl(' Successfully Updated!', 'inverse');
                     window.location.href = '#geoCoding/geoFence/Group';
                 }
             })
             .error(function(data, status, headers, config) {
                 console.log(data);
             });
     }
 })