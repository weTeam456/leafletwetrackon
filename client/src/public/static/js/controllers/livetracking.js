wertrackon
    // =========================================================================
    // LiveTracking related controllers
    // =========================================================================
    .controller("liveTrackingHomeCtrl", function($scope, $rootScope, $window, growlService, $location, $http, localStorageService) {
        var infoWindow = new google.maps.InfoWindow();
        // variable to hold current active InfoWindow
        var activeInfoWindow;
        var markerCluster;
        console.log("llmaptrackingHomeCtrl.... :" + $location.$$path);
        var basePath = 'http://localhost:4801/';
        var iconBase = basePath + 'client/src/public/static/img/markers/';
        var curdate = new Date();
        $rootScope.sdate = moment($rootScope.curdate).format('YYYY-MM-DD 00:00:00');
        $rootScope.edate = moment($rootScope.curdate).format('YYYY-MM-DD 23:59:59');

        var pathUrl = $location.$$path;
        if (pathUrl.indexOf('livemap') == -1 && pathUrl.indexOf('playback') == -1) {
            $window.location.href = "#/liveTracking/livemap";
        } else {
            $window.location.href = "#" + pathUrl;
        }
        $scope.isActiveTab = true;
        $rootScope.isRefresh = true;

        $rootScope.filterOnStateChange = function (state) {
            if (state == 'livemap') {
                $scope.show = false;
            } else $scope.show = true;
        };
        $scope.showFilterFn = function () {
            $scope.showFilter = !$scope.showFilter;
        };
    })

    .controller('liveTrackingCtrl', function($scope, $rootScope, $http, $timeout, $state, $location, $window, leafletData) {
       console.log("llmaptrackingLiveMapCtrl");

        $rootScope.isRefresh = true;
        $scope.$parent.isActiveTab = true;
        var basePath = 'http://localhost:4801/';
        var iconBase = basePath + 'client/src/public/static/img/markers';
        var init_lat = "17.031032";
        var init_lng = "81.78976";

        $scope.lltmap;
        $scope.initialise = function() {
            $scope.ltmap = {
            center : {
                lat: 17.004393,
                lng: 81.783325,
                zoom: 13
            },
            layers: {
                    baselayers: {
                        googleTerrain: {
                            name: 'Google Terrain',
                            layerType: 'TERRAIN',
                            type: 'google'
                        },
                        googleHybrid: {
                            name: 'Google Hybrid',
                            layerType: 'HYBRID',
                            type: 'google'
                        },
                        googleRoadmap: {
                            name: 'Google Streets',
                            layerType: 'ROADMAP',
                            type: 'google'
                        },
                        osm: {
                        name: 'OpenStreetMap',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                    mapbox_light: {
                        name: 'Mapbox Light',
                        // url: 'https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i349018013!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0',
                        url: 'https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicGF0cmlja3IiLCJhIjoiY2l2aW9lcXlvMDFqdTJvbGI2eXUwc2VjYSJ9.trTzsdDXD2lMJpTfCVsVuA',
                        type: 'xyz'
                    },
                    bingRoad: {
                        name: 'Bing Road',
                        type: 'bing',
                        key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                        attribution: 'Map data &copy; <a href="http://dreamstep.in"></a> Dreamstep Software Innovations Pvt Ltd,</a></a>',
                        layerOptions: {
                            type: 'Road'
                        }
                    }
                    }
                },
                markersWatchOptions: {
                 doWatch: false,
                 isDeep: false,
                 individual: {
                      doWatch: false,
                      isDeep: false
                      }
            },
            events: {
                map: {
                    enable: ['moveend', 'popupopen', 'click'],
                    logic: 'emit'
                },
                marker: {
                    enable: [],
                    logic: 'emit'
                }
            },
            markers: {},
        }
            
            setVehicleMarkers();
        }

        $scope.leftMenuVisible = true;
        $scope.showLeftMenu = function () {
            console.log("click button");
            $scope.leftMenuVisible = !$scope.leftMenuVisible;
        };

        var setVehicleMarkers = function() {
            var markersList = [];
            $http.get($rootScope.BaseUrl + "/vehicles").success(function(data) {
                if (status == 257) $rootScope.sessionCheck();

                $scope.markers = [];
                $scope.vehiclesList = [];
                $scope.masterVehicleMarkers = [];
                $scope.totalOfflineVehicleMarkers = [];
                // $scope.totalRepairVehicles = [];
                markersList = data.data;
                // console.log(markersList.length);
                for (i = 0; i < markersList.length; i++) {
                    data = markersList[i];

                 var markerData = {
                        asrt_id: data.asrt_id,
                        ast_sts_id: data.ast_sts_id,
                        asrt_grp_id: data.asrt_grp_id,
                        crnt_Ast_sts_id: data.crnt_Ast_sts_id,
                        asrt_ctgry_id: data.asrt_ctgry_id,
                        asrt_sts_id: data.asrt_sts_id,
                        asrt_type_id: data.asrt_type_id,
                        asrt_ctgry_nm: data.asrt_ctgry_nm,
                        asrt_type_nm: data.asrt_type_nm,
                        asrt_sts_nm: data.asrt_sts_nm,
                        asrt_nm: data.asrt_nm,
                        asrt_grp_nm: data.asrt_grp_nm,
                        startFence: data.startFence,
                        dev_imei_nu: data.dev_imei_nu,
                        icon_path: data.icon_path,
                        lat: data.lat,
                        lng: data.lng,
                        odmtr_ct: data.odmtr_ct,
                        onl_offl: data.onl_offl,
                        title: data.asrt_nm,
                        speed_ct: data.spd_ct,
                        crw_ph: data.crw_ph,
                        crw_nm: data.crw_nm,
                        vehstat: data.vehstat,
                        checked: true,
                        id: i + 1,
                        focus: true,
                        opacity: 1
                    };

                        if (data.spd_ct == 0 && data.asrt_ctgry_id == 1) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/tractor_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 1) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/tractor_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 2) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/van_parked.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 2) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/van_online.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 3) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/lorry_parked.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 3) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/Lorry_online.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 4) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/dumpPlcr_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 4) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/dumpPlcr_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 5) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/tipper_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 5) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/tipper_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 8) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/waterTank_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 8) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/waterTank_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 10) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/tataMgc_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 10) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/tataMgc_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 7) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/JCB_o.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 7) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/JCB_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 9) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/buldozer_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 9) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/buldozer_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 6) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/dogsA_p.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 6) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/dogsA_r.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 11) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/isuzuu.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 11) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/isuzuu.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 12) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/GC.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 12) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/GC_park.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct == 0 && data.asrt_ctgry_id == 13) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/GC.png',
                                // iconSize: [25, 25]
                            }
                        } else if (data.spd_ct > 0 && data.asrt_ctgry_id == 13) {
                            markerData['icon'] = {
                                iconUrl: iconBase + '/GC_park.png',
                                // iconSize: [25, 25]
                            }
                        }

                    // ON MAP MARKERS
                    $scope.markers.push(markerData);
                    console.log($scope.markers);
                    // left side menu - vehicle list
                    if (markerData.onl_offl == 1) {
                        $scope.vehiclesList.push(markerData);
                        $scope.masterVehicleMarkers.push(markerData);
                    }
                    // OFFLINE VEHICLES 
                    $scope.totalOfflineVehicleMarkers.push(markerData);
                    //REPAIR VEHICLES
                    // $scope.totalRepairVehicles.push(markerData);
                }
                $scope.showOfflineVehicles();
                $scope.showRepairVehicles();
                $scope.showContractExpired();
            });
        };




var refreshVehicleMarkers = function() {
            var markersList = [];
            $http.get($rootScope.BaseUrl + "/vehicles").success(function(data) {
                if (status == 257) $rootScope.sessionCheck();

                markersList = data.data;
                for (i = 0; i < markersList.length; i++) {
                    data = markersList[i];

                    for (var j = 0; j < $scope.markers.length; j++) {
                        if ($scope.markers[j].asrt_nm == data.asrt_nm) {
                            $scope.markers[j].lat = data.lat;
                            $scope.markers[j].lng = data.lng;
                        }
                    }

                    for (j = 0; j < $scope.vehiclesList.length; j++) {
                        if ($scope.vehiclesList[j].asrt_nm == data.asrt_nm) {
                            $scope.vehiclesList[j].lat = data.lat;
                            $scope.vehiclesList[j].lng = data.lng;
                        }
                    }

                    for (j = 0; j < $scope.masterVehicleMarkers.length; j++) {
                        if ($scope.masterVehicleMarkers[j].asrt_nm == data.asrt_nm) {
                            $scope.masterVehicleMarkers[j].lat = data.lat;
                            $scope.masterVehicleMarkers[j].lng = data.lng;
                        }
                    }

                    for (j = 0; j < $scope.totalOfflineVehicleMarkers.length; j++) {
                        if ($scope.totalOfflineVehicleMarkers[j].asrt_nm == data.asrt_nm) {
                            $scope.totalOfflineVehicleMarkers[j].lat = data.lat;
                            $scope.totalOfflineVehicleMarkers[j].lng = data.lng;
                        }
                    }
                }
            });
        };


 $scope.checkAllVehicles = function(selectedValue) {
            for (var i = 0; i < $scope.vehiclesList.length; i++) {
                $scope.vehiclesList[i].checked = selectedValue;
            }
            if (selectedValue) {
                $scope.markers = $scope.vehiclesList;
            } else {
                $scope.markers = [];
            }
        };

        $scope.vehicleSelected = function (args) {

            if (args.checked) {
                console.log(args.checked);
                var isVehicleExists = false;
                for (var i = 0; i < $scope.markers.length; i++) {
                    if ($scope.markers[i].asrt_nm == args.asrt_nm) {
                        console.log($scope.markers[i].asrt_nm == args.asrt_nm);
                        isVehicleExists = true;
                    }
                }
                if (!isVehicleExists) {
                    $scope.markers.push(args);
                       leafletData.getDirectiveControls().then(function (controls) {
                        controls.markers.create({} ,$scope.markers);
                            controls.markers.create($scope.markers,$scope.markers);
                            $scope.markers = $scope.markers;
                        });
                    console.log($scope.markers);
                }
            } else {
                for (var i = 0; i < $scope.markers.length; i++) {
                    if ($scope.markers[i].asrt_nm == args.asrt_nm) {
                        $scope.markers.splice(i, 1);
                        break;
                    }
                }
            }
        };

        $scope.checked = false;

        $scope.close = function() {
            $scope.checked = false;
        };
        //Graph operations functions *******************************
        $scope.buildGraphFormatData = function(inputData, key, url, string) {
            var resBuiltData = [];
            for (var i = 0; i <= inputData.length - 1; i++) {
                resBuiltData.push(inputData[i][key]);
            }
            if (resBuiltData) {
                var goptions = {
                    "chart": {
                        "type": 'discreteBarChart',
                        "height": 200,
                        // "width": 250,
                        "margin": {
                            //   "top": 50,
                            //   "right": 20,
                            "bottom": 75,
                            "left": 40
                        },
                        "showValues": true,
                        "showControls": false,
                        "focusEnable": false,
                        "clipEdge": true,
                        "duration": 500,
                        "stacked": true,
                        "xAxis": {
                            "axisLabel": "Date",
                            "rotateLabels": 45,
                            "showMaxMin": false,
                            tickFormat: function(d) {
                                return resBuiltData[d];
                            }
                        },
                        "yAxis": {
                            "axisLabel": string,
                            "axisLabelDistance": -20,
                            tickFormat: function(d) {
                                return d3.format(',0f')(d);
                            }
                        },
                        "legend": {
                            "width": 600,
                            "height": 50,
                            "align": true,
                            "maxKeyLength": 20,
                            "rightAlign": false,
                            "padding": 100,
                        },
                        callback: function(chart) {
                            chart.multibar.dispatch.on('elementClick', function(e) {
                                $window.location.href = url;
                            });
                        }
                    }
                };
                return goptions;
            }
        };

        //Dist Count
        function generatedata2() {
            var tvl_dist = [];
            for (var i = 0; i <= $scope.dstnsCvrd.length - 1; i++) {
                tvl_dist.push({
                    x: i,
                    y: $scope.dstnsCvrd[i].tvl_dist
                });
            }
            var dataArray = [{
                values: tvl_dist, //values - represents the array of {x,y} data points
                key: "Distance" //key  - the name of the series.
            }];
            return dataArray;
        }

        //Landmarks count
        function generatedata4() {
            var fnce_covered = [],
                fnce_uncovered = [];
            for (var i = 0; i <= $scope.lndmrkcovchrtlst.length - 1; i++) {
                fnce_covered.push({
                    x: i,
                    y: $scope.lndmrkcovchrtlst[i].fnce_covered
                });
            }
            var dataArray = [{
                values: fnce_covered, //values - represents the array of {x,y} data points
                key: "Covered" //key  - the name of the series.
            }];
            return dataArray;
        }

        // Trips count
        function generatedata3() {
            var tripsCount = [];
            for (var i = 0; i <= $scope.triplst.length - 1; i++) {
                tripsCount.push({
                    x: i,
                    y: $scope.triplst[i].trips_ct
                });
            }
            var dataArray = [{
                values: tripsCount, //values - represents the array of {x,y} data points
                key: "trips_ct" //key  - the name of the series.
            }];
            return dataArray;
        }
        // coverd collection points last 7 days count
        function generatedata1() {
            var lndmrks_ct = [];
            // console.log($scope.lndmarksCvrd);
            for (var i = 0; i <= $scope.lndmarksCvrd.length - 1; i++) {
                lndmrks_ct.push({
                    x: i,
                    y: $scope.lndmarksCvrd[i].lndmrks_ct
                });
            }
            var dataArray = [{
                values: lndmrks_ct, //values - represents the array of {x,y} data points
                key: "lndmrks_ct" //key  - the name of the series.
            }];
            return dataArray;
        }


 $scope.$on('leafletDirectiveMarker.click', function (event, args) {

            var markerObj = args.model;
            $scope.sidenavDtaHdr = "Vehicle Details";
            $scope.title = markerObj.title;
            $scope.asrt_ctgry_nm = markerObj.asrt_ctgry_nm;
            $scope.crw_nm = markerObj.crw_nm;
            $scope.crw_ph = markerObj.crw_ph;
            $scope.asrt_grp_nm = markerObj.asrt_grp_nm;
            $scope.asrt_type_nm = markerObj.asrt_type_nm;

            if (markerObj.speed_ct === 0) {
                $scope.status = "Parked";
            } else {
                $scope.status = "Running";
            }

            $scope.checked = true;

                        // Getting Selected Vehicle Data
            $http.get($rootScope.BaseUrl + 'dywseDstnsCnt/' + markerObj.asrt_id)
                .success(function (resdata, status) {
                    // if (status == 257) $rootScope.sessionCheck();
                    $scope.dstnsCvrd = resdata.data;
                    var string = "Distance Count";
                    $scope.options2 = $scope.buildGraphFormatData($scope.dstnsCvrd, 'trvl_dt', "#/vts/rpt/distance_report", string);
                    $scope.dataset2 = generatedata2();
                }).error(function (data, status, headers, config) {
                    if (status == 500) {
                        $rootScope.sessionCheck();
                    }
                });

            $http.get($rootScope.BaseUrl + 'tripRptLst/' + markerObj.asrt_id)
                .success(function (resdata, status) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.triplst = resdata.data;
                    console.log($scope.triplst);
                    var string = "Trips Count";
                    if($scope.triplst.length == 0){
                         $scope.options3 = {
                            chart: {
                              type: 'discreteBarChart',
                              noData: 'Your custom message',
                              xAxis: {
                                showMaxMin: false,
                              }
                            }
                        }
                    } else {
                        $scope.options3 = $scope.buildGraphFormatData($scope.triplst, 'trp_dt', "#/vts/rpt/trips_detail_report", string);
                        $scope.dataset3 = generatedata3();
                    }
                }).error(function (data, status, headers, config) {
                    // console.log(data);
                });


            $http.get($rootScope.BaseUrl + 'lndmrkCovChrtLst/' + markerObj.asrt_id)
                .success(function (resdata, status) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.lndmrkcovchrtlst = resdata.data;
                    var string = "landmarks Count";
                    $scope.options4 = $scope.buildGraphFormatData($scope.lndmrkcovchrtlst, 'fdate', "#/vts/rpt/landmark_inout_report", string);
                    $scope.dataset4 = generatedata4();
                }).error(function (data, status, headers, config) {
                    if (status == 500) {
                        $rootScope.sessionCheck();
                    }
                });
        });


var getAsrtList = function() {
            $http.get($rootScope.BaseUrl + "/getAsrtType").success(function(resdata, status, headers, config) {
                if (status == 200) {
                    $scope.assrtTypeList = resdata.data;
                } else {
                    // console.log("Data = " + resdata);
                }
            }).error(function(data, status, headers, config) {
                $scope.message = "Unable retrive data from Server.";
            });
        };


 // Call set markers function by default load of controller
        if (!$scope.ltmap) {
            $scope.initialise();
            getAsrtList();
        }

        //refresh marker position on map changed in every 35 sec..
        setInterval(function() {
            if (!$rootScope.isRefresh) return;
            refreshVehicleMarkers();
        }, 60000);

        // Sorting vehicle markers on selection of category type
        $scope.getAssertList = function() {
            console.log($scope.asrt_type_id + ',' + $scope.asrt_grp_id + ',' + $scope.asrt_ctgry_id);
            if (($scope.asrt_ctgry_id == 0 || $scope.asrt_ctgry_id == null) &&
                ($scope.asrt_type_id == 0 || $scope.asrt_type_id == null) &&
                ($scope.asrt_grp_id == 0 || $scope.asrt_grp_id == null)) {
                $scope.vehiclesList = [];
                $scope.markers = [];
                for (var i = 0; i < $scope.masterVehicleMarkers.length; i++) {
                    $scope.vehiclesList.push($scope.masterVehicleMarkers[i]);
                    $scope.markers.push($scope.masterVehicleMarkers[i]);
                    $scope.vehiclesList[i].checked = true;
                }
                return;
            } else {

                $scope.vehiclesList = [];
                $scope.markers = [];

                for (var i = 0; i < $scope.masterVehicleMarkers.length; i++) {
                    if ($scope.masterVehicleMarkers[i].asrt_type_id == $scope.asrt_type_id &&
                        ($scope.asrt_grp_id == undefined || $scope.asrt_grp_id == null || $scope.asrt_grp_id == 0) &&
                        ($scope.asrt_ctgry_id == undefined || $scope.asrt_ctgry_id == null || $scope.asrt_ctgry_id == 0)) {

                        $scope.vehiclesList.push($scope.masterVehicleMarkers[i]);
                        $scope.markers.push($scope.masterVehicleMarkers[i]);
                        console.log("1");
                    } else if ($scope.masterVehicleMarkers[i].asrt_type_id == $scope.asrt_type_id &&
                        $scope.masterVehicleMarkers[i].asrt_grp_id == $scope.asrt_grp_id &&
                        ($scope.asrt_ctgry_id == undefined || $scope.asrt_ctgry_id == null || $scope.asrt_ctgry_id == 0)) {

                        $scope.vehiclesList.push($scope.masterVehicleMarkers[i]);
                        $scope.markers.push($scope.masterVehicleMarkers[i]);
                        console.log("2");
                    } else if ($scope.masterVehicleMarkers[i].asrt_ctgry_id == $scope.asrt_ctgry_id &&
                        $scope.masterVehicleMarkers[i].asrt_type_id == $scope.asrt_type_id &&
                        $scope.masterVehicleMarkers[i].asrt_grp_id == $scope.asrt_grp_id) {

                        $scope.vehiclesList.push($scope.masterVehicleMarkers[i]);
                        $scope.markers.push($scope.masterVehicleMarkers[i]);

                        leafletData.getDirectiveControls().then(function (controls) {
                        controls.markers.create({} ,$scope.markers);
                            controls.markers.create($scope.markers,$scope.markers);
                            $scope.markers = $scope.markers;
                        });
                        console.log("3");
                       
                    } else if ($scope.masterVehicleMarkers[i].asrt_ctgry_id == $scope.asrt_ctgry_id &&
                        $scope.masterVehicleMarkers[i].asrt_type_id == $scope.asrt_type_id &&
                        ($scope.asrt_grp_id == 0 || $scope.asrt_grp_id == undefined || $scope.asrt_grp_id == null)) {

                        $scope.vehiclesList.push($scope.masterVehicleMarkers[i]);
                        $scope.markers.push($scope.masterVehicleMarkers[i]);
                        console.log("4");
                    }
                }
                for (i = 0; i < $scope.vehiclesList.length; i++) {
                    $scope.vehiclesList[i].checked = true;
                }
            }
        };

        $scope.getGrpList = function() {
            // console.log("Area Changed");
            $scope.asrtGrpList = [];
            $scope.asrt_ctgry_id = 0;
            $scope.asrt_grp_id = 0;

            for (var j = 0; j < $scope.masterVehicleMarkers.length; j++) {
                var isExistGroupId = true;
                if ($scope.masterVehicleMarkers[j].asrt_type_id == $scope.asrt_type_id) {

                    for (var i = 0; i < $scope.asrtGrpList.length; i++) {
                        if ($scope.asrtGrpList[i].asrt_grp_id == $scope.masterVehicleMarkers[j].asrt_grp_id) {
                            isExistGroupId = false;
                        }
                    }
                    if (isExistGroupId) {
                        $scope.asrtGrpList.push({
                            asrt_grp_id: $scope.masterVehicleMarkers[j].asrt_grp_id,
                            asrt_grp_nm: $scope.masterVehicleMarkers[j].asrt_grp_nm
                        });

                    }
                } else if ($scope.asrt_ctgry_id == 0 && $scope.asrt_grp_id == 0 && $scope.asrt_type_id == 0) {
                    // TODO: If required
                    console.log("Not Handled for populating groups when area is ALL");

                }
            }
            $scope.getCatList();
            $scope.getAssertList();

        };

        $scope.getCatList = function() {
            console.log($scope.asrt_grp_id);
            $scope.asrtCtgryList = [];

            for (var j = 0; j < $scope.masterVehicleMarkers.length; j++) {
                var isExistGroupId = true;
                for (var i = 0; i < $scope.asrtCtgryList.length; i++) {
                    if ($scope.asrtCtgryList[i].asrt_ctgry_id == $scope.masterVehicleMarkers[j].asrt_ctgry_id) {
                        isExistGroupId = false;
                    }
                }

                if ($scope.masterVehicleMarkers[j].asrt_grp_id == $scope.asrt_grp_id) {
                    if (isExistGroupId) {
                        $scope.asrtCtgryList.push({
                            asrt_ctgry_id: $scope.masterVehicleMarkers[j].asrt_ctgry_id,
                            asrt_ctgry_nm: $scope.masterVehicleMarkers[j].asrt_ctgry_nm
                        });
                    }

                } else if ($scope.asrt_ctgry_id == 0 && $scope.asrt_grp_id == 0 && $scope.asrt_type_id == 0) {
                    if (isExistGroupId) {
                        $scope.asrtCtgryList.push({
                            asrt_ctgry_id: $scope.masterVehicleMarkers[j].asrt_ctgry_id,
                            asrt_ctgry_nm: $scope.masterVehicleMarkers[j].asrt_ctgry_nm
                        });
                    }
                } else if ($scope.asrt_grp_id == 0 || $scope.asrt_grp_id == null) {
                    if (isExistGroupId) {
                        if ($scope.masterVehicleMarkers[j].asrt_type_id == $scope.asrt_type_id) {
                            $scope.asrtCtgryList.push({
                                asrt_ctgry_id: $scope.masterVehicleMarkers[j].asrt_ctgry_id,
                                asrt_ctgry_nm: $scope.masterVehicleMarkers[j].asrt_ctgry_nm
                            });
                        }
                    }
                }
            }
            $scope.getAssertList();
        };

        $scope.showOfflineVehicles = function() {
            $scope.offlineVehicles = [];
            if ($scope.offlineVehicles) {
                $scope.showOffline = $scope.totalOfflineVehicleMarkers;
            }
            for (var i = 0; i < $scope.totalOfflineVehicleMarkers.length; i++) {
                if($scope.totalOfflineVehicleMarkers[i].onl_offl == 0 && $scope.totalOfflineVehicleMarkers[i].ast_sts_id ==1 && $scope.totalOfflineVehicleMarkers[i].ast_sts_id !==6 && $scope.totalOfflineVehicleMarkers[i].ast_sts_id !==3){
                // if ($scope.totalOfflineVehicleMarkers[i].onl_offl == 0 && $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id !==6 && $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id !==5 && $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id !==4 && $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id !==3 && $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id !==2) {
                    // && $scope.totalOfflineVehicleMarkers[i].ast_sts_id !== 1 && $scope.totalOfflineVehicleMarkers[i].ast_sts_id !== 3
                        // if ($scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id == 2) {
                    $scope.offlineVehicles.push({
                        asrt_nm: $scope.totalOfflineVehicleMarkers[i].asrt_nm,
                        asrt_type_nm: $scope.totalOfflineVehicleMarkers[i].asrt_type_nm,
                        asrt_grp_nm: $scope.totalOfflineVehicleMarkers[i].asrt_grp_nm,
                        asrt_ctgry_nm: $scope.totalOfflineVehicleMarkers[i].asrt_ctgry_nm,
                        icon_path: $scope.totalOfflineVehicleMarkers[i].icon_path,
                        onl_offl: $scope.totalOfflineVehicleMarkers[i].onl_offl,
                        ast_sts_id: $scope.totalOfflineVehicleMarkers[i].ast_sts_id,
                        crnt_Ast_sts_id: $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id,
                        asrt_sts_nm: $scope.totalOfflineVehicleMarkers[i].asrt_sts_nm
                    });
                    console.log($scope.offlineVehicles);
                }
            }
        };

        $scope.showRepairVehicles = function() {
            $scope.repairVehicles = [];
            for (i = 0; i < $scope.totalOfflineVehicleMarkers.length; i++) {
                if ($scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id == 3) {
                    $scope.repairVehicles.push({
                        asrt_nm: $scope.totalOfflineVehicleMarkers[i].asrt_nm,
                        asrt_type_nm: $scope.totalOfflineVehicleMarkers[i].asrt_type_nm,
                        asrt_grp_nm: $scope.totalOfflineVehicleMarkers[i].asrt_grp_nm,
                        asrt_ctgry_nm: $scope.totalOfflineVehicleMarkers[i].asrt_ctgry_nm,
                        icon_path: $scope.totalOfflineVehicleMarkers[i].icon_path,
                        ast_sts_id: $scope.totalOfflineVehicleMarkers[i].ast_sts_id,
                        crnt_Ast_sts_id: $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id,
                        asrt_sts_nm: $scope.totalOfflineVehicleMarkers[i].asrt_sts_nm
                    });
                }
            }
        };

        $scope.showContractExpired = function() {
            $scope.contractExpired = [];
            for (i = 0; i < $scope.totalOfflineVehicleMarkers.length; i++) {
                if ($scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id == 6) {
                    $scope.contractExpired.push({
                        asrt_nm: $scope.totalOfflineVehicleMarkers[i].asrt_nm,
                        asrt_type_nm: $scope.totalOfflineVehicleMarkers[i].asrt_type_nm,
                        asrt_grp_nm: $scope.totalOfflineVehicleMarkers[i].asrt_grp_nm,
                        asrt_ctgry_nm: $scope.totalOfflineVehicleMarkers[i].asrt_ctgry_nm,
                        icon_path: $scope.totalOfflineVehicleMarkers[i].icon_path,
                        ast_sts_id: $scope.totalOfflineVehicleMarkers[i].ast_sts_id,
                        crnt_Ast_sts_id: $scope.totalOfflineVehicleMarkers[i].crnt_Ast_sts_id,
                        asrt_sts_nm: $scope.totalOfflineVehicleMarkers[i].asrt_sts_nm
                    });
                }
            }
        };

        $scope.create = function(url, asrt_id) {

            $rootScope.curdate = new Date();
            $rootScope.sdate = moment($rootScope.curdate).format('YYYY-MM-DD 00:00:00');

            $rootScope.edate = moment($rootScope.curdate).format('YYYY-MM-DD 23:59:59');
            $rootScope.asrt_id = url.asrt_id;

            window.location.href = '/ds#/liveTracking/playback';
        };
    })

    .controller('playbackCtrl', function($scope, $rootScope, $http, $timeout, growlService, $window) {
        console.log("Playback Controller");
        var basePath = 'http://localhost:4801/';
        var iconBase = basePath + 'client/src/public/static/img/markers/';
        var init_lat = "17.031032";
        var init_lng = "81.78976";
        var timerId, index, marker, polygon, line, polyline, point;
        var marker = null;
        var markerSpeed = 150;
        var pauseindex = 0;
        var points = [];
        var endPOBMarker;

        $rootScope.filterOnStateChange('playback');
        $scope.$parent.isActiveTab = false;
        $rootScope.isRefresh = false;
        $scope.isTripCompleted = false;
        $scope.isDisabled = false;
        $scope.play_pause = "PAUSE";
        $scope.showTimeline = true;

        this.openCalendar = function (e, date) {
            that.open[date] = true;
        };
        $scope.openfrom = function ($event, openedf) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[openedf] = true;
        };

        $scope.openTo = function ($event, openedt) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[openedt] = true;
        };


        var vehicleMarker = new L.marker({
            icon: iconBase + 'van_online.png',
        });

        var startPBMarker = new L.marker({
            icon: iconBase + 'marker-YELLOW-START.png'
        });

        var endPBMarker = new L.marker({
            iconUrl: iconBase + 'marker-YELLOW-END.png'
        });

        if ($rootScope.asrt_id) {
            $scope.asrt_id = angular.toJson($rootScope.asrt_id);
        }

        $http.get($rootScope.BaseUrl + "/getCurRunnVeh")
            .success(function (resdata, status, headers, config) {
                if (status == 200) {
                    $scope.playBackVehNUm = resdata.data;
                } else {
                    console.log("Data = " + resdata);
                }
            }).
            error(function (data, status, headers, config) {
                $scope.message = "Unable retrive data from Server.";
            });

        //Populatin g Trips dropdown in play back
        $scope.getTrips = function () {
            $http.get($rootScope.BaseUrl + "/getTripsByAsrtId/" + $scope.asrt_id)
                .success(function (resdata, status, headers, config) {
                    if (status == 200) {
                        $scope.getTripsByAssertID = resdata.data;
                        // console.log($scope.getTripsByAssertID);
                    } else {
                        console.log("Data = " + resdata);
                    }
                }).
                error(function (data, status, headers, config) {
                    $scope.message = "Unable retrive data from Server.";
                });
        };

        $scope.isTrpsEnbld = function (dt) {
            var setdt = moment(dt).format('YYYY-MM-DD');
            var curdate = moment(new Date()).format('YYYY-MM-DD');
            if (curdate == setdt)
                $scope.showTrpsDD = false;
            else
                $scope.showTrpsDD = true;
        };

        //ngChange for vehTripDtls
        $scope.getPbVehDtls = function (trip_id) {
            var pbVehDtls;
            for (i = 0; i < $scope.getTripsByAssertID.length; i++) {
                if ($scope.getTripsByAssertID[i].trip_nu == trip_id) {
                    pbVehDtls = $scope.getTripsByAssertID[i];
                }
            }


            $scope.sdate = pbVehDtls.actl_strt;
            //If End Date Is Null It has to show CurDate in End Date
            if (pbVehDtls.actl_end)
                $scope.edate = pbVehDtls.actl_end;
            else
                $scope.edate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
            $scope.asrt_id = pbVehDtls.asrt_id;
            $scope.trp_rn_id = pbVehDtls.trp_rn_id;
        };

        /*Black Holes*/
        $scope.showGarbagePointOnMap = function (fnce_id, lat, lng) {
            console.log(lat, lng);
            console.log(fnce_id);
            $scope.latlng = [];
            var myLatlng;
            myLatlng = new L.latLng(lat, lng);
            dpCircle = new L.circle(myLatlng, 40, {
                fillOpacity: 1.0,
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: 'red',
                fillOpacity: 0.35
            }).addTo($rootScope.playbackmap)
                .bindPopup(myLatlng)
            for (index in myLatlng) {
                myLatlng = new L.latLng(lat, lng);
                $rootScope.playbackmap.setZoom(17);
            }
        };


        $scope.showDumperAreas = function () {
            $scope.DumperAreas = [];
            $scope.finalStr = [];
            var iconFile = '';
            $http.get("../apiv1/getDumpingAreas").success(function (data) {
                $scope.DumperAreas = data.data;
                for (i = 0; i < $scope.DumperAreas.length; i++) {
                    data = $scope.DumperAreas[i];

                    var thisIcon = new L.Icon({
                        iconUrl: iconBase + 'dmprArea.png',
                        iconSize: [30, 41],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var marker = L.marker([$scope.DumperAreas[i].lat, $scope.DumperAreas[i].lng], {
                        draggable: false, // Make the icon dragable
                        title: 'Hover Text', // Add a title
                        opacity: 5,
                        icon: thisIcon
                    } // Adjust the opacity
                    )
                        .addTo($rootScope.playbackmap)
                        .bindPopup("<div style='color: #000;'><b style='line-height: 21px; color: #1a56a2;'>Fence Name:  </b>" + $scope.DumperAreas[i].fnce_nm + "</div>")
                        .closePopup();


                    var x = $scope.DumperAreas[i].fncs_dtl_tx.replace("POLYGON((", "").replace("))", "");
                    // console.log(x);
                    var buildStr = '';
                    var strArr = x.split(',');
                    for (var j = 0; j < strArr.length; j++) {
                        var tmpStr = strArr[j].trim(" ");
                        var cords = tmpStr.split(' ');
                        (buildStr === '') ? buildStr = buildStr + '[' + cords[1] + ',' + cords[0] + ']' : buildStr = buildStr + ',[' + cords[1] + ',' + cords[0] + ']';
                    }
                    $scope.finalStr.push(JSON.parse('[' + buildStr + ']'));
                    // console.log($scope.finalStr);
                }

                var A = $scope.finalStr[0];
                var B = $scope.finalStr[1];
                var C = $scope.finalStr[2];

                // console.log(A, B, C);

                polygonPoints = [A, B, C];
                polygon = L.polygon(polygonPoints, {
                    color: '#0d2c6c',
                    weight: 1,
                    opacity: 0.5,
                    fillOpacity: 0.2
                }).addTo($rootScope.playbackmap);
                // zoom the map to the polygon
                $rootScope.playbackmap.addLayer(polygon);
            });
        };

        // this function is called on playback button click
        $scope.trackVehicle = function () {
            if ($scope.asrt_id == undefined) {
                swal("Please select Vehicle!");
                return;
            }
            $scope.infoWindow = true;
            $scope.isTripCompleted = false;
            $scope.isDisabled = true;
            $scope.fencInOutDtls = [];
            points = [];
            if ($scope.trp_rn_id) {
                $scope.showTimeline = true;
            } else {
                $scope.showTimeline = false;
            }
            if ($rootScope.playbackmap || $rootScope.playbackmap == null) {
                $scope.initialize();
            }
            var startdate = moment($scope.sdate).format('YYYY-MM-DD HH:mm:ss');
            var enddate = moment($scope.edate).format('YYYY-MM-DD HH:mm:ss');
            var data = {
                "checkbit": "byVeh",
                "sdate": startdate,
                "edate": enddate,
                "asrt_id": $scope.asrt_id,
                "trp_rn_id": $scope.trp_rn_id
            };

            // console.log(data);
            // var points = [];
            $http.post($rootScope.BaseUrl + "/playback/playBackByVeh", data)
                .success(function (resdata, status) {
                    var playData = resdata.data;
                    console.log(playData);
                    $scope.asrt_nm = playData.mapPoints["0"].asrt_nm;
                    // console.log($scope.asrt_nm);
                    for (i = 0; i < playData.fncDtls.length; i++) {
                        $scope.fencInOutDtls.push({
                            SNo: playData.fncDtls[i].SNo,
                            fnce_id: playData.fncDtls[i].fnce_id,
                            fnce_nm: playData.fncDtls[i].fnce_nm,
                            fnc_in: playData.fncDtls[i].fnc_in,
                            fnc_ut: playData.fncDtls[i].fnc_ut,
                            asrt_nm: playData.fncDtls[i].asrt_nm,
                            fnce_cvr_sts: playData.fncDtls[i].fnce_cvr_sts,
                            stoppage: playData.fncDtls[i].stoppage,
                            lati: playData.fncDtls[i].lat,
                            longi: playData.fncDtls[i].lng
                        });
                    }

                    if (playData != '' && playData.lat.length > 0) {
                        // To avoid duplicates for start & end points
                        points.push({
                            lat: playData.lat[0],
                            lng: playData.lng[0],
                            speed_ct: playData.mapPoints[0].speed_ct,
                            Signal: playData.mapPoints[0].Signal,
                            asrt_nm: playData.mapPoints[0].asrt_nm
                        });

                        for (var c = 0; c < playData.lat.length; c++) {
                            if (playData.lat[c] != playData.lat[0] && playData.lng[c] != playData.lng[0]) {
                                if (playData.lat[c] != playData.lat[playData.lat.length - 1] && playData.lng[c] != playData.lng[playData.lat.length - 1]) {

                                    points.push({
                                        lat: playData.lat[c],
                                        lng: playData.lng[c],
                                        speed_ct: playData.mapPoints[c].speed_ct,
                                        Signal: playData.mapPoints[c].Signall,
                                        asrt_nm: playData.mapPoints[c].asrt_nm
                                    });
                                }
                            }
                        }
                        points.push({
                            lat: playData.lat[playData.lat.length - 1],
                            lng: playData.lng[playData.lat.length - 1],
                            //speed_ct:data.mapPoints[c].speed_ct
                        });

                        for (var k in points) {
                            console.log(points[k].lat, points[k].lng);
                        }



                        /*Start Fence*/
                        var startIcon = new L.Icon({
                            iconUrl: iconBase + 'marker-YELLOW-START.png',
                            iconSize: [25, 41],
                            iconAnchor: [12, 41],
                            popupAnchor: [1, -34],
                            shadowSize: [41, 41]
                        });

                         startPBMarker = L.marker([points[0].lat, points[0].lng], {
                            icon: startIcon
                        }).addTo($rootScope.playbackmap);


                        var endIcon = new L.Icon({
                            iconUrl: iconBase + 'marker-YELLOW-END.png',
                            iconSize: [25, 41],
                            iconAnchor: [12, 41],
                            popupAnchor: [1, -34],
                            shadowSize: [41, 41]
                        });

                        endPBMarker = L.marker([points[k-1].lat, points[k-1].lng], {
                            icon: endIcon
                        }).addTo($rootScope.playbackmap);



                        // console.log(points[0].lat, points[0].lng);
                       

                        /*Playback*/
                        marker = null;
                        line = L.polyline([], {
                            color: '#F71212'
                        }).addTo($rootScope.playbackmap);
                        index = 0;
                        timerId = update();

                    } else {
                        console.log("No Data Found");
                    }
                })
                .error(function (status, data) {
                    console.log('Not Successfully' + data);
                });
        };

        $scope.routeChecked = function (v) {
            if (v.checked) {
                var isRouteExists = false;
                polyline = L.polyline(points, { color: '#008000' }).addTo($rootScope.playbackmap);
                console.log(polyline._leaflet_id);
            } else if (!v.checked) {
                var isRouteExists = true;
                // var polyline = {};
                $rootScope.playbackmap.removeLayer(polyline);
                console.log("has to clear;@#!@$!");
            }
        }
        function update() {
            // console.log(points);
            return setTimeout(function () {
                pauseindex = index;
                if ($scope.isTripCompleted) {
                    return;
                }
                if (points.length) {
                    redraw(points.shift());
                    timerId = update();
                }
            }, (markerSpeed));
        }

        var timeOutIds = [];
        function redraw(point) {
            var waitTime = 0;
            timeOutIds = [];
            var greenIcon = new L.Icon({
                iconUrl: iconBase + 'van_online.png',
                iconSize: [30, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            });

               // for (var k in point) {
               //              console.log(point[k].speed_ct);
               //              if(points[k].speed_ct == 0){
               //                  console.log(points[k].speed_ct);
               //              }
               //          }

            if(point.speed_ct == 0){
                console.log(point.speed_ct);
                marker = L.marker(point, {
                    icon: greenIcon
                }).addTo($rootScope.playbackmap);
            }


            if (!marker) {
                marker = L.marker(point, {
                    icon: greenIcon
                }).addTo($rootScope.playbackmap)
            }

            line.addLatLng(point);
            marker.setLatLng(point);
            index++;
            // var bounds = line.getBounds();
            // console.log(bounds);
            $rootScope.playbackmap.panTo(point, animate = false);
            // $rootScope.playbackmap.fitBounds(bounds);

            /*End Fence*/
            try {
                if (!_.isEmpty(points)) {
                    // console.log(points);
                    $scope.pbMarkerEndPosition = {
                        lat: points[points.length - 1].lat,
                        lng: points[points.length - 1].lng
                    };
                    // console.log($scope.pbMarkerEndPosition);
                    endPBMarker.setLatLng($scope.pbMarkerEndPosition);

                    var stoppageCounter = 0;
                    var stoppageStartTimeIndex = 0;
                    var stoppageTime = 0;
                    var plength = points.length;

                    // console.log(plength);

                    for(var k=0;k<points.length;k++){
                        waitTime = 500 * k;
                        // console.log(waitTime);
                        var startTime;
                        var endTime;
                        if(points[k].speed_ct == 0){

                            // console.log(points[k].speed_ct == 0);
                            if(points[k].lat == points[k+1].lat && points[k].lng == points[k+1].lng) {
                                // console.log(points[k].lat == points[k+1].lat && points[k].lng == points[k+1].lng);
                                if(stoppageStartTimeIndex == 0) {
                                    stoppageStartTimeIndex = k;
                                    stoppageCounter = k;
                                } else {
                                    stoppageCounter++;
                                }
                            } else {
                                startTime = moment(points[stoppageStartTimeIndex].Signal, "dd/mm/yyyy hh:mm");
                                endTime = moment(points[stoppageStartTimeIndex].Signal, "dd/mm/yyyy hh:mm");

                                stoppageTime = (endTime - startTime) / 60000;
                                
                                // console.log(endTime+','+startTime);

                                points[stoppageStartTimeIndex]["stoppageTime"] = stoppageTime;
                                stoppageStartTimeIndex = 0;
                                stoppageCounter = 0;
                            }
                        }
                        timeOutIds.push(setTimeout(moveMarker(k), 500 * k));
                    }
               }
            } catch (e) {
                console.log(e);
            }


                    // console.log("timeOutIds length :" + timeOutIds.length);


            ///////////////////////  console.log(index, points.length);
            checkTripCompleted(point.lat, point.lng);
        }

        var moveMarker = function(k) {
            // console.log(k);
             
             if (points[k].speed_ct) {
                    $scope.trackvehspeed = points[k].speed_ct;
                }
                if (points[k].signal) {
                    $scope.trackveh_ts = points[k].signal;
                }
                $scope.trackvehlat = points[k].lat;
                $scope.trackvehlng = points[k].lng;
                $scope.asrt_nm = points[k].asrt_nm;
                // console.log($scope.trackvehspeed +","+ $scope.trackvehlat +","+ $scope.trackvehlng +","+ $scope.asrt_nm +","+ $scope.trackveh_ts);
                 if (points[k].stoppageTime != undefined && points[k].stoppageTime != 0) {
                    // console.log(points[k].stoppageTime != undefined && points[k].stoppageTime != 0);
                 }

        }



        var checkTripCompleted = function (lat, lon) {
            // console.log(lat, lon);
            try {
                if (lat == $scope.pbMarkerEndPosition.lat && lon == $scope.pbMarkerEndPosition.lng) {
                    // console.log(lat == $scope.pbMarkerEndPosition.lat && lon == $scope.pbMarkerEndPosition.lng);

                    $timeout(function () {
                        swal("TRIP COMPLETED");
                        $scope.isTripCompleted = true;
                        $scope.isDisabled = false;
                    }, 100);
                }
            } catch (e) {
                console.log(e);
            }
        }

        $scope.pausePlayBack = function () {
            if ($scope.play_pause == "PAUSE") {
                $scope.play_pause = "PLAY";
                if (timerId) {
                    clearTimeout(timerId);
                }
            } else if ($scope.play_pause == "PLAY") {
                $scope.play_pause = "PAUSE";
                timerId = update();
            }
        }

        $scope.clearMarkersonMap = function () {
            console.log("clearMarkersonMap");
            $scope.isTripCompleted = true;
            $scope.isDisabled = false;
            points = [];
            clearTimeout(timerId);
            $rootScope.playbackmap.removeLayer(marker);
            $rootScope.playbackmap.removeLayer(line);
            $rootScope.playbackmap.removeLayer(startPBMarker);
            $rootScope.playbackmap.removeLayer(endPBMarker);
        }

        try {
           var AGL;
            var googleLayer = 'https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i349018013!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0',
                attribution='',
                AGL = L.tileLayer(googleLayer, { maxZoom: 18, attribution: attribution });
            $rootScope.playbackmap = new L.Map('playbackmap', { drawControl: true, layers: [AGL], center: new L.LatLng(17.024676, 81.788438), zoom: 13 });

            // $rootScope.playbackmap = L.map('playbackmap', { drawControl: true }).setView([17.024676, 81.788438], 13);
            // var googleLayer = new L.tileLayer('https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i349018013!3m9!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0', {
            //     attribution: '',
            // }).addTo($rootScope.playbackmap);


         
         var drawnItems = new L.FeatureGroup({});

var coords = [new L.latLng(51.2, 4.5), new L.latLng(51.2, 4.6), new L.latLng(51.2, 4.9)];
var poly = new L.Polyline(coords, {
color: 'blue',
opacity: 1,
weight: 5
});

drawnItems.addLayer(poly);   
$rootScope.playbackmap.addLayer(drawnItems);


var drawControl =  L.Control.Draw({
    draw: {
        position: 'right',
        polygon: {
            title: 'Polygon',
            allowIntersection: false,
            drawError: {
                color: '#b00b00',
                timeout: 1000
            },
            shapeOptions: {
                color: '#bada55'
            },
            showArea: true
        },
        polyline: {
            metric: false
        },
        circle: {
            shapeOptions: {
                color: '#662d91'
            }
        }
    },
    edit: {
        featureGroup: drawnItems
    }
});

$rootScope.playbackmap.addControl(drawControl);

$rootScope.playbackmap.on('draw:created', function (e) {
    var type = e.layerType,
        layer = e.layer;

    if (type === 'marker') {
        layer.bindPopup('A popup!');
    }

    drawnItems.addLayer(layer);
    console.log('adding layer', layer, drawnItems);
});

            //     var drawnItems = new L.FeatureGroup({
            //             draw: {
            //                 position: 'topleft',
            //                 polygon: {
            //                     title: 'Draw a sexy polygon!',
            //                     allowIntersection: false,
            //                     drawError: {
            //                         color: '#b00b00',
            //                         timeout: 1000
            //                     },
            //                     shapeOptions: {
            //                         color: '#bada55'
            //                     },
            //                     showArea: true
            //                 },
            //                 polyline: {
            //                     metric: false
            //                 },
            //                 circle: {
            //                     shapeOptions: {
            //                         color: '#662d91'
            //                     }
            //                 }
            //             },
            //         edit: {
            //         featureGroup: drawnItems
            //         }
            //     });
            //     $rootScope.playbackmap.addLayer(drawnItems);

            // $rootScope.playbackmap.on('draw:drawstop', function (e) {
            //     var type = e.layerType,
            //         layer = e.layer;
            //     console.log(e);        
            // });

            // $rootScope.playbackmap.on('draw:created', function (e) {
            //     var type = e.layerType,
            //         layer = e.layer;

            //     if (type === 'marker') {
            //         layer.bindPopup('A popup!');
            //     }

            //     drawnItems.addLayer(layer);
            // });


            // // var drawControl = new L.Control.Draw({
            // //     draw: {
            // //         position: 'topleft',
            // //     },
            // //     edit: {
            // //         featureGroup: drawnItems
            // //     }
            // // });
            // // $rootScope.playbackmap.addControl(drawControl);

            //     // var drawControl = new L.Control.Draw({
            //     //     draw: {},
            //     //     edit: {
            //     //         featureGroup: drawnItems
            //     //     }
            //     // });
            //     // $rootScope.playbackmap.addControl(drawControl);

            //     // $rootScope.playbackmap.on('draw:created', function (e) {
            //     //     var type = e.layerType,
            //     //         layer = e.layer;
            //     //     drawnItems.addLayer(layer);
            //     // });

        } catch (e) {
            console.log(e);
        }

        $scope.initialize = function () {
            $scope.showDumperAreas();
            $scope.show = true;
        }
        $scope.initialize();

        if ($rootScope.asrt_id) {
            $scope.trackVehicle($rootScope.sdate, $rootScope.edate, $rootScope.asrt_id);
        }
    })