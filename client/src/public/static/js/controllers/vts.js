wertrackon
    // =========================================================================
    // VTS(Vehicle Tracking System) related controllers
    // =========================================================================
    
    .controller('vtsHomeCtrl', function(recentpostService,$scope){
        
        $scope.map = { zoom:10
                    ,center:{ latitude:17,longitude:81}
                   }
    })

    .controller('sidebarVTSCtrl',function($state,$http,sideBarMenuService,localStorageService ,$rootScope,checkPermService,$scope){
        var app_id = 3;
        $http.get('../../apiv1/user/menuLst/'+$rootScope.userCmpltDtls.usr_id+'/'+app_id)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            // console.log(resdata.data)
            $scope.appMenu(resdata.data)
        })
        .error(function(data, status, headers, config) {
            console.log(data);            
        });

        $scope.appMenu = function(responseData){
            $scope.menuitem = sideBarMenuService(responseData)
        }

        $rootScope.canRead = function(menuNm){
            return checkPermService.canRead(menuNm,$scope.menuitem);
        }
        $rootScope.canCreate = function(menuNm){
            return checkPermService.canCreate(menuNm,$scope.menuitem);
        }
        $rootScope.canUpdate = function(menuNm){
            return checkPermService.canUpdate(menuNm,$scope.menuitem);
        }
        $rootScope.canDelete = function(menuNm){
            return checkPermService.canDelete(menuNm,$scope.menuitem);
        }
        $rootScope.canEdit = function(menuNm){
            return checkPermService.canEdit(menuNm,$scope.menuitem);
        }
    })
    // =========================================================================
    // Vehicles controllers
    // =========================================================================
    .controller('geoFenceCtgryCtrl', function(recentpostService,$scope){



    })
    // =========================================================================
    // Devices controllers
    // =========================================================================
    .controller('geoFenceGrpCtrl', function(recentpostService,$scope){
        


    })
    // =========================================================================
    // Trips controllers
    // =========================================================================
    .controller('landmarkCtrl', function(recentpostService,$scope){
        
        $scope.map = { zoom:10
                       ,center:{ latitude:17,longitude:81}
                   }
    })

    // =========================================================================
    // Fuel Operations
    // =========================================================================
    .controller('landmarkCtgryCtrl', function(recentpostService,$scope){
        


    })
    // =========================================================================
    // Landmark Category Group controllers
    // =========================================================================
    .controller('landmarkGrpCtrl', function(recentpostService,$scope){



    })

.controller('vehDtlsCtrl', function(recentpostService,$scope,$http,$rootScope,uiGridConstants,$filter,NgTableParams){
    $scope.showTbl = false;
    $scope.showSrvLst = false;
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };

    $http.get($rootScope.BaseUrl + 'totlVehCnt')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.totlVehCntlst = resdata.data[0];
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

    $http.get($rootScope.BaseUrl + 'vehCntErly')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.vehCntErlylst = resdata.data[0];
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });
    
    $http.get($rootScope.BaseUrl + 'vehCntMidl')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.vehCntMidllst = resdata.data[0];
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

    $http.get($rootScope.BaseUrl + 'vehCntDly')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.vehCntDlylst = resdata.data[0];
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

$scope.vehDtlscolumns = [
                     {
                      name:'Date', field: 'dt',  headerCellClass: 'mis-reports-grid-header',
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Date</div>',
                      footerCellTemplate:'<div class="ui-grid-cell-contents">TOTAL</div>'},
                    {
                      name:'Vehicle Number', field: 'asrt_nm',  headerCellClass: 'mis-reports-grid-header',
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Vehicle Number</div>'},
                    {
                      name:'Vehicle Type', field: 'asrt_ctgry_nm',  headerCellClass: 'mis-reports-grid-header',
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Vehicle Type</div>'},  
                    {
                      name:'Number of Trips', field: 'trp_cnt',  headerCellClass: 'mis-reports-grid-header',
                      aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true,
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Number of Trips</div>',
                      footerCellClass: 'mis-report-cell-align',
                      footerCellTemplate:'<div class="ui-grid-region-footer-link">{{ col.getAggregationValue() }}</div>',
                      cellTemplate:'<div ng-click="grid.appScope.getTripDtlList(row.entity.asrt_id)" class="ui-grid-data-link"> {{COL_FIELD}}</div>'},
                    {
                      name:'Distance Travelled', field: 'trp_dst',  headerCellClass: 'mis-reports-grid-header',
                      aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true,
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Distance Travelled</div>',
                      footerCellClass: 'mis-report-cell-align',
                      footerCellTemplate:'<div class="ui-grid-region-footer-link">{{ col.getAggregationValue() }}</div>'},
                    {
                      name:'Fences Covered', field: 'fnc_cvd',  headerCellClass: 'mis-reports-grid-header',
                      aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true,
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Fences Covered</div>'},
                    {
                      name:'Trash Dumped', field: 'cpty_cnt',  headerCellClass: 'mis-reports-grid-header',
                      aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true,
                      cellClass:'mis-report-cell-align',
                      headerCellTemplate: '<div class="ui-grid-cell-contents mis-reports-grid-header">Trash Dumped</div>',
                      footerCellClass: 'mis-report-cell-align',
                      footerCellTemplate:'<div class="ui-grid-region-footer-link">{{ col.getAggregationValue() }}</div>'}
                  ];
$scope.vehDtlsGridOptions = {
        enableSorting: false,
        showColumnFooter: true,
        enableHorizontalScrollbar: uiGridConstants.scrollbars.ALWAYS,
        enableColumnResizing: true,

        columnDefs: $scope.vehDtlscolumns
  };
$scope.hide=function(target) {
            document.getElementById(target).style.display = 'none';
        }
$http.get($rootScope.BaseUrl + "/getCurRunnVeh")
        .success(function(resdata, status, headers, config) {
            if (status == 200) {
                $scope.allvehnumslst = resdata.data;
            } else {
                console.log("Data = " + resdata);
            }
        }).
    error(function(data, status, headers, config) {
        $scope.message = "Unable retrive data from Server.";
    });

    $scope.getavehdtl=function(asrtid,dtFrom,dtTo){

        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");
        console.log($scope.dtFrom);
        console.log($scope.dtTo);
        console.log(asrtid);

         var data =
            {
                "fromdt":$scope.dtFrom,
                "todt":$scope.dtTo,
                "vehid":asrtid
            }
            console.log(data);
            $http.post($rootScope.BaseUrl + 'vehdtlRptLst/',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.vehdtllst = resdata.data;
                    $scope.vehDtlsGridOptions.data = resdata.data;
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
 
    $scope.getTripDtlList=function(asrtid){
        $scope.showSrvLst = true;
        console.log(asrtid);

        $http.get($rootScope.BaseUrl + 'vehEchTripDtl/' + asrtid)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchTripDtlLst = resdata.data;
                console.log($scope.vehEchTripDtlLst);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
        }

        $scope.getVehFncCovdLst=function(trp_rn_id){
            $scope.showModal=false;
            $scope.showEchFnceModal = true;
            console.log(trp_rn_id);
            $http.get($rootScope.BaseUrl + 'vehEchTripFncDtl/' + trp_rn_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchTripFncDtlLst = resdata.data;
                console.log($scope.vehEchTripFncDtlLst);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
        } 
})
.controller('vehDashCtrl', function($scope,$http,$rootScope,NgTableParams,$stateParams,growlService){
    $scope.showGraph = false;

        $http.get($rootScope.BaseUrl + 'asrtLst')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.allvehslst = resdata.data;
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

        $scope.astid=$stateParams.asrt_id;

    console.log($stateParams.asrt_id);
$http.get($rootScope.BaseUrl + 'vehEchdyDstTrvld/' + $stateParams.asrt_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchdyDstTrvldDtl = resdata.data;
                console.log($scope.vehEchdyDstTrvldDtl);
                generatedatavehEchdyDst();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

function generatedatavehEchdyDst(){
    var dst=[];
    for(var i=0;i<=$scope.vehEchdyDstTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.vehEchdyDstTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetvehEchdyDst=  dataArray;
    var vehEchdyDst_chart1=[];
for(var i=0;i<=$scope.vehEchdyDstTrvldDtl.length-1;i++){
    vehEchdyDst_chart1.push($scope.vehEchdyDstTrvldDtl[i].trvl_dt)
}
$scope.vehEchdyDstoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return vehEchdyDst_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$http.get($rootScope.BaseUrl + 'vehEchdyTrpsTrvld/' + $stateParams.asrt_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchdyTrpsTrvldDtl = resdata.data;
                generatedatavehEchdyTrps();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

function generatedatavehEchdyTrps(){
    var trps_cnt=[];
    for(var i=0;i<=$scope.vehEchdyTrpsTrvldDtl.length-1; i++){
        trps_cnt.push({x:i,y:$scope.vehEchdyTrpsTrvldDtl[i].trps_cnt});
    }
    var dataArray = [
        {
            values: trps_cnt,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetvehEchdyTrps=  dataArray;
    var vehEchdyTrps_chart1=[];
for(var i=0;i<=$scope.vehEchdyTrpsTrvldDtl.length-1;i++){
    vehEchdyTrps_chart1.push($scope.vehEchdyTrpsTrvldDtl[i].yr_dt)
}
$scope.vehEchdyTrpsoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return vehEchdyTrps_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Trips Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}
    $http.get($rootScope.BaseUrl + 'vehEchdyColPntsCovd/' + $stateParams.asrt_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchdyColPntsCovdDtl = resdata.data;
                generatedatavehEchdyColPntsCovd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

function generatedatavehEchdyColPntsCovd(){
    var fnc_cnt=[];
    for(var i=0;i<=$scope.vehEchdyColPntsCovdDtl.length-1; i++){
        fnc_cnt.push({x:i,y:$scope.vehEchdyColPntsCovdDtl[i].fnc_cnt});
    }
    var dataArray = [
        {
            values: fnc_cnt,      //values - represents the array of {x,y} data points
            key: "Fence Covered", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetvehEchdyColPntsCovd=  dataArray;
    var vehEchdyColPntsCovd_chart1=[];
for(var i=0;i<=$scope.vehEchdyColPntsCovdDtl.length-1;i++){
    vehEchdyColPntsCovd_chart1.push($scope.vehEchdyColPntsCovdDtl[i].dt)
}
$scope.vehEchdyColPntsCovdoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return vehEchdyColPntsCovd_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CollectionPoints Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$http.get($rootScope.BaseUrl + 'vehEchdyDmprBnsCovd/' + $stateParams.asrt_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchdyDmprBnsCovdDtl = resdata.data;
                generatedatavehEchdyDmprBnsCovd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

function generatedatavehEchdyDmprBnsCovd(){
    var fnc_cnt=[];
    for(var i=0;i<=$scope.vehEchdyDmprBnsCovdDtl.length-1; i++){
        fnc_cnt.push({x:i,y:$scope.vehEchdyDmprBnsCovdDtl[i].fnc_cnt});
    }
    var dataArray = [
        {
            values: fnc_cnt,      //values - represents the array of {x,y} data points
            key: "Fence Covered", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetvehEchdyDmprBnsCovd=  dataArray;
    var vehEchdyDmprBnsCovd_chart1=[];
for(var i=0;i<=$scope.vehEchdyDmprBnsCovdDtl.length-1;i++){
    vehEchdyDmprBnsCovd_chart1.push($scope.vehEchdyDmprBnsCovdDtl[i].dt)
}
$scope.vehEchdyDmprBnsCovdoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return vehEchdyDmprBnsCovd_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "DumperBins Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

    $http.get($rootScope.BaseUrl + 'vehEchdyCptyDmpd/' + $stateParams.asrt_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchdyCptyDmpdDtl = resdata.data;
                generatedatavehEchdyCptyDmpd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

function generatedatavehEchdyCptyDmpd(){
    var cpty_cnt=[];
    for(var i=0;i<=$scope.vehEchdyCptyDmpdDtl.length-1; i++){
        cpty_cnt.push({x:i,y:$scope.vehEchdyCptyDmpdDtl[i].cpty_cnt});
    }
    var dataArray = [
        {
            values: cpty_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity Dumped", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetvehEchdyCptyDmpd=  dataArray;
    var vehEchdyCptyDmpd_chart1=[];
for(var i=0;i<=$scope.vehEchdyCptyDmpdDtl.length-1;i++){
    vehEchdyCptyDmpd_chart1.push($scope.vehEchdyCptyDmpdDtl[i].yr_dt)
}
$scope.vehEchdyCptyDmpdoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return vehEchdyCptyDmpd_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Capacity Dumped",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}    
})


.controller("echVehDylsDashCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService,$anchorScroll,$location){

    $scope.searchVehData   = '';
    $scope.displayfltrs = false;
    
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    $scope.vehCtgryid = null;
    $scope.vehCtgryModel = []; 
    $scope.vehCtgrySettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCtgryTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Select Vehicle Category"}


     $http.get($rootScope.BaseUrl + "/getAsrtType")
     .success(function(resdata, status, headers, config) {
              if (status == 257)  $rootScope.sessionCheck();
                  $scope.assrtTypeList = resdata.data;
              
             }).error(function(data, status, headers, config) {
              $scope.message = "Unable retrive data from Server.";
          });

        $scope.getGrpList=function(){
          console.log($scope.asrt_type_id);   
      $http.get($rootScope.BaseUrl + 'asrtGrpLstbyType/' +$scope.asrt_type_id)
     .success(function(resdata, status, headers, config) {
              if (status == 257)  $rootScope.sessionCheck();
                  $scope.asrtgrpList = resdata.data;
                  $scope.vehgrplst=$scope.asrtgrpList;
              
             }).error(function(data, status, headers, config) {
              $scope.message = "Unable retrive data from Server.";
          });
      }
     
    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtDshbrdCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleCtgryLst = resdata.data;
                $scope.vehctgrylst=$scope.vehicleCtgryLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };
    $scope.hide=function(target) {
            document.getElementById(target).style.display = 'none';
        }
   $scope.getAllCropVehDtls = function(vehgrpid,vehctgryid,dtFrom,dtTo){
  
        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

         var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehctgryid = '0';
            for (i = 0; i < $scope.vehCtgryModel.length; i++) {
                vehctgryid = vehctgryid + ',' + $scope.vehCtgryModel[i].asrt_ctgry_id;
            }
            if ($scope.asrt_type_id === undefined) {
              $scope.asrt_type_id = '0';
            };
            
        var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehtypeid":$scope.asrt_type_id,
            "vehgrp":vehgrp,
            "vehctgryid":vehctgryid
        }
        $http.post($rootScope.BaseUrl + 'allCropVehsDtl/',data)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.allCropVehsDtllst = resdata.data;
                $scope.loadfilter();
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        $http.post($rootScope.BaseUrl + 'totalVehDtls/',data)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totalVehDtllst = resdata.data;
                console.log($scope.totalVehDtllst);
                 for (var i = 0; i < $scope.totalVehDtllst.length; i++) {
                if ($scope.totalVehDtllst[i].ful_ct == null || $scope.totalVehDtllst[i].ful_ct == 'null') {
                  $scope.totalVehDtlfuellst=0;
                  console.log($scope.totalVehDtlfuellst);
                  }
                  else{
                  $scope.totalVehDtlfuellst=$scope.totalVehDtllst[i].ful_ct;
                  console.log($scope.totalVehDtlfuellst);
                }
              }
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getAllCropVehDtls();

    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 50, filter: $scope.filter
      }, {
        total: $scope.allCropVehsDtllst.length,
        getData: function($defer, params) {
            $scope.populateSummaryCards();
            var orderedData = params.filter() ? $filter('filter')($scope.allCropVehsDtllst, params.filter().term) : $scope.allCropVehsDtllst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
      // console.log($scope.tableFilter.settings().data);
    };
    
    $scope.count=0;
    
    $scope.shwFltrs=function(){
      $scope.count++;
      if ($scope.count % 2 ==1) {
        $scope.displayfltrs = true;
      } else {
        $scope.displayfltrs = false;
      }
    }
  
    // Custom filter
    $scope.populateSummaryCards = function() {
      
        $scope.totalTrips = 0;
        $scope.totalActTrips = 0;
        $scope.totalDistance = 0;
        $scope.totalTrashDumped = 0;
        
        for (var i=0;i< $scope.allCropVehsDtllst.length;i++) {
          if ($scope.allCropVehsDtllst[i].asrt_nm.indexOf($scope.filter.term) != -1) {
            if ($scope.allCropVehsDtllst[i].trips_ct) {
              $scope.totalActTrips = $scope.totalActTrips + $scope.allCropVehsDtllst[i].trips_ct;  
            }
            if ($scope.allCropVehsDtllst[i].trp_cnt) {
              $scope.totalTrips = $scope.totalTrips + $scope.allCropVehsDtllst[i].trp_cnt;
            }
            if ($scope.allCropVehsDtllst[i].trp_dst) {
              $scope.totalDistance = $scope.totalDistance  + $scope.allCropVehsDtllst[i].trp_dst;
            }
            if ($scope.allCropVehsDtllst[i].trp_dst) {
              $scope.totalTrashDumped = $scope.totalTrashDumped  + $scope.allCropVehsDtllst[i].cpty_cnt;
            }
          }
        }
    } 

    $scope.pdfDownload = function() {
       
         var tbldata = [];
        
          if ($scope.filter.term != '' || $scope.filter.term != 'undefined' || $scope.filter.term != undefined) {        
              for (var i=0; i < $scope.allCropVehsDtllst.length; i++) {  
                  if ($scope.allCropVehsDtllst[i].asrt_nm.indexOf($scope.filter.term) != -1) {
                      tbldata.push($scope.allCropVehsDtllst[i]);  
                  }
              }
          } else {
              tbldata = $scope.allCropVehsDtllst;
          }
  
          var jsonColumns = ['dt','asrt_nm','asrt_ctgry_nm','crw_nm','crw_ph','trp_cnt','trips_ct','fnc_cvd','trp_dst','cpty_cnt'];
          var colHeadings = ['Date','Vehicle Number','Vehicle Type','Driver Name','Driver Mobile Number','Total Trips','Actual Trips To Be Covered','Total Fences Covered','Distance Travelled','Trash Dumped'];  
          var rptHeading = 'Vehicle Details REPORT';
          var fileName = 'VEHDTLS-RPT';
          var align= 'landscape';
          apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
              
    }

    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'dt':'','asrt_nm':'', 'asrt_ctgry_nm':'Vehicle Details REPORT', 'crw_nm':'','crw_ph':'','trp_cnt':'', 'trips_ct':'', 'fnc_cvd':'','trp_dst':'', 'cpty_cnt':''});
    $scope.exprintData.push({'dt':'DATE','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE','crw_nm':'DISTANCE(IN KM)','crw_ph':'DATE','trp_cnt':'VEHICLE NUMBER','trips_ct':'VEHICLE TYPE','fnc_cvd':'DISTANCE(IN KM)','trp_dst':'DATE','cpty_cnt':'VEHICLE NUMBER'});
    for(var i=0;i<$scope.allCropVehsDtllst.length;i++){
            $scope.exprintData.push({'dt':$scope.allCropVehsDtllst[i].dt,'asrt_nm':$scope.allCropVehsDtllst[i].asrt_nm,'asrt_ctgry_nm':$scope.allCropVehsDtllst[i].asrt_ctgry_nm,'crw_nm':$scope.allCropVehsDtllst[i].crw_nm,'crw_ph':$scope.allCropVehsDtllst[i].crw_ph,'trp_cnt':$scope.allCropVehsDtllst[i].trp_cnt,'trips_ct':$scope.allCropVehsDtllst[i].trips_ct,'fnc_cvd':$scope.allCropVehsDtllst[i].fnc_cvd,'trp_dst':$scope.allCropVehsDtllst[i].trp_dst,'cpty_cnt':$scope.allCropVehsDtllst[i].cpty_cnt});
          }
    return $scope.exprintData;
  }
  $scope.getEchVehTrpDtls=function(dt,asrt_id){
        // document.getElementById(target).style.display = 'block';
        // $scope.showSrvLst = true;
        $scope.showModal = true;
        $http.get($rootScope.BaseUrl + 'vehEchTripDtl/' + asrt_id +'/'+dt)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchTripDtlLst = resdata.data;
                console.log($scope.vehEchTripDtlLst);
                $scope.loadVehTrpsfilter($scope.vehEchTripDtlLst);

            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
        }
    $scope.loadVehTrpsfilter = function(data){
      $location.hash('bottom');
      $anchorScroll();
        $scope.tableVehTrpsFilter = new NgTableParams({
        page: 1, count: 50
        }, {
        data: data
        });
    };

    $scope.getVehFncCovdLst=function(trp_rn_id,targetlst){
            $scope.showModal=false;
            $scope.showEchFnceModal = true;
            console.log(trp_rn_id);
            $http.get($rootScope.BaseUrl + 'vehEchTripFncDtl/' + trp_rn_id)
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehEchTripFncDtlLst = resdata.data;
                console.log($scope.vehEchTripFncDtlLst);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
        } 
})

.controller('collectionPntsDashCtrl', function($scope,$http,$rootScope,growlService){

    $http.get($rootScope.BaseUrl + 'totlndmrksCnt')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.totLndMrksDtl = resdata.data[0];
            console.log($scope.totLndMrksDtl);
        })
        .error(function(data, status, headers, config) {
            if(status == 500){
                $rootScope.sessionCheck();
            }
        });

    $http.get($rootScope.BaseUrl + 'totLndMrksCov')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.totLndMrksCovDtl = resdata.data[0];
            console.log($scope.totLndMrksCovDtl);
        })
        .error(function(data, status, headers, config) {
            if(status == 500){
                $rootScope.sessionCheck();
            }
        });

    $http.get($rootScope.BaseUrl + 'lndMrksNtCov')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.lndMrksNtCovDtl = resdata.data[0];
            console.log($scope.lndMrksNtCovDtl);
        })
        .error(function(data, status, headers, config) {
           if(status == 500){
                $rootScope.sessionCheck();
            }
        });

    // $scope.temp=[];
    $scope.data=[];
    $scope.options = {
        chart: {
                type: 'lineChart',
                height: 450,
                width:790,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 45,
                    left: 70
                }, 
                legendPosition:"right", 
                clipEdge: true,
                duration: 200,
                stacked: true,
                showControls: false,
                xAxis: {
                    axisLabel: $scope.slectedLndMrkIndex,
                    showMaxMin: false,
                    tickFormat: function (d) {
                        return $scope.lndMrk_chart[d];
                    }
                },
                yAxis: {
                    axisLabel: "Time",
                    axisLabelDistance: 10,
                     tickFormat: function (d) {
                  var tempTime = moment.duration(d);
                  var hourformat = (tempTime.get('hours') < 10) ? "0" + tempTime.get('hours') : tempTime.get('hours');
                  var minutesformat = (tempTime.get('minutes') < 10) ? "0" + tempTime.get('minutes') : tempTime.get('minutes');
                  var formatDur = hourformat +":"+ minutesformat;
                  return formatDur;
                }
                },
                scatter: {
                  onlyCircles: false,
                  width:150,
                  height:150
                }
            }
        };

    var getLndMrkYAxisAttributes = function(data,yAxisKeys) {
        var yLndMrkAxisAttr = [];
        var keys = Object.keys(data[0]);
        console.log(keys);
        for (var i=1;i<keys.length;i++) {
            
            for(var j=0;j<yAxisKeys.length;j++){
                if(keys[i]==yAxisKeys[j]){
                    var yObject = {};
                    yObject[keys[i]] = [];
                    yLndMrkAxisAttr.push(yObject);

                }
            }
        }
        console.log(yLndMrkAxisAttr);
        return yLndMrkAxisAttr;   
    }
    
    $scope.refreshLndMrkDataSet = function() {
        if($scope.slectedLndMrkIndex===undefined || $scope.slectedLndMrkIndex=='' || $scope.slectedLndMrkIndex=='undefined'){
            growlService.growl('Data Not Exists For The Selected Vehicle', 'inverse'); 
            return;
        }
        $http.get($rootScope.BaseUrl + 'lndMrkDshChrtLst/'+$scope.slectedLndMrkIndex)
             .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.lndmrkdshchrtlst = resdata.data;
                    console.log($scope.lndmrkdshchrtlst);
                    if($scope.lndmrkdshchrtlst.length != 0){
                       $scope.PopulateLndMrkGraphData();
                    $scope.api.refresh(); // Refresh chart 
                    }
                    else{
                        $scope.lndMrkDshGraphData=[];
                        growlService.growl('Data Not Exists For The Selected Vehicle', 'inverse');
                        $scope.api.refresh();
                    }
                   
                })
                .error(function(data, status, headers, config) {
                    if(status == 500){
                        $rootScope.sessionCheck();
                    }
                });
        }

    $scope.PopulateLndMrkGraphData = function() {
        var temp;
        var dataArray =[];
        $scope.lndMrk_chart=[];
        var yAxisKeys=['fncein_time'];
           var yLndMrkAxisAttr = getLndMrkYAxisAttributes($scope.lndmrkdshchrtlst,yAxisKeys);
            temp = yLndMrkAxisAttr;
            console.log(temp);
           
            for(var i=0;i<=$scope.lndmrkdshchrtlst.length-1;i++){
                $scope.lndMrk_chart.push($scope.lndmrkdshchrtlst[i].fncecov_dt)
            }
        for (var j=0;j<temp.length;j++) {
            var key = Object.keys(temp[j])[0];
            temp[j][key] = [];
            dataArray.push({"key":key,"values":[]});

        }
            for(var i=0;i<=$scope.lndmrkdshchrtlst.length-1; i++) {

                for (var j=0;j<yLndMrkAxisAttr.length;j++) {
                    if (yLndMrkAxisAttr[j].fncein_time) {
                        console.log($scope.lndmrkdshchrtlst[i].fncein_time);
                        yLndMrkAxisAttr[j].fncein_time.push({x:i,y:Date.parse("January 1, 1970, " +$scope.lndmrkdshchrtlst[i].fncein_time+ " UTC")});   
                    } 
                }
            }
              
            for (var k=0;k<dataArray.length;k++) {
                   console.log(dataArray[k].key);
                if (dataArray[k].key == "fncein_time") {
                    dataArray[k].values = yLndMrkAxisAttr[0].fncein_time;
                } 
            }
            $scope.lndMrkDshGraphData=dataArray;
            console.log(JSON.stringify(dataArray));

    }

    $http.get($rootScope.BaseUrl + 'lndMrksLst')
         .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.lndMrksLst = resdata.data;
            $scope.slectedLndMrkIndex=$scope.lndMrksLst[0].fnce_id;
            $scope.refreshLndMrkDataSet();
        })
        .error(function(data, status, headers, config) {
            if(status == 500){
                $rootScope.sessionCheck();
            }
        });
      // $scope.lndMrkDshGraphData=  [{"key":"fncein_time","values":[{"x":0,"y":"01:05"},{"x":1,"y":"05:05"},{"x":2,"y":"06:05"},{"x":3,"y":"10:05"},{"x":4,"y":"12:05"},{"x":5,"y":"05:05"},{"x":6,"y":"12:05"},{"x":7,"y":"09:05"}]}];
})

.controller("lndMrksNtCovRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
    $scope.searchVehData   = '';
    
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };

    $scope.getLndMrkNtCovLst = function(){
        $http.get($rootScope.BaseUrl + 'lndMrkNtCovLst')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting fence list");
                $scope.lndMrkNtCovDtlLst = resdata.data;
                // console.log($scope.lndMrkNtCovDtlLst);
                $scope.loadfilter();
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getLndMrkNtCovLst();

    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.lndMrkNtCovDtlLst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.lndMrkNtCovDtlLst, params.filter().term) : $scope.lndMrkNtCovDtlLst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    //  $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };
    $scope.pdfDownload = function()
    {
              var jsonColumns = ['Sno','fnce_nm'];
              var colHeadings = ['SNO','FENCE NAME'];
               var tbldata = $scope.lndMrkNtCovDtlLst;
                
              var rptHeading = 'LANDMARKS UNCOVERED REPORT';
              var fileName = 'LNDNTCOVD-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }
    $scope.excelDownload=function(){
    $scope.exprintData=[];
    $scope.exprintData.push({'Sno':'','fnce_nm':''});
    $scope.exprintData.push({'Sno':'SNO','fnce_nm':'FENCE NAME'});
    for(var i=0;i<$scope.lndMrkNtCovDtlLst.length;i++){
            $scope.exprintData.push({'Sno':$scope.lndMrkNtCovDtlLst[i].Sno,'fnce_nm':$scope.lndMrkNtCovDtlLst[i].fnce_nm});
          }
    return $scope.exprintData;
  }
})

.controller("vehicleCategoryCtrl",function($scope,$http,$rootScope,NgTableParams,growlService){
    $scope.searchVehData   = '';
    $rootScope.vehCtgryUpdData = '';
    
        $scope.updtVehCtgryData = function(v) {
          
          $rootScope.vehCtgryUpdData = v;
          window.location.href = '#/vts/md/vehicle_category_add_upd_master';
        };

        // getting asrt category list

        $scope.getVehCtgry = function() {
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleCtgryLst = resdata.data;
                $scope.loadfilter($scope.vehicleCtgryLst);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }
        $scope.getVehCtgry();

    $scope.deleteVehCtgry = function(data) {
        
        swal({
                title   : "Are you sure you want to delete this vehicle type?",
                text    : "Once you delete this it wont be displayed again. Make sure you want to do this",
                type    : "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Delete It",
                closeOnConfirm: true
            }, function() {
                
                $http.post($rootScope.BaseUrl + 'delVehicleCtgry',data)
                    .success(function (resdata, status) {
                        if(status == 257)  $rootScope.sessionCheck();
                        $scope.resMesg = resdata;
                        if(resdata.status == 601) {
                            growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                            $scope.getVehCtgry();
                        } else {
                            growlService.growl(' Successfully Deleted!', 'inverse');
                            $scope.getVehCtgry();
                        }
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                });
            });

    }

    $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
            page: 1, count: 10
        }, {
            data: data
        });
    };
    $scope.loadfilter($scope.vehiclesLst);
    $scope.PDFDownload = function(){
        console.log("pdf")
    };
    $scope.ExcelDownload = function(){
        console.log("excel")
    };
})

.controller("AddUpdVehCtgryCtrl",function($scope,$http,$rootScope,NgTableParams,growlService){

    $scope.asrt_ctgry_nm = '';
    if($rootScope.vehCtgryUpdData) {
        $scope.asrt_ctgry_nm = $rootScope.vehCtgryUpdData.asrt_ctgry_nm;
        // console.log($scope.asrt_ctgry_nm);
    }

    $scope.addNewDeprtmnt = function() {

        if($rootScope.vehCtgryUpdData) {
            var data = {
                "asrt_ctgry_nm": $scope.asrt_ctgry_nm,
                "asrt_ctgry_id": $rootScope.vehCtgryUpdData.asrt_ctgry_id,
                "clnt_id": 1,
                "tnt_id": 2,
                "a_in":1
            }
            var url = $rootScope.BaseUrl + 'updVehicleCtgry';
            $scope.addUpdVehCtgry(data,url);
        } else {
            var data = {
                "asrt_ctgry_nm": $scope.asrt_ctgry_nm,
                "clnt_id": 1,
                "tnt_id": 2,
                "a_in":1
              }
            var url = $rootScope.BaseUrl + 'addVehicleCtgry';
            $scope.addUpdVehCtgry(data,url);
        }
    }

    $scope.addUpdVehCtgry = function(data,url) {
        $http.post(url,data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if(resdata.status == 601) {
                        // console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');

                        window.location.href='#/vts/md/vehicle_dept_master';
                    } else {
                        growlService.growl(' Successfully Updated!', 'inverse');
                        window.location.href='#vts/md/vehicle_category_master';
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
            });
    }

})

.controller("vendorsCtrl",function($scope,$http,NgTableParams,$rootScope,growlService){
    $scope.searchVehData   = '';
   

        $scope.updtVendorData = function(vdata) {
          // $scope.showModal = true;
          // $scope.veh_num = v.veh_nm;
          // $scope.veh_type = v.dvce_nm;
          console.log(vdata);
          $rootScope.VendorDtls = vdata;

          window.location.href = '#/vts/md/vendor_add_upd_master';
        };

        $scope.submitVehData = function() {
        growlService.growl(' Successfully updated!', 'inverse'); 
          $scope.showModal = false;
        };

        $scope.cancel = function() {
          $scope.showModal = false;
        };

    $scope.getVendors = function() {
            $http.get($rootScope.BaseUrl + 'asrtGroupLst')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.groupsLst = resdata.data;
                    $scope.loadfilter($scope.groupsLst);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getVendors();

        $scope.deleteVendorData = function(delData) {
            console.log(delData);

            swal({
                title   : "Are you sure you want to delete this Vendor Group?",
                text    : "Once you delete this it wont be displayed again. Make sure you want to do this",
                type    : "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Delete It",
                closeOnConfirm: true
            }, function() {
                
                $http.post($rootScope.BaseUrl + 'delGroup',delData)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if(resdata.status == 601) {
                        console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                    } else {
                        growlService.growl(' Successfully Deleted!', 'inverse');
                        $scope.getVendors();
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
            });
        }
    $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
            page: 1, count: 10
        }, {
            data: data
        });
    };
    $scope.loadfilter($scope.vehiclesLst);
    $scope.PDFDownload = function(){
        // console.log("pdf")
    };
    $scope.ExcelDownload = function(){
        // console.log("excel")
    };
})


.controller("vendorsAddUpdCtrl",function($scope,$http,NgTableParams,$rootScope,growlService){

    if($rootScope.VendorDtls) {
        $scope.asrt_grp_nm = $rootScope.VendorDtls.asrt_grp_nm;
    }
    $scope.addNewVendor = function() {
        if($rootScope.VendorDtls) {
            var data = {
                    "asrt_grp_nm": $scope.asrt_grp_nm,
                    "asrt_grp_id": $rootScope.VendorDtls.asrt_grp_id,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in":1
                  }
                 var url = $rootScope.BaseUrl + 'updGroup';
                  $scope.addUpdVendor(data,url);
        } else {
            var data = {
                    "asrt_grp_nm": $scope.asrt_grp_nm,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in":1
                  }
                  var url = $rootScope.BaseUrl + 'addGroup';
                  $scope.addUpdVendor(data,url);
        }
    }

    $scope.addUpdVendor = function(data,url) {

        $http.post(url,data)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.resMesg = resdata;
                if(resdata.status == 601) {
                    growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                } else {
                    growlService.growl(' Successfully updated!', 'inverse');
                    // $scope.getVehicles();
                    window.location.href = '#vts/md/vendor_master';
                }
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

    }
       
})

.controller("AddUpdDepartCtrl",function($scope,$http,NgTableParams,$rootScope,growlService){

    $scope.dprt_nm = '';
    if($rootScope.updDeprt) {
        $scope.dprt_nm = $rootScope.updDeprt.dprt_nm;
    }

    $scope.addNewDeprtmnt = function() {
        
        if($rootScope.updDeprt) {
            var upddata = {
            "dprt_nm" : $scope.dprt_nm,
            "dprt_id" : $rootScope.updDeprt.dprt_id,
            "clnt_id" : 1,
            "tnt_id" : 2,
            "a_in" : 1
          }
        $scope.url = $rootScope.BaseUrl + 'updateDepartment';
        $scope.addUpdDprt(upddata);

        } else {
            var data = {
            "dprt_nm" : $scope.dprt_nm,
            "clnt_id" : 1,
            "tnt_id" : 2,
            "a_in" : 1
        }
        $scope.url = $rootScope.BaseUrl + 'addDepartment';
        $scope.addUpdDprt(data);
        }
    }

    $scope.addUpdDprt = function(data) {
            $http.post($scope.url,data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if(resdata.status == 601) {
                        // console.log(resdata);
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');

                        window.location.href='#/vts/md/vehicle_dept_master';
                    } else {
                        growlService.growl(' Successfully Updated!', 'inverse');
                        window.location.href='#vts/md/vehicle_dept_master';
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
            });

        }

    })


.controller("vehDepartCtrl",function($scope,$http,NgTableParams,$rootScope,growlService){
        
        $scope.searchVehData   = '';
      
        $scope.updtDepartmentData = function(v) {
          $scope.showModal = true;
          $rootScope.updDeprt = v;
          window.location.href = '#vts/md/addDeprtmnt';
        };

        $scope.deleteDprtData = function(data) {
            var delData = {
                "dprt_id" : data.dprt_id
            }

            swal({
            title   : "Are you sure you want to delete this Department ?",
            text    : "Once you delete this it wont be displayed again. Make sure you want to do this",
            type    : "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, Delete It",
            closeOnConfirm: true
            }, function() {
                
                $http.post($rootScope.BaseUrl + 'delDepartment',delData)
                    .success(function (resdata, status) {
                        if(status == 257)  $rootScope.sessionCheck();
                        $scope.resMesg = resdata;
                        if(resdata.status == 601) {
                            // console.log(resdata);
                            growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                        } else {
                            growlService.growl(' Successfully Deleted!', 'inverse');
                            $scope.getDepartments();
                        }
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                });
            });
        }

        $scope.getDepartments = function() {
            $http.get($rootScope.BaseUrl + 'DepartmentList')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.dprtmtLst = resdata.data;
                $scope.loadfilter($scope.dprtmtLst);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }
        $scope.getDepartments();

    $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
            page: 1, count: 10
        }, {
            data: data
        });
    };
    $scope.loadfilter($scope.vehiclesLst);
    $scope.PDFDownload = function(){
        console.log("pdf")
    };
    $scope.ExcelDownload = function(){
        console.log("excel")
    };
})


.controller("vehAddUpdDepartCtrl",function($scope,$http,$rootScope,NgTableParams,growlService){
            
            // getting asrtTypeLst list
            $http.get($rootScope.BaseUrl + 'asrtTypeLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.asrtTypeLst = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
          // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting imei list
            $http.get($rootScope.BaseUrl + 'deviceList')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.deviceList = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting Departments list
            $http.get($rootScope.BaseUrl + 'DepartmentList')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.DepartmentList = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            
            if($rootScope.vehDataTobeChange) {
                $scope.asrt = $rootScope.vehDataTobeChange;
            } else {
                $rootScope.vehDataTobeChange = '';
                $scope.asrt='';
            }

        $scope.asrt.asrt_ctgry_id = $scope.asrt.asrt_ctgry_id;

        $scope.asrt_ctgry_id = {"asrt_ctgry_id": $scope.asrt.asrt_ctgry_id};
        $scope.asrt_grp_id = {"asrt_grp_id": $scope.asrt.asrt_grp_id};
        $scope.crnt_dvce_id = {"dvce_id": $scope.asrt.crnt_dvce_id};
        $scope.asrt_type_id = {"asrt_type_id": $scope.asrt.asrt_type_id};
        $scope.dprt_id = {"dprt_id": $scope.asrt.dprt_id};

        $scope.addNewAssert = function() {

            if($rootScope.vehDataTobeChange ) {

                var data = {
                    "asrt_ctgry_id": $scope.asrt_ctgry_id.asrt_ctgry_id,
                    "asrt_grp_id": $scope.asrt_grp_id.asrt_grp_id,
                    "crnt_dvce_id" : $scope.crnt_dvce_id.dvce_id,
                    "asrt_type_id": $scope.asrt_type_id.asrt_type_id,
                    "asrt_nm": $scope.asrt.asrt_nm,
                    "dprt_id": $scope.dprt_id.dprt_id,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in":1,
                    "crnt_ast_sts_id": 1
                  }
                  var asrt_id = $scope.asrt.asrt_id;
                  var url = $rootScope.BaseUrl + 'UpdateVehicle/'+asrt_id;
                  $scope.addUpdateAsserts(data,url);

            } else {
                var data = {
                    "asrt_ctgry_id": $scope.asrt_ctgry_id.asrt_ctgry_id,
                    "asrt_grp_id": $scope.asrt_grp_id.asrt_grp_id,
                    "crnt_dvce_id" : $scope.crnt_dvce_id.dvce_id,
                    "asrt_type_id": $scope.asrt_type_id.asrt_type_id,
                    "asrt_nm": $scope.asrt.asrt_nm,
                    "dprt_id": $scope.dprt_id.dprt_id,
                    "clnt_id": 1,
                    "tnt_id": 2,
                    "a_in":1,
                    "crnt_ast_sts_id": 1
                  }
                  var url = $rootScope.BaseUrl + 'AddVehicle';

                  $scope.addUpdateAsserts(data,url);
            }
            
        }

        $scope.addUpdateAsserts = function(data,url) {
            console.log(data,url);
            $http.post(url,data)
                    .success(function (resdata, status) {
                        if(status == 257)  $rootScope.sessionCheck();
                        $scope.resMesg = resdata;
                        if(resdata.status == 601) {
                            growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                        } else {
                            growlService.growl(' Successfully updated!', 'inverse');
                            // $scope.getVehicles();
                            window.location.href = '#vts/md/vehicle_master';

                        }
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                    });
        }
})
.controller("vehiclesCtrl",function($scope,$http,$rootScope,NgTableParams,growlService,apppdfService){
    $scope.searchVehData   = '';
    $rootScope.vehDataTobeChange='';
    
    // delete volunteer record
    $scope.deleteVolunteer = function(v){
         if(confirm("Are you sure?")){
           var index = $scope.vehiclesLst.indexOf(v);
        $scope.vehiclesLst.splice(index, 1);
      growlService.growl('record deleted', 4000); }
    } //END DELETE 

        $scope.getVehicles = function() {
            $http.get($rootScope.BaseUrl + 'getAllasrtLst')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.vehiclesLst = resdata.data;
                    $scope.loadfilter($scope.vehiclesLst);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getVehicles();
         
     $scope.addNewVehData = function(asrt_name) {
          $scope.showModal = true;
          $scope.VehicleDropdown = 1;
          $scope.veh_num = null;
          $scope.veh_type = null;
          $scope.asrt = '';
          $scope.AddAssert = 1;
        };

        $scope.updtVehData = function(v) {
          // $scope.showModal = true;
          $scope.VehicleDropdown = 2;
          $rootScope.vehDataTobeChange = v;

          window.location.href = '#vts/md/addVehicle';
        };
        
        // deleting vehicle
        $scope.deleteVehData = function(data) {

            swal({
                title   : "Are you sure you want to delete this vehicle ?",
                text    : "Once you delete this it wont be displayed again. Make sure you want to do this",
                type    : "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Delete It",
                closeOnConfirm: true
            }, function() {
                
                $http.post($rootScope.BaseUrl + 'DeleteVehicle',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if(resdata.status == 601) {
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                    } else {
                        growlService.growl(' Successfully Deleted!', 'inverse');
                        $scope.getVehicles();
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
            });

        }

    $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
            page: 1, count: 10
        }, {
            data: data
        });
    };
    $scope.loadfilter($scope.vehiclesLst);

     $scope.PDFDownload = function()
    {
        console.log("pdfDownload");
              var jsonColumns = ['asrt_nm','asrt_ctgry_nm','asrt_grp_nm','dvce_nm'];
              var colHeadings = ['Vehicle Number','Vehicle Type','Vendor(Owner)','Device IMEI'];
               var tbldata = $scope.vehiclesLst;
                
              var rptHeading = 'TOTAL VEHILCES LIST';
              var fileName = 'TOT-VEHS';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }

    $scope.ExcelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'asrt_nm':'','asrt_ctgry_nm':'', 'asrt_grp_nm':'DISTANCE REPORT', 'dvce_nm':''});
    $scope.exprintData.push({'asrt_nm':'Vehicle Number','asrt_ctgry_nm':'Vehicle Type','asrt_grp_nm':'Vendor(Owner)','dvce_nm':'Device IMEI'});
    for(var i=0;i<$scope.vehiclesLst.length;i++){
            $scope.exprintData.push({'asrt_nm':$scope.vehiclesLst[i].asrt_nm,'asrt_ctgry_nm':$scope.vehiclesLst[i].asrt_ctgry_nm,'asrt_grp_nm':$scope.vehiclesLst[i].asrt_grp_nm,'dvce_nm':$scope.vehiclesLst[i].dvce_nm});
          }
    return $scope.exprintData;
  }
})
    
    // Driver Assignment Controller

    .controller("driverAssignmentCtrl",function($scope,$filter,apppdfService,clntDataService,tntDatabyClntService,$http,NgTableParams,growlService,$rootScope,localStorageService){
        
        $scope.showDrivertDtls = false;
        $scope.noDataFound = false;
        $scope.searchUsrData='';
        $scope.closedDtls = false;

        $scope.getDrivers = function(){
            $http.get('../../apiv1/driversList/')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.driversDtls = resdata.data;
                    // console.log($scope.driversDtls);
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getDrivers();

        $scope.getAsserts = function(){
            $http.get('../../apiv1/getAllasrtLst/')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.assertsList = resdata.data;
                    // console.log($scope.assertsList);
                    $scope.shoeTable = $scope.assertsList.length;
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getAsserts();

          $scope.formats = ['MM-dd-yyyy','dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $scope.format = $scope.formats[1];
          $scope.selectedSrvClass=0;
          $scope.selectedSrvClassReg=0;
          $scope.vclass_id="";
          this.dateModeOptions = {
            minMode: 'year',
            maxMode: 'year'
          };

          this.openCalendar = function(e, date) {
            that.open[date] = true;
          };
          $scope.openfrom = function($event, openedf) {
            $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
          };

          $scope.openTo = function($event, openedt) {
            $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
          };

        $scope.dtFrom = moment(new Date()).format('YYYY-MM-DD 00:00');
        $scope.dtTo = moment(new Date()).format('YYYY-MM-DD 23:59');
        console.log($scope.dtFrom , $scope.dtTo);

         $scope.assignDriver = function() {
            console.log($scope.dtFrom , $scope.dtTo , $scope.w);
            $scope.noDataFound = false;
            $scope.showDrivertDtls = false;
            $scope.finalObject = [];
            var data = {
                "frm_ts" : $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm"),
                "to_ts" : $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm"),
                "crw_id" : $scope.w.crw_id,
                "asrt_id" : $scope.w.asrt_id
            }
            // console.log(data);

            $http.post('../../apiv1/driverAssignment/',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.inserted = resdata.data;
                    console.log(resdata);
                    console.log(resdata.data.length);
                    console.log($scope.inserted.length);
                    if($scope.inserted) {
                      for(var i=0;i<$scope.inserted.length;i++) {

                        if($scope.inserted[i].close_drvr_in == 1) {
                          
                          $scope.closeDrvr = $scope.inserted[i];
                          console.log($scope.closeDrvr);
                          $scope.closedDtls = true;
                        }
                      }
                    }
                    
                    if($scope.closeDrvr || $scope.closeEntry) {
                      $scope.closedDtls = true;
                    }
                    
                    if(resdata.status == 331) {
                        growlService.growl('Duplicate Driver Entered', 'inverse');
                    } else {
                        growlService.growl(' Successfully Updated', 'inverse');
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });

         }

         $scope.getDriverDtls = function() {
            $scope.showDrivertDtls = true;
            $scope.noDataFound = true;
            // console.log($scope.dtFrom , $scope.dtTo , $scope.w);
            if($scope.w.crw_id && $scope.w.asrt_id) {
                var data = {
                    "frm_ts" : $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm"),
                    "to_ts" : $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm"),
                    "crw_id" : $scope.w.crw_id,
                    "asrt_id" : $scope.w.asrt_id
                }
              console.log(data);

                $http.post('../../apiv1/DriverAsigList/',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.DriverDtls = resdata.data;
                    $scope.shoeTable = $scope.DriverDtls.length;

                    console.log(resdata);
                    $scope.loadfilter($scope.DriverDtls);
                    // growlService.growl(' Successfully Updated', 'inverse');
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });

            }
         }

         $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };

    })
    
.controller("fuelOpsCtrl",function($scope,$filter,apppdfService,fuelpdfService,$location,$anchorScroll,clntDataService,tntDatabyClntService,$http,NgTableParams,growlService,$rootScope,localStorageService){
         
    $scope.showDrivertDtls = false;
    $scope.noDataFound = false;
    $scope.searchUsrData='';
    $scope.fuelHistoryTbl = false;
    $scope.searchUsrData = [];

    $scope.getAsrtCtgry = function() {

      $http.get('../../apiv1/asrtCtgryLst/')
           .success(function (resdata, status) {
              if(status == 257)  $rootScope.sessionCheck();
                $scope.asrtCtgryLst = resdata.data;
            })
           .error(function(data, status, headers, config) {
               console.log(data);
            });
      }
       
    $scope.getAsrtCtgry();

    $scope.getAsrtGroup = function() {
    
        $http.get('../../apiv1/asrtGroupLst/')
             .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                  $scope.asrtGrpLst = resdata.data;
                  // console.log($scope.asrtGrpLst);
              })
             .error(function(data, status, headers, config) {
                  console.log(data);
              });
    }
       
        $scope.getAsrtGroup();

        $scope.getVehicles = function() {
            // console.log($scope.w);
            $scope.noDataFound = true;
            if(!$scope.w) {
              $scope.showDrivertDtls = false;
              growlService.growl('All the fields are required', 'inverse');
            } else if($scope.w.asrt_grp_id && $scope.w.asrt_ctgry_id) {
              $scope.showDrivertDtls = true;
              $http.post('../../apiv1/getAsrtsForFuel/',$scope.w)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.fuelAsrtDtls = resdata.data;
                    // console.log($scope.fuelAsrtDtls);
                    $scope.loadfilter();
                    $scope.shoeTable = $scope.fuelAsrtDtls.length;
                })
                .error(function(data, status, headers, config) {
                    // console.log(data);
                });
            } else if($scope.w.asrt_grp_id && !$scope.w.asrt_ctgry_id) {
              $scope.showDrivertDtls = false;
              growlService.growl('Please Select category', 'inverse');
            } else if(!$scope.w.asrt_grp_id && $scope.w.asrt_ctgry_id) {
              $scope.showDrivertDtls = false;
              growlService.growl('Please Select Vehicle Group', 'inverse');
            } else {
              $scope.showDrivertDtls = false;
              growlService.growl('All the fields are required', 'inverse');
            }
        }

        $scope.addFuelDtls = function(data) {
            // console.log(data);
            if(data.new_fuel_ct) {
                $http.post('../../apiv1/addFuelDtls/',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    if(resdata.status == 331) {
                        growlService.growl('Updated Successfully', 'inverse');
                    } else {
                        growlService.growl('Added Successfully', 'inverse');
                    }

                    $scope.getVehicles();
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
            } else if(!data.new_fuel_ct) {
                growlService.growl('please enter fuel..!!', 'inverse');
            }
        }

    $scope.filter = { term: $scope.searchUsrData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.fuelAsrtDtls.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.fuelAsrtDtls, params.filter().term) : $scope.fuelAsrtDtls;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      })
    }
        
    $scope.fuelfilter = function(data){
        $scope.fuelFilter = new NgTableParams({
            page: 1, count: 10
        }, {
            data: data
        });
    };

    $scope.FuelHst = function(data) {
          $scope.fuelHistoryTbl = true;
          $scope.fuelCnt = '';
          $scope.distCnt = '';
          $scope.avgMileage = '';
          $scope.tobeTrvlDist = '';
          $scope.DiffDistance = '';
          // console.log(data);
          

          $http.get('../../apiv1/asrtFuelHistory/'+data.asrt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.asrtHistryLst = resdata.data;
                $scope.fuelfilter($scope.asrtHistryLst);
                $location.hash('bottom');
                $anchorScroll();
                $scope.month = [];
                $scope.date = [];
                var mnth = '';

                for(var i=0;i<$scope.asrtHistryLst.length;i++) {
                  $scope.fuelCnt = Number($scope.fuelCnt) + Number($scope.asrtHistryLst[i].fuel_ct);
                  $scope.distCnt = Number($scope.distCnt) + Number($scope.asrtHistryLst[i].dist);
                  mnth = moment($scope.asrtHistryLst[i].issed_dt , 'DD-MM-YYYY').format('MM-YYYY');
                  
                  if($scope.month != mnth) {
                    $scope.month.push(mnth);
                  }
                  $scope.date.push(moment($scope.asrtHistryLst[i].issed_dt , 'DD-MM-YYYY').format('DD'));
                }
                $scope.distance = $scope.distCnt.toFixed(2);

                $scope.avgMileage = ($scope.distCnt / $scope.fuelCnt).toFixed(2);
                $scope.tobeTrvlDist = ($scope.fuelCnt * $scope.asrtHistryLst[0].mileage_ct).toFixed(0);
                $scope.DiffDistance = ($scope.tobeTrvlDist - $scope.distance).toFixed(2);

                // console.log($scope.fuelCnt , $scope.distance , $scope.avgMileage ,$scope.month,$scope.date);
            })
            .error(function(data, status, headers, config) {
              console.log(data);
          });
    }

    $scope.filterCounts = function() {
          
        $scope.fuelCnt = '';
        $scope.distCnt = '';
        $scope.avgMileage = '';
        var filteredData = [];
        if ($scope.selectedMonth == null) {
          for(var i=0;i<$scope.asrtHistryLst.length;i++) {
              filteredData.push($scope.asrtHistryLst[i]);
              $scope.fuelCnt = Number($scope.fuelCnt) + Number($scope.asrtHistryLst[i].fuel_ct);
              $scope.distCnt = Number($scope.distCnt) + Number($scope.asrtHistryLst[i].dist);
          }

        } else {

          for(var i=0;i<$scope.asrtHistryLst.length;i++) {
            if($scope.asrtHistryLst[i].issed_dt.indexOf($scope.selectedMonth) > -1) {
              filteredData.push($scope.asrtHistryLst[i]);
              $scope.fuelCnt = Number($scope.fuelCnt) + Number($scope.asrtHistryLst[i].fuel_ct);
              $scope.distCnt = Number($scope.distCnt) + Number($scope.asrtHistryLst[i].dist);
            }
          }
        }
       
        if($scope.distCnt) {
          $scope.distance = $scope.distCnt.toFixed(2);
        }
       
        if($scope.fuelCnt) {
          $scope.avgMileage = ($scope.distCnt / $scope.fuelCnt).toFixed(2);
        }

        if($scope.tobeTrvlDist) {
          $scope.tobeTrvlDist = ($scope.fuelCnt * $scope.asrtHistryLst[0].mileage_ct).toFixed(0);
        }

        if($scope.DiffDistance) {
          $scope.DiffDistance = ($scope.tobeTrvlDist - $scope.distance).toFixed(2);
        }
        
        $scope.fuelfilter(filteredData);
    }
        
  $scope.pdfDownload = function() {
        // console.log("pdfDownload");
              var jsonColumns = ['asrt_nm','asrt_ctgry_nm','asrt_grp_nm','fuel_ct','date'];
              var colHeadings = ['Vehicle Name','Vehicle Ctgry','Vehicle Group','Last Refilled (in Ltrs)','Last Refilled Date'];
               var tbldata = $scope.fuelAsrtDtls;
                
              var rptHeading = 'FUEL-CONSUMPTION REPORT';
              var fileName = 'FUEL-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }

    $scope.excelDownload=function() {
    $scope.exprintData=[];
    $scope.exprintData.push({'asrt_nm':'','asrt_ctgry_nm':'', 'asrt_grp_nm':'FUEL REPORT', 'fuel_ct':'', 'date':''});
    $scope.exprintData.push({'asrt_nm':'Vehicle Name','asrt_ctgry_nm':'Vehicle Ctgry','asrt_grp_nm':'Vehicle Group','fuel_ct':'Last Refilled Dtls','date':'Last Refilled Date'});
    for(var i=0;i<$scope.fuelAsrtDtls.length;i++){
            $scope.exprintData.push({'asrt_nm':$scope.fuelAsrtDtls[i].asrt_nm,'asrt_ctgry_nm':$scope.fuelAsrtDtls[i].asrt_ctgry_nm,'asrt_grp_nm':$scope.fuelAsrtDtls[i].asrt_grp_nm,'fuel_ct':$scope.fuelAsrtDtls[i].fuel_ct,'date':$scope.fuelAsrtDtls[i].date});
          }
    return $scope.exprintData;
  }

  $scope.pullDownPdfDownload = function() {
      // console.log("pdfDownload");
      var jsonColumns = ['asrt_nm','fuel_ct','dist','issed_dt'];
      var colHeadings = ['Veh Name','Fuel Count','Distance','Issue Date'];
      var tbldata = [];
      var cardsData = [];

      var cardsHeadings = ['Total Distance Travelled :','Total Distance To Be Travelled','Distance Difference','Total fuel','Average Mileage'];
      var cardsJsonColumns = ['distance','tobeTrvlDist','DiffDistance','fuelCnt','avgMileage'];

      var cardsObj = [{
        "distance" : $scope.distance,
        "tobeTrvlDist" : $scope.tobeTrvlDist,
        "DiffDistance" : $scope.DiffDistance,
        "fuelCnt" : $scope.fuelCnt,
        "avgMileage" : $scope.avgMileage
      }];

      if ($scope.selectedMonth && $scope.selectedMonth.length != 0) {
        for (var j=0;j<$scope.asrtHistryLst.length;j++) {
          if($scope.asrtHistryLst[j].issed_dt.indexOf($scope.selectedMonth) > 0) {
              tbldata.push($scope.asrtHistryLst[j]);  
          }
        }

        cardsData.push(cardsObj);
      } else {
        tbldata = $scope.asrtHistryLst; 
        cardsData = cardsObj;
      }

      // var rptHeading = 'FUEL-CONSUMPTION REPORT';

      var rptHeading = 'FUEL-CONSUMPTION REPORT \n\n\n';
      var fileName = 'FUEL-RPT';
      var align= 'portrait';

      var headData1 = 'Total Distance Travelled : '+$scope.distance+' Kms';
      var headData4 = 'Total Distance To Be Travelled : '+$scope.tobeTrvlDist+' Kms';
      var headData5 = 'Distance Difference : '+$scope.DiffDistance+' Kms \n\n';
      var headData2 = 'Total fuel :'+ $scope.fuelCnt + ' Ltrs';
      var headData3 = 'Average Mileage : '+ $scope.avgMileage+' Kms/Ltr';
      fuelpdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align,cardsHeadings,cardsJsonColumns,cardsData);
  }

    $scope.pullDownExcelDownload=function() {
      $scope.exprintData=[];
      $scope.exprintData.push({'asrt_nm':'','fuel_ct':'', 'dist':'FUEL REPORT', 'issed_dt':''});
      $scope.exprintData.push({'asrt_nm':'Veh Name','fuel_ct':'Fuel Count','dist':'Distance','issed_dt':'Issue Date'});
      for(var i=0;i<$scope.asrtHistryLst.length;i++){
              $scope.exprintData.push({'asrt_nm':$scope.asrtHistryLst[i].asrt_nm,'fuel_ct':$scope.asrtHistryLst[i].fuel_ct,'dist':$scope.asrtHistryLst[i].dist,'issed_dt':$scope.asrtHistryLst[i].issed_dt});
            }
      return $scope.exprintData;
    }

  })

    // Alert Details Controller

    .controller("alertDtlsCtrl",function($scope,$filter,apppdfService,clntDataService,tntDatabyClntService,$http,NgTableParams,growlService,$rootScope,localStorageService){
        
        $scope.showAlertDtls = false;
        $scope.newUser=true;
        $scope.userFormDtls = {};
        
        $scope.searchUsrData='';

                $scope.getUsers = function(){
            $http.get('../../apiv1/users/')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.usersDtls = resdata.data;
                    console.log($scope.usersDtls);
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getUsers();
        // alert category get
        $scope.getAlertCtgry = function(){
            $http.get('../../apiv1/alertCtgry/')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.alertCtgryDtls = resdata.data;
                    console.log($scope.alertCtgryDtls);
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getAlertCtgry();

          $scope.formats = ['MM-dd-yyyy','dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $scope.format = $scope.formats[1];
          $scope.selectedSrvClass=0;
          $scope.selectedSrvClassReg=0;
          $scope.vclass_id="";
          this.dateModeOptions = {
            minMode: 'year',
            maxMode: 'year'
          };

          this.openCalendar = function(e, date) {
            that.open[date] = true;
          };
          $scope.openfrom = function($event, openedf) {
            $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
          };

          $scope.openTo = function($event, openedt) {
            $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
          };

         $scope.dtFrom = $filter('date')(new Date(), "yyyy-MM-dd HH:mm");
         $scope.dtTo = $filter('date')(new Date(), "yyyy-MM-dd HH:mm");
         console.log($scope.dtFrom , $scope.dtTo);

         $scope.getDtls = function() {
            
            $scope.showAlertDtls = true;
            console.log($scope.dtFrom , $scope.dtTo , $scope.w);


            var data = {
                "from_date" : $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm"),
                "to_date" : $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm"),
                "usr_id" : $scope.w.usr_id,
                "alert_cat_id" : $scope.w.alert_cat_id
            }
            console.log(data);
            if(data) {
                
            $http.post('../../apiv1/AlertList',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.alertDtls = resdata.data;
                    console.log($scope.alertCtgryDtls);
                    $scope.loadfilter($scope.alertDtls);
                    
                }).error(function(data, status, headers, config) {
                    console.log(data);
                });
            }

         }
         $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };

    })

    // =========================================================================
    //  alertSubscriptionCtrl controller
    // =========================================================================

    .controller("alertSubscriptionCtrl",function($scope,apppdfService,clntDataService,tntDatabyClntService,$http,NgTableParams,growlService,$rootScope,localStorageService){
        
        $scope.showUserSbscrDtls = false;
        $scope.subscribeUser=true;
        $scope.searchUsrData='';

        $scope.getUsers = function(){
            $http.get('../../apiv1/users/')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.usersDtls = resdata.data;
                    console.log($scope.usersDtls);
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getUsers();
        // alert category get
        $scope.getAlertCtgry = function(){
            $http.get('../../apiv1/alertCtgry/')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.alertCtgryDtls = resdata.data;
                    console.log($scope.alertCtgryDtls);
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getAlertCtgry();


        $scope.getSubscriptions = function() {
            $scope.showUserSbscrDtls = true;
            $http.get('../../apiv1/subscriptions/'+$scope.user_id)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.subscriptionDtls = resdata.data;
                    console.log($scope.subscriptionDtls);
                    $scope.loadfilter($scope.subscriptionDtls);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
       $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };

      $scope.w={};
      $scope.w.sms_alert_in=0;
      $scope.w.email_alert_in=0;
      $scope.w.app_alert_in=0;

        $scope.addNewSubscription = function() {
            console.log($scope.w);

            $http.post("../apiv1/addNewSubscription",$scope.w)
                  .success(function(resdata, status, headers, config) {
                      if (status == 200) {
                          if(resdata.status == 200){
                              // $scope.err_msg=null;
                              // $scope.getEntryData();
                              growlService.growl(resdata.data.success+' Successfully Subscribed.', 'inverse');
                              growlService.growl(resdata.data.error+' Already Subscribed.', 'inverse');
                              // cleanupEntry();
                              $scope.newEntryCollapse=true;
                          }
                          else if(resdata.status== 700) {
                              growlService.growl('Internal Database Error', 'inverse');
                          }else if(resdata.status== 601) {
                              growlService.growl(resdata.message, 'inverse');
                          }
                          else {
                              $scope.err_msg=resdata.message;
                          }

                      }
                      else {
                          growlService.growl('Unable to connect to server. Please check your internet connectivity.',inverse)
                      }
                  }).
                  error(function(data, status, headers, config) {
                      $scope.shloading = false;
                      $scope.err_msg = "Unable to connect to server. Please check your internet connectivity.";
                  });

        }

        $scope.updSubscribers = function(data) {
            console.log(data);
            $http.post('../../apiv1/updSubscription',data)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                // console.log(resdata);
                if(resdata.status == 601) {
                    console.log(resdata.errors);
                    growlService.growl('Invalid Parameters Send..!!', 'inverse');
                } else {
                    growlService.growl('Successfully Added Alert Type!', 'inverse');
                    $scope.getSubscriptions();
                }
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }

        $scope.newSubrClear = function(){
            $scope.w = {};
            growlService.growl('Data cleared', 'inverse'); 
        };

        $scope.smsalert = function(alert_cat_id,usr_id,sms_alert_in) {
            console.log(alert_cat_id,usr_id,sms_alert_in);
            var data = {
                "alert_cat_id" : alert_cat_id,
                "usr_id" : usr_id,
                "sms_alert_in" : sms_alert_in
            }
            $scope.updSubscribers(data);
        }

        $scope.appalert = function(alert_cat_id,usr_id,app_alert_in) {
            console.log(alert_cat_id,usr_id,app_alert_in);
            var data = {
                "alert_cat_id" : alert_cat_id,
                "usr_id" : usr_id,
                "app_alert_in" : app_alert_in
            }
            $scope.updSubscribers(data);
        }

        $scope.emailalert = function(alert_cat_id,usr_id,email_alert_in) {
            console.log(alert_cat_id,usr_id,email_alert_in);

            var data = {
                "alert_cat_id" : alert_cat_id,
                "usr_id" : usr_id,
                "email_alert_in" : email_alert_in
            }
            $scope.updSubscribers(data);
        }

})

.controller("hrFncsCovdCtrl", function($scope, $http, NgTableParams, growlService, $rootScope, $filter, $q) {
    var cache;
    $scope.vehgrpid = null;
    $scope.vehGrpModel = [];
    $scope.vehGrpSettings = {
        "displayProp": "asrt_grp_nm",
        "idProp": "asrt_grp_id",
        "enableSearch": true,
        "externalIdProp": "asrt_grp_id",
        "styleActive": true,
        "keyboardControls": true
    };
    $scope.vehGrpTextSettings = {
        "searchPlaceholder": "Search Vehicle Group",
        "buttonDefaultText": "Select Vehicle Group"
    };
    $scope.customFilter = '';

    $scope.vehCtgryid = null;
    $scope.vehCtgryModel = [];
    $scope.vehCtgrySettings = {
        "displayProp": "asrt_ctgry_nm",
        "idProp": "asrt_ctgry_id",
        "enableSearch": true,
        "externalIdProp": "asrt_ctgry_id",
        "styleActive": true,
        "keyboardControls": true
    }; 
    $scope.vehCtgryTextSettings = {
        "searchPlaceholder": "Search Vehicle Category",
        "buttonDefaultText": "Select Vehicle Category"
    };

    $scope.selectedvehCtgry = [{"asrt_ctgry_id": 4}]
    $scope.selectedvehGrp = [{"asrt_grp_id": 8}]

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

    // getting asrt_ctgry list
    $http.get($rootScope.BaseUrl + 'asrtDshbrdCtgryLst')
        .success(function(resdata, status) {
            if (status == 257) $rootScope.sessionCheck();
            $scope.vehicleCtgryLst = resdata.data;
            $scope.vehctgrylst = $scope.vehicleCtgryLst;
        })
        .error(function(data, status, headers, config) {
            console.log(status);
        });

    var margin = {
            top: 40,
            right: 20,
            bottom: 50,
            left: 20
        },
        width = 960 - margin.left - margin.right,
        height = 420 - margin.top - margin.bottom,
        gridSize = Math.floor(width / 24),
        legendElementWidth = gridSize * 2,
        buckets = 9,
         colors = ["#ffffd9", "#edf8b1", "#c7e9b4", "#7fcdbb", "#41b6c4", "#1d91c0", "#225ea8", "#253494", "#081d58"],
        // colors = ['#ccffcc', '#99ff99', '#66ff66', '#33ff33', '#00ff00', '#00cc00', '#009900', '#006600', '#003300'];
        days = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
        times = ["1am", "2pm", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm","10pm","11pm","12am"];

    var svg = d3.select("#chart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var dayLabels = svg.selectAll(".dayLabel")
        .data(days)
        .enter().append("text")
        .text(function(d) {
            return d;
        })

        .attr("x", 0)
        .attr("y", function(d, i) {
            return i * gridSize;
        })
        .style("text-anchor", "end")
        .attr("transform", "translate(-6," + gridSize / 2.5 + ")")
        .attr("class", function(d, i) {
            return ((i >= 0 && i <= 4) ? "dayLabel mono axis axis-workweek" : "dayLabel mono axis");
        });
    // dayLabels.exit().remove();

    var timeLabels = svg.selectAll(".timeLabel")
        .data(times)
        .enter().append("text")
        .text(function(d) {
            return d;
        })

        .attr("x", function(d, i) {
            return i * gridSize;
        })
        .attr("y", 0)
        .style("text-anchor", "middle")
        .attr("transform", "translate(" + gridSize / 2 + ", -6)")
        .attr("class", function(d, i) {
            return ((i >= 7 && i <= 16) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis");
        });

        // var chartData = [];
        var chartData = [{"day":"1","hour":"1","value":"0"},{"day":"1","hour":"2","value":"0"},{"day":"1","hour":"3","value":"0"},{"day":"1","hour":"4","value":"0"},{"day":"1","hour":"5","value":"0"},{"day":"1","hour":"6","value":"0"},{"day":"1","hour":"7","value":"0"},{"day":"1","hour":"8","value":"0"},{"day":"1","hour":"9","value":"0"},{"day":"1","hour":"10","value":"0"},{"day":"1","hour":"11","value":"0"},{"day":"1","hour":"12","value":"0"},{"day":"1","hour":"13","value":"0"},{"day":"1","hour":"14","value":"0"},{"day":"1","hour":"15","value":"0"},{"day":"1","hour":"16","value":"0"},{"day":"1","hour":"17","value":"0"},{"day":"1","hour":"18","value":"0"},{"day":"1","hour":"19","value":"0"},{"day":"1","hour":"20","value":"0"},{"day":"1","hour":"21","value":"0"},{"day":"1","hour":"22","value":"0"},{"day":"1","hour":"23","value":"0"},{"day":"1","hour":"24","value":"0"},{"day":"2","hour":"1","value":"0"},{"day":"2","hour":"2","value":"0"},{"day":"2","hour":"3","value":"0"},{"day":"2","hour":"4","value":"0"},{"day":"2","hour":"5","value":"0"},{"day":"2","hour":"6","value":"0"},{"day":"2","hour":"7","value":"0"},{"day":"2","hour":"8","value":"0"},{"day":"2","hour":"9","value":"0"},{"day":"2","hour":"10","value":"0"},{"day":"2","hour":"11","value":"0"},{"day":"2","hour":"12","value":"0"},{"day":"2","hour":"13","value":"0"},{"day":"2","hour":"14","value":"0"},{"day":"2","hour":"15","value":"0"},{"day":"2","hour":"16","value":"0"},{"day":"2","hour":"17","value":"0"},{"day":"2","hour":"18","value":"0"},{"day":"2","hour":"19","value":"0"},{"day":"2","hour":"20","value":"0"},{"day":"2","hour":"21","value":"0"},{"day":"2","hour":"22","value":"0"},{"day":"2","hour":"23","value":"0"},{"day":"2","hour":"24","value":"0"},{"day":"3","hour":"1","value":"0"},{"day":"3","hour":"2","value":"0"},{"day":"3","hour":"3","value":"0"},{"day":"3","hour":"4","value":"0"},{"day":"3","hour":"5","value":"0"},{"day":"3","hour":"6","value":"0"},{"day":"3","hour":"7","value":"0"},{"day":"3","hour":"8","value":"0"},{"day":"3","hour":"9","value":"0"},{"day":"3","hour":"10","value":"0"},{"day":"3","hour":"11","value":"0"},{"day":"3","hour":"12","value":"0"},{"day":"3","hour":"13","value":"0"},{"day":"3","hour":"14","value":"0"},{"day":"3","hour":"15","value":"0"},{"day":"3","hour":"16","value":"0"},{"day":"3","hour":"17","value":"0"},{"day":"3","hour":"18","value":"0"},{"day":"3","hour":"19","value":"0"},{"day":"3","hour":"20","value":"0"},{"day":"3","hour":"21","value":"0"},{"day":"3","hour":"22","value":"0"},{"day":"3","hour":"23","value":"0"},{"day":"3","hour":"24","value":"0"},{"day":"4","hour":"1","value":"0"},{"day":"4","hour":"2","value":"0"},{"day":"4","hour":"3","value":"0"},{"day":"4","hour":"4","value":"0"},{"day":"4","hour":"5","value":"0"},{"day":"4","hour":"6","value":"0"},{"day":"4","hour":"7","value":"0"},{"day":"4","hour":"8","value":"0"},{"day":"4","hour":"9","value":"0"},{"day":"4","hour":"10","value":"0"},{"day":"4","hour":"11","value":"0"},{"day":"4","hour":"12","value":"0"},{"day":"4","hour":"13","value":"0"},{"day":"4","hour":"14","value":"0"},{"day":"4","hour":"15","value":"0"},{"day":"4","hour":"16","value":"0"},{"day":"4","hour":"17","value":"0"},{"day":"4","hour":"18","value":"0"},{"day":"4","hour":"19","value":"0"},{"day":"4","hour":"20","value":"0"},{"day":"4","hour":"21","value":"0"},{"day":"4","hour":"22","value":"0"},{"day":"4","hour":"23","value":"0"},{"day":"4","hour":"24","value":"0"},{"day":"5","hour":"1","value":"0"},{"day":"5","hour":"2","value":"0"},{"day":"5","hour":"3","value":"0"},{"day":"5","hour":"4","value":"0"},{"day":"5","hour":"5","value":"0"},{"day":"5","hour":"6","value":"0"},{"day":"5","hour":"7","value":"0"},{"day":"5","hour":"8","value":"0"},{"day":"5","hour":"9","value":"0"},{"day":"5","hour":"10","value":"0"},{"day":"5","hour":"11","value":"0"},{"day":"5","hour":"12","value":"0"},{"day":"5","hour":"13","value":"0"},{"day":"5","hour":"14","value":"0"},{"day":"5","hour":"15","value":"0"},{"day":"5","hour":"16","value":"0"},{"day":"5","hour":"17","value":"0"},{"day":"5","hour":"18","value":"0"},{"day":"5","hour":"19","value":"0"},{"day":"5","hour":"20","value":"0"},{"day":"5","hour":"21","value":"0"},{"day":"5","hour":"22","value":"0"},{"day":"5","hour":"23","value":"0"},{"day":"5","hour":"24","value":"0"},{"day":"6","hour":"1","value":"0"},{"day":"6","hour":"2","value":"0"},{"day":"6","hour":"3","value":"0"},{"day":"6","hour":"4","value":"0"},{"day":"6","hour":"5","value":"0"},{"day":"6","hour":"6","value":"0"},{"day":"6","hour":"7","value":"0"},{"day":"6","hour":"8","value":"0"},{"day":"6","hour":"9","value":"0"},{"day":"6","hour":"10","value":"0"},{"day":"6","hour":"11","value":"0"},{"day":"6","hour":"12","value":"0"},{"day":"6","hour":"13","value":"0"},{"day":"6","hour":"14","value":"0"},{"day":"6","hour":"15","value":"0"},{"day":"6","hour":"16","value":"0"},{"day":"6","hour":"17","value":"0"},{"day":"6","hour":"18","value":"0"},{"day":"6","hour":"19","value":"0"},{"day":"6","hour":"20","value":"0"},{"day":"6","hour":"21","value":"0"},{"day":"6","hour":"22","value":"0"},{"day":"6","hour":"23","value":"0"},{"day":"6","hour":"24","value":"0"},{"day":"7","hour":"1","value":"0"},{"day":"7","hour":"2","value":"0"},{"day":"7","hour":"3","value":"0"},{"day":"7","hour":"4","value":"0"},{"day":"7","hour":"5","value":"0"},{"day":"7","hour":"6","value":"0"},{"day":"7","hour":"7","value":"0"},{"day":"7","hour":"8","value":"0"},{"day":"7","hour":"9","value":"0"},{"day":"7","hour":"10","value":"0"},{"day":"7","hour":"11","value":"0"},{"day":"7","hour":"12","value":"0"},{"day":"7","hour":"13","value":"0"},{"day":"7","hour":"14","value":"0"},{"day":"7","hour":"15","value":"0"},{"day":"7","hour":"16","value":"0"},{"day":"7","hour":"17","value":"0"},{"day":"7","hour":"18","value":"0"},{"day":"7","hour":"19","value":"0"},{"day":"7","hour":"20","value":"0"},{"day":"7","hour":"21","value":"0"},{"day":"7","hour":"22","value":"0"},{"day":"7","hour":"23","value":"0"},{"day":"7","hour":"24","value":"0"}]
       
        var drawChart = function() {
        console.log("called");

        d3.csv("client/src/public/static/assets/data.csv",
            function(d) {
                return {
                    day: +d.day,
                    hour: +d.hour,
                    value: +d.value
                };
            },
            function(error, data) {
                // console.log(data);
                var colorScale = d3.scale.quantile()
                    .domain([0, buckets - 1, d3.max(data, function(d) {
                        return d.value;
                    })])
                    .range(colors);
                console.log("*********************************");
                // console.log(data);
                var heatMap = svg.selectAll(".hour")
                    .data(chartData)
                    .enter().append("rect")
                    .attr("x", function(d) {
                        return (d.hour - 1) * gridSize;
                    })

                    .attr("y", function(d) {
                        return (d.day - 1) * gridSize;
                    })

                    .attr("rx", 4)
                    .attr("ry", 4)
                    .attr("width", gridSize)
                    .attr("height", gridSize)
                    .attr("class", "square")
                    .style("fill", function(d) {
                        return colorScale(d.value);
                    })

                    .style("stroke", "#ccc")
                    .attr('data-title', function(d) {
                        return 'Fences Covered : ' + d.value;
                    });

                $("rect").tooltip({
                    container: 'body',
                    html: true,
                    placement: 'right'
                });
          });
    };

    /**Code for first chart**/
    $scope.getHrlyFncsCvd = function() {

        var vehgrp = '0';
        for (i = 0; i < $scope.selectedvehGrp.length; i++) {
            vehgrp = vehgrp + ',' + $scope.selectedvehGrp[i].asrt_grp_id;
        }
        vehctgryid = '0';
        for (i = 0; i < $scope.selectedvehCtgry.length; i++) {
            vehctgryid = vehctgryid + ',' + $scope.selectedvehCtgry[i].asrt_ctgry_id;
        }

        var querydata = '';
        querydata = {
            "vehgrp": vehgrp,
            "vehctgryid": vehctgryid
        };
        // console.log(querydata);
        $scope.q_Data(querydata);
    }

     $scope.q_Data = function(querydata) {
      console.log(querydata);
        $http.post($rootScope.BaseUrl + 'hrlyFncsCovd/', querydata).success(function(resdata, status) {
            console.log(status);
            if (status == 200) { 
            console.log("Getting hourly fences covered counts for chart");
            // $scope.hrlyFncsCovdDtl = [];
            $scope.hrlyFncsCovdDtl = resdata.data;
            console.log($scope.hrlyFncsCovdDtl);
          }
            for (var i = 0; i < $scope.hrlyFncsCovdDtl.length; i++) {
                var d;
                if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Monday") != -1) {
                    d = 1;
                } else if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Tuesday") != -1) {
                    d = 2;
                } else if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Wednesday") != -1) {
                    d = 3;
                } else if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Thursday") != -1) {
                    d = 4;
                } else if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Friday") != -1) {
                    d = 5;
                } else if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Saturday") != -1) {
                    d = 6;
                } else if ($scope.hrlyFncsCovdDtl[i].day_nm.indexOf("Sunday") != -1) {
                    d = 7;
                }
                var h = parseInt($scope.hrlyFncsCovdDtl[i].hr_ts);
                var v = parseInt($scope.hrlyFncsCovdDtl[i].fnce_covered);

                chartData.push({
                      day: d,
                      hour: h,
                      value: v
                });
            }
            drawChart();
            //return $scope.hrlyFncsCovdDtl;
        }).error(function(data, status, headers, config) {
            console.log(status + "please check it...");
        });
    };
    
    $scope.getHrlyFncsCvd();
});