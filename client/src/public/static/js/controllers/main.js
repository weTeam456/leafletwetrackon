wertrackon
    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('materialadminCtrl', function($timeout, $state, $scope, growlService,AclService, $rootScope,$http,$window,localStorageService){
        //Welcome Message
      //  growlService.growl('Welcome back We TrackOn!', 'inverse')
        $rootScope.BaseUrl = "http://localhost:4801/apiv1/";
        
        // Detact Mobile Browser
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');
        
        // For Mainmenu Active Class
        this.$state = $state;    
        
        //Close sidebar on click
        this.sidebarStat = function(event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        }
        
        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;
        
        this.lvSearch = function() {
            this.listviewSearchStat = true; 
        }
        
        //Listview menu toggle in small screens
        this.lvMenuStat = false;
        
        //Blog
        this.wallCommenting = [];
        
        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'blue';

        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
        ]

        this.skinSwitch = function (color) {
            this.currentSkin = color;
        }

    //***********Checking Session***********//
    $rootScope.sessionCheck = function(){
        localStorageService.clearAll();
        swal({
                title: "Session Expired",
                text: "You will be Redirected to Login Page",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Please, Login!",
                closeOnConfirm: false
            }, function() {
                localStorageService.clearAll();
                sessionStorage.clear();
                $window.location.href = "/";
            });
        }

    //***********To get user complete details from localstorage***********//
        $rootScope.userCmpltDtls = localStorageService.get('userDtls');

    })


    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', function($timeout,$rootScope, messageService,$http,$scope,$window,localStorageService){
        $scope.userCmpltDtlsLst = $rootScope.userCmpltDtls;
        // Top Search
        this.openSearch = function(){
            angular.element('#header').addClass('search-toggled');
            angular.element('#top-search-wrap').find('input').focus();
        }

        this.closeSearch = function(){
            angular.element('#header').removeClass('search-toggled');
        }
        
        // Get messages and notification for header
        this.img = messageService.img;
        this.user = messageService.user;
        this.user = messageService.text;

        this.messageResult = messageService.getMessage(this.img, this.user, this.text);


        //Clear Notification
        this.clearNotification = function($event) {
            $event.preventDefault();
            
            var x = angular.element($event.target).closest('.listview');
            var y = x.find('.lv-item');
            var z = y.size();
            
            angular.element($event.target).parent().fadeOut();
            
            x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
            x.find('.grid-loading').fadeIn(1500);
            var w = 0;
            
            y.each(function(){
                var z = $(this);
                $timeout(function(){
                    z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                        z.remove();
                    });
                }, w+=150);
            })
            
            $timeout(function(){
                angular.element('#notifications').addClass('empty');
            }, (z*150)+200);
        }
        
        // Clear Local Storage
        this.clearLocalStorage = function() {
            
            //Get confirmation, if confirmed clear the localStorage
            swal({   
                title: "Are you sure?",   
                text: "All your saved localStorage values will be removed",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#F44336",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                localStorage.clear();
                swal("Done!", "localStorage is cleared", "success"); 
            });
            
        }

        // Logout
        this.logout = function() {
            //Get confirmation, if confirmed clear the localStorage
            
            swal({
              title: "Are you sure?",
              text: "You will be Redirected to Login Page",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#F44336",
              confirmButtonText: "Yes, Logout!",
              closeOnConfirm: false
            }, function(){
              $http.get("../../apiv1/logout")
                .success(function() {
                    localStorageService.clearAll();
                    sessionStorage.clear();
                    $window.location.href = "/";
                })
                .error(function() {
                    $scope.shloading = false;
                    $scope.err_msg = "Unable to connect to server. Please check your internet connectivity.";
                });
              });

          }
        
        //Fullscreen View
        this.fullScreen = function() {
            //Launch
            function launchIntoFullscreen(element) {
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
        }
    
    })

//==========================================================================
//appstore controller
//==========================================================================
.controller('appStoreCtrl', function($scope,localStorageService){
    $scope.appslst = localStorageService.get('appsLst');
})

//==========================================================================
//MUN DASHBOARD
//==========================================================================
.controller('munDashCtrl', function($scope,$window,$http,$rootScope,$stateParams){

               
$http.get($rootScope.BaseUrl + 'totVehStsCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting total vehicles status for chart");
                $scope.totVehStsDtl = resdata.data[0];
                // console.log($scope.totVehStsDtl);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })

$http.get($rootScope.BaseUrl + 'totTrpDistCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting total vehicles status for chart");
                $scope.totTrpDistCntDtl = resdata.data[0];
                // console.log($scope.totTrpDistCntDtl);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })

$http.get($rootScope.BaseUrl + 'totColPntsCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting total vehicles status for chart");
                $scope.totColPntsCntDtl = resdata.data[0];
                // console.log($scope.totColPntsCntDtl);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })

$http.get($rootScope.BaseUrl + 'totVehCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting total vehicles for chart");
                $scope.totVehCntDtl = resdata.data[0];
                // console.log($scope.totVehDtl);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })


            


/**Code for first chart**/
$http.get($rootScope.BaseUrl + 'vehStsCnts')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting vehicle status counts for chart");
                $scope.vehStatusDtl = resdata.data;
                // console.log($scope.vehStatusDtl);
                $scope.vehStsCntDtls();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
$scope.vehStsCntDtls=function(){
var vehTyp_chart1=[];
for(var i=0;i<=$scope.vehStatusDtl.length-1;i++){
    vehTyp_chart1.push($scope.vehStatusDtl[i].dt)
}
$scope.options1={
  "chart": {
    "type": "multiBarChart",
    "height": 450,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 45,
      "left": 45
    },
    
    "clipEdge": true,
    "duration": 500,
    "stacked": false,
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      tickFormat: function(d){
                    return vehTyp_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Early Delay Values",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    },
    legend: {   
        width:400,
        height:20,
        padding:32,
        margin: {   
            top:1,
            right:0,
            bottom:10,
            left:0
            }
        },
    callback: function(chart){
        chart.multibar.dispatch.on('elementClick', function(e){
                    $window.location.href="#/vts/rpt/trips_report";
        })
    
    }
  }
}
$scope.dataset1= generatedata1();
function generatedata1(){
    var Early=[], Delay=[];
    for(var i=0;i<=$scope.vehStatusDtl.length-1; i++){
        Early.push({x:i,y:$scope.vehStatusDtl[i].Early});
        Delay.push({x:i,y:$scope.vehStatusDtl[i].Delay});
    }
    var dataArray = [
        {
            values: Early,      //values - represents the array of {x,y} data points
            key: "Early", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        },
        {
            values: Delay,
            key: "Delay",
            color: '#01848f'
        }
    ];
    return dataArray;
}
}

/**Code for second chart**/
 $http.get($rootScope.BaseUrl + 'dywseDstnsCnt')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting landmarks list for chart");
                $scope.dstnsCvrd = resdata.data;
                // console.log($scope.dstnsCvrd);
                $scope.dywiseDstnsCvrd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

$scope.dywiseDstnsCvrd=function(){         
var vehTyp_chart2=[];
for(var i=0;i<=$scope.dstnsCvrd.length-1;i++){
    vehTyp_chart2.push($scope.dstnsCvrd[i].asrt_ctgry_nm)
}
$scope.options2={
  "chart": {
    "type": "multiBarChart",
    "height": 450,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 45,
      "left": 45
    },
    
    "clipEdge": true,
    "duration": 500,
    "stacked": false,
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      tickFormat: function (d){
                        return vehTyp_chart2[d];
                    }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
                        return d3.format(',0f')(d);
                    }
    },
    callback: function(chart){
        chart.multibar.dispatch.on('elementClick', function(e){
                    $window.location.href="#/vts/rpt/distance_report";
        })
    }
  }
}

$scope.dataset2= generatedata2();
function generatedata2(){
    var dist_ct1=[], dist_ct2=[];
    for(var i=0;i<=$scope.dstnsCvrd.length-1; i++){
        dist_ct1.push({x:i,y:$scope.dstnsCvrd[i].dist_ct1});
        dist_ct2.push({x:i,y:$scope.dstnsCvrd[i].dist_ct2});
    }
    var dataArray = [
        {
            values: dist_ct2,
            key: "Yesterday",
            color: '#67819d'
        },
        {
            values: dist_ct1,      //values - represents the array of {x,y} data points
            key: "Today" ,          //key  - the name of the series.
            color: '#fa6541'
        }
    ];

    return dataArray;
}
}

$http.get($rootScope.BaseUrl + 'lndmrkCovChrtLst')
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting landmarks list for chart");
                $scope.lndmrkcovchrtlst = resdata.data;
                // console.log($scope.lndmrkcovchrtlst);
                $scope.lndMrkCovDtls();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });

$scope.lndMrkCovDtls=function(){         
var vehTyp_chart4=[];
for(var i=0;i<=$scope.lndmrkcovchrtlst.length-1;i++){
    vehTyp_chart4.push($scope.lndmrkcovchrtlst[i].fdate)
}
$scope.options4={
  "chart": {
    "type": "multiBarChart",
    "height": 450,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 45,
      "left": 45
    },
    
    "clipEdge": true,
    "duration": 500,
    "stacked": true,
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      tickFormat: function (d){
                        return vehTyp_chart4[d];
                    }
    },
    "yAxis": {
      "axisLabel": "Landmarks Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
                        return d3.format(',0f')(d);
                    }
    },
    callback: function(chart){
        chart.multibar.dispatch.on('elementClick', function(e){
                    $window.location.href="#/vts/rpt/landmark_inout_report";
        })
    }
  }
}

$scope.dataset4= generatedata4();
function generatedata4(){
    var fnce_covered=[], fnce_uncovered=[];
    for(var i=0;i<=$scope.lndmrkcovchrtlst.length-1; i++){
        fnce_covered.push({x:i,y:$scope.lndmrkcovchrtlst[i].fnce_covered});
        fnce_uncovered.push({x:i,y:$scope.lndmrkcovchrtlst[i].fnce_uncovered});
    }
    var dataArray = [
        {
            values: fnce_covered,      //values - represents the array of {x,y} data points
            key: "Covered_Landmarks", //key  - the name of the series.
            color:"#99be3b"

        },
        {
            values: fnce_uncovered,
            key: "Uncovered_LandMarks",
            color:"#013e7f"
        }
    ];
    return dataArray;
}
}
})

.controller('santationDashCtrl', function($scope, $window,$http,$rootScope){

$http.get($rootScope.BaseUrl + 'mcrtotVehCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting total vehicles for chart");
                $scope.totVehDtlMcr = resdata.data[0];
                // console.log($scope.totVehDtl);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'shtotVehCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting total vehicles for chart");
                $scope.totVehDtlShah = resdata.data[0];
                // console.log($scope.totVehDtl);
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'mcrEchVehCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.echVehDtlMcr = resdata.data;

                for (var i = 0; i < $scope.echVehDtlMcr.length; i++) {                   
                    if($scope.echVehDtlMcr[i].asrt_ctgry_id=='1'){
                        $scope.mcrtrctscnt=$scope.echVehDtlMcr[i];
                    }
                    if($scope.echVehDtlMcr[i].asrt_ctgry_id=='4'){
                        $scope.mcrdmprscnt=$scope.echVehDtlMcr[i];
                    }
                    if($scope.echVehDtlMcr[i].asrt_ctgry_id=='5'){
                        $scope.mcrtipprscnt=$scope.echVehDtlMcr[i];
                    }
                };

            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'shEchVehCnt')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.echVehDtlShah = resdata.data;
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            }) 
$http.get($rootScope.BaseUrl + 'totTrctsDstTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totTrctsDstTrvldDtl = resdata.data;
                // console.log($scope.totTrctsDstTrvldDtl);

                for(var i=0;i < $scope.totTrctsDstTrvldDtl.length;i++) {

                    if($scope.totTrctsDstTrvldDtl[i].asrt_ctgry_id == '1' && $scope.totTrctsDstTrvldDtl[i].asrt_grp_id == '8') {
                        $scope.tractorDshBrd = $scope.totTrctsDstTrvldDtl[i];
                    }
                    if($scope.totTrctsDstTrvldDtl[i].asrt_ctgry_id == '5' && $scope.totTrctsDstTrvldDtl[i].asrt_grp_id == '8') {
                        $scope.tipperDshBrd = $scope.totTrctsDstTrvldDtl[i];
                    }

                    if($scope.totTrctsDstTrvldDtl[i].asrt_ctgry_id == '4' && $scope.totTrctsDstTrvldDtl[i].asrt_grp_id == '8') {
                        $scope.DumperDshBrd = $scope.totTrctsDstTrvldDtl[i];
                    }
                    if($scope.totTrctsDstTrvldDtl[i].asrt_ctgry_id == '1' && $scope.totTrctsDstTrvldDtl[i].asrt_grp_id == '14') {
                        $scope.shahtractorDshBrd = $scope.totTrctsDstTrvldDtl[i];
                    }
                    if($scope.totTrctsDstTrvldDtl[i].asrt_ctgry_id == '5' && $scope.totTrctsDstTrvldDtl[i].asrt_grp_id == '14') {
                        $scope.shahtipperDshBrd = $scope.totTrctsDstTrvldDtl[i];
                    }
                }
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'totDstCrDyTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totDstCrDyTrvldDtl = resdata.data;
                // console.log($scope.totDstCrDyTrvld);
                
                for(var i=0;i < $scope.totDstCrDyTrvldDtl.length;i++) {

                    if($scope.totDstCrDyTrvldDtl[i].asrt_ctgry_id == '1' && $scope.totDstCrDyTrvldDtl[i].asrt_grp_id == '8') {
                        $scope.tractorDyDshBrd = $scope.totDstCrDyTrvldDtl[i];
                    }
                    if($scope.totDstCrDyTrvldDtl[i].asrt_ctgry_id == '5' && $scope.totDstCrDyTrvldDtl[i].asrt_grp_id == '8') {
                        $scope.tipperDyDshBrd = $scope.totDstCrDyTrvldDtl[i];
                    }

                    if($scope.totDstCrDyTrvldDtl[i].asrt_ctgry_id == '4' && $scope.totDstCrDyTrvldDtl[i].asrt_grp_id == '8') {
                        $scope.DumperDyDshBrd = $scope.totDstCrDyTrvldDtl[i];
                    }
                    if($scope.totDstCrDyTrvldDtl[i].asrt_ctgry_id == '1' && $scope.totDstCrDyTrvldDtl[i].asrt_grp_id == '14') {
                        $scope.shahtractorDyDshBrd = $scope.totDstCrDyTrvldDtl[i];
                    }
                    if($scope.totDstCrDyTrvldDtl[i].asrt_ctgry_id == '5' && $scope.totDstCrDyTrvldDtl[i].asrt_grp_id == '14') {
                        $scope.shahtipperDyDshBrd = $scope.totDstCrDyTrvldDtl[i];
                    }
                }
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'totFnceCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totFnceCovdDtl = resdata.data;
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            }) 
$http.get($rootScope.BaseUrl + 'totFnceDyCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totFnceDyCovdDtl = resdata.data;
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })  
$http.get($rootScope.BaseUrl + 'totTripsCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totTripsCovdDtl = resdata.data;

                for(var i=0;i < $scope.totTripsCovdDtl.length;i++) {

                    if($scope.totTripsCovdDtl[i].asrt_ctgry_id == '1' && $scope.totTripsCovdDtl[i].asrt_grp_id == '8') {
                        $scope.tractortrpsDshBrd = $scope.totTripsCovdDtl[i];
                    }
                    if($scope.totTripsCovdDtl[i].asrt_ctgry_id == '5' && $scope.totTripsCovdDtl[i].asrt_grp_id == '8') {
                        $scope.tippertrpsDshBrd = $scope.totTripsCovdDtl[i];
                    }

                    if($scope.totTripsCovdDtl[i].asrt_ctgry_id == '4' && $scope.totTripsCovdDtl[i].asrt_grp_id == '8') {
                        $scope.dumpertrpsDshBrd = $scope.totTripsCovdDtl[i];
                    }
                    if($scope.totTripsCovdDtl[i].asrt_ctgry_id == '1' && $scope.totTripsCovdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtractortrpsDshBrd = $scope.totTripsCovdDtl[i];
                    }
                    if($scope.totTripsCovdDtl[i].asrt_ctgry_id == '5' && $scope.totTripsCovdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtippertrpsDshBrd = $scope.totTripsCovdDtl[i];
                    }
                }
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'totFnceBnsCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totFnceBnsCovdDtl = resdata.data;
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })  

$http.get($rootScope.BaseUrl + 'totFnceBnsDyCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totFnceBnsDyCovdDtl = resdata.data;
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })  

$http.get($rootScope.BaseUrl + 'totTripsDyCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totTripsDyCovdDtl = resdata.data;

                for(var i=0;i < $scope.totTripsDyCovdDtl.length;i++) {

                    if($scope.totTripsDyCovdDtl[i].asrt_ctgry_id == '1' && $scope.totTripsDyCovdDtl[i].asrt_grp_id == '8') {
                        $scope.tractorTrpsDyDshBrd = $scope.totTripsDyCovdDtl[i];
                    }
                    if($scope.totTripsDyCovdDtl[i].asrt_ctgry_id == '5' && $scope.totTripsDyCovdDtl[i].asrt_grp_id == '8') {
                        $scope.tipperTrpsDyDshBrd = $scope.totTripsDyCovdDtl[i];
                    }

                    if($scope.totTripsDyCovdDtl[i].asrt_ctgry_id == '4' && $scope.totTripsDyCovdDtl[i].asrt_grp_id == '8') {
                        $scope.dumperTrpsDyDshBrd = $scope.totTripsDyCovdDtl[i];
                    }
                    if($scope.totTripsDyCovdDtl[i].asrt_ctgry_id == '1' && $scope.totTripsDyCovdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtractorTrpsDyDshBrd = $scope.totTripsDyCovdDtl[i];
                    }
                    if($scope.totTripsDyCovdDtl[i].asrt_ctgry_id == '5' && $scope.totTripsDyCovdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtipperTrpsDyDshBrd = $scope.totTripsDyCovdDtl[i];
                    }
                }
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'totCapctyLftd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totCapctyLftdDtl = resdata.data;
                // console.log($scope.totCapctyLftdDtl);
                for(var i=0;i < $scope.totCapctyLftdDtl.length;i++) {

                    if($scope.totCapctyLftdDtl[i].asrt_ctgry_id == '1' && $scope.totCapctyLftdDtl[i].asrt_grp_id == '8') {
                        $scope.tractorCptyDshBrd = $scope.totCapctyLftdDtl[i];
                    }
                    if($scope.totCapctyLftdDtl[i].asrt_ctgry_id == '5' && $scope.totCapctyLftdDtl[i].asrt_grp_id == '8') {
                        $scope.tipperCptyDshBrd = $scope.totCapctyLftdDtl[i];
                    }

                    if($scope.totCapctyLftdDtl[i].asrt_ctgry_id == '4' && $scope.totCapctyLftdDtl[i].asrt_grp_id == '8') {
                        $scope.dumperCptyDshBrd = $scope.totCapctyLftdDtl[i];
                    }
                    if($scope.totCapctyLftdDtl[i].asrt_ctgry_id == '1' && $scope.totCapctyLftdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtractorCptyDshBrd = $scope.totCapctyLftdDtl[i];
                    }
                    if($scope.totCapctyLftdDtl[i].asrt_ctgry_id == '5' && $scope.totCapctyLftdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtipperCptyDshBrd = $scope.totCapctyLftdDtl[i];
                    }
                }
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })
$http.get($rootScope.BaseUrl + 'totCapctyDyLftd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.totCapctyDyLftdDtl = resdata.data;

                for(var i=0;i < $scope.totCapctyDyLftdDtl.length;i++) {

                    if($scope.totCapctyDyLftdDtl[i].asrt_ctgry_id == '1' && $scope.totCapctyDyLftdDtl[i].asrt_grp_id == '8') {
                        $scope.tractorCptyDyDshBrd = $scope.totCapctyDyLftdDtl[i];
                    }
                    if($scope.totCapctyDyLftdDtl[i].asrt_ctgry_id == '5' && $scope.totCapctyDyLftdDtl[i].asrt_grp_id == '8') {
                        $scope.tipperCptyDyDshBrd = $scope.totCapctyDyLftdDtl[i];
                    }

                    if($scope.totCapctyDyLftdDtl[i].asrt_ctgry_id == '4' && $scope.totCapctyDyLftdDtl[i].asrt_grp_id == '8') {
                        $scope.dumperCptyDyDshBrd = $scope.totCapctyDyLftdDtl[i];
                    }
                    if($scope.totCapctyDyLftdDtl[i].asrt_ctgry_id == '1' && $scope.totCapctyDyLftdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtractorCptyDyDshBrd = $scope.totCapctyDyLftdDtl[i];
                    }
                    if($scope.totCapctyDyLftdDtl[i].asrt_ctgry_id == '5' && $scope.totCapctyDyLftdDtl[i].asrt_grp_id == '14') {
                        $scope.shahtipperCptyDyDshBrd = $scope.totCapctyDyLftdDtl[i];
                    }
                }
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            })  

        $scope.cancel = function() {
          $scope.showModal = false;
        };
    $scope.mcrTrctsCntDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrVehsDistTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrVehsDistTrvldDtl = resdata.data;
                generatedata1();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata1(){
    var dst=[];
    for(var i=0;i<=$scope.mcrVehsDistTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.mcrVehsDistTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset1=  dataArray;
    var distTyp_chart1=[];
for(var i=0;i<=$scope.mcrVehsDistTrvldDtl.length-1;i++){
    distTyp_chart1.push($scope.mcrVehsDistTrvldDtl[i].trvl_dt)
}
$scope.options1={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    },
    legend: {   
        width:400,
        height:20,
        padding:32,
        margin: {   
            top:1,
            right:0,
            bottom:10,
            left:0
            }
        }
  }
}
}

  $scope.mcrTipprsCntDtls=function(){
    /**popup for snd chart**/
$http.get($rootScope.BaseUrl + 'mcrVehsTiprDistTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting vehicle status counts for chart");
                $scope.mcrTipprsDistTrvldDtl = resdata.data;
                generatedata2();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata2(){
     var dst=[];
    for(var i=0;i<=$scope.mcrTipprsDistTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.mcrTipprsDistTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset2=  dataArray;
    var distTyp_chart2=[];
for(var i=0;i<=$scope.mcrTipprsDistTrvldDtl.length-1;i++){
    distTyp_chart2.push($scope.mcrTipprsDistTrvldDtl[i].trvl_dt)
}
$scope.options2={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart2[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

 $scope.mcrDmprsCntDtls=function(){
    /**popup for snd chart**/
$http.get($rootScope.BaseUrl + 'mcrVehsDmprsDistTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrDmprsDistTrvldDtl = resdata.data;
                generatedata3();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata3(){
   var dst=[];
    for(var i=0;i<=$scope.mcrDmprsDistTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.mcrDmprsDistTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset3=  dataArray;
    var distTyp_chart3=[];
for(var i=0;i<=$scope.mcrDmprsDistTrvldDtl.length-1;i++){
    distTyp_chart3.push($scope.mcrDmprsDistTrvldDtl[i].trvl_dt)
}
$scope.options3={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart3[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrTrctsTripsCntDtls=function(){
    /**popup for mcr trips chart**/
$http.get($rootScope.BaseUrl + 'mcrTrctsTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting vehicle status counts for chart");
                $scope.mcrTrctsTripsTrvldDtl = resdata.data;
                generatedata4();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata4(){
    var trps_ct=[];
    for(var i=0;i<=$scope.mcrTrctsTripsTrvldDtl.length-1; i++){
        trps_ct.push({x:i,y:$scope.mcrTrctsTripsTrvldDtl[i].trps_ct});
    }
    var dataArray = [
        {
            values: trps_ct,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset4=  dataArray;
    var distTyp_chart4=[];
for(var i=0;i<=$scope.mcrTrctsTripsTrvldDtl.length-1;i++){
    distTyp_chart4.push($scope.mcrTrctsTripsTrvldDtl[i].trp_dt)
}
$scope.options4={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart4[d]
                }
    },
    "yAxis": {
      "axisLabel": "Trips Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrTipprsTripsCntDtls=function(){
    /**popup for mcr trips chart**/
$http.get($rootScope.BaseUrl + 'mcrTipprsTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrTipprsTripsTrvldDtl = resdata.data;
                generatedata5();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata5(){
    var trps_ct=[];
    for(var i=0;i<=$scope.mcrTipprsTripsTrvldDtl.length-1; i++){
        trps_ct.push({x:i,y:$scope.mcrTipprsTripsTrvldDtl[i].trps_ct});
    }
    var dataArray = [
        {
            values: trps_ct,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset5=  dataArray;
    var distTyp_chart5=[];
for(var i=0;i<=$scope.mcrTipprsTripsTrvldDtl.length-1;i++){
    distTyp_chart5.push($scope.mcrTipprsTripsTrvldDtl[i].trp_dt)
}
$scope.options5={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart5[d]
                }
    },
    "yAxis": {
      "axisLabel": "Trips Count",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrColPntsCntDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrColPntsCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrColPntsCntDtl = resdata.data;
                generatedata6();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata6(){
    var fnce_covered=[];
    for(var i=0;i<=$scope.mcrColPntsCntDtl.length-1; i++){
        fnce_covered.push({x:i,y:$scope.mcrColPntsCntDtl[i].fnce_covered});
    }
    var dataArray = [
        {
            values: fnce_covered,      //values - represents the array of {x,y} data points
            key: "Fence Covered", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset6=  dataArray;
    var distTyp_chart6=[];
for(var i=0;i<=$scope.mcrColPntsCntDtl.length-1;i++){
    distTyp_chart6.push($scope.mcrColPntsCntDtl[i].dt)
}
$scope.options6={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart6[d]
                }
    },
    "yAxis": {
      "axisLabel": "CollectionPoints Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrDmprBnsCntDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrDmprBnsCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrDmprBnsCovdCntDtl = resdata.data;
                generatedata7();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata7(){
    var fnce_covered=[];
    for(var i=0;i<=$scope.mcrDmprBnsCovdCntDtl.length-1; i++){
        fnce_covered.push({x:i,y:$scope.mcrDmprBnsCovdCntDtl[i].fnce_covered});
    }
    var dataArray = [
        {
            values: fnce_covered,      //values - represents the array of {x,y} data points
            key: "Fence Covered", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset7=  dataArray;
    var distTyp_chart7=[];
for(var i=0;i<=$scope.mcrDmprBnsCovdCntDtl.length-1;i++){
    distTyp_chart7.push($scope.mcrDmprBnsCovdCntDtl[i].dt)
}
$scope.options7={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart7[d]
                }
    },
    "yAxis": {
      "axisLabel": "CollectionPoints Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrTrctsCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrTrctsCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrTrctsCptyDtl = resdata.data;
                generatedata8();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}
function generatedata8(){
    var cp_cnt=[];
    for(var i=0;i<=$scope.mcrTrctsCptyDtl.length-1; i++){
        cp_cnt.push({x:i,y:$scope.mcrTrctsCptyDtl[i].cp_cnt});
    }
    var dataArray = [
        {
            values: cp_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset8=  dataArray;
    var distTyp_chart8=[];
for(var i=0;i<=$scope.mcrTrctsCptyDtl.length-1;i++){
    distTyp_chart8.push($scope.mcrTrctsCptyDtl[i].trp_dt)
}
$scope.options8={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart8[d]
                }
    },
    "yAxis": {
      "axisLabel": "TrashDumped InTons",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrTipprsCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrTipprsCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrTipprsCptyDtl = resdata.data;
                generatedata9();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}
function generatedata9(){
    var cp_cnt=[];
    for(var i=0;i<=$scope.mcrTipprsCptyDtl.length-1; i++){
        cp_cnt.push({x:i,y:$scope.mcrTipprsCptyDtl[i].cp_cnt});
    }
    var dataArray = [
        {
            values: cp_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset9=  dataArray;
    var distTyp_chart9=[];
for(var i=0;i<=$scope.mcrTipprsCptyDtl.length-1;i++){
    distTyp_chart9.push($scope.mcrTipprsCptyDtl[i].trp_dt)
}
$scope.options9={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart9[d]
                }
    },
    "yAxis": {
      "axisLabel": "TrashDumped InTons",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    },
  }
}
}

$scope.mcrDmprsTripsCntDtls=function(){
    /**popup for mcr trips chart**/
$http.get($rootScope.BaseUrl + 'mcrDmprsTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrDmprsTripsTrvldDtl = resdata.data;
                generatedata10();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata10(){
    var trps_ct=[];
    for(var i=0;i<=$scope.mcrDmprsTripsTrvldDtl.length-1; i++){
        trps_ct.push({x:i,y:$scope.mcrDmprsTripsTrvldDtl[i].trps_ct});
    }
    var dataArray = [
        {
            values: trps_ct,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset10=  dataArray;
    var distTyp_chart10=[];
for(var i=0;i<=$scope.mcrDmprsTripsTrvldDtl.length-1;i++){
    distTyp_chart10.push($scope.mcrDmprsTripsTrvldDtl[i].trp_dt)
}
$scope.options10={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart10[d]
                }
    },
    "yAxis": {
      "axisLabel": "Trips Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrDmprsCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrDmprsCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrDmprsCptyDtl = resdata.data;
                generatedata11();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}
function generatedata11(){
    var cp_cnt=[];
    for(var i=0;i<=$scope.mcrDmprsCptyDtl.length-1; i++){
        cp_cnt.push({x:i,y:$scope.mcrDmprsCptyDtl[i].cp_cnt});
    }
    var dataArray = [
        {
            values: cp_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset11=  dataArray;
    var distTyp_chart11=[];
for(var i=0;i<=$scope.mcrDmprsCptyDtl.length-1;i++){
    distTyp_chart11.push($scope.mcrDmprsCptyDtl[i].trp_dt)
}
$scope.options11={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart11[d]
                }
    },
    "yAxis": {
      "axisLabel": "TrashDumped InTons",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hirdTrctsCntDtls=function(){
    /**popup for shah dist chart**/
$http.get($rootScope.BaseUrl + 'hirdTrctsDistTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting vehicle status counts for chart");
                $scope.hirdTrctsDistTrvldDtl = resdata.data;
                generatedata12();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata12(){
    var dst=[];
    for(var i=0;i<=$scope.hirdTrctsDistTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.hirdTrctsDistTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset12=  dataArray;
    var distTyp_chart12=[];
for(var i=0;i<=$scope.hirdTrctsDistTrvldDtl.length-1;i++){
    distTyp_chart12.push($scope.hirdTrctsDistTrvldDtl[i].trvl_dt)
}
$scope.options12={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart12[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hirdTipprsCntDtls=function(){
    /**popup for shah dist chart**/
$http.get($rootScope.BaseUrl + 'hirdTipprsDistTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting vehicle status counts for chart");
                $scope.hirdTipprsDistTrvldDtl = resdata.data;
                generatedata13();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata13(){
   var dst=[];
    for(var i=0;i<=$scope.hirdTipprsDistTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.hirdTipprsDistTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset13=  dataArray;
    var distTyp_chart13=[];
for(var i=0;i<=$scope.hirdTipprsDistTrvldDtl.length-1;i++){
    distTyp_chart13.push($scope.hirdTipprsDistTrvldDtl[i].trvl_dt)
}
$scope.options13={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart13[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hirdTrctsTripsCntDtls=function(){
    /**popup for shah trips chart**/
$http.get($rootScope.BaseUrl + 'hirdTrctsTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.hirdTrctsTripsTrvldDtl = resdata.data;
                generatedata14();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata14(){
    var trps_ct=[];
    for(var i=0;i<=$scope.hirdTrctsTripsTrvldDtl.length-1; i++){
        trps_ct.push({x:i,y:$scope.hirdTrctsTripsTrvldDtl[i].trps_ct});
    }
    var dataArray = [
        {
            values: trps_ct,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset14=  dataArray;
    var distTyp_chart14=[];
for(var i=0;i<=$scope.hirdTrctsTripsTrvldDtl.length-1;i++){
    distTyp_chart14.push($scope.hirdTrctsTripsTrvldDtl[i].trp_dt)
}
$scope.options14={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart14[d]
                }
    },
    "yAxis": {
      "axisLabel": "Trips Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hirdTrctsCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'hirdTrctsCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.hirdTrctsCptyDtl = resdata.data;
                generatedata15();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}
function generatedata15(){
    var cp_cnt=[];
    for(var i=0;i<=$scope.hirdTrctsCptyDtl.length-1; i++){
        cp_cnt.push({x:i,y:$scope.hirdTrctsCptyDtl[i].cp_cnt});
    }
    var dataArray = [
        {
            values: cp_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset15=  dataArray;
    var distTyp_chart15=[];
for(var i=0;i<=$scope.hirdTrctsCptyDtl.length-1;i++){
    distTyp_chart15.push($scope.hirdTrctsCptyDtl[i].trp_dt)
}
$scope.options15={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart15[d]
                }
    },
    "yAxis": {
      "axisLabel": "TrashDumped InTons",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hirdTipprsTripsCntDtls=function(){
    /**popup for shah trips chart**/
$http.get($rootScope.BaseUrl + 'hirdTipprsTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.hirdTipprsTripsTrvldDtl = resdata.data;
                generatedata16();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata16(){
    var trps_ct=[];
    for(var i=0;i<=$scope.hirdTipprsTripsTrvldDtl.length-1; i++){
        trps_ct.push({x:i,y:$scope.hirdTipprsTripsTrvldDtl[i].trps_ct});
    }
    var dataArray = [
        {
            values: trps_ct,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset16=  dataArray;
    var distTyp_chart16=[];
for(var i=0;i<=$scope.hirdTipprsTripsTrvldDtl.length-1;i++){
    distTyp_chart16.push($scope.hirdTipprsTripsTrvldDtl[i].trp_dt)
}
$scope.options16={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart16[d]
                }
    },
    "yAxis": {
      "axisLabel": "Trips Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hrdColPntsCntDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'hrdColPntsCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.hrdColPntsCovdCntDtl = resdata.data;
                generatedata17();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedata17(){
    var fnce_covered=[];
    for(var i=0;i<=$scope.hrdColPntsCovdCntDtl.length-1; i++){
        fnce_covered.push({x:i,y:$scope.hrdColPntsCovdCntDtl[i].fnce_covered});
    }
    var dataArray = [
        {
            values: fnce_covered,      //values - represents the array of {x,y} data points
            key: "Fence Covered", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset17=  dataArray;
    var distTyp_chart17=[];
for(var i=0;i<=$scope.hrdColPntsCovdCntDtl.length-1;i++){
    distTyp_chart17.push($scope.hrdColPntsCovdCntDtl[i].dt)
}
$scope.options17={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart17[d]
                }
    },
    "yAxis": {
      "axisLabel": "CollectionPoints Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.hirdTipprsCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'hirdTipprsCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.hirdTipprsCptyDtl = resdata.data;
                generatedata18();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}
function generatedata18(){
    var cp_cnt=[];
    for(var i=0;i<=$scope.hirdTipprsCptyDtl.length-1; i++){
        cp_cnt.push({x:i,y:$scope.hirdTipprsCptyDtl[i].cp_cnt});
    }
    var dataArray = [
        {
            values: cp_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.dataset18=  dataArray;
    var distTyp_chart18=[];
for(var i=0;i<=$scope.hirdTipprsCptyDtl.length-1;i++){
    distTyp_chart18.push($scope.hirdTipprsCptyDtl[i].trp_dt)
}
$scope.options18={
  "chart": {
    "type": "discreteBarChart",
    "height": 450,
    "width":350,
    "margin": {
      "top": 30,
      "right": 20,
      "bottom": 60,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Dates",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return distTyp_chart18[d]
                }
    },
    "yAxis": {
      "axisLabel": "TrashDumped InTons",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchTrctDstDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchTrctDstTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchTrctDstTrvldDtl = resdata.data;
                generatedatamcrEchTrctDst();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchTrctDst(){
    var dst=[];
    for(var i=0;i<=$scope.mcrEchTrctDstTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.mcrEchTrctDstTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchTrctDst=  dataArray;
    var mcrEchTrctDst_chart1=[];
for(var i=0;i<=$scope.mcrEchTrctDstTrvldDtl.length-1;i++){
    mcrEchTrctDst_chart1.push($scope.mcrEchTrctDstTrvldDtl[i].asrt_nm)
}
$scope.mcrEchTrctDstoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return mcrEchTrctDst_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchTipprDstDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchTipprDstTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchTipprDstTrvldDtl = resdata.data;
                generatedatamcrEchTipprDst();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchTipprDst(){
    var dst=[];
    for(var i=0;i<=$scope.mcrEchTipprDstTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.mcrEchTipprDstTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchTipprDst=  dataArray;
    var mcrEchTipprDst_chart1=[];
for(var i=0;i<=$scope.mcrEchTipprDstTrvldDtl.length-1;i++){
    mcrEchTipprDst_chart1.push($scope.mcrEchTipprDstTrvldDtl[i].asrt_nm)
}
$scope.mcrEchTipprDstoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return mcrEchTipprDst_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchDmprDstDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchDmprDstTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchDmprDstTrvldDtl = resdata.data;
                generatedatamcrEchDmprDst();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchDmprDst(){
    var dst=[];
    for(var i=0;i<=$scope.mcrEchDmprDstTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.mcrEchDmprDstTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchDmprDst=  dataArray;
    var mcrEchDmprDst_chart1=[];
for(var i=0;i<=$scope.mcrEchDmprDstTrvldDtl.length-1;i++){
    mcrEchDmprDst_chart1.push($scope.mcrEchDmprDstTrvldDtl[i].asrt_nm)
}
$scope.mcrEchDmprDstoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return mcrEchDmprDst_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchTrctTripsDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchTrctTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchTrctTripsTrvldDtl = resdata.data;
                generatedatamcrEchTrctTrips();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchTrctTrips(){
    var trps_cnt=[];
    for(var i=0;i<=$scope.mcrEchTrctTripsTrvldDtl.length-1; i++){
        trps_cnt.push({x:i,y:$scope.mcrEchTrctTripsTrvldDtl[i].trps_cnt});
    }
    var dataArray = [
        {
            values: trps_cnt,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchTrips=  dataArray;
    var mcrEchTrctTrips_chart1=[];
for(var i=0;i<=$scope.mcrEchTrctTripsTrvldDtl.length-1;i++){
    mcrEchTrctTrips_chart1.push($scope.mcrEchTrctTripsTrvldDtl[i].asrt_nm)
}
$scope.mcrEchTrctTripsoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return mcrEchTrctTrips_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchTipprTripsDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchTipprTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchTipprTripsTrvldDtl = resdata.data;
                generatedatamcrEchTipprTrips();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchTipprTrips(){
    var trps_cnt=[];
    for(var i=0;i<=$scope.mcrEchTipprTripsTrvldDtl.length-1; i++){
        trps_cnt.push({x:i,y:$scope.mcrEchTipprTripsTrvldDtl[i].trps_cnt});
    }
    var dataArray = [
        {
            values: trps_cnt,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchTipprTrips=  dataArray;
    var mcrEchTipprTrips_chart1=[];
for(var i=0;i<=$scope.mcrEchTipprTripsTrvldDtl.length-1;i++){
    mcrEchTipprTrips_chart1.push($scope.mcrEchTipprTripsTrvldDtl[i].asrt_nm)
}
$scope.mcrEchTipprTripsoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return mcrEchTipprTrips_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchDmprTripsDtls=function(){
    
$http.get($rootScope.BaseUrl + 'mcrEchDmprTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchDmprTripsTrvldDtl = resdata.data;
                generatedatamcrEchDmprTrips();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchDmprTrips(){
    var trps_cnt=[];
    for(var i=0;i<=$scope.mcrEchDmprTripsTrvldDtl.length-1; i++){
        trps_cnt.push({x:i,y:$scope.mcrEchDmprTripsTrvldDtl[i].trps_cnt});
    }
    var dataArray = [
        {
            values: trps_cnt,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchDmprTrips=  dataArray;
    var mcrEchDmprTrips_chart1=[];
for(var i=0;i<=$scope.mcrEchDmprTripsTrvldDtl.length-1;i++){
    mcrEchDmprTrips_chart1.push($scope.mcrEchDmprTripsTrvldDtl[i].asrt_nm)
}
$scope.mcrEchDmprTripsoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return mcrEchDmprTrips_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchColPntCovdDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchColPntCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchColPntCovdDtl = resdata.data;
                generatedatamcrEchColPntCovd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchColPntCovd(){
    var fnc_cnt=[];
    for(var i=0;i<=$scope.mcrEchColPntCovdDtl.length-1; i++){
        fnc_cnt.push({x:i,y:$scope.mcrEchColPntCovdDtl[i].fnc_cnt});
    }
    var dataArray = [
        {
            values: fnc_cnt,      //values - represents the array of {x,y} data points
            key: "Fence_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchColPntCovd=  dataArray;
    var mcrEchColPntCovd_chart1=[];
for(var i=0;i<=$scope.mcrEchColPntCovdDtl.length-1;i++){
    mcrEchColPntCovd_chart1.push($scope.mcrEchColPntCovdDtl[i].asrt_nm)
}
$scope.mcrEchColPntCovdoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return mcrEchColPntCovd_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CollectionPoints Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchDmprBnsCovdDtls=function(){
   console.log("In mcrEchDmprBnsCovdDtls");
$http.get($rootScope.BaseUrl + 'mcrEchDmprBnsCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchDmprBnsCovdDtl = resdata.data;
                generatedatamcrEchDmprBnsCovd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchDmprBnsCovd(){
    var fnc_cnt=[];
    for(var i=0;i<=$scope.mcrEchDmprBnsCovdDtl.length-1; i++){
        fnc_cnt.push({x:i,y:$scope.mcrEchDmprBnsCovdDtl[i].fnc_cnt});
    }
    var dataArray = [
        {
            values: fnc_cnt,      //values - represents the array of {x,y} data points
            key: "Fence_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchDmprBnsCovd=  dataArray;
    var mcrEchDmprBnsCovd_chart1=[];
for(var i=0;i<=$scope.mcrEchDmprBnsCovdDtl.length-1;i++){
    mcrEchDmprBnsCovd_chart1.push($scope.mcrEchDmprBnsCovdDtl[i].asrt_nm)
}
$scope.mcrEchDmprBnsCovdoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return mcrEchDmprBnsCovd_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "DumperBins Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchTrctCptyDtls=function(){
    
$http.get($rootScope.BaseUrl + 'mcrEchTrctCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchTrctCptyDtl = resdata.data;
                generatedatamcrEchTrctCpty();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchTrctCpty(){
    var cpty_cnt=[];
    for(var i=0;i<=$scope.mcrEchTrctCptyDtl.length-1; i++){
        cpty_cnt.push({x:i,y:$scope.mcrEchTrctCptyDtl[i].cpty_cnt});
    }
    var dataArray = [
        {
            values: cpty_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchTrctCpty=  dataArray;
    var mcrEchTrctCpty_chart1=[];
for(var i=0;i<=$scope.mcrEchTrctCptyDtl.length-1;i++){
    mcrEchTrctCpty_chart1.push($scope.mcrEchTrctCptyDtl[i].asrt_nm)
}
$scope.mcrEchTrctCptyoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return mcrEchTrctCpty_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CapacityDumped(In Tons)",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchTipprCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchTipprCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchTipprCptyDtl = resdata.data;
                generatedatamcrEchTipprCpty();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchTipprCpty(){
    var cpty_cnt=[];
    for(var i=0;i<=$scope.mcrEchTipprCptyDtl.length-1; i++){
        cpty_cnt.push({x:i,y:$scope.mcrEchTipprCptyDtl[i].cpty_cnt});
    }
    var dataArray = [
        {
            values: cpty_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchTipprCpty=  dataArray;
    var mcrEchTipprCpty_chart1=[];
for(var i=0;i<=$scope.mcrEchTipprCptyDtl.length-1;i++){
    mcrEchTipprCpty_chart1.push($scope.mcrEchTipprCptyDtl[i].asrt_nm)
}
$scope.mcrEchTipprCptyoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return mcrEchTipprCpty_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CapacityDumped(In Tons)",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.mcrEchDmprCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'mcrEchDmprCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.mcrEchDmprCptyDtl = resdata.data;
                generatedatamcrEchDmprCpty();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatamcrEchDmprCpty(){
    var cpty_cnt=[];
    for(var i=0;i<=$scope.mcrEchDmprCptyDtl.length-1; i++){
        cpty_cnt.push({x:i,y:$scope.mcrEchDmprCptyDtl[i].cpty_cnt});
    }
    var dataArray = [
        {
            values: cpty_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetmcrEchDmprCpty=  dataArray;
    var mcrEchDmprCpty_chart1=[];
for(var i=0;i<=$scope.mcrEchDmprCptyDtl.length-1;i++){
    mcrEchDmprCpty_chart1.push($scope.mcrEchDmprCptyDtl[i].asrt_nm)
}
$scope.mcrEchDmprCptyoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return mcrEchDmprCpty_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CapacityDumped(In Tons)",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchTractsDstDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchTractsDstTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchTractsDstTrvldDtl = resdata.data;
                generatedatashahEchTractsDst();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchTractsDst(){
    var dst=[];
    for(var i=0;i<=$scope.shahEchTractsDstTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.shahEchTractsDstTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchTractsDst=  dataArray;
    var shahEchTractsDst_chart1=[];
for(var i=0;i<=$scope.shahEchTractsDstTrvldDtl.length-1;i++){
    shahEchTractsDst_chart1.push($scope.shahEchTractsDstTrvldDtl[i].asrt_nm)
}
$scope.shahEchTractsDstoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return shahEchTractsDst_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchTipprDstDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchTipprDstTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchTipprDstTrvldDtl = resdata.data;
                generatedatashahEchTipprDst();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchTipprDst(){
    var dst=[];
    for(var i=0;i<=$scope.shahEchTipprDstTrvldDtl.length-1; i++){
        dst.push({x:i,y:$scope.shahEchTipprDstTrvldDtl[i].dst});
    }
    var dataArray = [
        {
            values: dst,      //values - represents the array of {x,y} data points
            key: "Distance", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchTipprDst=  dataArray;
    var shahEchTipprDst_chart1=[];
for(var i=0;i<=$scope.shahEchTipprDstTrvldDtl.length-1;i++){
    shahEchTipprDst_chart1.push($scope.shahEchTipprDstTrvldDtl[i].asrt_nm)
}
$scope.shahEchTipprDstoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return shahEchTipprDst_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchTractTripsDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchTractTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchTractTripsTrvldDtl = resdata.data;
                generatedatashahEchTractTrips();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchTractTrips(){
    var trps_cnt=[];
    for(var i=0;i<=$scope.shahEchTractTripsTrvldDtl.length-1; i++){
        trps_cnt.push({x:i,y:$scope.shahEchTractTripsTrvldDtl[i].trps_cnt});
    }
    var dataArray = [
        {
            values: trps_cnt,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchTractTrips=  dataArray;
    var shahEchTractTrips_chart1=[];
for(var i=0;i<=$scope.shahEchTractTripsTrvldDtl.length-1;i++){
    shahEchTractTrips_chart1.push($scope.shahEchTractTripsTrvldDtl[i].asrt_nm)
}
$scope.shahEchTractTripsoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return shahEchTractTrips_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchTipprTripsDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchTipprTripsTrvld')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchTipprTripsTrvldDtl = resdata.data;
                generatedatashahEchTipprTrips();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchTipprTrips(){
    var trps_cnt=[];
    for(var i=0;i<=$scope.shahEchTipprTripsTrvldDtl.length-1; i++){
        trps_cnt.push({x:i,y:$scope.shahEchTipprTripsTrvldDtl[i].trps_cnt});
    }
    var dataArray = [
        {
            values: trps_cnt,      //values - represents the array of {x,y} data points
            key: "Trips", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchTipprTrips=  dataArray;
    var shahEchTipprTrips_chart1=[];
for(var i=0;i<=$scope.shahEchTipprTripsTrvldDtl.length-1;i++){
    shahEchTipprTrips_chart1.push($scope.shahEchTipprTripsTrvldDtl[i].asrt_nm)
}
$scope.shahEchTipprTripsoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 480,
    "width":490,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 45,
      tickFormat: function(d){
                    return shahEchTipprTrips_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "Distance Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchColPntCovdDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchColPntCovd')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchColPntCovdDtl = resdata.data;
                generatedatashahEchColPntCovd();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchColPntCovd(){
    var fnc_cnt=[];
    for(var i=0;i<=$scope.shahEchColPntCovdDtl.length-1; i++){
        fnc_cnt.push({x:i,y:$scope.shahEchColPntCovdDtl[i].fnc_cnt});
    }
    var dataArray = [
        {
            values: fnc_cnt,      //values - represents the array of {x,y} data points
            key: "Fence_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchColPntCovd=  dataArray;
    var shahEchColPntCovd_chart1=[];
for(var i=0;i<=$scope.shahEchColPntCovdDtl.length-1;i++){
    shahEchColPntCovd_chart1.push($scope.shahEchColPntCovdDtl[i].asrt_nm)
}
$scope.shahEchColPntCovdoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return shahEchColPntCovd_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CollectionPoints Covered",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchTrctCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchTrctCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchTrctCptyDtl = resdata.data;
                generatedatashahEchTrctCpty();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchTrctCpty(){
    var cpty_cnt=[];
    for(var i=0;i<=$scope.shahEchTrctCptyDtl.length-1; i++){
        cpty_cnt.push({x:i,y:$scope.shahEchTrctCptyDtl[i].cpty_cnt});
    }
    var dataArray = [
        {
            values: cpty_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchTrctCpty=  dataArray;
    var shahEchTrctCpty_chart1=[];
for(var i=0;i<=$scope.shahEchTrctCptyDtl.length-1;i++){
    shahEchTrctCpty_chart1.push($scope.shahEchTrctCptyDtl[i].asrt_nm)
}
$scope.shahEchTrctCptyoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return shahEchTrctCpty_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CapacityDumped(In Tons)",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

$scope.shahEchTipprCptyDtls=function(){
    /**popup for first chart**/
$http.get($rootScope.BaseUrl + 'shahEchTipprCpty')
                .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.shahEchTipprCptyDtl = resdata.data;
                generatedatashahEchTipprCpty();
            })
                .error(function(data, status, headers, config) {
                if(status == 500){
                    $rootScope.sessionCheck();
                }
            });
}

function generatedatashahEchTipprCpty(){
    var cpty_cnt=[];
    for(var i=0;i<=$scope.shahEchTipprCptyDtl.length-1; i++){
        cpty_cnt.push({x:i,y:$scope.shahEchTipprCptyDtl[i].cpty_cnt});
    }
    var dataArray = [
        {
            values: cpty_cnt,      //values - represents the array of {x,y} data points
            key: "Capacity_Count", //key  - the name of the series.
            color: '#f9a11f'  //color - optional: choose your own line color.
        }
    ];
    $scope.datasetshahEchTipprCpty=  dataArray;
    var shahEchTipprCpty_chart1=[];
for(var i=0;i<=$scope.shahEchTipprCptyDtl.length-1;i++){
    shahEchTipprCpty_chart1.push($scope.shahEchTipprCptyDtl[i].asrt_nm)
}
$scope.shahEchTipprCptyoptions1={
  "chart": {
    "type": "discreteBarChart",
    "height": 492,
    "width":520,
    "margin": {
      "top": 10,
      "right": 20,
      "bottom": 80,
      "left": 45
    },
    "showValues": true,
    "clipEdge": true,
    "duration": 500,
    "valueFormat": (d3.format(".0f")),
    "xAxis": {
      "axisLabel": "Vehicles",
      "showMaxMin": false,
      "rotateLabels": 90,
      tickFormat: function(d){
                    return shahEchTipprCpty_chart1[d]
                }
    },
    "yAxis": {
      "axisLabel": "CapacityDumped(In Tons)",
      "axisLabelDistance": -20,
      tickFormat: 
          function (d){
            return d3.format(',0f')(d);
      }
    }
  }
}
}

})

    // =========================================================================
    // Best Selling Widget
    // =========================================================================

    .controller('bestsellingCtrl', function(bestsellingService){
        // Get Best Selling widget Data
        this.img = bestsellingService.img;
        this.name = bestsellingService.name;
        this.range = bestsellingService.range; 
        
        this.bsResult = bestsellingService.getBestselling(this.img, this.name, this.range);
    })

 
    // =========================================================================
    // Todo List Widget
    // =========================================================================

    .controller('todoCtrl', function(todoService){
        
        //Get Todo List Widget Data
        this.todo = todoService.todo;
        
        this.tdResult = todoService.getTodo(this.todo);
        
        //Add new Item (closed by default)
        this.addTodoStat = false;
    })


    // =========================================================================
    // Recent Items Widget
    // =========================================================================

    .controller('recentitemCtrl', function(recentitemService){
        
        //Get Recent Items Widget Data
        this.id = recentitemService.id;
        this.name = recentitemService.name;
        this.parseInt = recentitemService.price;
        
        this.riResult = recentitemService.getRecentitem(this.id, this.name, this.price);
    })


    // =========================================================================
    // Recent Posts Widget
    // =========================================================================
    
    .controller('recentpostCtrl', function(recentpostService){
        
        //Get Recent Posts Widget Items
        this.img = recentpostService.img;
        this.user = recentpostService.user;
        this.text = recentpostService.text;
        
        this.rpResult = recentpostService.getRecentpost(this.img, this.user, this.text);
    })


//=================================================
// CALENDAR
//=================================================
    .controller('calendarCtrl', function($modal){
    
        //Create and add Action button with dropdown in Calendar header. 
        this.month = 'month';
    
        this.actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
                            '<li class="dropdown" dropdown>' +
                                '<a href="" dropdown-toggle><i class="zmdi zmdi-more-vert" style="color:#ffffff;"></i></a>' +
                                '<ul class="dropdown-menu dropdown-menu-right">' +
                                    '<li class="active">' +
                                        '<a data-calendar-view="month" href="">Month View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="basicWeek" href="">Week View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="agendaWeek" href="">Agenda Week View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="basicDay" href="">Day View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="agendaDay" href="">Agenda Day View</a>' +
                                    '</li>' +
                                '</ul>' +
                            '</div>' +
                        '</li>';

            
        //Open new event modal on selecting a day
        this.onSelect = function(argStart, argEnd) {            
            var modalInstance  = $modal.open({
                templateUrl: 'addEvent.html',
                controller: 'addeventCtrl',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    calendarData: function() {
                        var x = [argStart, argEnd];
                        return x;
                    }
                }
            });
        }
    })

    //Add event Controller (Modal Instance)
    .controller('addeventCtrl', function($scope, $modalInstance, calendarData){
        
        //Calendar Event Data
        $scope.calendarData = {
            eventStartDate: calendarData[0],
            eventEndDate:  calendarData[1]
        };
    
        //Tags
        $scope.tags = [
            'bgm-teal',
            'bgm-red',
            'bgm-pink',
            'bgm-blue',
            'bgm-lime',
            'bgm-green',
            'bgm-cyan',
            'bgm-orange',
            'bgm-purple',
            'bgm-gray',
            'bgm-black',
        ]
        
        //Select Tag
        $scope.currentTag = '';
        
        $scope.onTagClick = function(tag, $index) {
            $scope.activeState = $index;
            $scope.activeTagColor = tag;
        } 
        
        //Add new event
        $scope.addEvent = function() {
            if ($scope.calendarData.eventName) {

                //Render Event
                $('#calendar').fullCalendar('renderEvent',{
                    title: $scope.calendarData.eventName,
                    start: $scope.calendarData.eventStartDate,
                    end:  $scope.calendarData.eventEndDate,
                    allDay: true,
                    className: $scope.activeTagColor

                },true ); //Stick the event

                $scope.activeState = -1;
                $scope.calendarData.eventName = '';     
                $modalInstance.close();
            }
        }
        
        //Dismiss 
        $scope.eventDismiss = function() {
            $modalInstance.dismiss();
        }
    })

    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCtrl', function(){
    
        //Input Slider
        this.nouisliderValue = 4;
        this.nouisliderFrom = 25;
        this.nouisliderTo = 80;
        this.nouisliderRed = 35;
        this.nouisliderBlue = 90;
        this.nouisliderCyan = 20;
        this.nouisliderAmber = 60;
        this.nouisliderGreen = 75;
    
        //Color Picker
        this.color = '#03A9F4';
        this.color2 = '#8BC34A';
        this.color3 = '#F44336';
        this.color4 = '#FFC107';
    })


    // =========================================================================
    // PHOTO GALLERY
    // =========================================================================

    .controller('photoCtrl', function(){
        
        //Default grid size (2)
        this.photoColumn = 'col-md-2';
        this.photoColumnSize = 2;
    
        this.photoOptions = [
            { value: 2, column: 6 },
            { value: 3, column: 4 },
            { value: 4, column: 3 },
            { value: 1, column: 12 },
        ]
    
        //Change grid
        this.photoGrid = function(size) {
            this.photoColumn = 'col-md-'+size;
            this.photoColumnSize = size;
        }
    
    })


    // =========================================================================
    // ANIMATIONS DEMO
    // =========================================================================
    .controller('animCtrl', function($timeout){
        
        //Animation List
        this.attentionSeekers = [
            { animation: 'bounce', target: 'attentionSeeker' },
            { animation: 'flash', target: 'attentionSeeker' },
            { animation: 'pulse', target: 'attentionSeeker' },
            { animation: 'rubberBand', target: 'attentionSeeker' },
            { animation: 'shake', target: 'attentionSeeker' },
            { animation: 'swing', target: 'attentionSeeker' },
            { animation: 'tada', target: 'attentionSeeker' },
            { animation: 'wobble', target: 'attentionSeeker' }
        ]
        this.flippers = [
            { animation: 'flip', target: 'flippers' },
            { animation: 'flipInX', target: 'flippers' },
            { animation: 'flipInY', target: 'flippers' },
            { animation: 'flipOutX', target: 'flippers' },
            { animation: 'flipOutY', target: 'flippers'  }
        ]
         this.lightSpeed = [
            { animation: 'lightSpeedIn', target: 'lightSpeed' },
            { animation: 'lightSpeedOut', target: 'lightSpeed' }
        ]
        this.special = [
            { animation: 'hinge', target: 'special' },
            { animation: 'rollIn', target: 'special' },
            { animation: 'rollOut', target: 'special' }
        ]
        this.bouncingEntrance = [
            { animation: 'bounceIn', target: 'bouncingEntrance' },
            { animation: 'bounceInDown', target: 'bouncingEntrance' },
            { animation: 'bounceInLeft', target: 'bouncingEntrance' },
            { animation: 'bounceInRight', target: 'bouncingEntrance' },
            { animation: 'bounceInUp', target: 'bouncingEntrance'  }
        ]
        this.bouncingExits = [
            { animation: 'bounceOut', target: 'bouncingExits' },
            { animation: 'bounceOutDown', target: 'bouncingExits' },
            { animation: 'bounceOutLeft', target: 'bouncingExits' },
            { animation: 'bounceOutRight', target: 'bouncingExits' },
            { animation: 'bounceOutUp', target: 'bouncingExits'  }
        ]
        this.rotatingEntrances = [
            { animation: 'rotateIn', target: 'rotatingEntrances' },
            { animation: 'rotateInDownLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInDownRight', target: 'rotatingEntrances' },
            { animation: 'rotateInUpLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInUpRight', target: 'rotatingEntrances'  }
        ]
        this.rotatingExits = [
            { animation: 'rotateOut', target: 'rotatingExits' },
            { animation: 'rotateOutDownLeft', target: 'rotatingExits' },
            { animation: 'rotateOutDownRight', target: 'rotatingExits' },
            { animation: 'rotateOutUpLeft', target: 'rotatingExits' },
            { animation: 'rotateOutUpRight', target: 'rotatingExits'  }
        ]
        this.fadeingEntrances = [
            { animation: 'fadeIn', target: 'fadeingEntrances' },
            { animation: 'fadeInDown', target: 'fadeingEntrances' },
            { animation: 'fadeInDownBig', target: 'fadeingEntrances' },
            { animation: 'fadeInLeft', target: 'fadeingEntrances' },
            { animation: 'fadeInLeftBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInRight', target: 'fadeingEntrances'  },
            { animation: 'fadeInRightBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInUp', target: 'fadeingEntrances'  },
            { animation: 'fadeInBig', target: 'fadeingEntrances'  }
        ]
        this.fadeingExits = [
            { animation: 'fadeOut', target: 'fadeingExits' },
            { animation: 'fadeOutDown', target: 'fadeingExits' },
            { animation: 'fadeOutDownBig', target: 'fadeingExits' },
            { animation: 'fadeOutLeft', target: 'fadeingExits' },
            { animation: 'fadeOutLeftBig', target: 'fadeingExits'  },
            { animation: 'fadeOutRight', target: 'fadeingExits'  },
            { animation: 'fadeOutRightBig', target: 'fadeingExits'  },
            { animation: 'fadeOutUp', target: 'fadeingExits'  },
            { animation: 'fadeOutUpBig', target: 'fadeingExits'  }
        ]
        this.zoomEntrances = [
            { animation: 'zoomIn', target: 'zoomEntrances' },
            { animation: 'zoomInDown', target: 'zoomEntrances' },
            { animation: 'zoomInLeft', target: 'zoomEntrances' },
            { animation: 'zoomInRight', target: 'zoomEntrances' },
            { animation: 'zoomInUp', target: 'zoomEntrances'  }
        ]
        this.zoomExits = [
            { animation: 'zoomOut', target: 'zoomExits' },
            { animation: 'zoomOutDown', target: 'zoomExits' },
            { animation: 'zoomOutLeft', target: 'zoomExits' },
            { animation: 'zoomOutRight', target: 'zoomExits' },
            { animation: 'zoomOutUp', target: 'zoomExits'  }
        ]

        //Animate    
        this.ca = '';
    
        this.setAnimation = function(animation, target) {
            if (animation === "hinge") {
                animationDuration = 2100;
            }
            else {
                animationDuration = 1200;
            }
            
            angular.element('#'+target).addClass(animation);
            
            $timeout(function(){
                angular.element('#'+target).removeClass(animation);
            }, animationDuration);
        }
    
    })

.controller("tripsCtrl",function($scope,$http,NgTableParams,growlService){
    $scope.searchVehData   = '';

     $scope.addNewVehData = function() {
          $scope.showModal = true;
          $scope.veh_num = null;
          $scope.veh_type = null;
        };

        $scope.updtVehData = function(v) {
          $scope.showModal = true;
          $scope.veh_num = v.veh_nm;
          $scope.veh_type = v.dvce_nm;
        };

        $scope.submitVehData = function() {
        growlService.growl(' Successfully updated!', 'inverse'); 
          $scope.showModal = false;
        };

        $scope.cancel = function() {
          $scope.showModal = false;
        };

    // this.editInfo = false;
    //     this.edit = function(idx,item){
    //         console.log(idx)
    //         this.editInfo = idx;
    //         // var itemToEdit = item;
    //     };
    //     this.save = function(idx,item){
    //         console.log(idx)
    //         this.editInfo = false;
    //         // var itemToEdit = item;
    //         growlService.growl(' Successfully updated!', 'inverse'); 
    //     };

    $scope.vehiclesLst=[{
        "sno":"1",
        "veh_nm":"11z1111",
        "dvce_nm":"Tractor"
    },{
        "sno":"2",
        "veh_nm":"22z1012",
        "dvce_nm":"Bull"
    },{
        "sno":"3",
        "veh_nm":"29z1313",
        "dvce_nm":"Tanker"
    },{
        "sno":"4",
        "veh_nm":"04z1410",
        "dvce_nm":"Tripper"
    },{
        "sno":"5",
        "veh_nm":"25z1010",
        "dvce_nm":"Tractor"
    },{
        "sno":"6",
        "veh_nm":"29z1610",
        "dvce_nm":"Dumper"
    },{
        "sno":"7",
        "veh_nm":"29z1017",
        "dvce_nm":"JCB"
    },{
        "sno":"8",
        "veh_nm":"29z8010",
        "dvce_nm":"Water Tank"
    },{
        "sno":"9",
        "veh_nm":"29z1090",
        "dvce_nm":"Bull"
    },{
        "sno":"10",
        "veh_nm":"29z1010",
        "dvce_nm":"Tripper"
    },{
        "sno":"1",
        "veh_nm":"11z1111",
        "dvce_nm":"Tractor"
    },{
        "sno":"2",
        "veh_nm":"22z1012",
        "dvce_nm":"Bull"
    },{
        "sno":"3",
        "veh_nm":"29z1313",
        "dvce_nm":"Tanker"
    },{
        "sno":"4",
        "veh_nm":"04z1410",
        "dvce_nm":"Tripper"
    },{
        "sno":"5",
        "veh_nm":"25z1010",
        "dvce_nm":"Tractor"
    },{
        "sno":"6",
        "veh_nm":"29z1610",
        "dvce_nm":"Dumper"
    },{
        "sno":"7",
        "veh_nm":"29z1017",
        "dvce_nm":"JCB"
    },{
        "sno":"8",
        "veh_nm":"29z8010",
        "dvce_nm":"Water Tank"
    },{
        "sno":"9",
        "veh_nm":"29z1090",
        "dvce_nm":"Bull"
    },{
        "sno":"10",
        "veh_nm":"29z1010",
        "dvce_nm":"Tripper"
    },{
        "sno":"1",
        "veh_nm":"11z1111",
        "dvce_nm":"Tractor"
    },{
        "sno":"2",
        "veh_nm":"22z1012",
        "dvce_nm":"Bull"
    },{
        "sno":"3",
        "veh_nm":"29z1313",
        "dvce_nm":"Tanker"
    },{
        "sno":"4",
        "veh_nm":"04z1410",
        "dvce_nm":"Tripper"
    },{
        "sno":"5",
        "veh_nm":"25z1010",
        "dvce_nm":"Tractor"
    },{
        "sno":"6",
        "veh_nm":"29z1610",
        "dvce_nm":"Dumper"
    },{
        "sno":"7",
        "veh_nm":"29z1017",
        "dvce_nm":"JCB"
    },{
        "sno":"8",
        "veh_nm":"29z8010",
        "dvce_nm":"Water Tank"
    },{
        "sno":"9",
        "veh_nm":"29z1090",
        "dvce_nm":"Bull"
    },{
        "sno":"10",
        "veh_nm":"29z1010",
        "dvce_nm":"Tripper"
    }];
    $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
            page: 1, count: 10
        }, {
            data: data
        });
    };
    $scope.loadfilter($scope.vehiclesLst);
    $scope.PDFDownload = function(){
        console.log("pdf")
    };
    $scope.ExcelDownload = function(){
        console.log("excel")
    };
})
.controller("distanceRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
    $scope.searchVehData   = '';
    $scope.displayfltrs = false;
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };
    
   
   $scope.getDistRptLst = function(vehcatid,vehgrpid,dtFrom,dtTo){
        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

         var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }
        // console.log(vehgrp);
        // console.log(vehcat);
        // console.log($scope.dtFrom);
        // console.log($scope.dtTo);
        var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }
        // console.log(data);
        $http.post($rootScope.BaseUrl + 'distRptLst/',data)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.distlst = resdata.data;
                $scope.loadfilter();
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getDistRptLst();

    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.distlst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.distlst, params.filter().term) : $scope.distlst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      })
  }
    //  $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };
    $scope.count=0;
     $scope.shwFltrs=function(){
        console.log("in filters");
          $scope.count++;
          if ($scope.count % 2 ==1) {
            $scope.displayfltrs = true;
          }
          else{
            $scope.displayfltrs = false;
          }
  
}
    $scope.pdfDownload = function()
    {
        console.log("pdfDownload");
              var jsonColumns = ['dt','crw_nm','asrt_nm','asrt_ctgry_nm','dst'];
              var colHeadings = ['DATE','Driver Name','VEHICLE NUMBER','VEHICLE TYPE','DISTANCE(IN KM)'];
               var tbldata = $scope.distlst;
                
              var rptHeading = 'DISTANCE REPORT';
              var fileName = 'DIST-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }

    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'dt':'','crw_nm':'','asrt_nm':'', 'asrt_ctgry_nm':'DISTANCE REPORT', 'dst':''});
    $scope.exprintData.push({'dt':'DATE','crw_nm':'DRIVER NAME','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE','dst':'DISTANCE(IN KM)'});
    for(var i=0;i<$scope.distlst.length;i++){
            $scope.exprintData.push({'dt':$scope.distlst[i].dt,'crw_nm':$scope.distlst[i].crw_nm,'asrt_nm':$scope.distlst[i].asrt_nm,'asrt_ctgry_nm':$scope.distlst[i].asrt_ctgry_nm,'dst':$scope.distlst[i].dst});
          }
    return $scope.exprintData;
  }
})
.controller("tripsRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
    $scope.searchVehData   = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };

    $scope.getTripLst = function(vehcatid,vehgrpid,dtFrom,dtTo){
        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

        var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }
        
        var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }
        // console.log(data);
        $http.post($rootScope.BaseUrl + 'tripRptLst/',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting fence list");
                $scope.triplst = resdata.data;
                console.log($scope.triplst);
                $scope.loadfilter();
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getTripLst();

    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.triplst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.triplst, params.filter().term) : $scope.triplst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    //  $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };
    $scope.count=0;
$scope.shwFltrs=function(){
  $scope.count++;
  if ($scope.count % 2 ==1) {
    $scope.displayfltrs = true;
  }
  else{
    $scope.displayfltrs = false;
  }
  
}
    $scope.pdfDownload = function()
    {
              var jsonColumns = ['dt','crw_nm','asrt_nm','asrt_ctgry_nm','trp_cnt','dtst_tvl'];
              var colHeadings = ['TRIP DATE','DRIVER NAME','VEHICLE NUMBER','VEHICLE TYPE','TOTAL TRIPS','COVERED DISTANCE(IN KMS)'];
               var tbldata = $scope.triplst;
                
              var rptHeading = 'TRIPS REPORT';
              var fileName = 'TRIPS-RPT';
              var align= 'landscape';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }
    $scope.excelDownload=function(){
    $scope.exprintData=[];
    $scope.exprintData.push({'dt':'','crw_nm':'','asrt_nm':'','asrt_ctgry_nm':'','trp_cnt':'','dtst_tvl':'TRIPS REPORT', 'tvl_tm':''});
    $scope.exprintData.push({'dt':'TRIP DATE','crw_nm':'DRIVER NAME','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE',  'trp_cnt':'TOTAL TRIPS','dtst_tvl':'COVERED DISTANCE(IN KMS)', 'tvl_tm':'TIME TAKEN(IN MINS)'});
    for(var i=0;i<$scope.triplst.length;i++){
            $scope.exprintData.push({'dt':$scope.triplst[i].dt,'crw_nm':$scope.triplst[i].crw_nm,'asrt_nm':$scope.triplst[i].asrt_nm,'asrt_ctgry_nm':$scope.triplst[i].asrt_ctgry_nm,'trp_cnt':$scope.triplst[i].trp_cnt,'dtst_tvl':$scope.triplst[i].dtst_tvl, 'tvl_tm':$scope.triplst[i].tvl_tm});
          }
    return $scope.exprintData;
  }
})

.controller("landmarkInOutRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
     $scope.searchVehData   = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };
    $scope.count=0;
$scope.shwFltrs=function(){
  $scope.count++;
  if ($scope.count % 2 ==1) {
    $scope.displayfltrs = true;
  }
  else{
    $scope.displayfltrs = false;
  }
  
}

     $scope.getLandMrkLst = function(vehcatid,vehgrpid,dtFrom,dtTo){
        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

        var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }

            var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }

        $http.post($rootScope.BaseUrl + 'lndmrkRptLst',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting landmarks list");
                $scope.lndmrklst = resdata.data;
                // console.log($scope.lndmrklst);
                $scope.loadfilter();
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            })
    }
    $scope.getLandMrkLst();
    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.lndmrklst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.lndmrklst, params.filter().term) : $scope.lndmrklst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    //  $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };
    $scope.pdfDownload = function()
    {
              var jsonColumns = ['fnce_nm','asrt_nm','asrt_ctgry_nm','time_in','time_out','timdiff'];
              var colHeadings = ['LANDMARK NAME','VEHICLE NUMBER','VEHICLE TYPE','TIME IN','TIME OUT','STOPPAGE TIME'];
               var tbldata = $scope.lndmrklst;
                
              var rptHeading = 'LandMark IN-OUT REPORT';
              var fileName = 'LNDMRK-INOUT-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }
    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'fnce_nm':'','asrt_nm':'','asrt_ctgry_nm':'','time_in':'','time_out':'TRIPS REPORT', 'timdiff':''});
    $scope.exprintData.push({'fnce_nm':'LANDMARK NAME','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE','time_in':'TIME IN','time_out':'TIME OUT', 'timdiff':'STOPPAGE TIME'});
    for(var i=0;i<$scope.lndmrklst.length;i++){
            $scope.exprintData.push({'fnce_nm':$scope.lndmrklst[i].fnce_nm,'asrt_nm':$scope.lndmrklst[i].asrt_nm,'asrt_ctgry_nm':$scope.lndmrklst[i].asrt_ctgry_nm,'time_in':$scope.lndmrklst[i].time_in,'time_out':$scope.lndmrklst[i].time_out, 'timdiff':$scope.lndmrklst[i].timdiff});
          }
    return $scope.exprintData;
  }
   
})

.controller("milageRptCtrl", function($scope, $http, NgTableParams, $filter, $rootScope, apppdfService) {
    $scope.searchVehData   = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

    this.openCalendar = function(e, date) {
        that.open[date] = true;
    };
    $scope.openfrom = function($event, openedf) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedf] = true;
    };
    $scope.opento = function($event, openedt) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedt] = true;
    };


    $scope.getMileageRptLst = function() {

        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

         var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }

        var data = {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }
        // console.log(data);
        // $http.get($rootScope.BaseUrl + 'mileageRptLst' + '?fromdt=' + $scope.dtFrom + '&todt=' + $scope.dtTo)
        $http.post($rootScope.BaseUrl + 'mileageRptLst/',data)
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.mileageList = resdata.data;
                // console.log($scope.mileageList);
                $scope.loadfilter();
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getMileageRptLst();

    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.mileageList.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.mileageList, params.filter().term) : $scope.mileageList;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    // $scope.loadfilter = function(data) {
    //     $scope.tableFilter = new NgTableParams({
    //         page: 1,
    //         count: 10
    //     }, {
    //         data: data
    //     });
    // };
    $scope.count=0;
        $scope.shwFltrs=function(){
          $scope.count++;
          if ($scope.count % 2 ==1) {
            $scope.displayfltrs = true;
          }
          else{
            $scope.displayfltrs = false;
          }
          
        }
    $scope.pdfDownload = function() {
        console.log("pdfDownload");
        var jsonColumns = ['SNo', 'asrt_nm', 'asrt_ctgry_nm', 'trvl_hr','tot_trips','mileage_ct', 'asrt_grp_nm','distance','milege_ct'];
        var colHeadings = ['Sno', 'VEHICLE NUMBER', 'VEHICLE CATEGORY','TRAVEL TIME','TOTAL TRIPS','TOTAL MILEAGE', 'VEHICLE TYPE','DISTANCE', 'KM (PER LTR)'];
        var tbldata = $scope.mileageList;

        var rptHeading = 'MILEAGE REPORT';
        var fileName = 'MILEAGE-RPT';
        var align = 'portrait';
        apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align);
    }

    $scope.excelDownload = function() {
        $scope.exprintData = [];
        $scope.exprintData.push({
            'Sno': '',
            'asrt_nm': '',
            'asrt_ctgry_nm': 'MILEAGE REPORT',
            'mileage_ct': '',
            'asrt_grp_nm':'',
            'trvl_hr':'',
            'tot_trips':'',
            'distance': '',
            'milege_ct': ''
        });

        $scope.exprintData.push({
            'trvl_dt': 'DATE',
            'asrt_nm': 'VEHICLE NUMBER',
            'asrt_ctgry_nm': 'VEHICLE CATEGORY',
            'mileage_ct': 'TOTAL MILEGE',
            'asrt_grp_nm':'VEHICLE TYPE',
            'trvl_hr':'TRAVEL TIME',
            'tot_trips':'TOTAL TRIPS',
            'distance': 'DISTANCE',
            'milege_ct': 'KM (PER LTR)'
        });

        for (var i = 0; i < $scope.mileageList.length; i++) {
            $scope.exprintData.push({
                'Sno': $scope.mileageList[i].Sno,
                'asrt_nm': $scope.mileageList[i].asrt_nm,
                'asrt_ctgry_nm': $scope.mileageList[i].asrt_ctgry_nm,
                'asrt_grp_nm': $scope.mileageList[i].asrt_grp_nm,
                'mileage_ct': $scope.mileageList[i].mileage_ct,
                'trvl_hr': $scope.mileageList[i].trvl_hr,
                'distance': $scope.mileageList[i].distance,
                'milege_ct': $scope.mileageList[i].milege_ct,
                'tot_trips': $scope.mileageList[i].tot_trips
            });
        }
        return $scope.exprintData;
    }
})

 .controller("heatMapWeeklyRpt", function($scope,$http,$rootScope, NgTableParams, $filter, localStorageService){

        this.openCalendar = function(e, date) {
            that.open[date] = true;
        };
        var today = new Date();
        $scope.AvailableDate = new Date();
        $scope.ExpireDate = new Date();
        $scope.dateFormat = 'dd-MM-yyyy';
        $scope.availableDateOptions = {
            formatYear: 'yy',
            startingDay: 1,
             minDate: new Date(2017, 5, 01),
            maxDate: today
        };
        $scope.expireDateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            minDate: new Date(2017, 5, 01),
            maxDate: today
        };
        $scope.availableDateFromPopup = {
            opened: false
        };
        $scope.availableDateToPopup = {
            opened: false
        };
        $scope.expireDateFromPopup = {
            opened: false
        };
        $scope.expireDateToPopup = {
            opened: false
        };
        $scope.ChangeExpiryMinDate = function(availableDate) {
            if (availableDate != null) {
                var expiryMinDate = new Date(availableDate);
                $scope.expireDateOptions.minDate = expiryMinDate;
                $scope.ExpireDate = expiryMinDate;
            }
        };
        $scope.ChangeExpiryMinDate();
        $scope.OpenFromDate = function() {
            $scope.availableDateFromPopup.opened = !$scope.availableDateFromPopup.opened;
        };
        $scope.OpenToDate = function() {
            $scope.availableDateToPopup.opened = !$scope.availableDateToPopup.opened;
        };
        $scope.OpenExpireFromDate = function() {
            $scope.expireDateFromPopup.opened = !$scope.expireDateFromPopup.opened;
        };
        $scope.OpenExpireToDate = function() {
            $scope.expireDateToPopup.opened = !$scope.expireDateToPopup.opened;
        };
       $scope.openfrom = function($event, openedf) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[openedf] = true;
        };

        $scope.openTo = function($event, openedt) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[openedt] = true;
        };

         var init_lat = "17.031032";
         var init_lng = "81.78976";
         var pointarray, heatmap;
         var weeklyHeatMapRpt;
         var vehicleData = [];
         var heatLayer;

        var assertIds = function(){
            $http.get($rootScope.BaseUrl + '/getAsrtGrp/'+1).success(function(data) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.asrtGrpiD = data.data;
                console.log($scope.asrtGrpiD);
            });
         };

        $scope.getAsrtCate = function(){
            $http.get($rootScope.BaseUrl + '/getAsrtCat/'+ $scope.asrt_grp_id).success(function(data){
                $scope.asrtCatiD = data.data;
                console.log($scope.asrtCatiD);
            });
         };

         $scope.getAsrtIds = function(){
            $http.get($rootScope.BaseUrl + '/getAsrtList/'+ $scope.asrt_ctgry_id).success(function(data){
                $scope.asrtId  = data.data;
                console.log($scope.asrtId);
            });
        };
        
        var initialise = function() {
            $scope.heatMap = {
                    center: {
                    latitude: init_lat,
                    longitude: init_lng
                    },
                    zoom: 12,
                    heatLayerCallback: function (layer) {
                        heatLayer = layer;
                    },
                    showHeat: true
                };
                assertIds();
        };

        if (!$scope.heatMap) {
            initialise();
        }

        $scope.heatMapGenerate = function() {
            console.log($scope.sdate+','+$scope.edate);
            vehicleData = [];
            if ($scope.sdate === undefined || $scope.edate === undefined) {
                swal("Please select Date");
                return;
            }
            var startdate = moment($scope.sdate).format('YYYYMMDD');
            var enddate = moment($scope.edate).format('YYYYMMDD');

            var data = {
                "sdate": startdate,
                "edate": enddate,
                "asrt_grp_id": $scope.asrt_grp_id,
                "asrt_ctgry_id": $scope.asrt_ctgry_id
            };
            console.log(data);
            genHeatMapLayer(data);  
        };

        var genHeatMapLayer = function(data) {
           
            $http.post($rootScope.BaseUrl + 'weeklyHeatMapRpt', data)
                .success(function(resdata, status, headers, config) {
                    if (status == 200) {
                        weeklyHeatMapRpt = resdata.data;
                        for (var i = 0; i < weeklyHeatMapRpt.length; i++) {
                            vehicleData.push(new google.maps.LatLng(weeklyHeatMapRpt[i].lat,weeklyHeatMapRpt[i].lng));
                        }
                        pointArray = new google.maps.MVCArray(vehicleData);
                        heatLayer.setData(pointArray);

                    } else {
                        console.log("status is not 200 , Data = " + resdata);
                    }
                }).error(function(data, status, headers, config) {
                    $scope.message = "Unable retrive data from Server.";
                });
        };
})

.controller("dumpCoveredRptCtrl", function($scope, $http, NgTableParams, $filter, $rootScope, localStorageService) {

    var basePath = 'http://localhost:4801/';
    var iconBase = basePath + 'client/src/public/static/img/markers';
    var init_lat = "17.031032";
    var init_lng = "81.78976";
    var infowindow = new google.maps.InfoWindow();

    $scope.searchVehData = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    // get collection type of that garbage
    $scope.getGroup = function() {
        $http.get($rootScope.BaseUrl + 'getGarbageType')
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.getGroupData = resdata.data;
                // console.log($scope.getGroupData);
            })
            .error(function(data, status, headers, config) {
                // console.log(data);
            });
    }
    $scope.getGroup();

    $scope.fnce_grp_id = null;
    $scope.grpCatModel = [];
    $scope.grpCatSettings = {
        "displayProp": "fnce_grp_nm",
        "idProp": "fnce_grp_id",
        "enableSearch": true,
        "externalIdProp": "fnce_grp_id",
        "styleActive": true,
        "keyboardControls": true
    };
    $scope.grpCatTextSettings = {
        "searchPlaceholder": "Search Collection Point",
        "buttonDefaultText": "Select Collection Point"
    }

    this.openCalendar = function(e, date) {
        that.open[date] = true;
    };
    $scope.openfrom = function($event, openedf) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedf] = true;
    };
    $scope.opento = function($event, openedt) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedt] = true;
    };

    $scope.getDumpCoveredRptLst = function() {

        var fnce_grp_id = '0';
        for (i = 0; i < $scope.grpCatModel.length; i++) {
            fnce_grp_id = fnce_grp_id + ',' + $scope.grpCatModel[i].fnce_grp_id;
        }

        var data = {
            "dtFrom": $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss"),
            "dtTo": $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss"),
            "fnce_grp_id": fnce_grp_id
        }
        // console.log(data);
        garbageCollectionPoints(data);
    }

    var garbageCollectionPoints = function(data) {
        var markersList = [];
        var iconFile = '';
        if (data) {
            $http.post($rootScope.BaseUrl + 'getCvrdDumpersStatus', data)
                .success(function(data) {
                    if (status == 257) $rootScope.sessionCheck();
                    $scope.cvrCount = 0;
                    $scope.unCvrCount = 0;
                    $scope.dumperBinsUnCovrdTotal = 0;
                    $scope.dumperBinsCovrdTotal = 0;
                    $scope.garbageMarkers = [];
                    $scope.garbageCollectionPoints = [];
                    $scope.coveredPnt = [];
                    $scope.unCoveredPnt = [];
                    $scope.totCovrdBins = [];
                    $scope.totUnCovrdBins = [];
                    markersList = data.data;
                    // console.log(markersList);
                    for (i = 0; i < markersList.length; i++) {
                        var data = markersList[i];
                        if (data.fnce_grp_id == 3 && data.covr_status == 0) {
                            iconFile = iconBase + '/dustbin_old.png';
                            $scope.unCoveredPnt.push(markersList[i]);
                            // console.log($scope.totUnCovrdBins);
                            // $scope.dumperBinsUnCovrdTotal++;
                            $scope.unCvrCount++;
                        } else if (data.fnce_grp_id == 3 && data.covr_status == 1) {
                            iconFile = iconBase + '/dumperBin_covered.png';
                            $scope.coveredPnt.push(markersList[i]);
                            // console.log($scope.totCovrdBins);
                            // $scope.dumperBinsCovrdTotal++;
                            $scope.cvrCount++;
                        } else if (data.covr_status == 0) {
                            iconFile = iconBase + '/dustbin.png';
                            $scope.unCoveredPnt.push(markersList[i]);
                            $scope.unCvrCount++;
                        } else if (data.covr_status == 1) {
                            iconFile = iconBase + '/dbin-covered.png';
                            $scope.coveredPnt.push(markersList[i]);
                            $scope.cvrCount++;
                        }

                        var markerData = {
                            latitude: data.lat,
                            longitude: data.lng,
                            title: data.fnce_nm,
                            fnce_id: data.fnce_id,
                            icon: iconFile,
                            id: i
                        }
                        $scope.garbageCollectionPoints.push(markerData);
                        $scope.garbageMarkers.push(markerData);
                    }
                });
        }
    };

    $scope.showGarbageMarkers = function() {
        $scope.garbageMarkers = [];
        if ($scope.garbagePoints) {
            $scope.garbageMarkers = $scope.garbageCollectionPoints;
            console.log($scope.garbageMarkers); 
        }
    }

    $scope.showMunicpalWards = function() {
        if ($scope.muncipalAreas) {
            console.log("show");
            getGeoFnceDtls();
        } else {
            for (var i = 0; i < $scope.beforeFnce.length; i++) {
                $scope.beforeFnce[i] = [];
            }
            console.log("hide");
        }
    }

    $scope.garbageMarkerClick = function(gMarkerInstance, event, gMarkerObj) {
        console.log(gMarkerObj);
        $scope.title = gMarkerObj.title;
        $scope.fnce_id = gMarkerObj.fnce_id;
        console.log($scope.fnce_id);
        infowindow.setContent('<b>Fence Address:&nbsp;</b>' + $scope.title + ',<br> <b>Fence ID:&nbsp;</b>' + $scope.fnce_id);
        // InfoWindow.setContent('<div style='color: #000;'><b style='line-height: 21px;    color: #1a56a2;'>Fence Address:  </b>" + $scope.title +"<br><b style="line-height: 21px;    color: #1a56a2;">Fence ID:  </b>" + $scope.fnce_id + "</div>"')
        infowindow.open($scope.dumpMap, gMarkerInstance);
    }

    $scope.initialise = function() {
        $scope.dumpMap = {
            center: {
                latitude: init_lat,
                longitude: init_lng
            },
            mapTypeId: google.maps.MapTypeId.HYBRID,
            zoom: 16,
            tilt: 0,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.RIGHT_CENTER
            },
            mapTypeControl: false,
            scaleControl: true,
            streetViewControl: true,
            rotateControl: true,
            fullscreenControl: false
        };

        var data = {
            "dtFrom": $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss"),
            "dtTo": $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss"),
            "fnce_grp_id": 1
        }
        // console.log(data); 
        garbageCollectionPoints(data);
    }

    // Call set markers function by default load of controller
    if (!$scope.dumpMap) {
        $scope.initialise();
        console.log("Initial Loading.........");
    }


    

    var muncipalWards = function() {
            for (var i = 0; i < $scope.geoFencesCords.length; i++) {
                // console.log($scope.geoFencesCords.length);
                $scope.geoFenceCordDtls.push({
                    fnce_id: $scope.geoFencesCords[i].fnce_id,
                    fncs_clr_cd: $scope.geoFencesCords[i].fncs_clr_cd,
                    fncs_dtl_tx: $scope.geoFencesCords[i].fncs_dtl_tx
                });
                $scope.polyCoords = $scope.geoFenceCordDtls[i].fncs_dtl_tx;
                $scope.polyCoordsColor = $scope.geoFenceCordDtls[i].fncs_clr_cd;
                $scope.polyCoordsID = $scope.geoFenceCordDtls[i].fnce_id;

                var x = $scope.polyCoords.replace("POLYGON((", "").replace("))", "");
                var buildStr = '';
                // var polyColor = '';
                var strArr = x.split(',');
                for (var j = 0; j < strArr.length; j++) {
                    var tmpStr = strArr[j].trim(" ");
                    var cords = tmpStr.split(' ');
                    (buildStr === '') ? buildStr = buildStr + '{"latitude":' + cords[1] + ',"longitude":' + cords[0] + '}': buildStr = buildStr + ',{"latitude":' + cords[1] + ',"longitude":' + cords[0] + '}';
                }
                $scope.finalStr.push(JSON.parse('[' + buildStr + ']'));
                $scope.finalStr[i].fncs_clr_cd = $scope.geoFenceCordDtls[i].fncs_clr_cd;
                $scope.finalStr[i].fnce_id = $scope.geoFenceCordDtls[i].fnce_id;
            }
            for (k = 0; k < $scope.finalStr.length; k++) {

                $scope.d_ffff = {
                    id: $scope.finalStr[k].fnce_id,
                    path: $scope.finalStr[k],
                    stroke: {
                        color: $scope.finalStr[k].fncs_clr_cd,
                        weight: 3
                    },
                    editable: false,
                    draggable: false,
                    geodesic: false,
                    visible: true,
                    fill: {
                        color: $scope.finalStr[k].fncs_clr_cd,
                        opacity: 0.6
                    },
                    events: {
                        click: function(polyCoords) {
                            alert("Polygon ID =" + polyCoords.fnce_id);
                            polyCoords.fill.opacity = '0.1';
                        }
                    }
                };
                $scope.beforeFnce.push($scope.d_ffff);
                $scope.geoFences = $scope.beforeFnce;

            }
        }

        var getGeoFnceDtls = function() {

        $scope.geoFenceCordDtls = [];
        $scope.beforeFnce = [];
        $scope.finalStr = [];
        console.log("loading overlays on map");

        $scope.geoFencesCords = localStorageService.get('muncipalWards');
        console.log($scope.geoFencesCords);

        if ($scope.geoFencesCords == null || $scope.geoFencesCords == undefined) {
            console.log($scope.geoFencesCords == null);
            $http.get($rootScope.BaseUrl + "/getFences")
                .success(function(resdata, status, headers, config) {
                    if (status == 200) {
                        console.log("Set Data");
                        $scope.geoFencesCords = resdata.data;
                        localStorageService.set('muncipalWards', resdata.data);
                        muncipalWards();
                    } else {
                        console.log("No data Found......");
                    }
                })
        } else {
            const startTime = performance.now();
            muncipalWards();
            const duration = performance.now() + startTime;
            console.log(`muncipalWards took ${duration}ms`);
        }
    }
     
})

.controller("vehicleRptCtrl", function($scope, $filter, apppdfService, clntDataService, tntDatabyClntService, $http, NgTableParams, growlService, $rootScope, localStorageService) {

    $scope.showUserSbscrDtls = false;
    $scope.newUser = true;
    $scope.userFormDtls = {};

    $scope.searchUsrData = '';

    $scope.formats = ['MM-dd-yyyy', 'dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    $scope.selectedSrvClass = 0;
    $scope.selectedSrvClassReg = 0;
    $scope.vclass_id = "";
    this.dateModeOptions = {
        minMode: 'year',
        maxMode: 'year'
    };

    this.openCalendar = function(e, date) {
        that.open[date] = true;
    };
    $scope.openfrom = function($event, openedf) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedf] = true;
    };

    $scope.openTo = function($event, openedt) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedt] = true;
    };

    //$scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd");
    $scope.dtFrom = $filter('date')(new Date(), "yyyy-MM-dd");
    $scope.dtTo = $filter('date')(new Date(), "yyyy-MM-dd");
    // console.log($scope.dtFrom, $scope.dtTo);



    $http.get($rootScope.BaseUrl + 'asrtLst')
        .success(function(resdata, status) {
            if (status == 257) $rootScope.sessionCheck();
            $scope.allvehslst = resdata.data;
            $scope.slectedVehIndex = $scope.allvehslst[0].asrt_id;
        })
        .error(function(data, status, headers, config) {
            if (status == 500) {
                $rootScope.sessionCheck();
            }
        });


    $scope.getVehRptData = function() {
        var data = {
            "from_date": $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm"),
            "to_date": $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm"),
            "asrt_id": $scope.veh.asrt_id
        }
        // console.log(data);
        $http.post($rootScope.BaseUrl + 'postVehRptgetData', data)
            .success(function(resdata, status) {
                // console.log(resdata);
                // if(status == 200)  $rootScope.sessionCheck();
                $scope.getVehAsrtRptData = resdata.data.singleAsrtDtl;
                // console.log($scope.getVehAsrtRptData);
                $scope.getVehLastData = resdata.data.groupAsrtDtl;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }

    $scope.vehicleMaxTripsCvrd = function() {
        $scope.vehRptGet = [];
        // console.log(vehRptGet);
        for (var i = 0; i <= $scope.getVehLastData.length - 1; i++) {
            console.log($scope.getVehLastData.length);
      
            $scope.vehRptGet.push($scope.getVehLastData[i].max_trp, $scope.getVehLastData[i].min_trp, $scope.getVehLastData[i].avg_trp);
        }
        // console.log($scope.vehRptGet.length);

    }
    var chart;
    var arr = [];
    nv.addAssetGraph = function(labels) {

        // var arr=[];
        // arr.push()
        chart = nv.models.multiBarChart()
            .margin({
                bottom: 30,
                top: 55
            })
            .groupSpacing(0.2)
            .reduceXTicks(false)
            .showControls(true)
            .showLegend(true)
            .staggerLabels(false);

        chart.yAxis.axisLabel(dataForChart.ylabel)
            .tickFormat(d3.format(''))
            .axisLabelDistance(50);

        d3.select('#chart svg')
            .datum(dataForChart(labels))
            .transition().duration(500)
            .call(chart);
        nv.utils.windowResize(chart.update);

        chart.multibar.dispatch.on('elementClick', function(e) {
            nv.log('New State:', JSON.stringify(e));

        });
        return chart;
    };

    var dataForChart = function(labels) {

        var sections = [];

        for (var j = 0; j < labels.length; j++) {

            var section = new Array();
            section.key = labels[j];
            section.values = new Array();
            section.color = "#134e13";
            sections.push(section);
        }
        var date = '';
        console.log(arr);
        console.log(arr[0][0].asrt_ctgry_nm);
        console.log(arr[1][0].asrt_ctgry_nm);
        for (var i = 0; i < arr.length; i++) {
            //console.log($scope.getVehLastData.groupAsrtDtl);
            console.log(arr[i]);

            date = arr[i][0].fm_dt + arr[i][0].to_dt;
            var x = date.split(" ");
            sections[0].values[i] = {
                "y": arr[i][0].max_trp,
                "x": date
            };

            sections[1].values[i] = {
                "y": arr[i][0].min_trp,
                "x": date
            };
            sections[2].values[i] = {
                "y": arr[i][0].avg_trp,
                "x": date
            };
        }
        //console.log(sections[0].values[0]);
        //}

        // }
        //                 for (var j=0;j<$scope.getVehAsrtRptData.length;j++){
        //                     //console.log($scope.getVehLastData.groupAsrtDtl);

        //                                sections[0].values[j] = {"y":$scope.getVehAsrtRptData[j].max_trp, "x": $scope.getVehLastData[j].asrt_grp_nm};

        //                                sections[1].values[j] = {"y":$scope.getVehAsrtRptData[j].min_trp ,"x": $scope.getVehLastData[j].asrt_grp_nm};
        //                                sections[2].values[j] = {"y":$scope.getVehAsrtRptData[j].avg_trp , "x": $scope.getVehLastData[j].asrt_grp_nm};
        //                   //console.log(sections[0].values[0]);
        //                   //}

        // }

        sections[0].color = "#134e13";
        sections[1].color = "#FF8C00";
        sections[2].color = "#d34313";

        var dataArray = new Array();

        for (var k = 0; k < sections.length; k++) {
            dataArray.push(sections[k]);
        }

        return dataArray;

    };
    $scope.getVehRptData = function() {
        var data = {
            "from_date": $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm"),
            "to_date": $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm"),
            "asrt_id": $scope.veh.asrt_id
        }
        // console.log(data);

        $http.post($rootScope.BaseUrl + 'postVehRptgetData', data)
            .success(function(resdata, status) {
                // console.log(resdata);
                $scope.assetData = resdata;
                // if(status == 200)  $rootScope.sessionCheck();
                $scope.getVehAsrtRptData = resdata.data.singleAsrtDtl;
                // console.log($scope.getVehAsrtRptData);
                // console.log($scope.getVehAsrtRptData.length);
                $scope.getVehLastData = resdata.data.groupAsrtDtl;
                // console.log($scope.getVehLastData);

                //var arr=[];

                // console.log($scope.getVehLastData);
                arr.push($scope.getVehAsrtRptData, $scope.getVehLastData);
                // console.log(arr.length);
                var sections = ["Max", "Min", "Avg"];
                nv.addAssetGraph(sections);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }

})

.controller("driverPerformanceRptCtrl", function($scope, $http, NgTableParams, $filter, $rootScope, apppdfService) {

    $scope.searchVehData = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = [];
    $scope.vehCatSettings = {
        "displayProp": "asrt_ctgry_nm",
        "idProp": "asrt_ctgry_id",
        "enableSearch": true,
        "externalIdProp": "asrt_ctgry_id",
        "styleActive": true,
        "keyboardControls": true
    };
    $scope.vehCatTextSettings = {
        "searchPlaceholder": "Search Vehicle Category",
        "buttonDefaultText": "Search Vehicle Category"
    }
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = [];
    $scope.vehGrpSettings = {
        "displayProp": "asrt_grp_nm",
        "idProp": "asrt_grp_id",
        "enableSearch": true,
        "externalIdProp": "asrt_grp_id",
        "styleActive": true,
        "keyboardControls": true
    };
    $scope.vehGrpTextSettings = {
        "searchPlaceholder": "Search Vehicle Group",
        "buttonDefaultText": "Select Vehicle Group"
    }
    $scope.customFilter = '';
    $scope.vehcrewid = null;
    $scope.vehCrewModel = [];
    $scope.vehCrewSettings = {
        "displayProp": "crw_nm",
        "idProp": "crw_id",
        "enableSearch": true,
        "externalIdProp": "crw_id",
        "styleActive": true,
        "keyboardControls": true
    };
    $scope.vehCrewTextSettings = {
        "searchPlaceholder": "Search Driver",
        "buttonDefaultText": "Select Driver"
    }
    $scope.customFilter = '';

    // getting asrt_group list
    // $scope.getDrivers = function() {
        $http.get($rootScope.BaseUrl + 'driversList')
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.driversDtls = resdata.data;
                $scope.drvrsLst=$scope.driversDtls;
                console.log($scope.driversDtls);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    
    // $scope.getDrivers();

    $http.get($rootScope.BaseUrl + 'asrtGroupLst')
        .success(function(resdata, status) {
            if (status == 257) $rootScope.sessionCheck();
            $scope.vehicleGroupLst = resdata.data;
            $scope.vehgrplst = $scope.vehicleGroupLst;
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

    // getting asrt category list
    $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
        .success(function(resdata, status) {
            if (status == 257) $rootScope.sessionCheck();
            $scope.vehicleTypeLst = resdata.data;
            $scope.vehcatlst = $scope.vehicleTypeLst;

        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

$scope.count=0;
$scope.shwFltrs=function(){
      $scope.count++;
      if ($scope.count % 2 ==1) {
        $scope.displayfltrs = true;
      }
      else{
        $scope.displayfltrs = false;
      }
}
    this.openCalendar = function(e, date) {
        that.open[date] = true;
    };
    $scope.openfrom = function($event, openedf) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedf] = true;
    };
    $scope.opento = function($event, openedt) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope[openedt] = true;
    };

    $scope.getDrvrPrfRptLst = function(vehcatid, vehgrpid, vehcrwid, dtFrom, dtTo) {
        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

        var vehgrp = '0';
        for (i = 0; i < $scope.vehGrpModel.length; i++) {
            vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
        }
        var vehcat = '0';
        for (i = 0; i < $scope.vehCatyModel.length; i++) {
            vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
        }
        var vehcrew = '0';
        for (i = 0; i < $scope.vehCrewModel.length; i++) {
            vehcrew = vehcrew + ',' + $scope.vehCrewModel[i].crw_id;
        }

        var data = {
            "fromdt": $scope.dtFrom,
            "todt": $scope.dtTo,
            "vehcat": vehcat,
            "vehgrp": vehgrp,
            "vehcrew": vehcrew
        }
        console.log(data);
        $http.post($rootScope.BaseUrl + 'crewPerformanceRptLst', data)
            .success(function(resdata, status) {
                if (status == 257) $rootScope.sessionCheck();
                $scope.crewPrmceLst = resdata.data;
                console.log($scope.crewPrmceLst);
                $scope.loadfilter();
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getDrvrPrfRptLst();
    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.crewPrmceLst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.crewPrmceLst, params.filter().term) : $scope.crewPrmceLst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    // $scope.loadfilter = function(data) {
    //     $scope.tableFilter = new NgTableParams({
    //         page: 1,
    //         count: 10
    //     }, {
    //         data: data
    //     });
    // };

    $scope.pdfDownload = function() {
        console.log("pdfDownload");
        var jsonColumns = ['SNo', 'asrt_grp_nm', 'asrt_ctgry_nm', 'asrt_nm', 'crew_nm', 'crew_ph', 'fnce_covered', 'total_dist'];
        var colHeadings = ['SNo', 'VEHICLE GROUP', 'VEHICLE CATEGORY', 'VEHICLE NUMBER', 'CREW NAME', 'CREW NUMBER', 'FENCES COVERED (TOT)', 'DISTANCE'];
        var tbldata = $scope.crewPrmceLst;

        var rptHeading = 'DRIVER PERFORMANCE REPORT';
        var fileName = 'DRIVER_PERFORMANCE-RPT';
        var align = 'landscape';
        apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName, align);
    }

    $scope.excelDownload = function() {
        $scope.exprintData = [];
        $scope.exprintData.push({
            'SNo': '',
            'crew_nm': 'DRIVER PERFORMANCE REPORT',
            'crew_ph': '',
            'asrt_grp_nm': '',
            'asrt_ctgry_nm': '',
            'asrt_nm': '',
            'fnce_covered': '',
            'total_dist': ''
        });
        $scope.exprintData.push({
            'SNo': 'SNo',
            'crew_nm': 'CREW NAME',
            'crew_ph': 'CREW NUMBER',
            'asrt_grp_nm': 'VEHICLE GROUP',
            'asrt_ctgry_nm': 'VEHICLE CATEGORY',
            'asrt_nm': 'VEHICLE NUMBER',
            'fnce_covered': 'FENCES COVERED (TOT)',
            'total_dist': 'DISTANCE'
        });
        for (var i = 0; i < $scope.crewPrmceLst.length; i++) {
            $scope.exprintData.push({
                'SNo': $scope.crewPerformanceRptLst[i].SNo,
                'crew_nm': $scope.crewPrmceLst[i].crew_nm,
                'crew_ph': $scope.crewPrmceLst[i].crew_ph,
                'asrt_grp_nm': $scope.crewPrmceLst[i].asrt_grp_nm,
                'asrt_ctgry_nm': $scope.crewPrmceLst[i].asrt_ctgry_nm,
                'asrt_nm': $scope.crewPrmceLst[i].asrt_nm,
                'fnce_covered': $scope.crewPrmceLst[i].fnce_covered,
                'total_dist': $scope.crewPrmceLst[i].total_dist
            });
        }
        return $scope.exprintData;
    }
})

.controller("tripdtlRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
     $scope.searchVehData   = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';
        $scope.hide=function(target) {
            document.getElementById(target).style.display = 'none';
        }
    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };


   $scope.gettripDtlsLst = function(vehcatid,vehgrpid,dtFrom,dtTo){
         $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

        var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }
       
        var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }
        $http.post($rootScope.BaseUrl + 'tripDtlsRptLst',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting fence list");
                $scope.tripdtlslst = resdata.data;
                // console.log($scope.tripdtlslst);
                $scope.loadfilter($scope.tripdtlslst);
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.gettripDtlsLst();

    $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
        page: 1, count: 10
        }, {
        data: data
        });
    };

    $scope.getallTripsLst = function(trp_rn_id,target){
        document.getElementById(target).style.display = 'block';
        $scope.newUser=true;
        $http.get($rootScope.BaseUrl + 'alltripslst/'+trp_rn_id)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting fence list");
                $scope.alltripslst = resdata.data;
                // console.log($scope.alltripslst);
                $scope.loadfilterpulldown($scope.alltripslst);
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    // $scope.getallTripsLst();

     $scope.loadfilterpulldown = function(data){
        $scope.tableFilterPullDn = new NgTableParams({
        page: 1, count: 10
        }, {
        data: data
        });
    };
    $scope.pdfDownload = function()
    {
        console.log("pdfDownload");
              var jsonColumns = ['fnce_nm','asrt_nm','asrt_ctgry_nm','strt_dt','end_dt', 'actl_trvl_tm','dst_cov'];
              var colHeadings = ['NAME','VEHICLE NUMBER','VEHICLE TYPE','START DATE','END DATE','TRAVEL TIME','DISTANCE COVERED'];
               var tbldata = $scope.tripdtlslst;
                
              var rptHeading = 'TRIP_DETAILS REPORT';
              var fileName = 'TRIP-DTL-RPT';
              var align= 'landscape';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }

    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'fnce_nm':'','asrt_nm':'', 'asrt_ctgry_nm':'TRIP-DETAILS REPORT', 'strt_dt':'','end_dt':'','actl_trvl_tm':'','dst_cov':''});
    $scope.exprintData.push({'fnce_nm':'NAME','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE','strt_dt':'START DATE','end_dt':'END DATE','actl_trvl_tm':'TRAVEL TIME','dst_cov':'DISTANCE COVERED'});
    for(var i=0;i<$scope.tripdtlslst.length;i++){
            $scope.exprintData.push({'fnce_nm':$scope.tripdtlslst[i].fnce_nm,'asrt_nm':$scope.tripdtlslst[i].asrt_nm,'asrt_ctgry_nm':$scope.tripdtlslst[i].asrt_ctgry_nm,'strt_dt':$scope.tripdtlslst[i].strt_dt,'end_dt':$scope.tripdtlslst[i].end_dt,'actl_trvl_tm':$scope.tripdtlslst[i].actl_trvl_tm,'dst_cov':$scope.tripdtlslst[i].dst_cov});
          }
    return $scope.exprintData;
  }
$scope.pullDownPdfDownload = function()
    {
        console.log("pdfDownload");
              var jsonColumns = ['asrt_nm','fnce_nm','arl_ts','dep_ts'];
              var colHeadings = ['VEHICLE NUMBER','NAME','ARRIVAL','DEPARTURE'];
               var tbldata = $scope.alltripslst;
                
              var rptHeading = 'ALL-TRIP-DETAILS REPORT';
              var fileName = 'ALL-TRIP-DTL-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }
  

    $scope.pullDownExcelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'asrt_nm':'','fnce_nm':'', 'arl_ts':'TRIP-DETAILS REPORT', 'dep_ts':''});
    $scope.exprintData.push({'asrt_nm':'VEHICLE NUMBER','fnce_nm':'NAME','arl_ts':'ARRIVAL','dep_ts':'DEPARTURE'});
    for(var i=0;i<$scope.alltripslst.length;i++){
            $scope.exprintData.push({'asrt_nm':$scope.alltripslst[i].asrt_nm,'fnce_nm':$scope.alltripslst[i].fnce_nm,'arl_ts':$scope.alltripslst[i].arl_ts,'dep_ts':$scope.alltripslst[i].dep_ts});
          }
    return $scope.exprintData;
  }

})
.controller("landmarkCovRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
    $scope.searchVehData   = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };


    $scope.getLndMrkCovLst = function(vehcatid,vehgrpid,dtFrom,dtTo){
          $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

         var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }

        var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }
        $http.post($rootScope.BaseUrl + 'lndMrkCovRptLst',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting fence list");
                $scope.lndMarkCovlst = resdata.data;
                // console.log($scope.lndMarkCovlst);
                $scope.loadfilter();
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getLndMrkCovLst();
    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.lndMarkCovlst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.lndMarkCovlst, params.filter().term) : $scope.lndMarkCovlst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    //  $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };

    $scope.count=0;
        $scope.shwFltrs=function(){
          $scope.count++;
          if ($scope.count % 2 ==1) {
            $scope.displayfltrs = true;
          }
          else{
            $scope.displayfltrs = false;
          }  
        }

    $scope.pdfDownload = function()
    {
        console.log("pdfDownload");
              var jsonColumns = ['dt','asrt_nm','asrt_ctgry_nm','fnce_covered'];
              var colHeadings = ['DATE','VEHICLE NUMBER','VEHICLE TYPE','COVERED LANDMARKS'];
               var tbldata = $scope.lndMarkCovlst;
                
              var rptHeading = 'LANDMARKS-COVERAGE REPORT';
              var fileName = 'LNDMRK-COV-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }

    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'dt':'','asrt_nm':'', 'asrt_ctgry_nm':'LANDMARK-COVERAGE REPORT', 'fnce_covered':''});
    $scope.exprintData.push({'dt':'DATE','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE','fnce_covered':'COVERED LANDMARKS'});
    for(var i=0;i<$scope.lndMarkCovlst.length;i++){
            $scope.exprintData.push({'dt':$scope.lndMarkCovlst[i].dt,'asrt_nm':$scope.lndMarkCovlst[i].asrt_nm,'asrt_ctgry_nm':$scope.lndMarkCovlst[i].asrt_ctgry_nm,'fnce_covered':$scope.lndMarkCovlst[i].fnce_covered});
          }
    return $scope.exprintData;
  }
})
.controller("fenceInOutRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,assetsService,apppdfService){
    $scope.searchVehData   = '';
    $scope.curdate = new Date();
    
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    $scope.vehcatid = null;
    $scope.vehCatyModel = []; 
    $scope.vehCatSettings = {"displayProp": "asrt_ctgry_nm", "idProp": "asrt_ctgry_id", "enableSearch": true, "externalIdProp": "asrt_ctgry_id", "styleActive": true, "keyboardControls": true};
    $scope.vehCatTextSettings={"searchPlaceholder":"Search Vehicle Category","buttonDefaultText":"Search Vehicle Category"}
    $scope.customFilter = '';

    $scope.vehgrpid = null;
    $scope.vehGrpModel = []; 
    $scope.vehGrpSettings = {"displayProp": "asrt_grp_nm", "idProp": "asrt_grp_id", "enableSearch": true, "externalIdProp": "asrt_grp_id", "styleActive": true, "keyboardControls": true};
    $scope.vehGrpTextSettings={"searchPlaceholder":"Search Vehicle Group","buttonDefaultText":"Select Vehicle Group"}
    $scope.customFilter = '';

    // getting asrt_group list
          $http.get($rootScope.BaseUrl + 'asrtGroupLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleGroupLst = resdata.data;
                $scope.vehgrplst=$scope.vehicleGroupLst;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });

            // getting asrt category list
            $http.get($rootScope.BaseUrl + 'asrtCtgryLst')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.vehicleTypeLst = resdata.data;
                $scope.vehcatlst=$scope.vehicleTypeLst;
                
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };
    $scope.count=0;
$scope.shwFltrs=function(){
  $scope.count++;
  if ($scope.count % 2 ==1) {
    $scope.displayfltrs = true;
  }
  else{
    $scope.displayfltrs = false;
  }
  
}
     $scope.getFnceLst = function(){     
    $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
    $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");

     var vehgrp = '0';
            for (i = 0; i < $scope.vehGrpModel.length; i++) {
                vehgrp = vehgrp + ',' + $scope.vehGrpModel[i].asrt_grp_id;
            }
        var vehcat = '0';
            for (i = 0; i < $scope.vehCatyModel.length; i++) {
                vehcat = vehcat + ',' + $scope.vehCatyModel[i].asrt_ctgry_id;
            }

             var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo,
            "vehcat":vehcat,
            "vehgrp":vehgrp
        }
        $http.post($rootScope.BaseUrl + 'fnceRptLst',data)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                console.log("Getting fence list");
                $scope.fencelst = resdata.data;
                // console.log($scope.fencelst);
                $scope.loadfilter();
            })
                .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getFnceLst();
    $scope.filter = { term: $scope.searchVehData }; 

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.fencelst.length,
        getData: function($defer, params) {
            var orderedData = params.filter() ? $filter('filter')($scope.fencelst, params.filter().term) : $scope.fencelst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
}
    //  $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };
    $scope.pdfDownload = function()
    {
              var jsonColumns = ['fnce_nm','asrt_nm','asrt_ctgry_nm','time_in','time_out','timdiff'];
              var colHeadings = ['FENCE NAME','VEHICLE NUMBER','VEHICLE TYPE','TIME IN','TIME OUT','TIME DIFFERENCE'];
               var tbldata = $scope.fencelst;
                
              var rptHeading = 'Fence IN-OUT REPORT';
              var fileName = 'FNCE-INOUT-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }
    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'fnce_nm':'','asrt_nm':'','asrt_ctgry_nm':'','time_in':'','time_out':'FENCE-IN-OUT REPORT', 'timdiff':''});
    $scope.exprintData.push({'fnce_nm':'FENCE NAME','asrt_nm':'VEHICLE NUMBER','asrt_ctgry_nm':'VEHICLE TYPE','time_in':'TIME IN','time_out':'TIME OUT', 'timdiff':'TIME DIFFERENCE'});
    for(var i=0;i<$scope.fencelst.length;i++){
            $scope.exprintData.push({'fnce_nm':$scope.fencelst[i].fnce_nm,'asrt_nm':$scope.fencelst[i].asrt_nm,'asrt_ctgry_nm':$scope.fencelst[i].asrt_ctgry_nm,'time_in':$scope.fencelst[i].time_in,'time_out':$scope.fencelst[i].time_out, 'timdiff':$scope.fencelst[i].timdiff});
          }
    return $scope.exprintData;
  }
})
.controller("warcovgRptCtrl",function($scope,$http,NgTableParams,$filter,$rootScope,apppdfService){
    $scope.searchVehData   = '';
    $scope.curdate = new Date();
    $scope.dtFrom = $filter('date')($scope.curdate, "yyyy-MM-dd 00:00:00");
    $scope.dtTo = $filter('date')($scope.curdate, "yyyy-MM-dd 23:59:00");

    this.openCalendar = function(e, date) {
    that.open[date] = true;
  };
  $scope.openfrom = function($event, openedf) {
    $event.preventDefault();$event.stopPropagation();$scope[openedf] = true;
  };
  $scope.opento = function($event, openedt) {
    $event.preventDefault();$event.stopPropagation();$scope[openedt] = true;
  };
    
   $scope.getWrdCovgRptLst = function(dtFrom,dtTo){
        $scope.dtFrom = $filter('date')($scope.dtFrom, "yyyy-MM-dd HH:mm:ss");
        $scope.dtTo = $filter('date')($scope.dtTo, "yyyy-MM-dd HH:mm:ss");
        var data =
        {
            "fromdt":$scope.dtFrom,
            "todt":$scope.dtTo
        }
        // console.log(data);
        $http.post($rootScope.BaseUrl + 'wrdcovgRptLst/',data)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.wrdcovglst = resdata.data;
                $scope.loadfilter();
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    $scope.getWrdCovgRptLst();

    $scope.filter = { term: $scope.searchVehData }; 
    // $scope.loadfilter = function(data){
    //     $scope.tableFilter = new NgTableParams({
    //     page: 1, count: 10
    //     }, {
    //     data: data
    //     });
    // };

    $scope.loadfilter = function(){
      $scope.tableFilter = new NgTableParams({
        page: 1, count: 10, filter: $scope.filter
      }, {
        total: $scope.wrdcovglst.length,
        getData: function($defer, params) {
            $scope.populateSummaryCards();
            var orderedData = params.filter() ? $filter('filter')($scope.wrdcovglst, params.filter().term) : $scope.wrdcovglst;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
         }
      });
    };
    
    $scope.count=0;
$scope.shwFltrs=function(){
  $scope.count++;
  if ($scope.count % 2 ==1) {
    $scope.displayfltrs = true;
  }
  else{
    $scope.displayfltrs = false;
  }
  
}
    // Custom filter
    $scope.populateSummaryCards = function() {
      
        $scope.totalCvdCollectionPnts = 0;
        $scope.totalCollectionPnts = 0;
        $scope.totalCvdDmprBns = 0;
        $scope.totalDmprBns = 0;
        
        for (var i=0;i< $scope.wrdcovglst.length;i++) {
          // if ($scope.wrdcovglst[i].fnce_grp_nm.indexOf($scope.filter.term) != -1) {
            if ($scope.wrdcovglst[i].fnce_grp_nm==='Collection Points') {
            if ($scope.wrdcovglst[i].fns_crd) {
              $scope.totalCvdCollectionPnts = $scope.totalCvdCollectionPnts + $scope.wrdcovglst[i].fns_crd;  
            }
            if ($scope.wrdcovglst[i].tot_fncs) {
              $scope.totalCollectionPnts = $scope.totalCollectionPnts + $scope.wrdcovglst[i].tot_fncs;
            }
        }
        if ($scope.wrdcovglst[i].fnce_grp_nm==='Dumper Bins') {
            if ($scope.wrdcovglst[i].fns_crd) {
              $scope.totalCvdDmprBns = $scope.totalCvdDmprBns  + $scope.wrdcovglst[i].fns_crd;
            }
            if ($scope.wrdcovglst[i].tot_fncs) {
              $scope.totalDmprBns = $scope.totalDmprBns  + $scope.wrdcovglst[i].tot_fncs;
            }
        }
          // }
        }
    } 

    $scope.pdfDownload = function()
    {
        console.log("pdfDownload");
              var jsonColumns = ['zone_fnce_nm','ward_fnce_nm','fnce_grp_nm','tot_fncs','fns_crd','min_cvr','max_cvr'];
              var colHeadings = ['Zone Name','Ward Name','Collection Area Category Name','Total Count','Covered Count','Minimum Coverage Count','Maximum Coverage Count'];
               var tbldata = $scope.wrdcovglst;
                
              var rptHeading = 'WARD-WISE_COVERAGE_REPORT';
              var fileName = 'WRDCOVG-RPT';
              var align= 'portrait';
              apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
    }

    $scope.excelDownload=function()
    {
    $scope.exprintData=[];
    $scope.exprintData.push({'zone_fnce_nm':'','ward_fnce_nm':'', 'fnce_grp_nm':'WARD-WISE COVERAGE REPORT', 'tot_fncs':'', 'fns_crd':'', 'min_cvr':'', 'max_cvr':''});
    $scope.exprintData.push({'zone_fnce_nm':'Zone Name','ward_fnce_nm':'Ward Name','fnce_grp_nm':'Collection Area Category Name','tot_fncs':'Total Count','fns_crd':'Covered Count','min_cvr':'Minimum Coverage Count','max_cvr':'Maximum Coverage Count'});
    for(var i=0;i<$scope.wrdcovglst.length;i++){
            $scope.exprintData.push({'zone_fnce_nm':$scope.wrdcovglst[i].zone_fnce_nm,'ward_fnce_nm':$scope.wrdcovglst[i].ward_fnce_nm,'fnce_grp_nm':$scope.wrdcovglst[i].fnce_grp_nm,'tot_fncs':$scope.wrdcovglst[i].tot_fncs,'fns_crd':$scope.wrdcovglst[i].fns_crd,'min_cvr':$scope.wrdcovglst[i].min_cvr,'max_cvr':$scope.wrdcovglst[i].max_cvr});
          }
    return $scope.exprintData;
  }
});