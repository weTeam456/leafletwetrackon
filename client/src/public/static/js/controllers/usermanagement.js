wertrackon
// =========================================================================
// User Management related controllers
// =========================================================================

    // =========================================================================
    // User Management Side bar controller
    // =========================================================================
    .controller('sidebarUMCtrl',function($state,$http,localStorageService,sideBarMenuService,$rootScope,checkPermService,$scope){
        var app_id = 1;
        $http.get('../../apiv1/user/menuLst/'+$rootScope.userCmpltDtls.usr_id+'/'+app_id)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            // console.log(resdata.data)
            $scope.appMenu(resdata.data)
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });

        $scope.appMenu = function(responseData){
            $scope.menuitem = sideBarMenuService(responseData)
        }
        
        $rootScope.canRead = function(menuNm){
            return checkPermService.canRead(menuNm,$scope.menuitem);
        }
        $rootScope.canCreate = function(menuNm){
            return checkPermService.canCreate(menuNm,$scope.menuitem);
        }
        $rootScope.canUpdate = function(menuNm){
            return checkPermService.canUpdate(menuNm,$scope.menuitem);
        }
        $rootScope.canDelete = function(menuNm){
            return checkPermService.canDelete(menuNm,$scope.menuitem);
        }
        $rootScope.canEdit = function(menuNm){
            return checkPermService.canEdit(menuNm,$scope.menuitem);
        }
        
    })

    // =========================================================================
    // User Management Dashboard controller - Sony - 29/04/2017
    // =========================================================================
    .controller('userAppHomeCtrl', function($scope,$rootScope){
        /**Code for user active count chart**/
        $scope.useractiveDtls = [{
                "date":"27-04",
                "total_users":2578,
                "active" : 2576,
                "inactive":2,
            }, {
                "date":"28-04",
                "total_users":2578,
                "active": 1576,
                "inactive":1002
            }, {
                "date":"29-04",
                "total_users":2578,
                "active": 2526,
                "inactive":52
            }, {
                "date":"30-04",
                "total_users":2578,
                "active": 2076,
                "inactive":502
            }, {
                "date":"01-05",
                "total_users":2578,
                "active": 2324,
                "inactive":254
            },
            {
                "date":"02-05",
                "total_users":2578,
                "active": 1778,
                "inactive":800
            }];
        var day=[];
        for(var i=0;i<=$scope.useractiveDtls.length-1;i++){
            day.push($scope.useractiveDtls[i].date)
        }
        $scope.useractive_options={
            "chart": {
                "type": "multiBarChart",
                "height": 450,
                // "margin": {
                // "top": 20,
                // "right": 20, 
                // "bottom": 40,
                // },
                "useInteractiveGuideline": true,
                "clipEdge": true,
                "duration": 500,
                "xAxis": {
                    "axisLabel": "Date",
                    "showMaxMin": false,
                     tickFormat: 
                        function(d){
                            return day[d]
                        }
                },
                yDomain: [0, 2578],
                "yAxis": {
                    "axisLabel": "Status Counts",
                    tickFormat: 
                        function (d){
                            return d3.format(',0f')(d);
                    }
                }
            }
        }
        $scope.useractive_dataset= generatedata1();
        function generatedata1(){
            var total_users_active=[], total_users_inactive=[];
            for(var i=0;i<=$scope.useractiveDtls.length-1; i++){
                total_users_active.push({x:i,y:$scope.useractiveDtls[i].active});
                total_users_inactive.push({x:i,y:$scope.useractiveDtls[i].inactive});
            }
            var dataArray = [
                {
                    values: total_users_active,      //values - represents the array of {x,y} data points
                    key: "Total Active Users", //key  - the name of the series.
                    color: '#ff7f0e',  //color - optional: choose your own line color.
                    total_users:$scope.useractiveDtls[0].total_users
                },
                {
                    values: total_users_inactive,
                    key: "Total InActive Users",
                    color: '#2ca02c'
                }
            ];
            return dataArray;
        }

        // vehicle status chart
        $scope.vehactiveDtls = [{
                "date":"27-04",
                "total_vehicles":3000,
                "active" : 3000,
                "inactive": 578,
            }, {
                "date":"28-04",
                "total_vehicles":3000,
                "active": 2778,
                "inactive":222
            }, {
                "date":"29-04",
                "total_vehicles":3000,
                "active": 2,
                "inactive": 2998
            }, {
                "date":"30-04",
                "total_vehicles":3000,
                "active": 2076,
                "inactive":924
            }, {
                "date":"01-05",
                "total_vehicles":3000,
                "active": 2746,
                "inactive":254
            },
            {
                "date":"02-05",
                "total_vehicles":3000,
                "active": 1898,
                "inactive":1102
            }];

            var days=[];
        for(var i=0;i<=$scope.vehactiveDtls.length-1;i++){
            days.push($scope.vehactiveDtls[i].date)
        }

         $scope.vehactive_options={
            "chart": {
                "type": "multiBarChart",
                "height": 450,
                "useInteractiveGuideline": true,
                "clipEdge": true,
                "duration": 500,
                // "stacked": true,
                "xAxis": {
                    "axisLabel": "Date",
                    "showMaxMin": false,
                     tickFormat: 
                        function(d){
                            return days[d]
                        }
                },
                yDomain: [0, 2578],
                "yAxis": {
                    "axisLabel": "Status Counts",
                    tickFormat: 
                        function (d){
                            return d3.format(',0f')(d);
                    }
                }
            }
        }

        $scope.vehactive_dataset= generatedata2();
        function generatedata2(){
            var total_vehicles_active=[], total_vehicles_inactive=[];
            for(var i=0;i<=$scope.vehactiveDtls.length-1; i++){
                total_vehicles_active.push({x:i,y:$scope.vehactiveDtls[i].active});
                total_vehicles_inactive.push({x:i,y:$scope.vehactiveDtls[i].inactive});
            }
            var vehDataArray = [
                {
                    values: total_vehicles_active,      //values - represents the array of {x,y} data points
                    key: "Total Active Vehicles", //key  - the name of the series.
                    color: '#ff7f0e',  //color - optional: choose your own line color.
                    total_vehicles:$scope.vehactiveDtls[0].total_vehicles
                },
                {
                    values: total_vehicles_inactive,
                    key: "Total InActive Vehicles",
                    color: '#2ca02c'
                }
            ];
            return vehDataArray;
        }

    })
    //=================================================
    // Profile
    //=================================================

    .controller('profileCtrl', function($scope,growlService,$http,$rootScope,localStorageService){
        
        //Get Profile Information from profileService Service
        
        //User
        $scope.fst_nm = $rootScope.userCmpltDtls.fst_nm;
        $scope.lst_nm = $rootScope.userCmpltDtls.lst_nm;
        $scope.fullName = $rootScope.userCmpltDtls.fst_nm+" "+$rootScope.userCmpltDtls.lst_nm;
        $scope.designation = $rootScope.userCmpltDtls.dsgns_nm;
        $scope.mobileNumber =  $rootScope.userCmpltDtls.mobile_nu;
        $scope.emailAddress =  $rootScope.userCmpltDtls.eml_tx;
        $scope.dsgnId = $rootScope.userCmpltDtls.dsgns_id;
        $scope.usr_nm = $rootScope.userCmpltDtls.usr_nm;

        $scope.getDesignations = function(){
            $http.get('../../apiv1/designations/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.designations = resdata.data;
                })
                .error(function(data, status, headers, config) {
                console.log(data)
            });
       }
         $scope.dsgnIdget = function(dsgnId){
            $scope.dsgnId=dsgnId
            console.log($scope.dsgnId);
         }

         $scope.ProfileEdit = function(mobileNumber,emailAddress,designation,fst_nm,lst_nm){
            var updtPrflDtl={
                "fst_nm":fst_nm,
                "lst_nm":lst_nm,
                "mobile_nu":mobileNumber,
                "eml_tx":emailAddress,
                "usr_nm":$scope.usr_nm,
                "dsgns_id":$scope.dsgnId
             }            
            // updating profile details in table
           $http.post('../../apiv1/user/updateMyProfile',updtPrflDtl)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.resMesg = resdata;
                    if(resdata.status == 601) {
                        growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
                    } else {
                        growlService.growl(' Successfully updated!', 'inverse');
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });          
        }
        this.editInfo = 0;
        this.editContact = 0;
    
        
        this.submit = function(item, message) { 
            if(item === 'profileInfo') {
                this.editInfo = 0;
            }
            
            if(item === 'profileContact') {
                this.editContact = 0;
            }
          
        }
        $scope.upload_picture= function(){
            console.log("hi");
        }


    })

    // =========================================================================
    //  Users controller
    // =========================================================================

    .controller("usersCtrl",function($scope,apppdfService,clntDataService,tntDatabyClntService,$http,NgTableParams,growlService,$rootScope,localStorageService){
        
        $scope.newUser=true;
        $scope.userFormDtls = {};
        
        $scope.searchUsrData='';

        clntDataService.then(function(response){
             $scope.clientDtls = response.data.data;
        });

        $scope.getTenants = function(clntid){
            tntDatabyClntService(clntid).then(function(response){
                $scope.tenantDtls = response.data.data;
            });
        }

        $scope.cancel = function() {
          $scope.showModal = false;
          $scope.userFormDtls = {};
        };
            //Edit
        this.editInfo = false;
        this.edit = function(idx,item){
            this.editInfo = idx;
        };

        this.save = function(idx,item){
            this.editInfo = false;
            growlService.growl(' Successfully updated!', 'inverse'); 
        };
        
        $scope.getUsers = function(){
            $http.get('../../apiv1/users/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.usersDtls = resdata.data;
                    $scope.loadfilter($scope.usersDtls);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
        }
        $scope.getUsers();

        $scope.getDesignations = function(clnt_id,tnt_id){
            $http.get('../../apiv1/designations/'+clnt_id+'/'+tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.designations = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            }); 
        }
        $scope.getProfiles = function(clnt_id,tnt_id){
            $http.get('../../apiv1/allProfiles/'+clnt_id+'/'+tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                profileFormat(resdata.data);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }
        function profileFormat(profileDtls){
            // console.log(profileDtls);
            var temp = [];
            if (temp.length == 0) {
                temp.push({app_prfle_id:profileDtls[0].app_prfle_id,app_prfle_nm:profileDtls[0].app_prfle_nm})
            }
            for(var k=0;k<profileDtls.length;k++){
                var flag = false;
                for (var j = 0; j < temp.length; j++) {
                    if (temp[j].app_prfle_id == profileDtls[k].app_prfle_id)
                        flag = true;
                }
                if (!flag){
                    temp.push({app_prfle_id:profileDtls[k].app_prfle_id,app_prfle_nm:profileDtls[k].app_prfle_nm})
                }
            }
            for (var j = 0; j < temp.length; j++) {
                var final = [];
                for(var k=0;k<profileDtls.length;k++){
                    if (temp[j].app_prfle_id == profileDtls[k].app_prfle_id)
                    final.push({app_id:profileDtls[k].app_id,mnu_prfle_id:profileDtls[k].mnu_prfle_id})
                }
                temp[j].mnudtls = final;
            }
            // console.log(temp)
            $scope.appProfiles = temp;
        }
        $scope.loadfilter = function(data){
        
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };
    
        $scope.PDFDownload = function(){
                var jsonColumns = ['usr_id','fst_nm','lst_nm','usr_nm','mobile_nu','eml_tx'];
                var colHeadings = ['S.no','First Name','Last Name','User Name','Mobile Number','Email Address'];
                var tbldata = $scope.usersDtls;

                var rptHeading = 'Users Details';
                var fileName = 'USR_DTL';
                var align= 'portrait';
                apppdfService.pdfDownload(jsonColumns, colHeadings, tbldata, rptHeading, fileName,align);
        }
        $scope.ExcelDownload=function(){
            $scope.exprintData=[];
            $scope.exprintData.push({'usr_id':'','fst_nm':'', 'lst_nm':'', 'usr_nm':'', 'mobile_nu':'', 'eml_tx':''});
            $scope.exprintData.push({'usr_id':'S.no','fst_nm':'First Name','lst_nm':'Last Name','usr_nm':'User Name','mobile_nu':'Mobile Number','eml_tx':'Email Address'});
            for(var i=0;i<$scope.usersDtls.length;i++){
                    $scope.exprintData.push({
                        'usr_id':$scope.usersDtls[i].usr_id,
                        'fst_nm':$scope.usersDtls[i].fst_nm,
                        'lst_nm':$scope.usersDtls[i].lst_nm,
                        'usr_nm':$scope.usersDtls[i].usr_nm,
                        'mobile_nu':$scope.usersDtls[i].mobile_nu,
                        'eml_tx':$scope.usersDtls[i].eml_tx
                    });
            }
            return $scope.exprintData;
        }
        $scope.selectedInfo = {};
        $scope.editUserInfo = function(item){
            $scope.selectedInfo = angular.copy(item);
        };
     
    $scope.pwdPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    $scope.profilesData = {};$scope.menuProfiles=[];
    
    $scope.adminCreateUser = function() {
        $scope.profilesData = JSON.parse($scope.profilesData);
        $scope.userFormDtls.app_prfle_id = $scope.profilesData.app_prfle_id;
        // console.log($scope.userFormDtls);

        $http.post('../../apiv1/user/create',$scope.userFormDtls)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            if(resdata.status == 200){
                assignUsers_Mnu(resdata.data[0].usr_id);
            }else if(resdata.status == 601) {
                console.log(resdata.errors);
                growlService.growl('Invalid Parameters Send..!!', 'inverse');
            }else if(resdata.status == 700){
                if( resdata=='UsernameExistsException')
                    growlService.growl('User Already Exist', 'danger');
            }
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });
    }
    function assignUsers_Mnu(usr_id){
        $scope.menuProfiles = [];
        angular.forEach($scope.profilesData.mnudtls,function(data){
             if(data.mnu_prfle_id != null)
                $scope.menuProfiles.push({app_id:data.app_id,mnu_prfle_id:data.mnu_prfle_id})
        })
        $http.post('../../apiv1/user/assignMenu/'+usr_id+'/'+$scope.userFormDtls.clnt_id+'/'+$scope.userFormDtls.tnt_id,$scope.menuProfiles)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            
            growlService.growl('Successfully created new user!', 'inverse');
            $scope.getUsers();
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });
    }
    $scope.addUserClear = function(){
        // console.log($scope.userFormDtls);
        $scope.userFormDtls = {};
        growlService.growl('Data cleared', 'inverse'); 
    };
    $scope.deleteUser = function(item){
        swal({
                title   : "Are you sure you want to delete this user?",
                text    : "Once you delete user can not access this. Make sure you want to do this",
                type    : "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, Delete It",
                closeOnConfirm: true
            }, function() {
                $http.post('../../apiv1/user/delete',item)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    if(resdata.status == 200){
                        $scope.resMesg = resdata;
                        growlService.growl('user is deleted successfully', 'inverse');
                        $scope.getUsers();
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                });
            });
    }
    $scope.saveUserInfo = function(item) {
        // updating users in table                                                
        $http.post('../../apiv1/user/update',item)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            $scope.resMesg = resdata;
            if(resdata.status == 601) {
                 console.log(resdata.errors);
                growlService.growl(' Invalid Data, Please Check..!!', 'inverse');
            } else {
                growlService.growl(' Successfully updated!', 'inverse');
            }
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        });
    }
})
    // =========================================================================
    // Password Reset Group controllers
    // =========================================================================
    .controller('changePwdCtrl', function(recentpostService,$scope,$http,$rootScope,growlService){
        $scope.userData = {}
        $scope.pwdPattern=/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/;
        
        $scope.changePwd = function() {
            if($scope.userData.password == $scope.userData.cnfrmPwd){
                $http.post('../../apiv1/changePwd',{username : $scope.userCmpltDtls.usr_nm , password : $scope.userData.password})
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    growlService.growl(' Successfully changed!', 'inverse');
                })
                .error(function (status, err) {
                    growlService.growl(' Something went wrong, please try again!', 'inverse');
                });
            }else growlService.growl('Not matched', 'warning');
        }
       $scope.resetPrflPwdEdit = function() {
            $scope.userData = {};
       } 

    })
    // =========================================================================
    // Password Resets controllers
    // =========================================================================
    .controller('pwdResetsCtrl', function($rootScope,NgTableParams,$scope,growlService,$http){
        $scope.searchUsrData='';
        $scope.pwdResetUsr = function(){
            $http.get('../../apiv1/getPwdResetUsr/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.pwdRsetUsrs = resdata.data;
                $scope.loadfilter($scope.pwdRsetUsrs);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            })
        }
        $scope.pwdResetUsr();
    
        $scope.loadfilter = function(data){
        $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };
        $scope.loadfilter($scope.pwdResetsLst);
        $scope.PDFDownload = function(){
            console.log("pdf")
        };
        $scope.ExcelDownload = function(){
            console.log("excel")
        };
        $scope.editPwd=function(item){
            console.log(item)
            $scope.ResetPwd=true;
            $scope.pwdResetUsrDtls.username = item.usr_nm;
            $scope.pwdResetUsrDtls.phno = item.mobile_nu;       
        }
        $scope.pwdResetUsrDtls = {};
        $scope.resetPwd = function() {
            console.log($scope.pwdResetUsrDtls);  
            if($scope.pwdResetUsrDtls && $scope.pwdResetUsrDtls.password) {
                $http.post($rootScope.BaseUrl+"changePwd",$scope.pwdResetUsrDtls)
                .success(function(resdata, status, headers, config) {
                    growlService.growl("Successfully changed",'success');
                })
                .error(function(data, status, headers, config) {
                    console.log(status);
                    growlService.growl("Unable to connect to server. Please check your internet connectivity.");
                });
            }else growlService.growl("Please fill the form..",'success');
            
        }
    })
    // =========================================================================
    // App Profile controllers
    // =========================================================================
    .controller('AppProfilesCtrl', function(clntDataService,tntDatabyClntService,growlService,$scope,NgTableParams,$http,$rootScope){
            $scope.searchUsrData='';
            $scope.newAppPrfl = true;
            $scope.editAppPrfl = true;

        $http.get('../../apiv1/users/getappPrfl/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
            .success(function (resdata, status) {
                usrAppsDtls(resdata.data);
            })
            .error(function(data, status, headers, config) {
                if(status == 257){
                    $rootScope.sessionCheck();
                }
            });
        function usrAppsDtls(data){
            var temp = [];
            if (temp.length == 0) {
                temp.push({usr_nm: data[0].usr_nm,app_prfle_nm:data[0].app_prfle_nm});
            }
            for (var i = 0; i < data.length; i++) {
                var flag = false;
                for (var j = 0; j < temp.length; j++) {
                    if (temp[j].usr_nm == data[i].usr_nm && temp[j].app_prfle_nm == data[i].app_prfle_nm)
                        flag = true;
                }
                if (!flag)  temp.push({usr_nm: data[i].usr_nm,app_prfle_nm:data[i].app_prfle_nm});
            }
            for (var j = 0; j < temp.length; j++) {
                final = [];
                for (var k = 0; k < data.length; k++) {
                    if (temp[j].usr_nm == data[k].usr_nm && temp[j].app_prfle_nm == data[k].app_prfle_nm)
                        final.push({appName: data[k].app_nm});
                }
                temp[j].appNames = final;
            }
            $scope.apPrflUsrs = temp;
            $scope.loadfilter($scope.apPrflUsrs);
        }
        $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };
        $scope.loadfilter($scope.userDtlLst);
        $scope.PDFDownload = function(){
            console.log("pdf")
        };
        $scope.ExcelDownload = function(){
            console.log("excel")
        };
        clntDataService.then(function(response){
             $scope.clientDtls = response.data.data;
        });

        $scope.getTenants = function(clntid){
            tntDatabyClntService(clntid).then(function(response){
                $scope.tenantDtls = response.data.data;
            });
        }
        $http.get('../../apiv1/getApps/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            // console.log(resdata.data);
            $scope.appsDtls = resdata.data;
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        })
        
        if($scope.userCmpltDtls.clnt_id==401){
            $http.get('../../apiv1/users')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.allUserDtls = resdata.data;
                // console.log($scope.allUserDtls);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }else{
            $http.get('../../apiv1/users/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.allUserDtls = resdata.data;
                // console.log($scope.allUserDtls);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }
        $scope.moveUp = function(num) {
            if (num > 0) {
                tmp = $scope.appsLst[num - 1];
                $scope.appsLst[num - 1] = $scope.appsLst[num];
                $scope.appsLst[num] = tmp;
                $scope.selectedRow--;
            }
        }
        $scope.moveDown = function(num) {
            if (num < $scope.appsLst.length - 1) {
                tmp = $scope.appsLst[num + 1];
                $scope.appsLst[num + 1] = $scope.appsLst[num];
                $scope.appsLst[num] = tmp;
                $scope.selectedRow++;
            }
        }
        $scope.counter = 1;$scope.appUsers = [];$scope.appPrflNM = '';
        $scope.appsLst=[{  sqnceId:$scope.counter++,appID:''}];
        $scope.cancelApp = function(){
            $scope.appUsers=[];
            $scope.err_msg='';
            $scope.counter = 1;
            $scope.appsLst=[{  sqnceId:$scope.counter++, appID:''}];
        }
        $scope.addApp = function(){
            $scope.appsLst.push({  sqnceId:$scope.counter++, appID:''});
        }
        $scope.err_msg='';
        $scope.removeApp = function(idx){
            $scope.err_msg='';
            $scope.appsLst.splice(idx, 1);
        }
        $scope.createAppPrfl = function(){
            if($scope.appsLst.length>1 && $scope.userCmpltDtls.clnt_id == 401){
                $http.post("../../apiv1/create/appPrf/"+$scope.clnt_id+"/"+$scope.tnt_id+"/"+$scope.appPrflNM,$scope.appsLst)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck()
                    growlService.growl('app profile created successfully', 'inverse');
                    $scope.appsLst=[{  sqnceId:$scope.counter++,appID:''}];
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                })
            }else if($scope.appsLst.length>1 && $scope.userCmpltDtls.clnt_id != 401){
                $http.post("../../apiv1/create/appPrf/"+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id+"/"+$scope.appPrflNM,$scope.appsLst)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck()
                    assignUsers();
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                })
            }else growlService.growl('Please check what you have entered', 'inverse');
            function assignUsers(){
                if($scope.appUsers && $scope.appsLst.length>1){
                    $http.post("../../apiv1/appPrf/assignusers/"+$scope.appPrflNM,$scope.appUsers)
                    .success(function (resdata, status) {
                        if(status == 257)  $rootScope.sessionCheck()
                        growlService.growl('app profile created successfully', 'inverse');
                        $scope.appsLst=[{  sqnceId:$scope.counter++,appID:''}];
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                    })
                }
            }
        };
    })

    // =========================================================================
    // Menu Profile controllers
    // =========================================================================
    .controller('menuProfilesCtrl', function($rootScope,clntDataService,tntDatabyClntService,$http,NgTableParams,growlService,$scope){
        $scope.newMenu = true;
        $scope.openMenuOptions = false;
        $scope.searchUsrData='';
        $http.get('../../apiv1/getmenuPrfl/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            usrMnuDtls(resdata.data);            
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        })
        function usrMnuDtls(data){
            var temp = [];
            if (temp.length == 0) {
                temp.push({usr_nm: data[0].usr_nm,app_nm:data[0].app_nm,mnu_prfle_nm:data[0].mnu_prfle_nm});
            }
            for (var i = 0; i < data.length; i++) {
                var flag = false;
                for (var j = 0; j < temp.length; j++) {
                    if (temp[j].usr_nm == data[i].usr_nm && temp[j].app_nm == data[i].app_nm && temp[j].mnu_prfle_nm == data[i].mnu_prfle_nm)
                        flag = true;
                }
                if (!flag)  temp.push({usr_nm: data[i].usr_nm,app_nm:data[i].app_nm,mnu_prfle_nm:data[i].mnu_prfle_nm});
            }
            for (var j = 0; j < temp.length; j++) {
                final = [];
                for (var k = 0; k < data.length; k++) {
                    if (temp[j].usr_nm == data[k].usr_nm && temp[j].app_nm == data[k].app_nm && temp[j].mnu_prfle_nm == data[k].mnu_prfle_nm)
                        final.push({menuitem: data[k].mnu_itm_nm});
                }
                temp[j].menuitems = final;
            }
            $scope.usrMnuPrlDtls = temp;
            $scope.loadfilter($scope.usrMnuPrlDtls);
        }
        $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };
        $scope.loadfilter($scope.userDtlLst);
        $scope.PDFDownload = function(){
            console.log("pdf")
        };
        $scope.ExcelDownload = function(){
            console.log("excel")
        };
        if($scope.userCmpltDtls.clnt_id==401){
            $http.get('../../apiv1/users')
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.allUserDtls = resdata.data;
                // console.log($scope.allUserDtls);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }else{
            $http.get('../../apiv1/users/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.allUserDtls = resdata.data;
                // console.log($scope.allUserDtls);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
        }

        clntDataService.then(function(response){
             $scope.clientDtls = response.data.data;
        });

        $scope.getTenants = function(clntid){
            tntDatabyClntService(clntid).then(function(response){
                $scope.tenantDtls = response.data.data;
            });
        }
        
        $http.get('../../apiv1/getApps/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
        .success(function (resdata, status) {
            if(status == 257)  $rootScope.sessionCheck();
            // console.log(resdata.data);
            $scope.appsDtls = resdata.data;
        })
        .error(function(data, status, headers, config) {
            console.log(data);
        })

        $scope.getMnuItms = function(appid){
            // console.log(appid);
            $http.get('../../apiv1/getMnuItmsbyappID/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id+'/'+appid)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.appMnuItems = resdata.data;
                $scope.openMenuOptions = true;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            })
            $http.get('../../apiv1/getSubMnuItmsbyappID/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id+'/'+appid)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.appSubMnuItems = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            })
        }
        
        $scope.moveUp = function(num) {
            if (num > 0) {
                tmp = $scope.menuitem[num - 1];
                $scope.menuitem[num - 1] = $scope.menuitem[num];
                $scope.menuitem[num] = tmp;
                $scope.selectedRow--;
            }
        }
        $scope.moveDown = function(num) {
            if (num < $scope.menuitem.length - 1) {
                tmp = $scope.menuitem[num + 1];
                $scope.menuitem[num + 1] = $scope.menuitem[num];
                $scope.menuitem[num] = tmp;
                $scope.selectedRow++;
            }
        }
        $scope.counter = 1;$scope.menuUsers = [];
        $scope.menuitem=[{  sqnceId:$scope.counter++, mnu_itm_id:'', permc:1, permr:1, permu:1,  permd:1, permo:1, perme:1 }];
        $scope.cancelMnu = function(){
            $scope.menuUsers=[];
            $scope.err_msg='';
            $scope.counter = 1;
            $scope.menuitem=[{  sqnceId:$scope.counter++, mnu_itm_id:'', permc:1, permr:1, permu:1,  permd:1, permo:1, perme:1 }];
        }
        $scope.addMenuItem = function(){
            $scope.menuitem.push({  sqnceId:$scope.counter++, mnu_itm_id:'', permc:1, permr:1, permu:1,  permd:1, permo:1, perme:1 });
        }
        $scope.err_msg='';
        $scope.removeMenuItem = function(idx){
            $scope.err_msg='';
            $scope.menuitem.splice(idx, 1);
        }
        $scope.createmenu = function(){
            // console.log($scope.menuitem)
            if($scope.menuitem.length>1 && $scope.userCmpltDtls.clnt_id == 401){
                $http.post("../../apiv1/create/menu/"+$scope.clnt_id+"/"+$scope.tnt_id+"/"+$scope.appID+"/"+$scope.menuNM,$scope.menuitem)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck()
                    console.log("menu create success");
                    assignUsers();
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                })
            }else if($scope.menuitem.length>1 && $scope.userCmpltDtls.clnt_id != 401){
                $http.post("../../apiv1/create/menu/"+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id+"/"+$scope.appID+"/"+$scope.menuNM,$scope.menuitem)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck()
                    console.log("menu create success");
                    assignUsers();
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                })
            }else growlService.growl('Please check what you have entered', 'inverse');
        }; 
        function assignUsers(){
            if($scope.userCmpltDtls.clnt_id == 401 && $scope.menuUsers){
                $http.post("../../apiv1/menu/assignusers/"+$scope.appID+"/"+$scope.menuNM,$scope.menuUsers)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck()
                    console.log("assign success");
                    growlService.growl('Profile created successfully', 'inverse');
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                })
            }else if($scope.userCmpltDtls.clnt_id != 401 && $scope.menuUsers){
                $http.post("../../apiv1/menu/assignusers/"+$scope.appID+"/"+$scope.menuNM,$scope.menuUsers)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck()
                    console.log("assign success");
                    growlService.growl('Profile created successfully', 'inverse');
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                })
            }
        } 
        
    })
    // =========================================================================
    // Designation  controllers
    // =========================================================================
    .controller('designationCtrl', function(recentpostService,$scope,growlService,$rootScope,NgTableParams,$http){
        $scope.searchUsrData='';
           $scope.dsgnsUsrsGet = function(){ 
           $http.get('../../apiv1/getUsrsWthDsgns/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
                    .success(function (resdata, status) {
                       if(status == 257)  $rootScope.sessionCheck();
                        $scope.resMesg = resdata.data;
                        $scope.loadfilter($scope.resMesg);
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                    })    
           }
        $scope.dsgnsUsrsGet(); 
        $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };
        // $scope.loadfilter($scope.userDtlLst);
        $scope.PDFDownload = function(){
            console.log("pdf")
        };
        $scope.ExcelDownload = function(){
            console.log("excel")
        };
        $scope.selectedInfo = {};
        $scope.editDesignationPermission = function(item){
            $scope.selectedInfo = angular.copy(item);
        };
         $scope.dsgnIdget = function(dsgnId){
            $scope.dsgnId=dsgnId
            console.log($scope.dsgnId);
        }
        $scope.saveDesignationPermission = function(item){
            if ($scope.editInfo !== false) {
                $scope.selected = angular.copy(item);
                $scope.dtls={
                    "user_id": item.usr_id,
                    "dsgnId": $scope.dsgnId
                }
                console.log($scope.dtls);
                   $http.post('../../apiv1/updtDsgn',$scope.dtls)
                    .success(function (resdata, status) {
                        if(status == 257)  $rootScope.sessionCheck();
                        $scope.designations = resdata.data;
                        $scope.dsgnsUsrsGet();
                        growlService.growl(' Successfully updated!', 'inverse');  
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                    }); 
               
            }
        };
        $scope.getDesignations = function(){
        $http.get('../../apiv1/designations/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                $scope.designations = resdata.data;
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            }); 
       }
       
        
        $scope.addNew = function(u){
           $scope.userDtlLst.push({
                    "sno":u.no,
                    "user_nm":u.user_nm,
                    "fst_nm":u.fst_nm,
                    "lst_nm":u.lst_nm,
                    "desig": u.desig
           });
           $scope.loadfilter($scope.userDtlLst);
           growlService.growl('Successfully added new row!', 'inverse'); 
           $scope.addData=false;
           u.no="";
           u.user_nm="";
           u.fst_nm="";
           u.lst_nm="";
           u.desig="";
        };
    })
    // =========================================================================
    // Heirarchy  controllers
    // =========================================================================
    .controller('heirarchyCtrl', function(recentpostService,$scope,growlService,$rootScope,NgTableParams,$http){
        $scope.searchUsrData='';
           $http.get('../../apiv1/getUserHeirarchy/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
            .success(function (resdata, status) {
                if(status == 257)  $rootScope.sessionCheck();
                console.log(resdata.data)
                $scope.loadfilter(resdata.data);
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            })
        $scope.loadfilter = function(data){
            $scope.tableFilter = new NgTableParams({
                page: 1, count: 10
            }, {
                data: data
            });
        };
        
        $scope.PDFDownload = function(){
            console.log("pdf")
        };
        $scope.ExcelDownload = function(){
            console.log("excel")
        };
        $scope.selectedInfo = {};
        $scope.EditOrgLvls = function(item){
            $scope.selectedInfo = angular.copy(item);
            $http.get('../../apiv1/organisational/level/'+$rootScope.userCmpltDtls.clnt_id+'/'+$rootScope.userCmpltDtls.tnt_id)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    $scope.orgLvlLst = resdata.data;
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                }); 
        };
        $scope.saveOrgLvls = function(item){
            if ($scope.editInfo !== false) {
                $scope.selected = angular.copy(item);
                growlService.growl(' Successfully updated!', 'inverse'); 
                $http.post('../../apiv1/Heirarchy/level',$scope.selected)
                .success(function (resdata, status) {
                    if(status == 257)  $rootScope.sessionCheck();
                    growlService.growl(' Successfully updated!', 'inverse'); 
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                }); 
            }
        };
})