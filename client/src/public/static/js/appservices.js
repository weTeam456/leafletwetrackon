wertrackon

    // =========================================================================
    // clientDataService - Sony Angel
    // =========================================================================
    
        .service('clntDataService', function($http){
            var callback = $http.get('../apiv1/client')
                            .success(function (resdata, status) {
                                if(status == 257)  $rootScope.sessionCheck();
                                return resdata.data;
                            })
                            .error(function(data, status, headers, config) {
                                console.log(data);
                            })
            return callback;            
        })
    
    // =========================================================================
    // tntDatabyClntService - Sony Angel
    // =========================================================================
    
        .service('tntDatabyClntService', function($http){
            var callback =  function (clntid){
                    return $http.get('../../apiv1/client/'+clntid+'/tenants')
                            .success(function (resdata, status) {
                                if(status == 257)  $rootScope.sessionCheck();
                                // console.log(resdata.data)
                                return resdata.data;
                            })
                            .error(function(data, status, headers, config) {
                                console.log(data);
                            })
                        }
            return callback;            
        })

    // =========================================================================
    // assetsService
    // =========================================================================

        .service('assetsService', function($rootScope, $http) {
            var assetsService = {};

            assetsService.resdata = {};

            //Gets the list of asset tpyes
            assetsService.getAsrtLst = function() {
                $http.get($rootScope.BaseUrl + 'asrtLst')
                    .success(function(resdata) {
                        assetsService.resdata.assetlst = resdata.data;
                    });

                return  assetsService.resdata;
            };

            return assetsService;
        })

    // =========================================================================
    // sideBarMenuService - Sony Angel
    // =========================================================================
    
        .service('sideBarMenuService', function($http){
            var callback = function(responseData){
                // console.log(responseData)
                    var temp = [];
                    if (temp.length == 0) {
                        if(responseData[0].prnt_mnu_itm_id == responseData[0].mnu_itm_id){
                            temp.push({
                                prnt_mnu_itm_id: responseData[0].prnt_mnu_itm_id,
                                prnt_mnu_itm_nm: responseData[0].mnu_itm_nm,
                                prnt_mnu_itm_icn_tx:responseData[0].mnu_itm_icn_tx,
                                prnt_sqnce_id : responseData[0].sqnce_id
                            });
                        }
                    }
                    for(var l=0;l<responseData.length;l++){
                        if(responseData[l].prnt_mnu_itm_id != 0){
                            var flag = false;
                            for (var j = 0; j < temp.length; j++) {
                                if (responseData[l].prnt_mnu_itm_id !== 0 && temp[j].prnt_mnu_itm_id == responseData[l].prnt_mnu_itm_id)
                                    flag = true;
                            }
                            if (!flag){
                                if(responseData[l].prnt_mnu_itm_id == responseData[l].mnu_itm_id){
                                    temp.push({
                                        prnt_mnu_itm_id: responseData[l].prnt_mnu_itm_id,
                                        prnt_mnu_itm_nm: responseData[l].mnu_itm_nm,
                                        prnt_mnu_itm_icn_tx:responseData[l].mnu_itm_icn_tx,
                                        prnt_sqnce_id : responseData[l].sqnce_id
                                    });
                                }
                            }
                        }else{
                                temp.push(responseData[l])
                            }
                    }
                    for (var j = 0; j < temp.length; j++) {
                        final = [];
                        for(var k=0;k<responseData.length;k++){
                            if(responseData[k].prnt_mnu_itm_id != 0){
                                if (responseData[k].prnt_mnu_itm_id != responseData[k].mnu_itm_id && responseData[k].prnt_mnu_itm_id !== 0 && temp[j].prnt_mnu_itm_id == responseData[k].prnt_mnu_itm_id)
                                    final.push({
                                        c_in:responseData[k].c_in,
                                        d_in:responseData[k].d_in,
                                        e_in:responseData[k].e_in,
                                        o_in:responseData[k].o_in,
                                        r_in:responseData[k].r_in,
                                        u_in:responseData[k].u_in,
                                        mnu_itm_nm: responseData[k].mnu_itm_nm,
                                        mnu_itm_icn_tx:responseData[k].mnu_itm_icn_tx,
                                        mnu_itm_url_tx:responseData[k].mnu_itm_url_tx,
                                        sqnce_id : responseData[k].sqnce_id
                                    });
                            }
                        }
                        temp[j].appNames = final;
                    }
                    //  console.log(temp);
                    return temp;
                }
            return callback;
        })
    
    // =========================================================================
    // checkPermService - Sony Angel
    // =========================================================================
    
        .service('checkPermService', function($http){
            var pid_object;
            var callback = {
                canRead :  function(menuNm,menuItems) {
                            angular.forEach( menuItems, function (pLst) {
                                if(pLst.mnu_itm_nm === menuNm){
                                    pid_object = pLst;
                                }else{
                                    angular.forEach(pLst.appNames, function (sbLst) {
                                        if (sbLst.mnu_itm_nm === menuNm) {
                                            pid_object = sbLst;
                                        }
                                    })
                                }
                                return pid_object;
                            })
                            // console.log(pid_object);
                            if(pid_object){
                                var create_perm = pid_object.r_in;
                                if (create_perm == 0) return false;  
                                else return true;
                            }
                        },
                canCreate :  function(menuNm,menuItems) {
                            angular.forEach( menuItems, function (pLst) {
                                if(pLst.mnu_itm_nm === menuNm){
                                    pid_object = pLst;
                                }else{
                                    angular.forEach(pLst.appNames, function (sbLst) {
                                        if (sbLst.mnu_itm_nm === menuNm) {
                                            pid_object = sbLst;
                                        }
                                    })
                                }
                                return pid_object;
                            })
                            // console.log(pid_object);
                            if(pid_object){
                                var create_perm = pid_object.c_in;
                                if (create_perm == 0) return false;  
                                else return true;
                            }
                        },
                canUpdate :  function(menuNm,menuItems) {
                            angular.forEach( menuItems, function (pLst) {
                                if(pLst.mnu_itm_nm === menuNm){
                                    pid_object = pLst;
                                }else{
                                    angular.forEach(pLst.appNames, function (sbLst) {
                                        if (sbLst.mnu_itm_nm === menuNm) {
                                            pid_object = sbLst;
                                        }
                                    })
                                }
                                return pid_object;
                            })
                            // console.log(pid_object);
                            if(pid_object){
                                var create_perm = pid_object.u_in;
                                if (create_perm == 0) return false;  
                                else return true;
                            }
                        },
                canDelete :  function(menuNm,menuItems) {
                            angular.forEach( menuItems, function (pLst) {
                                if(pLst.mnu_itm_nm === menuNm){
                                    pid_object = pLst;
                                }else{
                                    angular.forEach(pLst.appNames, function (sbLst) {
                                        if (sbLst.mnu_itm_nm === menuNm) {
                                            pid_object = sbLst;
                                        }
                                    })
                                }
                                return pid_object;
                            })
                            // console.log(pid_object);
                            if(pid_object){
                                var create_perm = pid_object.d_in;
                                if (create_perm == 0) return false;  
                                else return true;
                            }
                        },
                canEdit :  function(menuNm,menuItems) {
                            angular.forEach( menuItems, function (pLst) {
                                if(pLst.mnu_itm_nm === menuNm){
                                    pid_object = pLst;
                                }else{
                                    angular.forEach(pLst.appNames, function (sbLst) {
                                        if (sbLst.mnu_itm_nm === menuNm) {
                                            pid_object = sbLst;
                                        }
                                    })
                                }
                                return pid_object;
                            })
                            // console.log(pid_object);
                            if(pid_object){
                                var create_perm = pid_object.e_in;
                                if (create_perm == 0) return false;  
                                else return true;
                            }
                        }
            }
            return callback;            
        })

.directive('selectpicker', function () {
    return {
     restrict: "A",
     require: "ngModel",
     link: function(scope, element, attrs, ctrl) {
        setTimeout(function() {
            element.selectpicker('refresh');
        }, 500);
    }
   }
  });