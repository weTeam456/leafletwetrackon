wertrackon
        .run(function (AclService,$rootScope,$http, $location,$window,localStorageService) {

            $rootScope.aclAbilities = localStorageService.get('aclData');
            // console.log($rootScope.aclAbilities);
            AclService.setAbilities($rootScope.aclAbilities);
            AclService.attachRole('User_Access_Control');

            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
                if(error === 'Unauthorized'){
                    $window.location.href = 'ds#/unauthorized';
                }
            })
        })

        .config(function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/appStore");

            $stateProvider

                     .state('unauthorized', {
                        url: '/unauthorized',
                        templateUrl: 'client/src/unauthorized.html'
                    })

                    //------------------------------
                    // DREAM APP STORE
                    //------------------------------

                    .state('appStore', {
                        url: '/appStore',
                        templateUrl: 'client/src/views/appstore/dreamappStore.html',
                        controller : 'appStoreCtrl'
                    })

                    //------------------------------
                    // VTS
                    //------------------------------

                    .state('support', {
                        url: '/support',
                        templateUrl: 'client/src/views/support/supportAppHome.html',
                        resolve: {
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/vendors/sparklines/jquery.sparkline.min.js',
                                            'client/src/public/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
           // =========================================


                   .state('MyTickets', {
                        url: '/MyTickets',
                        views: {
                            // the main template will be placed here (relatively named)
                            '': { templateUrl: 'client/src/views/support/supportTicket.html',
                                controller: 'dashBrdTicketctrlr' 
                            },
                            // the child views will be defined here (absolutely named)
                            'viewA@MyTickets': { templateUrl: 'client/src/views/support/dashBrdopentickets.html',
                                },
                            // for column two, we'll define a separate controller 
                            'viewB@MyTickets': { 
                                templateUrl: 'client/src/views/support/dashBrdclosedtickets.html',                     
                            }
                        }
                        
                    })
           // =========================================

                     .state('MyTickets.pane5', {
                       url: '/:open_id/:closed_id',
                      views: {
                        "viewA": {
                          templateUrl: 'client/src/views/support/dashBrdopentickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        },
                         "viewB": {
                          templateUrl: 'client/src/views/support/dashBrdclosedtickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        }
                      
                      }
                    })  

                     .state('MyTickets.pane4', {
                       url: '/:open_id/:closed_id',
                      views: {
                        "viewA": {
                          templateUrl: 'client/src/views/support/dashBrdopentickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        },
                         "viewB": {
                          templateUrl: 'client/src/views/support/dashBrdclosedtickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        }
                      
                      }
                    })  

                     .state('MyTickets.pane3', {
                       url: '/:open_id/:closed_id',
                      views: {
                        "viewA": {
                          templateUrl: 'client/src/views/support/dashBrdopentickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        },
                         "viewB": {
                          templateUrl: 'client/src/views/support/dashBrdclosedtickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        }
                      
                      }
                    })  

                     .state('MyTickets.pane2', {
                       url: '/:open_id/:closed_id',
                      views: {
                        "viewA": {
                          templateUrl: 'client/src/views/support/dashBrdopentickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        },
                         "viewB": {
                          templateUrl: 'client/src/views/support/dashBrdclosedtickets.html',
                          controller: 'dashBrdTicketctrlr',
                        //  data: {}
                        }
                      
                      }
                    })  

                     .state('openTkts', {
                        url: '/openTkts',
                        templateUrl: 'client/src/views/support/open.html',
                        controller: 'TicketDtlctrlr'
                    })
                       .state('ticketId', {
                        url: '/openTkts/:tkt_id',
                        templateUrl: 'client/src/views/support/Ticketstatuschange.html',
                        controller: 'TicketStatusChangectrlr'
                    })

                      .state('acceptedTicket', {
                        url: '/support/acceptedTicket',
                        templateUrl: 'client/src/views/support/acceptedTktDtl.html',
                        controller: 'acceptedTktDtlctrlr'
                    })
                    .state('closeticketId', {
                        url: '/support/acceptedTicket/:tkt_id',
                        templateUrl: 'client/src/views/support/acceptTktStsChange.html',
                        controller: 'acceptTktStsChangectrlr'
                    }) 

                    .state('supportCreateTicket', {
                        url: '/support/CreateTicket',
                        templateUrl: 'client/src/views/support/CreateTicket.html'
                    })

                    //------------------------------
                    // Live Tracking
                    //------------------------------

                    .state('liveTracking', {
                        url: '/liveTracking',
                        templateUrl: 'client/src/views/livetracking/map.html',
                        controller: 'liveTrackingHomeCtrl',
                         resolve: {
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                     .state('liveTracking.livemap', {
                        url: '/livemap',
                        templateUrl: 'client/src/views/livetracking/liveTracking.livemap.html',
                        controller: 'liveTrackingCtrl',
                        resolve: {
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                    .state('liveTracking.playback', {
                        url: '/playback',
                        templateUrl: 'client/src/views/livetracking/liveTracking.playback.html',
                        controller: 'playbackCtrl',
                         resolve: {
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                    //------------------------------
                    // Geo Fence
                    //------------------------------



                    .state('geoCoding', {
                        url: '/geoCoding',
                        templateUrl: 'client/src/views/geocodning/geoCodingHome.html',
                        resolve: {
                            'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
                  .state('lndMrkGrp', {
                        url: '/geoCoding/landmark/Groups',
                        templateUrl: 'client/src/views/geocodning/landmark_group.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/landmark/Groups')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                   .state('addUpdatLndMrkGroup', {
                        url: '/geoCoding/landmark/addUpdtLandMarkGrp',
                        templateUrl: 'client/src/views/geocodning/addLandMark_group.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/landmark/Groups')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                   })
                   .state('lndMrkCtgry', {
                        url: '/geoCoding/landmark/Category',
                        templateUrl: 'client/src/views/geocodning/landmark_category.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/landmark/Category')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                   .state('updtLandMarkCate', {
                        url: '/geoCoding/landmark/addUpdtLandMarkCate',
                        templateUrl: 'client/src/views/geocodning/addLandMark_category.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/landmark/Category')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
                   .state('lndMrks', {
                        url: '/geoCoding/landmarks',
                        templateUrl: 'client/src/views/geocodning/landmarks.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/landmarks')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })


                      .state('geoFenceGrp', {
                        url: '/geoCoding/geoFence/Group',
                        templateUrl: 'client/src/views/geocodning/geoFence_group.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/geoFence/Group')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                   .state('addUpdtgeoFenceGrp', {
                        url: '/geoCoding/geoFence/addUpdtgeoFenceGrp',
                        templateUrl: 'client/src/views/geocodning/addGeoFence_group.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/geoFence/Group')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                   })
                   .state('geoFenceCtgry', {
                        url: '/geoCoding/geoFence/Category',
                        templateUrl: 'client/src/views/geocodning/geoFence_category.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/geoFence/Category')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                   .state('updtGeoFenceCate', {
                        url: '/geoCoding/geoFence/updtGeoFenceCate',
                        templateUrl: 'client/src/views/geocodning/addGeoFence_Category.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/geoFence/Category')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                   })
                   .state('geoFence', {
                        url: '/geoCoding/geoFence',
                        templateUrl: 'client/src/views/geocodning/geoFence.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/geoCoding/geoFence')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
                   //------------------------------
                    // VTS
                    //------------------------------

                    .state('vtsHome', {
                        url: '/vts',
                        templateUrl: 'client/src/views/vts/dashboard/vtsHome.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vtsDshbrd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([

                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js',
                                            // 'client/src/public/static/vendors/sparklines/jquery.sparkline.min.js',
                                            'client/src/public/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                    .state('vtsDashboard', {
                        url: '/vtsDshbrd',
                        templateUrl: 'client/src/views/vts/dashboard/vtsDashboard.html',
                        controller : 'santationDashCtrl',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vtsDshbrd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js',
                                            // 'client/src/public/static/vendors/sparklines/jquery.sparkline.min.js',
                                            'client/src/public/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                    //------------------------------
                    // Zone wise dashboard
                    //------------------------------

                    .state('vtsDbdEchVeh', {
                        url: '/vtsDbdEchVeh',
                        templateUrl: 'client/src/views/vts/dashboard/zoneDashboard.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vtsDshbrd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
 
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            // 'client/src/public/static/vendors/sparklines/jquery.sparkline.min.js',
                                             'client/src/public/static/vendors/pdfmake/vfs_fonts.js',
                                            'client/src/public/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                    //------------------------------
                    // Vehicle wise dashboard
                    //------------------------------

                    .state('vtsDbdVeh', {
                        url: '/vtsDbdVeh/:asrt_id',
                        templateUrl: 'client/src/views/vts/dashboard/vehicleDashboard.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vtsDshbrd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js',
                                            'client/src/public/static/vendors/sparklines/jquery.sparkline.min.js',
                                            'client/src/public/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })


                     .state('vehicleMst', {
                        url: '/vts/md/vehicle_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_vehicles.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vehicle_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     .state('vendorMst', {
                        url: '/vts/md/vendor_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_vendor.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vendor_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     // add or update vendors
                     .state('addUpdvendorMst', {
                        url: '/vts/md/vendor_add_upd_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_vendors_add_upd.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vendor_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })  

                     .state('vehicleCtgryMst', {
                        url: '/vts/md/vehicle_category_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_vehicle_category.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vehicle_category_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     // add or update new veh category
                     .state('vehCtgryAddUpdMst', {
                        url: '/vts/md/vehicle_category_add_upd_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_veh_category_add_upd.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vehicle_category_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })

                     .state('vehicleDeptMst', {
                        url: '/vts/md/vehicle_dept_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_vehicle_departments.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vehicle_dept_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                      // added new route for add/update departments
                     .state('addVehicleDeptMst', {
                        url: '/vts/md/addDeprtmnt',
                        templateUrl: 'client/src/views/vts/masterdata/dept_add_upd.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vehicle_dept_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
                     // added new route for add/update vehicles
                     .state('addUpdVehicles', {
                        url: '/vts/md/addVehicle',
                        templateUrl: 'client/src/views/vts/masterdata/md_veh_add_upd.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/vehicle_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                     .state('tripsMst', {
                        url: '/vts/md/trips_master',
                        templateUrl: 'client/src/views/vts/masterdata/md_trips.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/md/trips_master')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     .state('distanceRpt', {
                        url: '/vts/rpt/distance_report',
                        templateUrl: 'client/src/views/vts/reports/rp_distance.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/distance_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                 // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     .state('fenceInOutRpt', {
                        url: '/vts/rpt/fence_inout_report',
                        templateUrl: 'client/src/views/vts/reports/rp_fence_inout.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/fence_inout_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     .state('landmarkInOutRpt', {
                        url: '/vts/rpt/landmark_inout_report',
                        templateUrl: 'client/src/views/vts/reports/rp_landmark_inout.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/landmark_inout_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                     .state('milageRpt', {
                        url: '/vts/rpt/milage_report',
                        templateUrl: 'client/src/views/vts/reports/rp_milage.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/milage_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                      .state('dumpCoveredRpt', {
                        url: '/vts/rpt/dumpCovered_report',
                        templateUrl: 'client/src/views/vts/reports/rp_dumpCovered.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vtsDshbrd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                    .state('vehicleRpt', {
                        url: '/vts/rpt/vehicle_report',
                        templateUrl: 'client/src/views/vts/reports/rp_vehicle.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/vehicle_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })

                    .state('driverPerformanceRpt', {
                        url: '/vts/rpt/driverPerformance_report',
                        templateUrl: 'client/src/views/vts/reports/rp_driverPerformance.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/driverPerformance_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                    .state('coverageRpt', {
                        url: '/vts/rpt/reports/coverage_report',
                        templateUrl: 'client/src/views/vts/reports/rp_coverage.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/reports/coverage_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    }) 
                    .state('tripsRpt', {
                        url: '/vts/rpt/trips_report',
                        templateUrl: 'client/src/views/vts/reports/rp_trips.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/trips_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })  
                    .state('tripsdetailRpt', {
                        url: '/vts/rpt/trips_detail_report',
                        templateUrl: 'client/src/views/vts/reports/rp_tripdetails.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/trips_detail_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                        .state('landmarkntcovdRpt', {
                        url: '/vts/rpt/reports/rp_landmark_ntcovd',
                        templateUrl: 'client/src/views/vts/reports/rp_landmark_ntcovd.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/reports/rp_landmark_ntcovd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })

                    .state('vehdtlsRpt', {
                        url: '/vts/rpt/vehdtls_report',
                        templateUrl: 'client/src/views/vts/reports/rp_vehdtls.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/vehdtls_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })  
                    
                    .state('fencscovdhrRpt', {
                        url: '/vts/rpt/fencs_covd_hour_report',
                        templateUrl: 'client/src/views/vts/reports/rp_fncscovdhr.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/fencs_covd_hour_report')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })

                      .state('fuelOps', {
                        url: '/vts/ops/fuel_operations',
                        templateUrl: 'client/src/views/vts/operations/op_fuel.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/ops/fuel_operations')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                    .state('alertSubscription', {
                        url: '/vts/ops/alertsSubscription',
                        templateUrl: 'client/src/views/vts/operations/alertSubscription.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/ops/alertsSubscription')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                      .state('alerts', {
                        url: '/vts/ops/alertDtls',
                        templateUrl: 'client/src/views/vts/operations/smsAlertDtls.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/ops/alertDtls')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                      .state('driverAssignment', {
                        url: '/vts/ops/driverAssignment',
                        templateUrl: 'client/src/views/vts/operations/driverAssignment.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/ops/driverAssignment')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
                    .state('wardCovgRpt', {
                        url: '/vts/rpt/wardCovgRpt',
                        templateUrl: 'client/src/views/vts/reports/rp_wardcoverage.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/rpt/wardCovgRpt')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })


                     .state('weeklyHeatMapRpt', {
                        url: '/vts/weeklyHeatMapRpt',
                        templateUrl: 'client/src/views/vts/reports/rp_weekly_heat_map.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vts/weeklyHeatMapRpt')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })

                    .state('vtsDbdColPnts', {
                        url: '/vtsDbdColPnts',
                        templateUrl: 'client/src/views/vts/dashboard/collectionpntsDashboard.html',
                        controller: 'collectionPntsDashCtrl',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/vtsDshbrd')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js',
                                            'client/src/public/static/vendors/sparklines/jquery.sparkline.min.js',
                                            'client/src/public/static/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })

                    //------------------------------
                    // HEADERS
                    //------------------------------
                    .state('headers', {
                        url: '/headers',
                        templateUrl: 'client/src/views/common-2.html'
                    })

                    .state('headers.textual-menu', {
                        url: '/textual-menu',
                        templateUrl: 'client/src/views/textual-menu.html'
                    })

                    .state('headers.image-logo', {
                        url: '/image-logo',
                        templateUrl: 'client/src/views/image-logo.html'
                    })

                    .state('headers.mainmenu-on-top', {
                        url: '/mainmenu-on-top',
                        templateUrl: 'client/src/views/mainmenu-on-top.html'
                    })


                    //------------------------------
                    // USER MANAGEMENT 
                    //------------------------------

                    .state('userMgmt', {
                        url: '/usermanagement',
                        templateUrl: 'client/src/views/usermanagement/userAppHome.html',
                        resolve: {
                            'acl' : ['$q', 'AclService', function($q, AclService) {
                                if(AclService.can('#/usermanagement')){
                                    return true;
                                } else {
                                    return $q.reject('Unauthorized');
                                }
                            }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'vendors',
                                        insertBefore: '#app-level-js',
                                        files: [
                                            'client/src/public/static/lib/nv.d3.min.js'
                                        ]
                                    }
                                ])
                            }
                        }
                    })

                   .state('pages', {
                        url: '/pages',
                        templateUrl: 'client/src/views/common.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/pages/profile/MyProfile')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })


                    //Profile

                    .state('pages.profile', {
                        url: '/profile',
                        templateUrl: 'client/src/views/usermanagement/profile/profile.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/pages/profile/MyProfile')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                    .state('pages.profile.profile-pwdReset', {
                        url: '/profile-pwdReset',
                        templateUrl: 'client/src/views/usermanagement/profile/profile-pwdReset.html'
                    })


                    .state('pages.profile.userMgmtProfile', {
                        url: '/MyProfile',
                        templateUrl: 'client/src/views/usermanagement/MyProfile.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/pages/profile/MyProfile')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                    .state('userMgmtUsers', {
                        url: '/usermanagement/users',
                        templateUrl: 'client/src/views/usermanagement/users.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/users')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })
                    .state('userMgmtPwdResets', {
                        url: '/usermanagement/pwdResets',
                        templateUrl: 'client/src/views/usermanagement/pwdResets.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/pwdResets')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })

                    .state('userMgmtAppProfiles', {
                        url: '/usermanagement/appProfiles',
                        templateUrl: 'client/src/views/usermanagement/appProfiles.html',
                        resolve: {
                            'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/appProfiles')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'css',
                                        insertBefore: '#app-level',
                                        files: [
                                            'client/src/public/static/vendors/bower_components/chosen/chosen.min.css'
                                        ]
                                    },
                                    {
                                        name: 'vendors',
                                        files: [
                                            'client/src/public/static/vendors/bower_components/chosen/chosen.jquery.js',
                                            'client/src/public/static/vendors/bower_components/angular-chosen-localytics/chosen.js',
                                        ]
                                    }
                                ])
                            }
                        }
                    })

                    .state('userMgmtMenuProfiles', {
                        url: '/usermanagement/menuProfiles',
                        templateUrl: 'client/src/views/usermanagement/menuProfiles.html',
                         resolve: {
                             'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/menuProfiles')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                            loadPlugin: function($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'css',
                                        insertBefore: '#app-level',
                                        files: [
                                            'client/src/public/static/vendors/bower_components/chosen/chosen.min.css'
                                        ]
                                    },
                                    {
                                        name: 'vendors',
                                        files: [
                                            'client/src/public/static/vendors/bower_components/chosen/chosen.jquery.js',
                                            'client/src/public/static/vendors/bower_components/angular-chosen-localytics/chosen.js',
                                        ]
                                    }
                                ])
                            }
                        }
                    })
                    .state('userMgmtDesignations', {
                        url: '/usermanagement/designations',
                        templateUrl: 'client/src/views/usermanagement/designations.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/designations')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }],
                                loadPlugin: function($ocLazyLoad) {
                                    return $ocLazyLoad.load([
                                        {
                                            name: 'vendors',
                                            insertBefore: '#app-level-js',
                                            files: [
                                                // 'client/src/public/static/lib/pdfmake/pdfmake.min.js',
                                                'client/src/public/static/vendors/pdfmake/vfs_fonts.js'
                                            ]
                                        }
                                    ])
                                }
                        }
                    })  
                    .state('userMgmtHeirarchy', {
                        url: '/usermanagement/heirarchy',
                        templateUrl: 'client/src/views/usermanagement/heirarchy.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/heirarchy')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
                    .state('userMgmtSettings', {
                        url: '/usermanagement/settings',
                        templateUrl: 'client/src/views/usermanagement/settings.html',
                        resolve: {
                                'acl' : ['$q', 'AclService', function($q, AclService) {
                                    if(AclService.can('#/usermanagement/settings')){
                                        return true;
                                    } else {
                                        return $q.reject('Unauthorized');
                                    }
                                }]
                        }
                    })
        });