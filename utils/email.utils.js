var nodemailer = require('nodemailer');

var ses = require('nodemailer-ses-transport');
var awstransporter = nodemailer.createTransport(ses({
    accessKeyId: 'AWSACCESSKEY',
    secretAccessKey: 'AWS/Secret/key'
}));


// create reusable transporter object using the default SMTP transport 
var transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');
 
// setup e-mail data with unicode symbols 
var mailOptions = {
    from: '"Sunil Mulagada 👥" <sunil.mulagada@dreamstep.in>', // sender address 
    to: 'msunilkumar@gmail.com, baz@blurdybloop.com', // list of receivers 
    subject: 'Testing nodemailer ✔', // Subject line 
    text: 'Hello world 🐴', // plaintext body 
    html: '<b>Hello world 🐴</b>' // html body 
};
 
// send mail with defined transport object 
transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});



// send mail with defined transport object 
awstransporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});