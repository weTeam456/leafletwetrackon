var moment = require('moment');

exports.secToDateString = function(seconds) {

		var d = moment.duration(seconds, 'seconds');
		var days = Math.floor(d.asDays());
		var hours = Math.floor(d.asHours()) - days * 24;
		var mins = Math.floor(d.asMinutes()) - (hours+days * 24) * 60;
		var sec = seconds % 60;
		return days+" days :: "+hours+" hours :: "+mins+" mins :: "+sec+" sec";

};

exports.MinToDateString = function(seconds) {

		var d = moment.duration(os.uptime(), 'minutes');
		var days = Math.floor(d.asDays());
		var hours = Math.floor(d.asHours()) - days * 24;
		var mins = minutes % 60;
		return days+" days :: "+hours+" hours :: "+mins+" mins";

};