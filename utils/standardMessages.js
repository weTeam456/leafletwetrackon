/********************************************************************************************
---------------------------------------------------------------------------------------------
File              : logmessages.js
Description       : All the standard messages and codes used in the application are listed here.
---------------------------------------------------------------------------------------------
********************************************************************************************/
var moment = require('moment');
var STD_DATE_FORMAT = "YYYY-MM-DD";
var message =  { "SUCCESS"                :{"code":200,"message":null}
			 	   , "INVALID_QUERY_PERMS"    :{"code":600,"message":"Invalid Parameters send"}
			 	   , "MODEL_ERR"              :{"code":700,"message":"Internal Database Error"}
			       , "IVALID_DATA" 			  :{"code":601,"message":"Internal Data Provided"}
			       , "NO_REQ_FIELDS" 		  :{"code":600,"message":"Not all required fields are send"}
			       , "IVALID_DATE" 			  :{"code":601,"message":"Invalid date format. All dates should be in "+STD_DATE_FORMAT+" format"}
			       , "UN_REQ_FIELDS" 		  :{"code":601,"message":"Invalid parameters send"}
				   , "DB_CONNECTION_ISSUE"    :{"code":700,"message":"Database Query/Connection Error"}

            };

exports.message =  message;
