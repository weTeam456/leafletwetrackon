// Standard Inclusions
var log    = require(appRoot+'/utils/logmessages');
var std    = require(appRoot+'/utils/standardMessages');

/**************************************************************************************
* Controller     : getModuleMetaData
* Parameters     : None
* Description    : Get Module Related Metadata and generates the  File Code
* Change History :
* 11/29/2016    - Sunil Mulagada - Initial Function
* 12/30/2016    - Sunil Mulagada - Added A condition if callback is not send. Then it returns a Promise
***************************************************************************************/
exports.execQuery = function (ConPool,Qry,cntxtDtls,callback){
  
      if(callback && typeof callback == "function") {

                  ConPool.getConnection(function(err, connection) {    // get connection from Connection Pool 
                        if (err) { log.db.conError(cntxtDtls,Qry,err.code,err.fatal); callback(err, null); return err;  }     

                        // Execute the query
                        connection.query( Qry, function(err, rows) {
                              connection.release();                  // Release connection back to Pool  
                              if(err) { log.db.qryError(cntxtDtls,Qry,err.code,err.fatal); callback(true, null); return; } // Handle Query Errors          
                              callback(false, rows);                 // Send the results back  
                              return ;     
                        });
                  });

      }else{ 
            return new Promise (function(resolve,reject){
                 ConPool.getConnection(function(err, connection) {    // get connection from Connection Pool 
                        if (err) { log.db.conError(cntxtDtls,Qry,err.code,err.fatal); 
                              reject({"err_status":std.message.DB_CONNECTION_ISSUE.code,"err_message":std.message.DB_CONNECTION_ISSUE.message});
                         } else  {   // Execute the query
                                    connection.query( Qry, function(err, rows) {  
                                          connection.release();                  // Release connection back to Pool  
                                          if(err) { log.db.qryError(cntxtDtls,Qry,err.code,err.fatal); 
                                              reject({"err_status":std.message.DB_QUERY_ISSUE.code,"err_message":std.message.DB_QUERY_ISSUE.message});    
                                            } // Handle Query Errors 
                                          else   {
                                                resolve(rows);                 // Send the results back  
                                          }  
                                    }); // End of Qry Execuiton
                         }

                  }); // End of get Connection

            }); // End of Promise
      } // End of Else

};