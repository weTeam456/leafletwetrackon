/********************************************************************************************
---------------------------------------------------------------------------------------------
File              : logmessages.js
Description       : All the utility functions related to log files
---------------------------------------------------------------------------------------------
********************************************************************************************/
var moment      = require('moment');
var DEBUG_MODE  = true;
var DEBUG_LEVEL = (typeof(appSettings.app.debug_level) != 'undefined') ? appSettings.app.debug_level : "INFO";
var chalk = require('chalk');

let header     = chalk.bold.blue;
let warn       = chalk.bold.green;
let info       = chalk.bold.yellow
let err        = chalk.bold.red


/**************************************************************************************
* Util Function  : Print the message to the standard output
* Parameters     : None
* Description    : Get the list of all the Zones
* Change History :
* 03/19/2016    - Sunil Mulagada - Initial Function
*
***************************************************************************************/
exports.message = function(category,moduleDtl,id,message) {  
    var showLog = false;  
	if (DEBUG_MODE == true) {
		if(DEBUG_LEVEL=="INFO") { showLog = true; }
		else if(DEBUG_LEVEL=="WARN" && (category == "info" || category == "err")) { showLog = true; }
		else if(DEBUG_LEVEL=="ERR" || category == "ERROR" ) { showLog = true; }
	}
	if(showLog == true){
		let pirntMsg =info;
		 if(category=="WARN") pirntMsg=warn;
		 else if(category=="ERR" || category == "ERROR") pirntMsg=err;
		 else pirntMsg=info;
		 var contect="";
		 if( typeof(moduleDtl) != 'undefined'){
			 context=moduleDtl.mod_name+"|"+moduleDtl.scriptName+"|"+moduleDtl.fcd
		 }
		console.log("["+pirntMsg(category)+":"+context+":"+id+":"+moment().format('MM-DD-YYYY h:mm:ss')+"] "+message);
	}
}



/**************************************************************************************
* Util Function  : Print the message to the standard output
* Parameters     : None
* Description    : Get the list of all the Zones
* Change History :
* 03/19/2016    - Sunil Mulagada - Initial Function
*
***************************************************************************************/
exports.db ={

	conError : function(category,module,id,message) {  
		    var showLog = false;  
			var context={};
			if (DEBUG_MODE == true) {
				if(DEBUG_LEVEL=="INFO") { showLog = true; }
				else if(DEBUG_LEVEL=="WARN" && (category == "info" || category == "err")) { showLog = true; }
				else if(DEBUG_LEVEL=="ERR" && category == "ERROR" ) { showLog = true; }
			}
			if(showLog == true){
			    let pirntMsg =info;
				if(category=="WARN") pirntMsg=warn;
				else if(category=="ERR" || category == "ERROR") pirntMsg=err;
				else pirntMsg=info;
				var contect="";
				if( typeof(moduleDtl) != 'undefined'){
					context=moduleDtl.mod_name+"|"+moduleDtl.scriptName+"|"+moduleDtl.fcd
				}
				console.log("["+pirntMsg(category)+":"+context+":"+id+":"+moment().format('MM-DD-YYYY h:mm:ss')+"] "+message);
			}
	},
	qryError : function(category,module,id,message) {  
		    var showLog = false;  
			if (DEBUG_MODE == true) {
				if(DEBUG_LEVEL=="INFO") { showLog = true; }
				else if(DEBUG_LEVEL=="WARN" && (category == "info" || category == "err")) { showLog = true; }
				else if(DEBUG_LEVEL=="ERR" && category == "ERROR" ) { showLog = true; }
			}
			if(showLog == true){
			    let pirntMsg =info;
				if(category=="WARN") pirntMsg=warn;
				else if(category=="ERR" || category == "ERROR") pirntMsg=err;
				else pirntMsg=info;
				var contect="";
				if( typeof(moduleDtl) != 'undefined'){
					context=moduleDtl.mod_name+"|"+moduleDtl.scriptName+"|"+moduleDtl.fcd
				}				
				console.log("["+pirntMsg(category)+":"+context+":"+id+":"+moment().format('MM-DD-YYYY h:mm:ss')+"] "+message);
			}
	}

}



