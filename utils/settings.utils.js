var nconf 	     =  require('nconf')
	  fs           =  require('fs'),
    path         =  require('path');



  // add multiple files, hierarchically. notice the unique key for each file
  var loadConfigFiles = function(){
    nconf.file('app', 'config/app.config.json');
    nconf.file('ds-services', 'config/ds-services.config.json');
    nconf.file('ds-utils', 'config/ds-utils.config.json');
    nconf.file('passport', 'config/passport.config.js');
  }


  
/*****************************************************************************
* Function 		  : getSettings
* Description   : get all the settings
* Arguments		  : callback function
******************************************************************************/
exports.getSettings = function(callback) {
  loadConfigFiles();
  var t_settings= { "app": nconf.get('app'),
					          "dsServices":nconf.get('ds-services'),
					          "dsUtils":nconf.get('ds_utils'),
				            "def":nconf.get('def')}	 

   return t_settings;
};


