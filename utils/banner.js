var chalk = require('chalk');
exports.startPrint = function(url) {
// style a string 

 
// combine styled and normal strings 
chalk.blue('Hello') + 'World' + chalk.red('!');
// §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
console.log( chalk.blue("\n           DDDDDDD    ")+ chalk.red(" SSSSSSSS ")+"\
             \n "+chalk.blue("          DDDDDDDD   ")+ chalk.red(" SS")+"\
             \n "+chalk.blue("          DD    DD   ")+ chalk.red(" SS")+"\
             \n "+chalk.blue("          DD    DD   ")+ chalk.red(" SSSSSSSS")+"\
             \n "+chalk.blue("          DD    DD         ")+ chalk.red(" SS")+"\
             \n "+chalk.blue("          DDDDDDDD         ")+ chalk.red(" SS")+"\
             \n "+chalk.blue("          DDDDDDD    ")+ chalk.red(" SSSSSSSS") );

console.log("\n\n © DreamStep Software Innovations Pvt Ltd.\
             \n "+chalk.green("  Server          ")+": "+url +"\
             \n"+chalk.green("   Start Time      ")+": "+moment().format('MM-DD-YYYY h:mm:ss')+"\
             \n"+chalk.green("   App Name        ")+": "+appSettings.app.app_name+"\
             \n"+chalk.green("   App Code        ")+": "+appSettings.app.app_cd+"\
             \n"+chalk.green("   App Id          ")+": "+appSettings.app.app_id+"\
             \n"+chalk.green("   Url Audit       ")+": "+(appSettings.app.audit_requests?'Enabled':'Disbled')+"\
             \n"+chalk.green("   App Base        ")+": "+appRoot+"\
             \n"+chalk.green("   Audit Directory ")+": "+appRoot+"/"+appSettings.app.audit_dir+"\
             \n"+chalk.green("   Log Directory   ")+": "+appRoot+"/"+appSettings.app.log_dir+"\
             \n"+chalk.green("   App Debug Mode  ")+": "+appSettings.app.debug_level);

};

exports.EndPrint = function(url) {

console.log("\n           DDDDDDD     SSSSSSSS\
             \n           DDDDDDDD    SS\
             \n           DD    DD    SS\
             \n           DD    DD    SSSSSSSS\
             \n           DD    DD          SS\
             \n           DDDDDDDD          SS\
             \n           DDDDDDD     SSSSSSSS\
             \n\n DreamStep Software Innovations Pvt Ltd.\
             \n   Server      : "+url +"\
             \n   Termination Time  : "+moment().format('MM-DD-YYYY h:mm:ss'));

};
