// var cognito = require('amazon-cognito-identity-js');
var AWS = require('aws-sdk');
var cognitoConfig = require('../config/aws-cognito.config.json');

var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var getCognitoCred = function(idToken){
    AWS.config.region = cognitoConfig.config.region;
    var cognitoCred = new AWS.CognitoIdentityServiceProvider({
        apiVersion : '2016-04-18',
        credentials:  new AWS.CognitoIdentityCredentials({
            IdentityPoolId: cognitoConfig.config.IdentityPoolId,
            Logins        : {                // Change the key below according to the specific region your user pool is in.
                'cognito-idp.eu-west-2.amazonaws.com/eu-west-2_E7dgnrbp7' : idToken
            }            //TODO: try Logins[key + userpool] = cognitoIdToken
        })
    })
    return cognitoCred;
}

 /****************************************************************************************************************
  * Gets loggedIn user details from conginto user pool
  * Author: Sony Angel
  ****************************************************************************************************************/
    exports.getloggedInUserDtls = function(req, callback) {
        var fnm="getloggedInUserDtls";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);

        getCognitoCred(req.body.idToken.jwtToken).getUser({AccessToken: req.body.accessToken.jwtToken}, function(err, data) {
            if(err) log.message("ERROR",cntxtDtls,100,err);
            callback(data);           // successful/failure response
        });
    }
    
 /****************************************************************************************************************
  * Creates a new user in conginto user pool
  * Author: Rajesh Muramalla
  ****************************************************************************************************************/
    exports.addUser = function(req, callback) {
        var fnm="addUser";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);
        
        var createParams = {
            UserPoolId          : cognitoConfig.config.userPoolId, /* required */
            Username            : req.body.usr_nm, /* required */
            TemporaryPassword   : req.body.newPassword,
            UserAttributes      : [
                                        {
                                            Name: 'email', /* required */
                                            Value: req.body.eml_tx
                                        },{
                                            Name : 'phone_number',
                                            Value : '+'+req.body.mobile_nu
                                        }/* more items */
                                    ]
        };

        getCognitoCred(req.body.idToken).adminCreateUser(createParams, function(err, data) {
            if (err) log.message("ERROR",cntxtDtls,100,err); // an error occurred
            callback(err.code,data);
        });       
       //    assigning group to the user
    }

 /****************************************************************************************************************
  * Updates a existent user in conginto user pool
  * Author: Rajesh Muramalla
  ****************************************************************************************************************/
    exports.updatesUser = function(req, callback) {
        
        var fnm="updatesUser";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);

        var updateParams = {
            UserAttributes: [ 
                { 
                    Name    : 'name',
                    Value   : req.body.usr_nm
                }, {
                    Name    : 'email',
                    Value   : req.body.eml_tx
                }, {
                    Name    : 'phone_number',
                    Value   : '+'+req.body.mobile_nu
                }
            ],
            Username: req.body.usr_nm,
            UserPoolId: cognitoConfig.config.userPoolId
        };
        getCognitoCred(req.body.idToken).adminUpdateUserAttributes(updateParams, function(err, data){
            if(err) log.message("ERROR",cntxtDtls,100,err);
            callback(err,data);
        });
    }

/****************************************************************************************************************
  * Deletes a existent user in conginto user pool
  * Author: Sony Angel
  ****************************************************************************************************************/
    exports.deleteUser = function(req, callback) {
        
        var fnm="deleteUser";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);

        var deleteParams = {
            Username: req.body.usr_nm,
            UserPoolId: cognitoConfig.config.userPoolId
        };
        
        getCognitoCred(req.body.idToken).adminDeleteUser(deleteParams, function(err, data) {
            if (err) log.message("ERROR",cntxtDtls,100,err);
                // console.log(err, err.stack); // an error occurred
            callback(err,data);
        });
    }

/****************************************************************************************************************
  * Changes user password in conginto user pool
  * Author: Sony Angel
  ****************************************************************************************************************/
    exports.changePassword = function(req, callback) {
        
        var fnm="changePassword";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);
        var params = {
            PreviousPassword: req.body.crntPwd, /* required */
            ProposedPassword: req.body.newPwd, /* required */
            AccessToken: req.body.accessToken
        };
        getCognitoCred(req.body.idToken).changePassword(params, function(err, data) {
            if (err) log.message("ERROR",cntxtDtls,100,err);
                // console.log(err, err.stack); // an error occurred
            callback(err,data);           // successful response
        });
    }
    
 /****************************************************************************************************************
  * Updates a existent user password in conginto user pool
  * Author: Vijaya Lakshmi
  ****************************************************************************************************************/
    exports.updatesUserPwd = function(req, callback) {
        
        var fnm="updatesUserPwd";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);

        var updatePwdParams = {
            Username: req.body.usrnm,
            UserPoolId: cognitoConfig.config.userPoolId
        };
        getCognitoCred(req.body.idToken).adminGetUser(updatePwdParams, function(err,userData){
           
            if (userData.UserAttributes[0].phone_number_verified === undefined) {
                var params = {
                    Username: req.body.usrnm,
                    UserPoolId: cognitoConfig.config.userPoolId,
                    UserAttributes: [
                        {
                            Name: "phone_number_verified",
                            Value: "true"
                        }
                    ]
                };
                // updating phone number verified user attribute
                getCognitoCred(req.body.idToken).adminUpdateUserAttributes(params, function(err, updatedAttributes){
                    if (err) log.message("ERROR",cntxtDtls,100,err.stack);
                    // updating phone number verified user attribute
                    getCognitoCred(req.body.idToken).adminResetUserPassword(updatePwdParams, function(err, data) {
                        if (err) log.message("ERROR",cntxtDtls,100,err.stack); // an error occurred
                        callback(err,data);           // successful response
                    });
                        
                });

            } else {
                // updating phone number verified user attribute
                getCognitoCred(req.body.idToken).adminResetUserPassword(updatePwdParams, function(err, data) {
                    if (err) log.message("ERROR",cntxtDtls,100,err.stack); // an error occurred
                    callback(err,data);           // successful response
                });
            }

        });

    }

/****************************************************************************************************************
  * Updates a existent user in conginto user pool
  * Author: Vijays Lakshmi
  ****************************************************************************************************************/
    exports.updateUsrPrfl = function(req, callback) {
        
        var fnm="updateUsrPrfl";
        log.message("INFO",cntxtDtls,100,`In ${fnm}`);

        var updateParams = {
            UserAttributes: [ 
                { 
                    Name    : 'name',
                    Value   : req.body.usr_nm
                }, {
                    Name    : 'email',
                    Value   : req.body.eml_tx
                }, {
                    Name    : 'phone_number',
                    Value   : '+'+req.body.mobile_nu
                }
            ],
            Username: req.body.usr_nm,
            UserPoolId: cognitoConfig.config.userPoolId
        };
        getCognitoCred(req.body.idToken).adminUpdateUserAttributes(updateParams, function(err, data){
            if(err) log.message("ERROR",cntxtDtls,100,err);
            callback(err,data);
        });
    }