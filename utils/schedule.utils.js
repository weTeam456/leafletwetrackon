var schedule = require("node-schedule");

var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var moduoleNm   = "UTIL-SCHEDULER";

exports.scheduleScripts = function(callback) {

	schedule.scheduleJob('*/1 * * * *', function(){
		log_schedule(0,"Running Every 1 min");	
	});

	schedule.scheduleJob('*/15 * * * *', function(){
		 log_schedule(0,"Running Every 15 minutes");
	});

	schedule.scheduleJob('*/30 * * * *', function(){
		 log_schedule(0,"Running Every 30 minutes");
	});

	schedule.scheduleJob('0 * * * *', function(){
		 log_schedule(0,"Running Every 1 hour");
	});

};

var log_schedule= function(dtlIn,msg){
    if(dtlIn==0 & appSettings.dsUtils.scheduler.log_scheule_run==true)  
        log.message ("INFO",moduoleNm,0,msg);
    else if(dtlIn==1 & appSettings.dsUtils.scheduler.log_detailed_run==true)
        log.message ("INFO",moduoleNm,0,msg);

}