var nconf 	     =  require('nconf')
	  fs           =  require('fs'),
    path         =  require('path');
var args = process.argv.slice(2);

const booStrapTmplDir = "boot/tmpl";
const ServDir = "server";

if(args.length==0){
  console.log("Usage :: node "+process.argv[1].split('/')[process.argv[1].split('/').length-1]+" \
              \n\t -h     ------> For help \
              \n\t -c  <app|api> <modulename [modulename]* >  ------> For Creating Module Directories \
              \n\t -g  <app|api> <modulename [modulename]* >  ------> For Generating Module Directories and Files \
                     ");
}

args.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
});


const configDir = 'server/';

var g_settings={};
fs.readdir(configDir, (err, files) => {

  files.forEach(file => {
        fs.stat(configDir+file, function(err, stats) {
           // console.log("stats.isDirectory() :: "+stats.isDirectory());
            if(stats.isDirectory()){
              console.log("Server Components ::"+file);

            }

        });

        console.log(file)
  });

});





/*****************************************************************************
* Function 		: getSettings
* Description   : get all the settings
* Arguments		: callback function
******************************************************************************/
var getSettings = function(callback) {


      var g_settings={};
      fs.readdir(configDir, (err, files) => {
        
        if(err){
          callback(err,{});
          return;
        }

        var processed_files =0;

        files.forEach(file => {
          if(file.endsWith("config.json") ){
                var file_content = fs.readFile('config/'+file);
                //console.log(file_content);


                nconf.file(file.replace(new RegExp('.config.json' + '$'),''), 'config/'+file);
                g_settings[file.replace(new RegExp('.config.json' + '$'),'')] = nconf.get(file.replace(new RegExp('.config.json' + '$'),''));
          }
          if(++processed_files==files.length){
             // console.log("\n\n\n\g_settings"+ JSON.stringify(g_settings));
             callback(false,g_settings);
             return ;
          }


        });
      })


};

/*
getSettings(function(err,settings){
  if(err){
    console.log("Error In Loading Configuration Files.\n"+err);
  }
  //console.log("settings"+JSON.stringify(settings));
});
*/

