#!/usr/bin/env node

let path = require('path');
let program = require('commander');
require('shelljs/global');
let batchUtils = require('../../utils/batch.utils')
let BatchDtls = batchUtils.getNewBatchDtls();


let batchName = path.basename(__filename);
let batchDir   = __dirname;
let BatchId    = BatchDtls.batchId;
let BatStartTS = BatchDtls.startTs
let logFileNm=batchUtils.logFileName(batchDir,batchName,BatchId);

batchUtils.printBatchHeader([{"Batch Id":BatchDtls.batchId},{"Start Time":BatStartTS},{"Batch Directory":batchDir},{"Batch File":batchName},{"Log File":logFileNm}]);

if (!which('git')) {
  echo('Sorry, this script requires git');
  exit(1);
}

/*******************************************************************************************
 *   Description :: 
 * 
 *   NOTES::
 *   batchUtils.registerBatchStart(batchName,BatchId); -- > Need to be called At the begining of Batch
 * 
 *   Arguments   ::
 * 
 *   Version History :;
 *   S. No  **   Date       **   Modifier       **  Modification Description
 *   ----------------------------------------------------------------------------------------
 *   01     ** 01/05/2017   ** Sunil Mulagada   **  Initial function 
 * 
 * 
 */



program
  .version('0.0.1')
  .usage('[options] <file ...>')
  .option('-i, --integer <n>', 'An integer argument', parseInt)
  .option('-f, --float <n>', 'A float argument', parseFloat)
  .option('-r, --range <a>..<b>', 'A range', batchUtils.ParamsRange)
  .option('-l, --list <items>', 'A list', batchUtils.ParamsList)
  .option('-o, --optional [value]', 'An optional value')
  .option('-c, --collect [value]', 'A repeatable value', batchUtils.ParamsCollect, [])
  .option('-s, --service [service]', 'OPRS  OR  NON OPRS OR Divaya Darsanam', /^(oprs|nonoprs|divya)$/i)
  .parse(process.argv);


console.log(' int: %j', program.integer);
console.log(' float: %j', program.float);
console.log(' optional: %j', program.optional);
program.range = program.range || [];
console.log(' range: %j..%j', program.range[0], program.range[1]);
console.log(' list: %j', program.list);
console.log(' collect: %j', program.collect);
console.log(' args: %j', program.args);

console.log(' Reservation Type: %j', program.service);

