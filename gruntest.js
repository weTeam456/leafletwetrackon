module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    
  pkg: grunt.file.readJSON('package.json'),
  

    concat: {
            lib: {
                src: ['public/js/lib/jquery-2.1.4.min.js','public/js/lib/jquery-ui.js','public/js/lib/angular.js','public/js/lib/angular-router.js','public/js/lib/materialize.min.js','public/js/lib/angularjs-dropdown-multiselect.js','public/js/lib/angucomplete-alt.min.js','public/js/lib/load-google-maps.js','public/js/lib/lodash.min.js','public/js/lib/angular-google-maps.min.js','public/js/lib/**.js'],
                dest: 'build/temp/lib.js',
            },
            app: {
                src: ['build/temp/min-safe/**.js','build/temp/templates.js'],
                dest: 'build/temp/app.js',
            },
  },



  ngAnnotate: {
    options: {
        singleQuotes: true
    },
    build: {
        files: {
            'build/temp/min-safe/script.js': ['public/js/app/*.js']
        }
    }
  },

  uglify: {
      options: {
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        files: {
           // appclient js files
          'build/lib.min.js': ['build/temp/lib.js'],
          'build/app.min.js': ['build/temp/app.js']     
        }
      }
    },

 purifycss: {
    options: {},
    target: {
      src: ['index.html','public/pages/*.html', 'public/js/app/**.js','public/js/lib/**.js'],
      css: ['public/css/lib/**.css','public/css/app/**.css'],
      dest: 'build/temp/build.css'
    },
  },

    ngtemplates: {
        myapp: {
            options: {
                base: "web",
                module: "rtcwebapp",
            },
            src: "public/pages/*.html",
            dest: "build/temp/templates.js"
        }
    },
  
  cssmin: {
  options: {
    shorthandCompacting: false,
    roundingPrecision: -1
  },
  target: {
    files: {
      'build/build.min.css': ['build/temp/*.css']
    }
  }
},

   clean: {
      build: {
          src: ["build/temp/**"]
      } 
    },

    copy: {
      main: {
        files: [
          {expand: true, src: ['index.html'], dest: 'build/'},
          {expand: true, src: ['rtcpubweb.js'], dest: 'build/'},
          {expand: true, src: ['package.json'], dest: 'build/'},
          {expand: true, src: ['public/**','! public/js/min-safe/**','! public/js/**','! public/css/**'], dest: 'build/'},
          {expand: true, src: ['public/js/*.min.js'], dest: 'build/'},
          {expand: true, src: ['public/css/*.min.css'], dest: 'build/'},
          {expand: true, src: ['config/**'], dest: 'build/'},
          {expand: true, src: ['models/**'], dest: 'build/'},
          {expand: true, src: ['routes/**'], dest: 'build/'},
          {expand: true, src: ['utils/**'], dest: 'build/'},
          {expand: true, src: ['pubapp/**'], dest: 'build/'}
        ],
      },
    },

    htmlmin: {                                     // Task
    dist: {                                      // Target
      options: {                                 // Target options
        removeComments: true,
        collapseWhitespace: true
      },
      files: {                                   // Dictionary of files
        'build/index.min.html': 'index.html'
      }
    }
  },

    sonarRunner: {
        analysis: {
            options: {
                debug: true,
                separator: '\n',
                sonar: {
                    host: {
                        url: 'http://52.76.139.177:9000'
                    },
                    // jdbc: {
                    //    url: 'jdbc:mysql://52.77.187.108:3306/sonar',
                    //    username: 'RTCGEOCODE',
                    //    password: 'RtcGeo_123'
                    // },
                    login: 'dreamstep',
                    password: 'password',
                    projectKey: 'sonar:grunt-sonar-runner:0.1.0',
                    projectName: 'publicportal',
                    projectVersion: '0.10',
                    sources: ['public/js/script.js'].join(','),
                    language: 'js',
                    sourceEncoding: 'UTF-8'
                }
            }
        }
    }
  });
  
  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-ng-annotate'); 
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sonar-runner');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-purifycss');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  // Default task(s).
 // grunt.registerTask('default', ['ngAnnotate', 'uglify','cssmin','clean','copy']);
  grunt.registerTask('build', ['ngAnnotate','concat:lib','purifycss','cssmin','ngtemplates','concat:app','uglify','htmlmin','clean']);

};