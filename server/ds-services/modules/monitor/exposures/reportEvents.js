// Standard Inclusions
var log    = require(appRoot+'/utils/logmessages');
var std    = require(appRoot+'/utils/standardMessages');


var settings            = require(appRoot+'/utils/settings.utils');



appSettings         = settings.getSettings();


exports.reportNodeStart = function() {
    if(appSettings.dsServices.monitor.MONITOR_APP_START==true) {
            console.log("Report Node Started");
    }
    
}