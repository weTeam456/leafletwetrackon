var express = require('express');
var router  = express.Router();
// Standard Inclusions
var log    = require(appRoot+'/utils/logmessages');
var std    = require(appRoot+'/utils/standardMessages');

/****************************************************************
			  Application related Routess
*****************************************************************/

function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    // if (req.isAuthenticated())
        return next();
    // res.sendFile(path.join(appRoot + '/client/src/login.html'));
}

router.get('/',function(req, res) {
     res.sendFile(path.join(appRoot + '/client/src/login.html'));
});

router.get('/ds',isLoggedIn,function(req, res) {
     res.sendFile(path.join(appRoot + '/client/src/index.html'));
});

router.get('/resetPwd',function(req, res) {
     res.sendFile(path.join(appRoot + '/client/src/resetpwd.html'));
});

module.exports = router