// Standard Inclusions
var log = require(appRoot + '/utils/logmessages');
var std = require(appRoot + '/utils/standardMessages');
var df = require(appRoot + '/utils/dflower.utils');
var cntxtDtls = df.getModuleMetaData(__dirname, __filename);


// Model Inclusions
var geoCodeMdl = require('../models/geoCodeMdl');

/**************************************************************************************
 * Controller     : category_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.category_get = function(req, res) {
    var fnm = "category_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    geoCodeMdl.getCategory({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'category_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'category_get', {});
        });
}


/**************************************************************************************
 * Controller     : fencesGeo
 * Parameters     : None
 * Description    : 
 * Change History :
 * 11/07/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.fencesGeo = function(req, res) {
    var fnm = "fencesGeo";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    geoCodeMdl.fencesGeo({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'fencesGeo', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'fencesGeo', {});
        });
}


/**************************************************************************************
 * Controller     : fencesGeo
 * Parameters     : None
 * Description    : 
 * Change History :
 * 25/07/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.muncipalDumpAreas_get = function(req, res) {
    var fnm = "muncipalDumpAreas";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    geoCodeMdl.muncipalDumpAreas({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'muncipalDumpAreas', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'muncipalDumpAreas', {});
        });
}





/**************************************************************************************
 * Controller     : categories_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/


exports.categories_get = function(req, res) {
    var cateId = req.params.fnce_ctgry_id;
    var fnm = "categories_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.getCategories(cateId)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'categories_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'categories_get', {});
        });
}


/**************************************************************************************
 * Controller     : fenceLandMark_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 28/06/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/


exports.fenceLandMark_get = function(req, res) {
    var fnce_grp_id = req.params.fnce_grp_id;
    var fnm = "fenceLandMark_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.getFencesByGrpId(fnce_grp_id)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'fenceLandMark_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'fenceLandMark_get', {});
        });
}



/**************************************************************************************
 * Controller     : landMarks_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.landMarks_post = function(req, res) {
    var data = req.body;
    var fnm = "landMarks_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.saveOrUpdateLandMarks(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'landMarks_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'landMarks_post', {});
        });
}
/**************************************************************************************
 * Controller     : postGeoFences_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.postGeoFences_post = function(req, res) {
    var data = req.body;
    var fnm = "postGeoFences_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.postGeoFences(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'postGeoFences_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'postGeoFences_post', {});
        });
}


/**************************************************************************************
 * Controller     : landMarksGrps_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.landMarksGrps_get = function(req, res) {

    var fnm = "landMarksGrps_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.landMarksGrps_get({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'landMarksGrps_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'landMarksGrps_get', {});
        });
}

/**************************************************************************************
 * Controller     : landMarksGrps_ge
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.LandMarkGrps_post = function(req, res) {
    var data = req.body;
    var fnm = "LandMarkGrps_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.LandMarkGrps_pst(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'LandMarkGrps_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'LandMarkGrps_post', {});
        });
}


/**************************************************************************************
 * Controller     : LandMarkGrps_update
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.LandMarkGrps_update = function(req, res) {
    var data = req.body;
    var fnm = "LandMarkGrps_update";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.LandMarkGrps_update(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'LandMarkGrps_update', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'LandMarkGrps_update', {});
        });
}


/**************************************************************************************
 * Controller     : landMarksGrps_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.LandMarkCate_post = function(req, res) {
    var data = req.body;
    var fnm = "LandMarkCate_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.LandMarkCat_pst(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'LandMarkCate_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'LandMarkCate_post', {});
        });
}


/**************************************************************************************
 * Controller     : landMarksGrps_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.LandMarkCate_update = function(req, res) {
    var data = req.body;
    var fnm = "LandMarkCate_update";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.LandMarkCate_update(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'LandMarkCate_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'LandMarkCate_post', {});
        });
}


/**************************************************************************************
 * Controller     : landMarksGrps_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.delLandMarkGrpNme_delete = function(req, res) {
    var data = req.body;
    var fnm = "delLandMarkGrpNme_delete";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.delLandMarkGrpNme_delete(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'LandMarkCate_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'LandMarkCate_post', {});
        });
}



/**************************************************************************************
 * Controller     : landMarksGrps_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.delLandMarkCateNme_delete = function(req, res) {
    var data = req.body;
    var fnm = "delLandMarkCateNme_delete";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.delLandMarkCateNme_delete(data)
    .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'LandMarkCate_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'LandMarkCate_post', {});
        });
}




/**************************************************************************************
 * Controller     : landMarksGrps_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/

exports.dashBoards_get = function(req, res) {
    var fnm = "dashBoards_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    geoCodeMdl.dashBoards_get({})
    .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'dashBoards_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'dashBoards_get', {});
        });
}
