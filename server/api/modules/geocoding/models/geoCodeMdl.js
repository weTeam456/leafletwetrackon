// Standard Inclusions
var log = require(appRoot + '/utils/logmessages');
var std = require(appRoot + '/utils/standardMessages');
var df = require(appRoot + '/utils/dflower.utils');
var sqldb = require(appRoot + '/config/db.config');
var cntxtDtls = df.getModuleMetaData(__dirname, __filename);

var dbutil = require(appRoot + '/utils/db.utils');



/*****************************************************************************
 * Function      : get land marks categories
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getCategory = function({}, callback) {

    var QRY_TO_EXEC = `SELECT fnce_ctgry_id,fnce_ctgry_nm,clnt_id,tnt_id 
                       FROM fncs_ctgry_lst_t 
                       ORDER BY fnce_ctgry_id`;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};



/*****************************************************************************
 * Function      : get ward fences //-- 50 Municpal Wards
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/


exports.fencesGeo = function({}, callback) {

    // var QRY_TO_EXEC = `SELECT fnce_id,fncs_dtl_tx,fncs_clr_cd from fncs_lst_t WHERE fnce_grp_id = 4`;
    var QRY_TO_EXEC = "SELECT fnce_id,fnce_nm,fncs_dtl_tx,fncs_clr_cd from fncs_lst_t WHERE fnce_grp_id = 4 and fnce_id IN (1131,1132,1134,1135,1137,1138,1139,1140,1150,1152,1155,1156,1157,1158,1159,1160,1161,1162,1163,1167,1168,1169,1170,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1194,1195,1196,1197,1198,1199,1200,1201,1212,1213,1217,1218,1219,1233,1235,1236)";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : get Dumping Area Fences //-- 3
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.muncipalDumpAreas = function({}, callback) {

    var QRY_TO_EXEC =  `SELECT
                        fnce_id,
                        fnce_nm,
                        fncs_dtl_tx,
                        fncs_clr_cd
                        FROM
                        fncs_lst_t
                        WHERE
                        fnce_grp_id = 2`;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};



/*****************************************************************************
 * Function      : getFencesByGrpId
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.getFencesByGrpId = function(fnce_grp_id, callback) {
    // console.log(fnce_grp_id);
    var QRY_TO_EXEC = "SELECT * FROM fncs_lst_t WHERE a_in AND fnce_grp_id="+ fnce_grp_id;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : get land marks categories by ID
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.getCategories = function(cateId, callback) {

    var QRY_TO_EXEC = "SELECT fc.fnce_ctgry_id,fc.fnce_ctgry_nm,fg.fnce_grp_id,fg.fnce_grp_nm from fncs_ctgry_lst_t fc LEFT JOIN fncs_grp_lst_t fg ON fc.fnce_ctgry_id = fg.fnce_ctgry_id where fg.fnce_ctgry_id="+ cateId;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      :Land MArks
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.saveOrUpdateLandMarks = function(data, callback) {
    var QRY_TO_EXEC;
    if (data.fnce_id == 0) {
        QRY_TO_EXEC = "INSERT INTO fncs_lst_t(fnce_nm,fnce_type_id,fnce_ctgry_id,fnce_grp_id,lat,lng,rds_ct,a_in,clnt_id,tnt_id,i_ts) VALUES ('" + data.fnce_nm + "','" + data.fnce_type_id + "','" + data.fnce_ctgry_id + "','" + data.fnce_grp_id + "','" + data.latitude + "','" + data.longitude + "','" + data.rds_ct + "','" + data.a_in + "','" + data.clnt_id + "','" + data.tnt_id + "',current_timestamp());";
    } else {
        QRY_TO_EXEC = "update fncs_lst_t set fnce_nm='"+data.fnce_nm+"',fnce_type_id="+data.fnce_type_id+",fnce_ctgry_id="+ data.fnce_ctgry_id +", fnce_grp_id="+data.fnce_grp_id+", lat="+data.latitude+", lng="+data.longitude+", rds_ct="+data.rds_ct+", a_in = "+data.a_in+", clnt_id ="+data.clnt_id+", tnt_id ="+data.tnt_id+", u_ts = current_timestamp() where fnce_id="+data.fnce_id;        
    }
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : Geo Fences
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/



exports.postGeoFences = function(data, callback) {

    var QRY_TO_EXEC = "INSERT INTO fncs_lst_t(fnce_nm,fnce_type_id,fnce_ctgry_id,fnce_grp_id,fncs_dtl_tx,fncs_clr_cd,a_in,clnt_id,tnt_id,i_ts) VALUES ('" + data.fnce_nm + "','" + data.fnce_type_id + "','" + data.fnce_ctgry_id + "','" + data.fnce_grp_id + "','" + data.coordStr + "','" + data.fncs_clr_cd + "','" + data.a_in + "','" + data.clnt_id + "','" + data.tnt_id + "',current_timestamp());";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.landMarksGrps_get = function({}, callback) {

    var QRY_TO_EXEC = `SELECT * 
                       FROM fncs_grp_lst_t
                       ORDER BY fnce_grp_id`;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.LandMarkGrps_pst = function(data, callback) {
    // console.log(data);
    var QRY_TO_EXEC = "INSERT INTO fncs_grp_lst_t(fnce_grp_nm,fnce_ctgry_id,fnce_type_id,clnt_id,tnt_id,a_in,i_ts) VALUES ('" + data.fnce_grp_nm + "','" + data.fnce_ctgry_id + "','" + data.fnce_type_id + "','" + data.clnt_id + "','" + data.tnt_id + "','" + data.a_in + "',current_timestamp());";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.LandMarkGrps_update = function(data, callback) {
    // console.log(data);
    var QRY_TO_EXEC = "UPDATE fncs_grp_lst_t set fnce_grp_nm='" + data.fnce_grp_nm + "',clnt_id='" + data.clnt_id + "',tnt_id='" + data.tnt_id + "',u_ts=CURRENT_TIMESTAMP() where fnce_grp_id='" + data.fnce_grp_id + "'";

    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.LandMarkCat_pst = function(data, callback) {
    // console.log(data);
    var QRY_TO_EXEC = "INSERT INTO fncs_ctgry_lst_t(fnce_ctgry_nm,fnce_ctgry_id,clnt_id,tnt_id,a_in,i_ts) VALUES ('" + data.fnce_ctgry_nm + "','" + data.fnce_ctgry_id + "','" + data.clnt_id + "','" + data.tnt_id + "','" + data.a_in + "',current_timestamp());";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.LandMarkCate_update = function(data, callback) {
    // console.log(data);
 
   var QRY_TO_EXEC = "UPDATE fncs_ctgry_lst_t set fnce_ctgry_nm='" + data.fnce_ctgry_nm + "',clnt_id='" + data.clnt_id + "',tnt_id='" + data.tnt_id + "',u_ts=CURRENT_TIMESTAMP() where fnce_ctgry_id='" + data.fnce_ctgry_id + "'";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.delLandMarkGrpNme_delete = function(data, callback) {
    // console.log("SAFASFA"+data);
  
   var QRY_TO_EXEC = "DELETE FROM fncs_grp_lst_t WHERE fnce_grp_id= '"+data.fnce_grp_id+"' ";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};



/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.delLandMarkCateNme_delete = function(data, callback) {
    // console.log("SAFASFA"+data);
  
   var QRY_TO_EXEC = "DELETE FROM fncs_ctgry_lst_t WHERE fnce_ctgry_id= '"+data.fnce_ctgry_id.fnce_ctgry_id+"' ";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : 
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/

exports.dashBoards_get = function(data, callback) {

   var QRY_TO_EXEC =  `SELECT 
                       SUM(CASE WHEN fnce_ctgry_id=1 then 1 ELSE 0 END) as tl_ct,
                       SUM(CASE WHEN fnce_grp_id=3 then 1 ELSE 0 END) as bins,
                       SUM(CASE WHEN fnce_grp_id=1 then 1 ELSE 0 END) as points,
                       SUM(CASE WHEN fnce_grp_id=5 then 1 ELSE 0 END) as zones, 
                       SUM(CASE WHEN fnce_grp_id=2 then 1 ELSE 0 END) as dumpArea, 
                       SUM(CASE WHEN fnce_grp_id=4 then 1 ELSE 0 END) as wards,   
                       SUM(CASE WHEN fnce_ctgry_id=1 and MONTH(CURDATE())=MONTH(i_ts) then 1 ELSE 0 END) as cur_mnth_l_ct,  
                       SUM(CASE WHEN fnce_ctgry_id=1 and MONTH(CURDATE()-INTERVAL 1 MONTH)=MONTH(i_ts) then 1 ELSE 0 END) as lst_mnth_l_ct,
                       SUM(CASE WHEN fnce_ctgry_id=2 then 1 ELSE 0 END) as tf_ct, 
                       SUM(CASE WHEN fnce_ctgry_id=2 and MONTH(CURDATE())=MONTH(i_ts) then 1 ELSE 0 END) as cur_mnth_f_ct,
                       SUM(CASE WHEN fnce_ctgry_id=2 and MONTH(CURDATE()-INTERVAL 1 MONTH)=MONTH(i_ts) then 1 ELSE 0 END) as lst_mnth_f_ct,
                       (SELECT COUNT(*) FROM fncs_ctgry_lst_t) as fence_categories, 
                       (SELECT COUNT(*) FROM asrt_ctgry_lst_t) as assert_categories,
                       (SELECT COUNT(*) FROM fncs_grp_lst_t) as fence_groups
                       FROM fncs_lst_t WHERE a_in = 1`;
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


