// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);


// Model Inclusions
var playBackMdl = require('../models/playBackMdl');



/**************************************************************************************
* Controller     : playBackByVeh_get
* Parameters     : None
* Description    : 
* Change History :
* 04/05/2017    - #...
*
***************************************************************************************/

exports.playBackByVeh_get = function(req, res) {

    var fnm = "playBackByVeh_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    
    var data = req.body;
     // console.log(data);
    var checkbit = req.body.checkbit;
    // console.log(checkbit);
    if (data.checkbit == "byVeh") {
        playBackMdl.getImeiByVehid(data.asrt_id, function(err, rows) {
           if (err) {
                console.log("err " + err);
                res.send({
                    "status": std.message.MODEL_ERR.code,
                    "message": std.message.MODEL_ERR.message,
                    "data": []
                });
                }else {
                 var vdata = {
                    "device": rows[0].dev_imei_nu,
                    "sdate": data.sdate,
                    "edate": data.edate,
                    "trp_rn_id":data.trp_rn_id
                };
                // console.log(vdata);
                    playback('null',vdata, function(err, result) {
                    res.send(result);
                });
            } 
        });
    }
  }

  
var playback = function(asrt_nm, data, callback) {
    // console.log(data);
    // AllFncsForTripDtl_get();
    playBackMdl.getRunnPlayBackByVeh(data, function(err, results) {
        if (err) {
            console.log("err " + err);
            callback(true, {
                "status": std.message.MODEL_ERR.code,
                "message": std.message.MODEL_ERR.message,
                "data": []
            });
        }

        if (results.length > 0) {
            var asrt_nu = '';
            var lat = [];
            var lng = [];
            var event = [];
            var len = results.length - 1;
            var platlng = [];
            var ign_flag = 0;
            var ign_from = '';
            var ign_to = '';
            var pcount = 0;
            for (var i = 0; i <= len; i++) {
                lat[i] = results[i].lat;
                lng[i] = results[i].lng;

                if (i == 0 || i == len) {
                    var text = "Date: " + moment(results[i].add_date).format('DD-MM-YYYY HH:mm') + "<br>" + "Speed: " + results[i].speed_ct + "<br>" + " Address: " + results[i].lat + "<br>" + " Address: " + results[i].lng + "<br>";
                    platlng[pcount] = [results[i].lat, results[i].lng, text, '', '', ''];
                    pcount++;
                }

                if (results[i].ignition == 0) {
                    if (ign_flag == 0) {
                        ign_flag = 1;
                        ign_from = results[i].add_date;
                    }
                    continue;
                } else {
                    ign_to = results[i].add_date;
                    if (ign_from != '') {
                        var text = "Date: " + moment(results[i].add_date).format('DD-MM-YYYY HH:mm:ss') + "<br>" + "Address: " + results[i].lat + "<br>" + " Address: " + results[i].lng + "<br>";
                        var endtime = moment(ign_to).format('YYYY-MM-DD HH:mm:ss');
                        var starttime = moment(ign_from).format('YYYY-MM-DD HH:mm:ss');
                        //var timdiff = endtime-starttime;
                        //var d = moment(moment.duration(moment(starttime).diff(endtime))).format("hh:mm:ss");
                        var d = moment(endtime).diff(starttime, 'minutes');
                        var c_stop = moment.utc(d).format("HH:mm:ss");
                        //console.log(d);   
                        if (asrt_nm == "null") {
                            if (d > stop) {
                                var stoppage = d;
                                platlng[pcount] = [results[i].lat, results[i].lng, text, moment(ign_from).format('DD-MM-YYYY HH:mm:ss'), moment(ign_to).format('DD-MM-YYYY HH:mm:ss'), stoppage];
                                pcount++;
                            }
                        } else {
                            if (d > stop && d < 30) {
                                var stoppage = d;
                                platlng[pcount] = [results[i].lat, results[i].lng, text, moment(ign_from).format('DD-MM-YYYY HH:mm:ss'), moment(ign_to).format('DD-MM-YYYY HH:mm:ss'), stoppage];
                                pcount++;
                            }
                        }

                        ign_from = '';
                    }
                    ign_flag = 0;
                }
                var text = "Date: " + results[i].add_date + "<br>";
            }
            if (asrt_nm == "null") {
                if (!data.trp_rn_id || data.trp_rn_id == null) {
                    callback(false, {
                        "status": std.message.SUCCESS.code,
                        "message": "vehicle",
                        "data": {
                            "asrt_nu": results[0].asrt_nm,
                            "lat": lat,
                            "lng": lng,
                            "event": event,
                            'park': platlng,
                            "fncDtls": [],
                            "mapPoints": results
                        }
                    });
                } else if (data.trp_rn_id && data.trp_rn_id != null) {
                    playBackMdl.AllFncsForTripDtl_get(data.trp_rn_id, function(err, fncDtls) {
                        // console.log(fncDtls);
                        callback(false, {
                            "status": std.message.MODEL_ERR.code,
                            "message": "FenceInOutDtls",
                            "data": {
                                "trp_rn_id": data.trp_rn_id,
                                "lat": lat,
                                "lng": lng,
                                "event": event,
                                'park': platlng,
                                "fncDtls": fncDtls,
                                "mapPoints": results
                            }
                        });
                    })
                }

            }
        }
    });
}
