// Standard Inclusions
var log = require(appRoot + '/utils/logmessages');
var std = require(appRoot + '/utils/standardMessages');
var df = require(appRoot + '/utils/dflower.utils');
var cntxtDtls = df.getModuleMetaData(__dirname, __filename);


// Model Inclusions
var liveTrackMdl = require('../models/liveTrackingMdl');


/**************************************************************************************
 * Controller     : currRunnVehicles_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.currRunnVehicles_get = function(req, res) {
    var fnm = "currRunnVehicles_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res, result, cntxtDtls, fnm, util.inspect(result.array()), {});
            return;
        } else {
            liveTrackMdl.getCurrRunnVehicles({})
                .then(function(results) {
                    // log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'currRunnVehicles_get', {});
                }, function(error) {
                    // log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'currRunnVehicles_get', {});
                });
        }
    })
}

/**************************************************************************************
 * Controller     : tripsByAsrtId_get for play back (TRIPS DROP DWON)
 * Parameters     : None
 * Description    : 
 * Change History :
 * 27/06/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.tripsByAsrtId_get = function(req, res) {
    var fnm = "tripsByAsrtId_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var asrt_id = req.params.asrt_id;
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res, result, cntxtDtls, fnm, util.inspect(result.array()), {});
            return;
        } else {
            liveTrackMdl.getTripsByAssrts(asrt_id)
                .then(function(results) {
                    // log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'tripsByAsrtId_get', {});
                }, function(error) {
                    // log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'tripsByAsrtId_get', {});
                });
        }
    })
}

/**************************************************************************************
 * Controller     : tripsByAsrtId_get for play back (TRIPS DROP DWON)
 * Parameters     : None
 * Description    : 
 * Change History :
 * 28/06/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.garbagePointsByTrip_get = function(req, res) {
    var fnm = "garbagePointsByTrip_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var trip_run_id = req.params.trip_run_id;
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res, result, cntxtDtls, fnm, util.inspect(result.array()), {});
            return;
        } else {
            liveTrackMdl.getPointsByTrips(trip_run_id)
                .then(function(results) {
                    // log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'garbagePointsByTrip_get', {});
                }, function(error) {
                    // log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'garbagePointsByTrip_get', {});
                });
        }
    })
}

/**************************************************************************************
 * Controller     : getDumpers_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.getDumpers_get = function(req, res) {
    var fnm = "getDumpers_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var trip_run_id = req.params.trip_run_id;

    liveTrackMdl.getDumpers({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'getDumpers_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'getDumpers_get', {});
        });
}


/**************************************************************************************
 * Controller     : getDumpers_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.dumpAreas_get = function(req, res) {
    var fnm = "dumpAreas_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    liveTrackMdl.dumpArea_get({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'dumpAreas_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'dumpAreas_get', {});
        });
}




/**************************************************************************************
 * Controller     : asserType_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.asserType_get = function(req, res) {
    var fnm = "asserType_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    liveTrackMdl.asserType_get({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'asserType_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'asserType_get', {});
        });
}



/**************************************************************************************
 * Controller     : getAsrtLst
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.getAsrtLst = function(req, res) {
    var fnm ="getAsrtLst";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    liveTrackMdl.assertList_get({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'getAsrtLst', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'getAsrtLst', {});
        });
}


/**************************************************************************************
 * Controller     : currRunnVehicles_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 18/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.vehicles_get = function(req, res) {
    var fnm = "vehicles_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res, result, cntxtDtls, fnm, util.inspect(result.array()), {});
            return;
        } else {
            liveTrackMdl.getVehicles({})
                .then(function(results) {
                    // log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'vehicles_get', {});
                }, function(error) {
                    // log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'vehicles_get', {});
                });
        }
    })
}


/**************************************************************************************
 * Controller     : asrtLstId_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 17/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.asrtLstId_get = function(req, res) {
    var fnm = "asrtList_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var asrtTypeId = req.params.asrt_type_id;
    liveTrackMdl.getAsrtLstById(asrtTypeId)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'getAsrtLst', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'getAsrtLst', {});
        });

}


/**************************************************************************************
 * Controller     : asrtCatList_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 20/06/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.asrtCatList_get = function(req, res) {
    var fnm = "asrtCatList_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var asrt_grp_id = req.params.asrt_grp_id;
    
    liveTrackMdl.asrtCatList_get(asrt_grp_id)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'asrtCatList_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'asrtCatList_get', {});
        });

}


/**************************************************************************************
 * Controller     : asrtList_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 20/06/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.asrtList_get = function(req, res) {
    var fnm = "asrtList_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var asrt_ctgry_id = req.params.asrt_ctgry_id;
    liveTrackMdl.asrtList_get(asrt_ctgry_id)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'asrtList_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'asrtList_get', {});
        });

}

/**************************************************************************************
 * Controller     : asrtGrpList_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 20/06/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.asrtGrpList_get = function(req, res) {
    var fnm = "asrtGrpList_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var asrt_type_id = req.params.asrt_type_id;
    liveTrackMdl.asrtGrpList_get(asrt_type_id)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'asrtGrpList_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'asrtGrpList_get', {});
        });

}




/**************************************************************************************
 * Controller     : clearedCollectionPointsCount_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.clearedCollectionPointsCount_get = function(req, res) {
    var fnm = "clearedCollectionPointsCount_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    var landmrkId = req.params.landmrkId;
    liveTrackMdl.getCollectionPointsCt(landmrkId)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'clearedCollectionPointsCount_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'clearedCollectionPointsCount_get', {});
        });
}


