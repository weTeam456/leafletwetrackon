// Standard Inclusions
var log = require(appRoot + '/utils/logmessages');
var std = require(appRoot + '/utils/standardMessages');
var df = require(appRoot + '/utils/dflower.utils');
var sqldb = require(appRoot + '/config/db.config');
var cntxtDtls = df.getModuleMetaData(__dirname, __filename);

var dbutil = require(appRoot + '/utils/db.utils');


/*****************************************************************************
 * Function      : getAllZonesLst
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getCurrRunnVehicles = function({}, callback) {

     var QRY_TO_EXEC = `SELECT l.lat,l.lng,l.asrt_id,l.asrt_nm,l.dev_imei_nu,l.spd_ct,l.odmtr_ct,acl.asrt_ctgry_nm 
                        FROM ltrck_dtl_t l
                        LEFT OUTER JOIN asrt_lst_t al ON al.asrt_id = l.asrt_id
                        LEFT OUTER JOIN asrt_ctgry_lst_t acl ON acl.asrt_ctgry_id = al.asrt_ctgry_id 
                        WHERE data_received_ts > CURRENT_TIMESTAMP() - INTERVAL 1 hour 
                        ORDER BY asrt_id`;

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : getAllZonesLst
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.dumpArea_get = function({}, callback) {
     var QRY_TO_EXEC = `SELECT * from fncs_lst_t WHERE fnce_grp_id = 2 AND a_in = 1`;

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};





/*****************************************************************************
 * Function      : getTripsByAssrts
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getTripsByAssrts = function(asrt_id, callback) {
    // console.log(asrt_id);
     var QRY_TO_EXEC = `SELECT ROW_NUMBER() OVER (PARTITION BY null ORDER BY concat(t.actl_strt_ts)) as 'trip_nu',
                        t.trp_rn_id,
                        t.strt_fnce_id,
                        t.end_fnce_id,
                        t.asrt_id,
                        t.rte_id,
                        DATE_FORMAT(t.actl_strt_ts,'%Y-%m-%d %H:%i:%s') AS actl_strt,
                        DATE_FORMAT(t.actl_end_ts,'%Y-%m-%d %H:%i:%s') AS actl_end,
                        t.actl_trvl_tm,
                        t.actl_trvl_dstne_ct,
                        t.a_in
                        FROM trp_rn_dtl_t AS t
                        WHERE asrt_id = "`+asrt_id+`" AND DATE(actl_strt_ts) = CURDATE()`;
                    console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};







/*****************************************************************************
 * Function      : getDumpers
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getDumpers = function({}, callback) {

     var QRY_TO_EXEC = `SELECT f.*, (CASE WHEN fio.fnce_id is null then 0 ELSE 1 end) as covr_status
                        FROM fncs_lst_t as f
                        left outer JOIN (SELECT DISTINCT fnce_id FROM fnce_in_out_dtl_t WHERE DATE(fnce_in_ts) = CURDATE() AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 > 3) as fio on fio.fnce_id = f.fnce_id
                        where f.fnce_type_id = 1 AND a_in = 1
                        ORDER BY f.fnce_id`;

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : get Assert List for vehcile status
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.assertList_get = function({}, callback) {

    var QRY_TO_EXEC = "SELECT asrt_ctgry_id,asrt_ctgry_nm,clnt_id,tnt_id FROM `asrt_ctgry_lst_t`;";

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : get Assert List for vehcile status
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.asserType_get = function({}, callback) {

    var QRY_TO_EXEC = "SELECT * FROM asrt_type_lst_t WHERE asrt_type_id IN (1,4);";

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};




/*****************************************************************************
 * Function      : getAllZonesLst
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getVehicles = function({}, callback) {

     var QRY_TO_EXEC = `SELECT l.lat,l.lng,a.icon_path,asl.asrt_sts_nm,clt.crw_nm,l.ast_sts_id,al.crnt_Ast_sts_id,clt.crw_ph,l.asrt_id,l.asrt_nm,l.asrt_ctgry_id,l.strt_fnce_nm AS 'startFence',l.dev_imei_nu,l.spd_ct,l.data_received_ts,
                        CASE WHEN (l.data_received_ts < (CURRENT_TIMESTAMP () - INTERVAL 24 HOUR)) OR l.data_received_ts IS NULL THEN 0 ELSE 1 END AS onl_offl,
                        CASE WHEN l.spd_ct > 1 THEN 1 ELSE 0 END AS vehstat,l.odmtr_ct,ag.asrt_grp_nm,ag.asrt_grp_id,a.asrt_ctgry_nm,atl.asrt_type_id,atl.asrt_type_nm
                        FROM ltrck_dtl_t l
                        LEFT JOIN asrt_ctgry_lst_t a ON a.asrt_ctgry_id = l.asrt_ctgry_id
                        LEFT JOIN asrt_grp_lst_t ag ON ag.asrt_grp_id = l.asrt_grp_id
                        LEFT JOIN asrt_type_lst_t atl ON atl.asrt_type_id = l.asrt_type_id
                                                LEFT JOIN asrt_sts_lst_t asl ON asl.asrt_sts_id = l.ast_sts_id 
                        LEFT JOIN asrt_lst_t al ON al.asrt_id = l.asrt_id
                        LEFT JOIN drvr_asgnt_dtl_t dast ON dast.asrt_id = l.asrt_id AND CURRENT_TIMESTAMP() between dast.frm_ts and  dast.to_ts
                        LEFT JOIN crw_lst_t clt ON clt.crw_id = dast.crw_id
                        GROUP BY l.asrt_id`;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : get Assert List for vehcile status
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getAsrtLstById = function(asrtTypeId, callback) {

    var QRY_TO_EXEC = `SELECT l.lat,l.lng,a.icon_path,l.asrt_id,l.asrt_nm,l.asrt_ctgry_id,l.strt_fnce_nm as 'startFence',l.dev_imei_nu,l.spd_ct,
                       CASE WHEN spd_ct = 0 THEN 0 ELSE 1 END AS onl_offl,CASE WHEN data_received_ts > CURRENT_TIMESTAMP() - INTERVAL 24 hour then 1 ELSE 0 END AS vehstat, odmtr_ct 
                       FROM ltrck_dtl_t l
                       LEFT JOIN asrt_ctgry_lst_t a ON a.asrt_ctgry_id = l.asrt_ctgry_id
                       WHERE data_received_ts > CURRENT_TIMESTAMP() - INTERVAL 24 hour AND l.asrt_ctgry_id=`+asrtTypeId;
// console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : get Assert List for vehcile status
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.asrtGrpList_get = function(asrt_type_id, callback) {
    // console.log(asrt_type_id);
     var QRY_TO_EXEC = `SELECT ag.asrt_grp_nm,ag.asrt_grp_id
                        FROM asrt_lst_t al
                        LEFT JOIN asrt_grp_lst_t ag ON ag.asrt_grp_id = al.asrt_grp_id
                        WHERE al.asrt_type_id=`+asrt_type_id+`
                        GROUP BY ag.asrt_grp_id`;
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : asrtCatList_get
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.asrtCatList_get = function(asrt_grp_id, callback) {

     var QRY_TO_EXEC = `SELECT ac.asrt_ctgry_nm,ac.asrt_ctgry_id
                        FROM asrt_lst_t al
                        LEFT JOIN asrt_ctgry_lst_t ac ON ac.asrt_ctgry_id = al.asrt_ctgry_id
                        WHERE al.asrt_grp_id = `+ asrt_grp_id +`
                        GROUP BY ac.asrt_ctgry_id`
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};


/*****************************************************************************
 * Function      : asrtCatList_get
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.asrtList_get = function(asrt_ctgry_id, callback) {

     var QRY_TO_EXEC = `SELECT asrt_id,asrt_nm,capacity_ct,mileage_ct FROM asrt_lst_t WHERE asrt_ctgry_id =`+asrt_ctgry_id
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};




/*****************************************************************************
 * Function      : getCollectionPointsCt
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getCollectionPointsCt = function(fnce_id, callback) {
    // console.log("fnce_id"+fnce_id);
    var QRY_TO_EXEC = "SELECT DATE_FORMAT(fnce_in_ts,'%d-%m') as fcvrddate,COUNT(fnce_id) as lndmrks_ct FROM fnce_in_out_dtl_t WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 > 3 AND fnce_in_ts BETWEEN CURDATE() - INTERVAL 7 day AND CURDATE() + interval 1 day and fnce_id='"+fnce_id+"' GROUP BY DATE(fnce_in_ts) order by fcvrddate";
    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};