// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');






/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/



exports.getImeiByVehid = function(asrt_id, callback) {
    // console.log(asrt_id);
    var QRY_TO_EXEC = "SELECT dev_imei_nu FROM `ltrck_dtl_t` where asrt_id=" + asrt_id;

    sqldb.MySQLConPool.getConnection(function(err, connection) { // get connection from Connection Pool 
        if (err) {
            console.log("[ERROR:" + Date() + "]" + err.code + "QRY_TO_EXEC :: " + QRY_TO_EXEC);
            console.log(err.fatal);
            return err;
        }
        // Execute the query
        connection.query(QRY_TO_EXEC, function(err, rows) {
            if (err) {
                console.log(err);
                return;
            } // Handle Query Errors
            connection.release(); // Release connection back to Pool            
            callback(false, rows); // Send the results back       
        });
    });
};



exports.getRunnPlayBackByVeh = function(data,callback) {
    
    var QRY_TO_EXEC = "SELECT DATE_FORMAT(dta_rcve_ts,'%d/%m/%Y %h:%i') AS add_date, hdt.asrt_id,alt.asrt_nm, dvce_id as device_id, lat as lat, lng as lng,spd_ct as speed_ct, odmtr_ct as milage_ct,DATE_FORMAT(dta_rcve_ts,'%d/%m/%Y %h:%i') as 'Signall' FROM hstry_dtl_v hdt Join asrt_lst_t alt on alt.asrt_id = hdt.asrt_id WHERE dta_rcve_ts BETWEEN '"+data.sdate+"'AND '"+data.edate+"' AND dvce_id = '"+data.device+"' AND lat IS NOT NULL AND lng IS NOT NULL ORDER BY dta_rcve_ts";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};




exports.AllFncsForTripDtl_get = function(trp_rn_id,callback) {  

         var QRY_TO_EXEC = `SELECT ROW_NUMBER() OVER (PARTITION BY null ORDER BY concat(fdt.fnce_in_ts,' ',fdt.fnce_out_ts)) as 'SNo',fdt.asrt_id,asrt_nm,asrt_ctgry_nm,DATE_FORMAT(fnce_in_ts,'%h:%i') as fnc_in
                            ,DATE_FORMAT(fnce_out_ts,'%h:%i') as fnc_ut,trp_rn_id,flt.fnce_nm,flt.fnce_id,flt.lat,flt.lng,
                            TIME_FORMAT(TIMEDIFF(fnce_out_ts,fnce_in_ts),'%H:%i') as stoppage,
                            CASE WHEN TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts, fdt.fnce_in_ts)) / 60 >= fg.cvrg_tm then 1 ELSE 0 end as fnce_cvr_sts
                            FROM fnce_in_out_dtl_t fdt 
                            JOIN fncs_lst_t flt ON fdt.fnce_id = flt.fnce_id 
                            JOIN fncs_grp_lst_t fg ON flt.fnce_grp_id = fg.fnce_grp_id 
                            JOIN asrt_lst_t alt ON fdt.asrt_id=alt.asrt_id 
                            JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id 
                            WHERE flt.fnce_grp_id IN (1, 3) AND flt.a_in=1
                            AND trp_rn_id = "`+trp_rn_id+`"
                            ORDER BY concat(fdt.fnce_in_ts,' ',fdt.fnce_out_ts)`;
                            // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,fncDtls){
            callback(err,fncDtls);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
