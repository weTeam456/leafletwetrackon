// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');


/*****************************************************************************
* Function      : allOrgLvls_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.allOrgLvls_getMdl = function({data},callback) {
    var QRY_TO_EXEC = "select orgn_lvls_id,orgn_lvls_nm from orgn_lvls_lst_t";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : allClntOrgLvls_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.allClntOrgLvls_getMdl = function(data,callback) {
    var QRY_TO_EXEC = "select orgn_lvls_id,orgn_lvls_nm from orgn_lvls_lst_t where clnt_id ="+data.clientid+";";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : tntOrgLvlsByClnt_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.tntOrgLvlsByClnt_getMdl = function(data,callback) {
    var QRY_TO_EXEC = "select orgn_lvls_id,orgn_lvls_nm from orgn_lvls_lst_t where clnt_id ="+data.clientid+" and tnt_id ="+data.tenantid+";";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : getAllClntsMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.get_allClntsMdl = function({data},callback) {
    var QRY_TO_EXEC = "select * from clnt_dtl_t where a_in =1;";
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllClntsMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.get_allTntsByClnt = function(data,callback) {
    var QRY_TO_EXEC = "select * from tnt_lst_t where clnt_id="+data.clientid+";";
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getUsers
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAllUsers = function({data},callback) {
    var QRY_TO_EXEC = "select *,DATE_FORMAT(i_ts,'%m-%d-%Y') as i_timeStamp FROM usr_lst_t WHERE d_in is NULL";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getUsers
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.usersByClnt_getMdl = function(data,callback) {
    var QRY_TO_EXEC = "select *,DATE_FORMAT(i_ts,'%m-%d-%Y') as i_timeStamp FROM usr_lst_t WHERE d_in is NULL and clnt_id ="+data.clientid+" ;";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getUsers
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.usersByTnt_getMdl = function(data,callback) {
    var QRY_TO_EXEC = "select *,DATE_FORMAT(i_ts,'%m-%d-%Y') as i_timeStamp FROM usr_lst_t WHERE d_in is NULL and clnt_id ="+data.clientid+" and tnt_id = "+data.tenantid+";";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};