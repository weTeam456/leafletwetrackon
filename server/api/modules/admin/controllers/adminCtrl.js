// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

// Model Inclusions
var adminMdl = require('../models/adminMdl');
var adminVdl = require(appRoot+'/server/api/modules/admin/validators/adminVdl');


/**************************************************************************************
* Controller     : allOrgLvls_get
* Parameters     : req,res()
* Description    : To get all Organisational levels
* Change History :
* 25/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.allOrgLvls_get = function(req, res) {

    var fnm="allOrgLvls_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
        // Model gets called Here 
    adminMdl.allOrgLvls_getMdl({})
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}
/**************************************************************************************
* Controller     : allClntOrgLvls_get
* Parameters     : req,res()
* Description    : To get all Organisational levels based on client id
* Change History :
* 25/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.allClntOrgLvls_get = function(req, res) {

    var fnm="allClntOrgLvls_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Model gets called Here 
    adminMdl.allClntOrgLvls_getMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}
/**************************************************************************************
* Controller     : tntOrgLvlsByClnt_get
* Parameters     : req,res()
* Description    : To get all Organisational levels based in client and tenant id
* Change History :
* 25/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.tntOrgLvlsByClnt_get = function(req, res) {

    var fnm="tntOrgLvlsByClnt_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Model gets called Here 
        adminMdl.tntOrgLvlsByClnt_getMdl(req.params)
        .then(function(results){
            df.formatSucessRes(res,results,cntxtDtls,fnm,{});
        },function(error){
            df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
        });
}

/**************************************************************************************
* Controller     : allClients_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 20/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.allClients_get = function(req, res) {
    var fnm="allClients_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    adminMdl.get_allClntsMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : allTntsByClnt_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 20/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.allTntsByClnt_get = function(req, res) {
    var fnm="allTntsByClnt_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    adminMdl.get_allTntsByClnt(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : allUsersList_get
* Parameters     : None
* Description    : 
* Change History :
* 25/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.allUsersList_get = function(req, res) {

    var fnm="allUsersList_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    adminMdl.getAllUsers({})
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : usersClntLst_get
* Parameters     : req, res()
* Description    : To get all users by their client id
* Change History :
* 24/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.usersClntLst_get = function(req, res) {

    var fnm="usersClntLst_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    adminMdl.usersByClnt_getMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : usersTntLst_get
* Parameters     : req, res()
* Description    : To get all users by their client id and tenant id
* Change History :
* 24/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.usersTntLst_get = function(req, res) {

    var fnm="usersTntLst_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Model gets called Here 
    adminMdl.usersByTnt_getMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}