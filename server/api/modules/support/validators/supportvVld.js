var cmdSch =  require('../../../validators/commonVld');

var isAlpha = function(field) {
    var re = /\A[a-z0-9\s]+\Z/i;
    return re.test(field);
}

exports.supportVld = {
   "body": {
       'usr_id': {
           notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Client Id' // Error message for the parameter
          }
        },
        'asgn_usr_id': {
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Client Id' // Error message for the parameter
            }
        },
        'asgn_usr_nm':{
            notEmpty: true,
            errorMessage: 'Need asgn_usr_nm' // Error message for the parameter
            
        },
        'tkt_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },

        
    },
    
};

exports.cmntVld = {
   "body": {
       'usr_id': {
           notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Client Id' // Error message for the parameter
          }
        },
              
        'tkt_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },
          'cmnt_tx':{
            notEmpty: true,
            
            errorMessage: 'Need Comments' // Error message for the parameter
           
        },
        
        
    },
    
};

exports.acptTktStsVld = {
   "body": {
       'usr_id': {
           notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Client Id' // Error message for the parameter
          }
        },
              
        'tkt_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },
         'tckt_sts_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Ticket Status Id' // Error message for the parameter
            }
        },
          'tckt_sts_nm':{
            notEmpty: true,
            
            errorMessage: 'Need Ticket Status Name' // Error message for the parameter
           
        },
        
        
    },
    
};


exports.newTktVld = {
   "body": {
       'crt_usr_id': {
           notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Client Id' // Error message for the parameter
          }
        },
              
        'prty_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },
        'ctgry_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Ticket Status Id' // Error message for the parameter
            }
        },
         'tckt_ctlg_itm_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Ticket Status Id' // Error message for the parameter
            }
        },
          'tkt_ctlg_ctgry_id': {
           notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Client Id' // Error message for the parameter
          }
        },
              
        'assign_usr_grp_id':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },
         'dtTm':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Ticket Status Id' // Error message for the parameter
            }
        },
        'tkt_bdy_tx':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },
         'tkt_sbjct_tx':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Ticket Status Id' // Error message for the parameter
            }
        },
        
        
    },
    
};



