// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);
var tcktVld = require(appRoot+'/server/api/modules/support/validators/tcktVld');
var supportVld = require(appRoot+'/server/api/modules/support/validators/supportvVld');


// Model Inclusions
var supportMdl = require('../models/supportMdl');

/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getTicketDtlsByStsId_get = function(req, res) {

    var fnm="getTicketDtlsByStsId_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
       req.checkParams(tcktVld.tcktVld.req);
        var status_id=req.params.status_id;
          req.checkParams(tcktVld.tcktVld.status_id);

          supportMdl.getTicketDtlsByStsId(status_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}


/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getGrpOpenTicketDtls_get = function(req, res) {
var status_id=req.params.status_id;
    var fnm="getTicketDtlsByStsId_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getGrpOpenTicketDtls(status_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}

/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getOpenTicketDtls_get = function(req, res) {
var usr_id=req.params.usr_id;
    var fnm="getTicketDtlsByStsId_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getOpenTicketDtls(usr_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}


/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getticketStatusList_get = function(req, res) {
    var fnm="getticketStatusList_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getticketStatusList()
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}

/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.updateTicketDtls_post = function(req, res) {
var tkt_id=req.params.tkt_id;
var tkt_status_id=req.params.tkt_status_id;
    var fnm="updateTktStatus_post";
    log.message("INFO",cntxtDtls,100,`******************** In ${tkt_id} `);
    // Request Parameters
          supportMdl.updateTicketDtls(tkt_id,tkt_status_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}



/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getTicketDtlById_get = function(req, res) {
    var fnm="getTicketDtlById_get";
    var tkt_id=req.params.tkt_id;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getTicketDtlById(tkt_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}



/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getTicketCmnts_get = function(req, res) {
    var fnm="getTicketDtlById_get";
    var tkt_id=req.params.tkt_id;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getTicketCmnts(tkt_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}
/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getUsrGrpList_get = function(req, res) {
    var fnm="getTicketDtlById_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getUsrGrpList()
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}

/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getUsrList_get = function(req, res) {
    var fnm="getTicketDtlById_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getUsrList()
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}




/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.assignusr_post = function(req, res) {
req.checkBody(supportVld.supportVld.body);
    var fnm="assignusr_post";
    log.message("INFO",cntxtDtls,100,`******************** In  `);
    // Request Parameters

  req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
            return;
        } else {
            
          supportMdl.assignusr(req.body)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });
        }
    });
}

/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.submtCmnts_post = function(req, res) {
req.checkBody(supportVld.cmntVld.body);
    var fnm="submtCmnts_post";
    log.message("INFO",cntxtDtls,100,`submtCmnts_post ******************** In  `);
    // Request Parameters

  req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
            return;
        } else {
            
          supportMdl.submtCmnts(req.body)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });
        }
    });
}

/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getacceptedtktDtls_get = function(req, res) {
var usr_id=req.params.usr_id;
    var fnm="getTicketDtlsByStsId_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getacceptedtktDtls(usr_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });

}

/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.acptTktStatusChange_post = function(req, res) {
req.checkBody(supportVld.acptTktStsVld.body);
    var fnm="assignusr_post";
    log.message("INFO",cntxtDtls,100,`******************** In  `);
    // Request Parameters

  req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
            return;
        } else {
            
          supportMdl.acptTktStatusChange(req.body)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });
        }
    });
}

/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getTktPriorityDtls_get = function(req, res) {
    var fnm="gettktPriorityDtls_get";
   // var tkt_id=req.params.tkt_id;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getTktPriorityDtls()
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
          },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
   });

}


/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.getTktCtgryLst_get = function(req, res) {
    var fnm="gettktPriorityDtls_get";
 var typlst=req.params.typlst;
 var catlst=req.params.catlst;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.getTktCtgryLst(typlst,catlst)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
          },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
   });

}

/**************************************************************************************
* Controller     : getticketStatusList_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.gettktCtlgGrp_get = function(req, res) {
    var fnm="gettktPriorityDtls_get";
    var tkt_ctlg_typ_id=req.params.tkt_ctlg_typ_id;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters
          supportMdl.gettktCtlgGrp(tkt_ctlg_typ_id)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
          },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
   });

}



/**************************************************************************************
* Controller     : getTicketDtlsByStsId_get
* Parameters     : status_id
* Description    : 
* Change History :
* 05/05/2017    - Malli Ch
*
***************************************************************************************/
exports.submitNewTkt_post = function(req, res) {
req.checkBody(supportVld.newTktVld.body);
    var fnm="assignusr_post";
    log.message("INFO",cntxtDtls,100,`******************** In  `);
    // Request Parameters

  req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
            return;
        } else {
            
          supportMdl.submitNewTkt(req.body)
            .then(function(results){
               df.formatSucessRes(res,results,cntxtDtls,'currRunnVehicles_get',{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,'currRunnVehicles_get',{}); 
            });
        }
    });
}


