// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');




/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTicketDtlsByStsId = function(status_id,callback) {
    
  //  var QRY_TO_EXEC = "select t.tkt_id,t.tkt_sbjct_tx,s.tckt_sts_nm from tkct_dtl_t t join tckt_sts_lst_t s on s.tckt_sts_id=t.tckt_sts_id where t.tckt_sts_id="+status_id;

   var QRY_TO_EXEC =  "select t.tkt_id,t.tkt_sbjct_tx,s.tckt_sts_nm,g.tkt_ctlg_grp_nm,tp.tkt_ctlg_type_nm,c.tkt_ctlg_ctgry_nm from tkct_dtl_t t join tckt_sts_lst_t s on s.tckt_sts_id=t.tckt_sts_id join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id join tkt_ctlg_grp_lst_t g on g.tkt_ctlg_grp_id=i.ctlg_grp_id join tkt_ctlg_type_lst_t tp on tp.tkt_ctlg_type_id=i.tkt_ctlg_type_id join tkt_ctlg_ctgry_lst_t c on c.tkt_ctlg_ctgry_id=i.tkt_ctlg_ctgry_id where t.crt_usr_id=12 AND  t.tckt_sts_id="+status_id;
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};



/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getGrpOpenTicketDtls = function(status_id,callback) {
    
  //  var QRY_TO_EXEC = "select t.tkt_id,t.tkt_bdy_tx,t.tkt_sbjct_tx,ts.tckt_sts_nm from tkct_dtl_t t join usr_grp_rel_t g on g.usr_id=t.crt_usr_id join (select usr_sprt_grp_id  from usr_grp_rel_t where usr_id=12) ug on ug.usr_sprt_grp_id=g.usr_sprt_grp_id join tckt_sts_lst_t ts on ts.tckt_sts_id=t.tckt_sts_id where t.tckt_sts_id="+status_id;
       
       var QRY_TO_EXEC="select t.tkt_id,t.tkt_sbjct_tx,s.tckt_sts_nm,g.tkt_ctlg_grp_nm,tp.tkt_ctlg_type_nm,c.tkt_ctlg_ctgry_nm from tkct_dtl_t t join tckt_sts_lst_t s on s.tckt_sts_id=t.tckt_sts_id join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id join tkt_ctlg_grp_lst_t g on g.tkt_ctlg_grp_id=i.ctlg_grp_id join tkt_ctlg_type_lst_t tp on tp.tkt_ctlg_type_id=i.tkt_ctlg_type_id join tkt_ctlg_ctgry_lst_t c on c.tkt_ctlg_ctgry_id=i.tkt_ctlg_ctgry_id join usr_grp_rel_t ug on ug.usr_id=t.crt_usr_id join (select usr_sprt_grp_id  from usr_grp_rel_t where usr_id=12) ug1 on ug1.usr_sprt_grp_id=ug.usr_sprt_grp_id where   t.tckt_sts_id="+status_id;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getOpenTicketDtls = function(usr_id,callback) {
    
    // var QRY_TO_EXEC = `select t.tkt_id,t.crt_usr_id,t.tkt_sbjct_tx,t.tkt_bdy_tx 
    //                         from tkct_dtl_t t
    //                         join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id
    //                         join usr_grp_rel_t u on u.usr_sprt_grp_id=i.assign_usr_grp_id
    //                         where u.usr_id=118 and t.tckt_sts_id=1`;
       var QRY_TO_EXEC=  "select t.tkt_id,t.tkt_sbjct_tx,s.tckt_sts_nm,s.tckt_sts_id,g.tkt_ctlg_grp_nm,tp.tkt_ctlg_type_nm,p.prty_nm,p.prty_id,c.tkt_ctlg_ctgry_nm from tkct_dtl_t t join tckt_sts_lst_t s on s.tckt_sts_id=t.tckt_sts_id join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id join usr_grp_rel_t u on u.usr_sprt_grp_id=i.assign_usr_grp_id join tkt_ctlg_grp_lst_t g on g.tkt_ctlg_grp_id=i.ctlg_grp_id join tkt_ctlg_type_lst_t tp on tp.tkt_ctlg_type_id=i.tkt_ctlg_type_id join tkt_ctlg_ctgry_lst_t c on c.tkt_ctlg_ctgry_id=i.tkt_ctlg_ctgry_id join prty_lst_t p on p.prty_id=t.prty_id where u.usr_id=118 and t.tckt_sts_id=1";
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getUsrGrpList = function(tkt_id,callback) {
      var QRY_TO_EXEC = `select usr_sprt_grp_id,usr_sprt_grp_nm from usr_grp_lst_t order by usr_sprt_grp_id`;
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getticketStatusList = function(usr_id,callback) {
    
    var QRY_TO_EXEC = `select * from tckt_sts_lst_t`;
 
     // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updateTicketDtls = function(tkt_id,tkt_status_id,callback) {
    console.log("in model ********************");
    
    var QRY_TO_EXEC = "update tkct_dtl_t set tckt_sts_id="+tkt_status_id+",asgn_usr_id=118 where tkt_id="+tkt_id;
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTicketDtlById = function(tkt_id,callback) {
    
    // var QRY_TO_EXEC = `select t.tkt_id,t.crt_usr_id,t.tkt_sbjct_tx,t.tkt_bdy_tx 
    //                         from tkct_dtl_t t
    //                         join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id
    //                         join usr_grp_rel_t u on u.usr_sprt_grp_id=i.assign_usr_grp_id
    //                         where u.usr_id=118 and t.tckt_sts_id=1`;
       var QRY_TO_EXEC=  "select t.tkt_id,t.tkt_sbjct_tx,s.tckt_sts_nm,s.tckt_sts_id,ul.usr_nm as asgn_usr_nm,ul.usr_id as asgn_usr_id,g.tkt_ctlg_grp_nm,ug.usr_sprt_grp_nm,ug.usr_sprt_grp_id,tp.tkt_ctlg_type_nm,p.prty_nm,p.prty_id,c.tkt_ctlg_ctgry_nm from tkct_dtl_t t join tckt_sts_lst_t s on s.tckt_sts_id=t.tckt_sts_id join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id join usr_grp_rel_t u on u.usr_sprt_grp_id=i.assign_usr_grp_id join usr_grp_lst_t ug on ug.usr_sprt_grp_id=i.assign_usr_grp_id  join tkt_ctlg_grp_lst_t g on g.tkt_ctlg_grp_id=i.ctlg_grp_id join tkt_ctlg_type_lst_t tp on tp.tkt_ctlg_type_id=i.tkt_ctlg_type_id join tkt_ctlg_ctgry_lst_t c on c.tkt_ctlg_ctgry_id=i.tkt_ctlg_ctgry_id join prty_lst_t p on p.prty_id=t.prty_id left join usr_lst_t ul on ul.usr_id=t.asgn_usr_id where t.tkt_id="+tkt_id+" GROUP BY t.tkt_id";
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTicketCmnts = function(tkt_id,callback) {
      var QRY_TO_EXEC = `select * from tkct_cmnt_dtl_t where tkt_id=${tkt_id} ORDER BY i_ts desc`;
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getUsrList = function(tkt_id,callback) {
      var QRY_TO_EXEC = `select * from usr_lst_t order by usr_id`;
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.assignusr = function(usrDtls,callback) {
    // console.log(usrDtls);
     var QRY_TO_EXEC1 = `insert into tkct_cmnt_dtl_t(tkt_id,cmnt_tx,i_ts,usr_id)values(${usrDtls.tkt_id},'Assigned User:: ${usrDtls.asgn_usr_nm} ' ,current_timestamp(),${usrDtls.usr_id} )`;
  var QRY_TO_EXEC2 = "update tkct_dtl_t set asgn_usr_id="+usrDtls.asgn_usr_id+",tckt_sts_id=6 where tkt_id="+usrDtls.tkt_id+";";
    var QRY_TO_EXEC=QRY_TO_EXEC1 +";"+QRY_TO_EXEC2;
    // console.log(QRY_TO_EXEC);
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
      return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
    //  return;
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.submtCmnts = function(usrDtls,callback) {
    // console.log(usrDtls);
     var QRY_TO_EXEC = `insert into tkct_cmnt_dtl_t(tkt_id,cmnt_tx,i_ts,usr_id)values(${usrDtls.tkt_id}, '${usrDtls.cmnt_tx}'  ,current_timestamp(),${usrDtls.usr_id} )`;
     // console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
      return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
    //  return;
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getacceptedtktDtls = function(usr_id,callback) {
    
       var QRY_TO_EXEC=  "select t.tkt_id,t.tkt_sbjct_tx,s.tckt_sts_nm,s.tckt_sts_id,g.tkt_ctlg_grp_nm,tp.tkt_ctlg_type_nm,p.prty_nm,p.prty_id,c.tkt_ctlg_ctgry_nm from tkct_dtl_t t join tckt_sts_lst_t s on s.tckt_sts_id=t.tckt_sts_id join tckt_ctlg_itm_lst_t i on i.tckt_ctlg_itm_id=t.tckt_ctlg_itm_id join usr_grp_rel_t u on u.usr_sprt_grp_id=i.assign_usr_grp_id join tkt_ctlg_grp_lst_t g on g.tkt_ctlg_grp_id=i.ctlg_grp_id join tkt_ctlg_type_lst_t tp on tp.tkt_ctlg_type_id=i.tkt_ctlg_type_id join tkt_ctlg_ctgry_lst_t c on c.tkt_ctlg_ctgry_id=i.tkt_ctlg_ctgry_id join prty_lst_t p on p.prty_id=t.prty_id where u.usr_id=118 and t.tckt_sts_id=6";
 
        if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.acptTktStatusChange = function(usrDtls,callback) {
    // console.log(usrDtls);
     var QRY_TO_EXEC1 = `insert into tkct_cmnt_dtl_t(tkt_id,cmnt_tx,i_ts,usr_id)values(${usrDtls.tkt_id},'Ticket Status Changed To:: ${usrDtls.tckt_sts_nm} ' ,current_timestamp(),${usrDtls.usr_id} )`;
  var QRY_TO_EXEC2 = "update tkct_dtl_t set tckt_sts_id="+usrDtls.tckt_sts_id+" where tkt_id="+usrDtls.tkt_id+";";
    var QRY_TO_EXEC=QRY_TO_EXEC1 +";"+QRY_TO_EXEC2;
    // console.log(QRY_TO_EXEC);
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
      return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
    //  return;
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTktPriorityDtls = function(callback) {
    
       var QRY_TO_EXEC=  "select * from prty_lst_t";
 
        if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTktCtgryLst = function(typlst,catlst,callback) {
      var QRY_TO_EXEC = `select t.tkt_ctlg_ctgry_id,tg.tkt_ctlg_ctgry_nm,t.assign_usr_grp_id,t.tckt_ctlg_itm_id from tckt_ctlg_itm_lst_t t
                          join tkt_ctlg_ctgry_lst_t tg on tg.tkt_ctlg_ctgry_id=t.tkt_ctlg_ctgry_id
                          where t.tkt_ctlg_type_id=${typlst} and t.ctlg_grp_id=${catlst}
                          GROUP BY t.tkt_ctlg_ctgry_id
                          order by t.tkt_ctlg_ctgry_id`;
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettktCtlgGrp = function(tkt_ctlg_typ_id,callback) {
      var QRY_TO_EXEC = `select t.ctlg_grp_id,tg.tkt_ctlg_grp_nm from tckt_ctlg_itm_lst_t t
                            join tkt_ctlg_grp_lst_t tg on tg.tkt_ctlg_grp_id=t.ctlg_grp_id
                            where t.tkt_ctlg_type_id=${tkt_ctlg_typ_id}
                            GROUP BY t.ctlg_grp_id
                            order by t.ctlg_grp_id`;
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.submitNewTkt = function(tkt_ctlg_typ_id,callback) {
      var QRY_TO_EXEC = ``;
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};