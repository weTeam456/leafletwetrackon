// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');






/*****************************************************************************
* Function      : postdrvrImgDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/



exports.postdrvrImgDtls = function(data,newFile,callback) {
    
    var QRY_TO_EXEC = "INSERT INTO drvr_img_upld_dtl_t(img_url_tx,lat,lng,crew_id,asrt_id,i_ts) VALUES ('"+newFile+"','" + data.lat + "','" + data.lng + "','" + data.drvr_id + "','" + data.veh_nm + "',current_timestamp());";
    console.log(QRY_TO_EXEC);

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : postdrvrImgDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/



exports.getdrvrAsgndtls = function(data,callback) {
    
    var QRY_TO_EXEC = `select cl.crw_id, cl.crw_nm, al.asrt_nm, al.asrt_id, da.asnmt_id,DATE_FORMAT(da.i_ts,'%d/%m/%Y') as i_ts from drvr_asgnt_dtl_t da
                                JOIN crw_lst_t cl on cl.crw_id = da.crw_id
                                join asrt_lst_t al on al.asrt_id = da.asrt_id where cl.crw_id = ${data} and DATE_FORMAT(CURDATE(),'%Y-%m-%d') BETWEEN DATE_FORMAT(frm_ts,'%Y-%m-%d') AND DATE_FORMAT(to_ts,'%Y-%m-%d')`;
    // console.log(QRY_TO_EXEC);

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : post_TrpDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/



exports.post_TrpDtls = function(asrtId,callback) {
    
    var QRY_TO_EXEC = `select * from drvr_asgnt_dtl_t da
join trp_rn_dtl_t tr on tr.asrt_id= da.asrt_id 
where DATE_FORMAT(tr.i_ts,'%Y-%m-%d') = CURDATE()  and tr.asrt_id = ${asrtId} and (tr.actl_end_ts is null or tr.actl_strt_ts is NULL)`;
    // console.log(QRY_TO_EXEC);

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
// /*****************************************************************************
// * Function      : post_updtTrpDtls
// * Description   : 
// * Arguments     : callback function
// ******************************************************************************/



exports.post_updtTrpDtls = function(data,callback) {
    // console.log(data);
    // if(data == null){
    //        var QRY_TO_EXEC = `update trp_rn_dtl_t set actl_strt_ts = current_timestamp() where trp_rn_id =${data.trp_rn_id}` ;
    //  }
     if(data[0].actl_strt_ts == null){
         var QRY_TO_EXEC = `update trp_rn_dtl_t set actl_strt_ts = current_timestamp() where trp_rn_id =${data.trp_rn_id}` ;
    } else if(data[0].actl_strt_ts != null && data[0].actl_end_ts == null ){
         var QRY_TO_EXEC = `update trp_rn_dtl_t set actl_end_ts = current_timestamp() where trp_rn_id =${data.trp_rn_id}` ;
     }
    // console.log(QRY_TO_EXEC);
     if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
     else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

// /*****************************************************************************
// * Function      : get_recentImgs
// * Description   : 
// * Arguments     : callback function
// ******************************************************************************/



exports.get_recentImgs = function(crw_id,callback) {
    var QRY_TO_EXEC = `select *,DATE_FORMAT(i_ts,'%Y-%m-%d %h:%i:%s') as time_stp from drvr_img_upld_dtl_t where crew_id = ${crw_id} order BY i_ts DESC limit 10` ;
     if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
     else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

// /*****************************************************************************
// * Function      : getUsrDtls 
// * Description   : 
// * Arguments     : callback function
// ******************************************************************************/



exports.getUsrDtls = function(data,callback) {
    var QRY_TO_EXEC = `SELECT * from crw_lst_t where crw_ph = ${data.mble_nu}` ;
     if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
     else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


// /*****************************************************************************
// * Function      : getDvceDtls 
// * Description   : 
// * Arguments     : callback function
// ******************************************************************************/



exports.getDvceDtls = function(data,callback) {
    var QRY_TO_EXEC = `select mble_rgstr_id,mble_id,usr_id,mble_ph,(case WHEN mble_id="${data.token}" THEN 1 ELSE 0 END) as sts from mble_rgstr_dtl_t where mble_ph = ${data.mble_nu}` ;
     if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
     else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : post_mbleDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.post_mbleDtls = function (drvrDtls, dvceDtls, data, callback) {
    var LtstDvcId = data.token;
    if(dvceDtls==''){
          QRY_TO_EXEC = "INSERT INTO mble_rgstr_dtl_t(mble_id,usr_id,mble_ph,rgstn_ts,a_in,i_ts) VALUES('"+data.token+"','"+drvrDtls[0].crw_id+"','"+data.mble_nu+"',CURRENT_TIMESTAMP(),'1',CURRENT_TIMESTAMP())";
            if (callback && typeof callback == "function")
                         dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function (err, results) {
                            callback(err, results);
                            return;
                        });
                         else
                            return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
       }
       else if(dvceDtls[0].sts == 0 && dvceDtls !== ''){
       var QRY_TO_EXEC = "UPDATE  mble_rgstr_dtl_t SET mble_id = '"+LtstDvcId+"' WHERE usr_id = '"+dvceDtls[0].usr_id+"'";
       if (callback && typeof callback == "function")
                 dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function (err, results) {
                callback(err, results);
                return;

            });
             else
                return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
       }
    
};



