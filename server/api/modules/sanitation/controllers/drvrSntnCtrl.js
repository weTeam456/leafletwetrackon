// Standard Inclusions
var log = require(appRoot + '/utils/logmessages');
var std = require(appRoot + '/utils/standardMessages');
var df = require(appRoot + '/utils/dflower.utils');
var cntxtDtls = df.getModuleMetaData(__dirname, __filename);
var AWS = require('aws-sdk');
var awsS3= 'config/aws-s3.config.json';

// Model Inclusions
var sntnMdl = require('../models/drvrSntnMdl');


/**************************************************************************************
 * Controller     : drvrImgDtls_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 24/06/2017    - Vijaya Lakshmi
 ***************************************************************************************/
exports.drvrImgDtls_post = function(req, res) {
  AWS.config.loadFromPath(awsS3);
  var s3 = new AWS.S3();
  var Dtls =req.body; 
  var imageData =req.body.str;

  var data = imageData.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer(data, 'base64');
  var name = Date.now();

  var fnm = "drvrImgDtls_post";
  log.message("INFO", cntxtDtls, 100, `In ${fnm}`);

  var imgNm = name+".png";
  var params = {
    Bucket: 'dswetrack/apMuncipalApp/image_upload', 
    Key:imgNm , 
    ACL:  'public-read',
    Body: buf
    };
  s3.upload(params, function(err, data) {
    console.log(err, data);
    if(!err) imgDtls();
  });

  var imgDtls = function(){
       sntnMdl.postdrvrImgDtls(req.body,imgNm)
        .then(function(results) {
            log.message("INFO", cntxtDtls, 100, results);
            df.formatSucessRes(res, results, cntxtDtls, 'drvrImgDtls_post', {});
        }, function(error) {
            log.message("ERROR", cntxtDtls, 100, error);
            df.formatErrorRes(res, error, cntxtDtls, 'drvrImgDtls_post', {});
        });
      }
}
/**************************************************************************************
 * Controller     : drvrAsgndtls_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 24/06/2017    - Vijaya Lakshmi
 ***************************************************************************************/
exports.drvrAsgndtls_get = function(req, res) {
  var crw_id =req.params.drvrId;
   console.log(crw_id);
    var fnm = "drvrAsgndtls_get";
    log.message("INFO", cntxtDtls, 100, `In ${fnm}`);
            sntnMdl.getdrvrAsgndtls(crw_id)
                .then(function(results) {
                    log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'drvrAsgndtls_get', {});
                }, function(error) {
                    log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'drvrAsgndtls_get', {});
                });
}
/**************************************************************************************
 * Controller     : TrpDtls_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 27/06/2017    - Vijaya Lakshmi
 ***************************************************************************************/
exports.TrpDtls_post = function(req, res) {
  var asrtId =req.params.asrtId;
   console.log(asrtId);
    var fnm = "TrpDtls_post";
    log.message("INFO", cntxtDtls, 100, `In ${fnm}`);
            sntnMdl.post_TrpDtls(asrtId)
                .then(function(results) {
                    log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'TrpDtls_post', {});
                }, function(error) {
                    log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'TrpDtls_post', {});
                });
}
// /**************************************************************************************
//  * Controller     : updtTrpDtls_post
//  * Parameters     : None
//  * Description    : 
//  * Change History :
//  * 27/06/2017    - Vijaya Lakshmi
//  ***************************************************************************************/
// exports.updtTrpDtls_post = function(req, res) {
//  console.log("Vijaya");
//    console.log(req.body);
//     var fnm = "updtTrpDtls_post";
//     log.message("INFO", cntxtDtls, 100, `In ${fnm}`);
//             sntnMdl.post_updtTrpDtls(req.body)
//                 .then(function(results) {
//                     log.message("INFO", cntxtDtls, 100, results);
//                     df.formatSucessRes(res, results, cntxtDtls, 'currRunnVehicles_get', {});
//                 }, function(error) {
//                     log.message("ERROR", cntxtDtls, 100, error);
//                     df.formatErrorRes(res, error, cntxtDtls, 'currRunnVehicles_get', {});
//                 });
// }
/**************************************************************************************
 * Controller     : recentImgs_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 30/06/2017    - Vijaya Lakshmi
 ***************************************************************************************/
exports.recentImgs_get = function(req, res) {
  var crw_id = req.params.crw_id;
    var fnm = "recentImgs_get";
    log.message("INFO", cntxtDtls, 100, `In ${fnm}`);
            sntnMdl.get_recentImgs(crw_id)
                .then(function(results) {
                    log.message("INFO", cntxtDtls, 100, results);
                    df.formatSucessRes(res, results, cntxtDtls, 'recentImgs_get', {});
                }, function(error) {
                    log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'recentImgs_get', {});
                });
}
/**************************************************************************************
 * Controller     : mbleDtls_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 12/07/2017    - Vijaya Lakshmi
 ***************************************************************************************/
exports.mbleDtls_post = function (req, res) {
    var data = req.body;
    var fnm = "mbleDtls_post";
    log.message("INFO", cntxtDtls, 100, `In ${fnm}`);
                sntnMdl.getUsrDtls(data)
                .then(function(results) {
                    log.message("INFO", cntxtDtls, 100, results);
                    var drvrDtls = results;
                    sntnMdl.getDvceDtls(req.body, function (error, results) {
                      if (error) console.log(error);
                      if (results) console.log(results);
                      var dvceDtls = results;
                        sntnMdl.post_mbleDtls(drvrDtls, dvceDtls, data, function (error, results) {
                            if (error) console.log(error);
                            if (results) console.log(results);
                            df.formatSucessRes(res, results, cntxtDtls, fnm, {});
                        })

                  })
                }, function(error) {
                    log.message("ERROR", cntxtDtls, 100, error);
                    df.formatErrorRes(res, error, cntxtDtls, 'recentImgs_get', {});
                });


}

