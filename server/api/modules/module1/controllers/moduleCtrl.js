// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);
var geoFenceSch = require(appRoot+'/server/api/modules/geocoding/validators/geoFenceVld');

// Model Inclusions
var servicesmdl = require('../models/moduleMdl');


/**************************************************************************************
* Controller     : oprsHomeRpt_get
* Parameters     : None
* Description    : 
* Change History :
* 11/29/2016    - Sunil Mulagada - Initial Function
*
***************************************************************************************/
exports.oprsHomeRpt_get = function(req, res) {
    var fnm="oprsHomeRpt_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Request Parameters  req.checkParams();
    // Query Parameters req.checkQuery();
    // Request Body
    //req.checkBody(geoFenceSch.FenceGrpSch.body);
    // Validate Header 
    //req.checkHeaders(geoFenceSch.FenceGrpSch.header);

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                servicesmdl.getAllZonesLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });
      log.message("INFO",cntxtDtls,100,"test message");
      log.message("ERROR",cntxtDtls,100,"test message");
      log.message("WARN",cntxtDtls,100,"test message");
}