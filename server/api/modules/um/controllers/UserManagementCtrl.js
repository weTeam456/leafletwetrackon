// Standard Inclusions

var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var usrmngtVld = require(appRoot+'/server/api/modules/um/validators/umVld');

// Model Inclusions
var usrmngtmdl = require('../models/userManagementMdl');


/**************************************************************************************
* Controller     : userappsLst_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 22/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.userappsLst_get = function(req, res) {
    var fnm="userappsLst_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    usrmngtmdl.userappsLst_getMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : usermenu_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 12/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.usermenu_get = function(req, res) {
    var fnm="usermenu_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    usrmngtmdl.usermenu_getMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : userUrlAccess_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 01/06/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.userUrlAccess_get = function(req, res) {
    var fnm="userUrlAccess_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    // Model gets called Here 
    usrmngtmdl.userUrlAccess_getMdl(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}


/**************************************************************************************
* Controller     : designation_get
* Parameters     : None
* Description    : 
* Change History :
* 05/03/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/
exports.designation_get = function(req, res) {
    var fnm="designation_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                usrmngtmdl.getDsgns(req.params)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : allPrfl_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 01/06/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.allPrfl_get = function(req, res) {
    var fnm="allPrfl_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                usrmngtmdl.allPrfl_getMdl(req.params)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });

}

/**********************************************************************************************************
* Controller     : createUser_post
* Parameters     : req, res ()
* Description    : To create a new user in cognito and user dtls will be stored in our DB except password
* Change History :
* 09/05/2016    - Chaitanya,Sony,Vijaya - Initial Function
***********************************************************************************************************/
exports.createUser_post = function(req, res) {
    var fnm="createUser_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    
    req.checkBody(usrmngtVld.usrmngtVld.body);
    
    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
            return;
        } else {
            usrmngtmdl.addUser(req.body)
            .then(function(results){
                    usrmngtmdl.getUserId(req.body)
                    .then(function(results){
                        log.message("INFO",cntxtDtls,100,"local datbase insert success");
                        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
                    },function(error){
                        log.message("ERROR",cntxtDtls,100,"local datbase insert failure");
                        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                    });
            },function(error){
                log.message("ERROR",cntxtDtls,100,"local datbase insert failure");
                
            });
        }
    });
}

/**************************************************************************************
* Controller     : assignMnuToUsr_get
* Parameters     : req,res()
* Description    : 
* Change History :
* 01/06/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.assignMnuToUsr_get = function(req, res) {
    var fnm="assignMnuToUsr_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            var count=0;
            for(var i=0;i<req.body.length;i++){
                (function(j){
                    setTimeout(function(){
                        usrmngtmdl.assignMnuToUsr_Mdl(req.params,req.body[j])
                        .then(function(results){
                            count++;
                            if(count==req.body.length)
                                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                        },function(error){
                            df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                        });
                    },(j%req.body.length)*1000);
                })(i)
            }
        }
    });

}

/*******************************************************************************************
* Controller     : updateUser_post
* Parameters     : req, res ()
* Description    : To update a existent user in cognito and dtls will be stored in our DB
* Change History :
* 09/05/2016    - Chaitanya,Sony,Vijaya - Initial Function
*
********************************************************************************************/
exports.updateUser_post = function(req, res) {

    var fnm="updateUser_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.checkBody(usrmngtVld.updateUsrDtlsVld.body);
    
    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            // cognitoUtils.updatesUser(req,function(err,data){
                // if(err==null){
                    // log.message("INFO",cntxtDtls,100,"Details updated in Cognito userpool successfully :"+data);
                    usrmngtmdl.updateUsers(req.body)
                    .then(function(results){
                        log.message("INFO",cntxtDtls,100,"local datbase insert success");
                        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                    },function(error){
                        log.message("ERROR",cntxtDtls,100,"local datbase insert failure");
                        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                    });
                // }
            // });                    
        }
    });
}

/***************************************************************************************************************
* Controller     : deleteUser_post
* Parameters     : req, res ()
* Description    : To delete a existent user in cognito and delete indicator will be changed to 1 in our DB
* Change History :
* 09/05/2016    - Chaitanya,Sony,Vijaya - Initial Function
*
****************************************************************************************************************/
exports.deleteUser_post = function(req, res) {
    var fnm="deleteUser_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            usrmngtmdl.deleteUsers(req.body)
            .then(function(results){
                log.message("INFO",cntxtDtls,100,"local datbase insert success");
                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
            },function(error){
                log.message("ERROR",cntxtDtls,100,"local datbase insert failure");
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    });
}

/**************************************************************************************
* Controller     : UserProfile_update
* Parameters     : None
* Description    : 
* Change History :
* 05/04/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/

exports.UserProfile_update = function(req, res) {

    var fnm="UserProfile_update";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.checkBody(usrmngtVld.updtPrflVld.body);
    
    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            usrmngtmdl.updateProfile(req.body)
            .then(function(results){
                log.message("INFO",cntxtDtls,100,"local datbase insert success");
                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
            },function(error){
                log.message("ERROR",cntxtDtls,100,"local datbase insert failure");
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });                  
        }
    });
}

/**************************************************************************************
* Controller     : UsrsWthDsgns_get
* Parameters     : None
* Description    : 
* Change History :
* 05/06/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/
exports.UsrsWthDsgns_get = function(req, res) {

    var fnm="UsrsWthDsgns_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                usrmngtmdl.getUsersWthDsgns(req.params)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });
      
     // log.message("INFO",cntxtDtls,100,"test message");
      // log.message("ERROR",cntxtDtls,100,"test message");
      // log.message("WARN",cntxtDtls,100,"test message");

}

/**************************************************************************************
* Controller     : usrsHirchy_get
* Parameters     : None
* Description    : 
* Change History :
* 05/06/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/
exports.usrsHirchy_get = function(req, res) {

    var fnm="usrsHirchy_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                usrmngtmdl.usrsHirchy_getMdl(req.params)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : updHirchy_post
* Parameters     : req,res()
* Description    : to update heirarchy level of the org user
* Change History :
* 25/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.updHirchy_post = function(req, res) {

    var fnm="updHirchy_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                usrmngtmdl.updHirchy_postMdl(req.body)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : Designation_Update
* Parameters     : None
* Description    : 
* Change History :
* 05/08/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/
exports.dsgn_update = function(req, res) {
    var fnm="UserProfile_get";
    var dtls = req.body;
    // Model gets called Here 
    usrmngtmdl.updateDsgn(dtls)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}
/**************************************************************************************
* Controller     : Designation_Update
* Parameters     : None
* Description    : 
* Change History :
* 05/10/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/
exports.PwdResetUsr_get = function(req, res) {
    var fnm="UserProfile_get"
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here 
    usrmngtmdl.PwdResetUsr(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : Getting users for App Profiles
* Parameters     : req.body
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 17/05/2017    - Vijaya Lakshmi - Initial Function
*
***************************************************************************************/
exports.usersAppPrfl_get = function(req, res) {
    var fnm="usersAppPrfl_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    
    // Model gets called Here 
    usrmngtmdl.getAppPrflusrs(req.params)
    .then(function(results){
        df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}
/**************************************************************************************
* Controller     : To get user menu profiles
* Parameters     : req.body
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 18/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.usersMnuPrfl_get = function(req, res) {
    var fnm="usersMnuPrfl_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.body).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here 
            usrmngtmdl.getMnPrflusrs(req.params)
            .then(function(results){
                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
}

/**************************************************************************************
* Controller     : To get all apps
* Parameters     : req.body
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 19/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.apps_get = function(req, res) {
    var fnm="apps_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.body).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here 
            usrmngtmdl.getAppsMdl(req.params)
            .then(function(results){
                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
}

/**************************************************************************************
* Controller     : To get all menuItems
* Parameters     : req.params
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 19/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.mnuItms_get = function(req, res) {
    var fnm="mnuItms_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.params).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here 
            usrmngtmdl.getMnuItmsMdl(req.params)
            .then(function(results){
                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
}

/**************************************************************************************
* Controller     : To get all menuItems
* Parameters     : req.params
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 19/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.submnuItms_get = function(req, res) {
    var fnm="submnuItms_get";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.params).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here 
            usrmngtmdl.getSubMnuItmsMdl(req.params)
            .then(function(results){
                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
}
/**************************************************************************************
* Controller     : To create user menu profiles
* Parameters     : req.body
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 24/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.createAppPrf_post = function(req, res) {
    var fnm="createAppPrf_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    
    req.getValidationResult(req.body).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here
            appPrfl_nm = req.params.appPrflNm;
            data = req.body;
            usrmngtmdl.postCreateAppPrf(req.params)
            .then(function(results){
                var count=0;
                for(var i=0;i<data.length;i++){
                        data[i].sqnceId = i+1;
                        usrmngtmdl.appsFornewAppPrf(appPrfl_nm,data[i])
                        .then(function(results){
                            count++;
                            if(count==data.length)
                                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});
                        },function(error){
                            df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                        });
                }
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
}

/**************************************************************************************
* Controller     : To assign app profile to user
* Parameters     : req.params
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 24/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.assignAppPrfToUsr_post = function(req, res) {
    var fnm="assignAppPrflToUsr_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.params).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else {
            // Model gets called Here
            user_data = req.body;
            usrmngtmdl.getAppPrflId(req.params.appPrflNm)
            .then(function(results){
                    var count=0;
                    for(var i=0;i<user_data.length;i++){
                        usrmngtmdl.assignAppPrfToUsrMdl(results[0].app_prfle_id,user_data[i])
                            .then(function(results){
                                count++;
                                if(count==user_data.length)
                                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                            },function(error){
                                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                            });
                    }
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });                
        }
    })
}

/**************************************************************************************
* Controller     : To create user menu profiles
* Parameters     : req.body
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 18/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.createMnu_post = function(req, res) {
    var fnm="createMnu_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    
    req.getValidationResult(req.body).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here
            menu_nm = req.params.mnuNm;
            data = req.body;
            usrmngtmdl.postCreateMnu(req.params)
            .then(function(results){
                var count=0;
                for(var i=0;i<data.length;i++){
                    (function(j){
                        setTimeout(function(){
                            data[j].sqnceId = j+1;
                            if(!data[j].submnu_itm_id){
                                data[j].submnu_itm_id = data[j].mnu_itm_id;
                                data[j].mnu_itm_id = 0;
                            }
                            usrmngtmdl.itmsFornewMnu(menu_nm,data[j])
                            .then(function(results){
                                count++;
                                if(count==data.length)
                                        df.formatSucessRes(res,results,cntxtDtls,fnm,{});
                            },function(error){
                                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                            });
                        },(j%data.length)*2000);
                    })(i)
                }
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
}

/**************************************************************************************
* Controller     : To assign menu to user
* Parameters     : req.params
* Description    : To add a specific group to the requested user in cognito
* Change History :
* 19/05/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.assignMnuToUsr_post = function(req, res) {
    var fnm="assignMnuToUsr_post";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult(req.params).then(function(result) {

        if (!result.isEmpty()) {
            df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{});                 
            return;
        } else { 
            // Model gets called Here 
            user_data = req.body;
            usrmngtmdl.getMnuPrflId(req.params.mnuNm)
            .then(function(results){
                    menuDtls = {
                        menu_id : results[0].mnu_prfle_id,
                        app_id  : req.params.appId
                    }
                    
                    var count=0;
                    for(var i=0;i<user_data.length;i++){
                        usrmngtmdl.assignMnuToUsrMdl(menuDtls,user_data[i])
                        .then(function(results){
                            count++;
                            if(count==user_data.length){
                                
                                df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                            }
                        },function(error){
                            df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                        });
                    }
            },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            });
        }
    })
};
