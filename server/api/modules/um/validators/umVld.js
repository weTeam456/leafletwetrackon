exports.usrmngtVld = {
   "body": {
       'usr_nm': {
            notEmpty: true,
            errorMessage: 'User Name Required'
        },
        'eml_tx': {
            notEmpty: true,
            isEmail: {
                errorMessage: 'Invalid Email'
            }
        },
        'newPassword': {
            notEmpty: true,
            matches: {
                errorMessage: 'Invalid Email'
            }
        },
        'clnt_id':{
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Client Id' // Error message for the parameter
            }
        },
        'tnt_id':{
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            }
        },
        'fst_nm':{
            notEmpty: true,
            isAlpha:{
                errorMessage: 'Invalid First Name' // Error message for the parameter
            }
        },
        'lst_nm':{
            notEmpty: true,
            isAlpha:{
                errorMessage: 'Invalid Last Name' // Error message for the parameter
            }
        },
    },
    
}
exports.updateUsrDtlsVld = {
   "body": {
        'fst_nm':{
            notEmpty: true,
            isAlpha:{
                errorMessage: 'Invalid First Name' // Error message for the parameter
            }
        },
        'lst_nm':{
            notEmpty: true,
            isAlpha:{
                errorMessage: 'Invalid Last Name' // Error message for the parameter
            }
        },
        'eml_tx': {
            notEmpty: true,
            isEmail: {
                errorMessage: 'Invalid Email' // Error message for the parameter
            }
        },
        'mobile_nu':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Mobile Number' // Error message for the parameter
            }
        },
    },
    
}
exports.updtPrflVld = {
   "body": {
        'eml_tx': {
            notEmpty: true,
            isEmail: {
            errorMessage: 'Invalid Email'
            }
        },
        'mobile_nu':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Mobile Number' // Error message for the parameter
            }
        },
        'dsgns_id':{
        notEmpty: true,
        isInt:{
        errorMessage: 'Invalid Designation Id' // Error message for the parameter
           }
        },
        'fst_nm':{
            notEmpty: true,
            isAlpha:{
            errorMessage: 'Invalid First Name' // Error message for the parameter
            }
        },
        'lst_nm':{
            notEmpty: true,
            isAlpha:{
            errorMessage: 'Invalid Last Name' // Error message for the parameter
            }
        },
    },
    
}
exports.updtPwdlVld = {
   "body": {
        'usrnm':{
            notEmpty: true,
            isAlpha:{
            errorMessage: 'Invalid User Name' // Error message for the parameter
            }
        },
        'mobile_nu':{
            notEmpty: true,
            isInt:{
            errorMessage: 'Invalid Mobile Number' // Error message for the parameter
            }
        },
        'newPassword':{
            notEmpty: true,
            errorMessage: 'Invalid Mobile Number'
        },
    },
    
};