// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');



/*****************************************************************************
* Function      : userappsLst_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.userappsLst_getMdl = function(data,callback) {

    var QRY_TO_EXEC = `select a.app_id,a.app_nm,a.app_lgo_tx,a.app_url_tx 
                        from app_prfle_lst_t ap 
                        join app_prfle_app_rel_t apr on ap.app_prfle_id=apr.app_prfle_id 
                        join app_lst_t a on a.app_id=apr.app_id
                        join usr_lst_t u on u.app_prfle_id=ap.app_prfle_id 
                        and u.clnt_id=ap.clnt_id and u.tnt_id=ap.tnt_id
                        where u.usr_id =`+data.usrid+` and a.a_in =1 order by apr.sqnce_id;`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : usermenu_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.usermenu_getMdl = function(data,callback) {
    var QRY_TO_EXEC = `select mi.mnu_itm_id,mi.sqnce_id,mi.mnu_itm_nm,mi.mnu_itm_icn_tx,mi.mnu_itm_url_tx,mpi.prnt_mnu_itm_id,mpi.c_in,mpi.r_in,mpi.u_in,mpi.d_in,mpi.o_in,mpi.e_in 
                        from mnu_prfle_lst_t mp 
                        join mnu_prfle_itm_rel_t mpi on mp.mnu_prfle_id=mpi.mnu_prfle_id 
                        join mnu_itm_lst_t mi on mpi.mnu_itm_id=mi.mnu_itm_id or  mi.mnu_itm_id=mpi.prnt_mnu_itm_id
                        join usr_mnu_prfle_rel_t um on um.mnu_prfle_id = mp.mnu_prfle_id and um.app_id=mp.app_id
                        -- and um.clnt_id=mp.clnt_id and um.tnt_id=mp.tnt_id
                        join usr_lst_t u  on u.usr_id=um.usr_id
                        where u.usr_id=`+data.usrid+` and um.app_id=`+data.appid+` and mi.a_in =1 group by mi.sqnce_id order by mi.sqnce_id`;
                        
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : userUrlAccess_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.userUrlAccess_getMdl = function(data,callback) {
    var QRY_TO_EXEC = `select a.app_url_tx,mi.mnu_itm_url_tx
                        from usr_lst_t u
                        join app_prfle_lst_t ap on ap.app_prfle_id= u.app_prfle_id
                        join app_prfle_app_rel_t app on app.app_prfle_id = ap.app_prfle_id
                        join app_lst_t a on a.app_id = app.app_id
                        join usr_mnu_prfle_rel_t um on um.usr_id = u.usr_id
                        join mnu_prfle_lst_t mp on mp.mnu_prfle_id = um.mnu_prfle_id and mp.app_id = a.app_id
                        join mnu_prfle_itm_rel_t mpi on mpi.mnu_prfle_id = mp.mnu_prfle_id
                        join mnu_itm_lst_t mi on mi.mnu_itm_id = mpi.mnu_itm_id
                        where u.usr_id = `+data.usrid+` and mi.a_in=1 order by mi.app_id,mi.mnu_itm_id;`;
    console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getDsgns
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getDsgns = function(data,callback) {
    var QRY_TO_EXEC = "select * FROM dsgns_lst_t where clnt_id ="+data.clntId+" and tnt_id = "+data.tntId+" group by dsgns_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : allPrfl_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.allPrfl_getMdl = function(data,callback) {
    var QRY_TO_EXEC = `select ap.app_prfle_id,ap.app_prfle_nm,a.app_id,a.mnu_prfle_id 
                        from app_prfle_lst_t ap 
                        join app_prfle_app_rel_t apa on apa.app_prfle_id=ap.app_prfle_id 
                        join app_lst_t a on apa.app_id=a.app_id 
                        where ap.clnt_id =`+data.clntId+` and ap.tnt_id = `+data.tntId+` order by ap.app_prfle_id,a.app_id;`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : addUsers
* Description   : add users into the databse
* Arguments     : callback function
******************************************************************************/
exports.addUser = function(data,callback) {
    var QRY_TO_EXEC = "INSERT INTO usr_lst_t(usr_nm,pwd,pwd_tx,fst_nm,lst_nm,eml_tx,mobile_nu,clnt_id,tnt_id,dsgns_id,app_prfle_id,i_ts) VALUES('"+data.usr_nm+"','"+data.newPassword+"',SHA1('"+data.newPassword+"'),'"+data.fst_nm+"','"+data.lst_nm+"','"+data.eml_tx+"','"+data.mobile_nu+"','"+data.clnt_id+"','"+data.tnt_id+"','"+data.dsgId+"','"+data.app_prfle_id+"',current_timestamp())";

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getUserId
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getUserId = function(data,callback) {
    var QRY_TO_EXEC = `select usr_id from usr_lst_t where usr_nm='`+data.usr_nm+`';`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : assignMnuToUsr_Mdl
* Description   : insert users with their respective menus  
* Arguments     : callback function
******************************************************************************/
exports.assignMnuToUsr_Mdl = function(params,data,callback) {
    var QRY_TO_EXEC = "insert into usr_mnu_prfle_rel_t(usr_id,mnu_prfle_id,app_id,clnt_id,tnt_id)values("+params.usrid+","+data.mnu_prfle_id+","+data.app_id+","+params.clntId+","+params.tntId+");";

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : deleteUsers
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.deleteUsers = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE usr_lst_t set d_in = 1 , d_ts = CURRENT_TIMESTAMP() where usr_id = '"+data.usr_id+"' ";

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : updateUsers
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updateUsers = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE usr_lst_t set fst_nm = '"+data.fst_nm+"',lst_nm = '"+data.lst_nm+"',eml_tx = '"+data.eml_tx+"',mobile_nu = '"+data.mobile_nu+"',u_ts = CURRENT_TIMESTAMP(),dsgns_id = '"+data.dsgns_id+"' where usr_nm = '"+data.usr_nm+"' ";

    log.message("INFO",QRY_TO_EXEC,100,QRY_TO_EXEC);

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : Update User Profile 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.updateProfile = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE usr_lst_t set fst_nm = '"+data.fst_nm+"',lst_nm = '"+data.lst_nm+"',mobile_nu = '"+data.mobile_nu+"',eml_tx = '"+data.eml_tx+"',u_ts = CURRENT_TIMESTAMP(),dsgns_id = '"+data.dsgns_id+"' where usr_nm = '"+data.usr_nm+"' ;                                                                     ";

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : Getting User List with Designations
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getUsersWthDsgns = function(data,callback) {
    
    var QRY_TO_EXEC = "SELECT * FROM usr_lst_t as u JOIN dsgns_lst_t as d on u.dsgns_id = d.dsgns_id WHERE d_in is NULL and u.clnt_id = "+data.clntId+" and u.tnt_id = "+data.tntId+ ";" ;
    
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : usrsHirchy_getMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.usrsHirchy_getMdl = function(data,callback) {
    
    var QRY_TO_EXEC = `Select u.usr_id,u.usr_nm,d.dsgns_nm,ol.orgn_lvls_nm from usr_lst_t u 
                        join dsgns_lst_t d on u.dsgns_id = d.dsgns_id
                        join orgn_grp_lst_t og on u.orgn_grp_id = og.orgn_grp_id and u.clnt_id = og.clnt_id and u.tnt_id = og.tnt_id
                        join orgn_lvls_lst_t ol on og.orgn_lvls_id = ol.orgn_lvls_id 
                        where og.clnt_id =`+data.clntId+` and og.tnt_id =`+data.tntId+`;` ;
    
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : updHirchy_postMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.updHirchy_postMdl = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE orgn_grp_lst_t SET orgn_lvls_id="+data.orgn_lvls_id+" WHERE orgn_grp_id = (select orgn_grp_id from usr_lst_t where usr_id = "+data.usr_id+");" ;
    
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : Update User Designation
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.updateDsgn = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE usr_lst_t set dsgns_id = '"+data.dsgnId+"',u_ts = CURRENT_TIMESTAMP() where usr_id = '"+data.user_id+"' ";
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : Update User Designation
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.PwdResetUsr = function(data,callback) {
    
    var QRY_TO_EXEC = "SELECT * FROM usr_lst_t where d_in is NULL and clnt_id = "+data.clntId+" and tnt_id = "+data.tntId+";";
    
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getting users for App Profiles 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getAppPrflusrs = function(data,callback) {
    var QRY_TO_EXEC = `select u.usr_nm,ap.app_prfle_nm,a.app_nm
                        from usr_lst_t u 
                        join app_prfle_app_rel_t apr on apr.app_prfle_id=u.app_prfle_id
                        join app_prfle_lst_t ap on  apr.app_prfle_id =ap.app_prfle_id
                        join app_lst_t a on apr.app_id = a.app_id where u.d_in is null and a.a_in =1 and u.clnt_id=`+data.clntId+` and u.tnt_id = `+data.tntId+` order by u.usr_nm;`;

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getting users for Menu Profiles 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getMnPrflusrs = function(data,callback) {
    var QRY_TO_EXEC = `select u.usr_id,u.usr_nm,a.app_id,a.app_nm,um.mnu_prfle_id,mp.mnu_prfle_nm,mi.mnu_itm_nm,mi.mnu_itm_icn_tx,mi.mnu_itm_url_tx
                        from mnu_prfle_lst_t mp 
                        left join mnu_prfle_itm_rel_t mpi on mp.mnu_prfle_id=mpi.mnu_prfle_id 
                        join mnu_itm_lst_t mi on mpi.mnu_itm_id = mi.mnu_itm_id
                        join usr_mnu_prfle_rel_t um on um.mnu_prfle_id=mp.mnu_prfle_id
                        join app_lst_t a on um.app_id = a.app_id 
                        join usr_lst_t u on um.usr_id= u.usr_id
                        where  u.d_in is null and u.clnt_id=`+data.clntId+` and u.tnt_id = `+data.tntId+` and mi.a_in =1  order by mi.mnu_itm_id;`;
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getting app names
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getAppsMdl = function(data,callback) {
    var QRY_TO_EXEC = "SELECT * FROM app_lst_t where clnt_id = "+data.clntId+" and tnt_id = "+data.tntId+" order by app_id;";
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getting menu items
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getMnuItmsMdl = function(data,callback) {
    var QRY_TO_EXEC = "select mnu_itm_id,mnu_itm_nm from mnu_itm_lst_t where clnt_id ="+data.clntId+" and tnt_id = "+data.tntId+" and app_id = "+data.appId+" and a_in = 1 order by sqnce_id;";

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : getting menu items
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getSubMnuItmsMdl = function(data,callback) {
    var QRY_TO_EXEC = "select mnu_itm_id,mnu_itm_nm from mnu_itm_lst_t where mnu_itm_url_tx is not null and clnt_id ="+data.clntId+" and tnt_id = "+data.tntId+" and app_id = "+data.appId+" and a_in = 1;";

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : creating new app profile 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.postCreateAppPrf = function(data,callback) {
    
    var QRY_TO_EXEC = "insert into app_prfle_lst_t(app_prfle_nm,i_ts,clnt_id,tnt_id)values('"+data.appPrflNm+"',current_timestamp(),"+data.clntId+","+data.tntId+");";

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : assigning app to new app profile
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.appsFornewAppPrf = function(menu_nm,data,callback) {

    var QRY_TO_EXEC = "insert into app_prfle_app_rel_t(app_prfle_id,app_id,sqnce_id)select app_prfle_id,"+data.appID+","+data.sqnceId+" from app_prfle_lst_t where app_prfle_nm ='" +appPrfl_nm+"';";

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : to get app profile id based on app profile name 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getAppPrflId = function(appPrflNm,callback) {

  var QRY_TO_EXEC = "select app_prfle_id from app_prfle_lst_t where app_prfle_nm = '"+appPrflNm+"';";
   
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getting app names
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.assignAppPrfToUsrMdl = function(appPrflId,user_data,callback) {

  var QRY_TO_EXEC = "UPDATE usr_lst_t SET app_prfle_id="+appPrflId+" WHERE usr_id="+user_data.usr_id+";";
    
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : creating new menu 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.postCreateMnu = function(data,callback) {
    
    var QRY_TO_EXEC = "insert into mnu_prfle_lst_t(mnu_prfle_nm,i_ts,app_id,clnt_id,tnt_id)values('"+data.mnuNm+"',current_timestamp(),"+data.appId+","+data.clntId+","+data.tntId+");";

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : assigning menu items to new menu profile  
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.itmsFornewMnu = function(menu_nm,data,callback) {
    
    var QRY_TO_EXEC = "insert into mnu_prfle_itm_rel_t(mnu_prfle_id,mnu_itm_id,prnt_mnu_itm_id,sqnce_id,c_in,r_in,u_in,d_in,o_in,e_in)select mnu_prfle_id,"+data.submnu_itm_id+","+data.mnu_itm_id+","+data.sqnceId+","+data.permc+","+data.permr+","+data.permu+","+data.permd+","+data.permo+","+data.perme+" from mnu_prfle_lst_t where mnu_prfle_nm ='" +menu_nm+"';";
    
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : to get app profile id based on app profile name 
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.getMnuPrflId = function(mnuPrflNm,callback) {

  var QRY_TO_EXEC = "select mnu_prfle_id from mnu_prfle_lst_t where mnu_prfle_nm = '"+mnuPrflNm+"';";
   
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getting app names
* Description   : 
* Arguments     : callback function
******************************************************************************/

exports.assignMnuToUsrMdl = function(menuDtls,user_data,callback) {

  var QRY_TO_EXEC = "UPDATE usr_mnu_prfle_rel_t SET mnu_prfle_id="+menuDtls.menu_id+" WHERE usr_id="+user_data.usr_id+" and app_id="+menuDtls.app_id+";";
 
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
