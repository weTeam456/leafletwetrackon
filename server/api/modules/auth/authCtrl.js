// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

// Model Inclusions
var authMdl = require('./models/authMdl');

/**************************************************************************************
* Controller     : login_sess
* Parameters     : None
* Description    : To check if session with user details created or not
* Change History :
* 01/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.login_sess = function(req, res) {
    
    var fnm="login_sessPost";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    req.getValidationResult().then(function(result,error) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here
            if(!(typeof(req.session.passport)==='undefined')){
                result = req.session.passport.user;
                df.formatSucessRes(res,result,cntxtDtls,fnm,{});  
            }else{
                 df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
            };
        }
    });
}

/**************************************************************************************
* Controller     : forgot_pwd
* Parameters     : None
* Description    : To change password
* Change History :
* 27/06/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.forgot_pwd = function(req, res) {
    var fnm="forgot_pwddd";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
        // Model gets called Here
        authMdl.forgot_pwdMdl(req.body)
        .then(function(result){
            df.formatSucessRes(res,result,cntxtDtls,fnm,{});  
        },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
        });
}

/**************************************************************************************
* Controller     : change_pwd
* Parameters     : None
* Description    : To change password
* Change History :
* 27/06/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.change_pwd = function(req, res) {
    var fnm="change_pwd";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);

    // Model gets called Here
    authMdl.change_pwdMdl(req.body)
    .then(function(results){
        df.formatSucessRes(res,result,cntxtDtls,fnm,{});  
    },function(error){
        df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
    });
}

/**************************************************************************************
* Controller     : logout_sess
* Parameters     : None
* Description    : To destroy session when user logged out
* Change History :
* 02/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.logout_sess = function(req, res) {
    if(req.session.passport == undefined){
        req.session.destroy();
        var date = new Date();
        res.send("Logged Out at " +date);
        return;
    }else{
        var usr_nm = req.session.passport.user.username,
        date = new Date();
        req.session.destroy();
        res.send(usr_nm+" - Logged Out at " +date);
        return;
    }
}

/**************************************************************************************
* Controller     : getOrgnsLst
* Parameters     : None
* Description    : To get all tenants list
* Change History :
* 04/08/2017    - Srujana M - Initial Function
*
***************************************************************************************/
exports.getOrgnsLst = function(req, res) {
    var fnm="getOrgnsLst";
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
        // Model gets called Here
        authMdl.get_allOrgnsLstMdl()
        .then(function(result){
            df.formatSucessRes(res,result,cntxtDtls,fnm,{});  
        },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
        });
}

/**************************************************************************************
* Controller     : get_goToLgn
* Parameters     : None
* Description    : Goto login page
* Change History :
* 04/08/2017    - Srujana M - Initial Function
*
***************************************************************************************/
exports.get_goToLgn = function(req, res) {
    var fnm="get_goToLgn";
    var tnt_id = req.params.tnt_id;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
        // Model gets called Here
        authMdl.get_goToLgnMdl(tnt_id)
        .then(function(result){
            df.formatSucessRes(res,result,cntxtDtls,fnm,{});  
        },function(error){
                df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
        });
}