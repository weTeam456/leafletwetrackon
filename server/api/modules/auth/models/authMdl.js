// Standard Inclusions
var log = require(appRoot + '/utils/logmessages');
var std = require(appRoot + '/utils/standardMessages');
var df = require(appRoot + '/utils/dflower.utils');
var sqldb = require(appRoot + '/config/db.config');
var cntxtDtls = df.getModuleMetaData(__dirname, __filename);

var dbutil = require(appRoot + '/utils/db.utils');



/*****************************************************************************
 * Function      : forgot_pwdMdl
 * Description   : check user exit or not
 * Arguments     : callback function
 ******************************************************************************/
exports.forgot_pwdMdl = function(data, callback) {

    var QRY_TO_EXEC = `select usr_nm,mobile_nu from usr_lst_t where usr_nm = '${data.username}' and mobile_nu = '${data.phonenu}' and d_in is null;`;

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : change_pwdMdl
 * Description   : confirm password
 * Arguments     : callback function
 ******************************************************************************/
exports.change_pwdMdl = function(data, callback) {

    var QRY_TO_EXEC = `update usr_lst_t set pwd='${data.password}' , pwd_tx = SHA1('${data.password}') where usr_nm = '${data.username}' and d_in is null`;

    // console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : get_allOrgnsLstMdl
 * Description   : To get all tenants list
 * Arguments     : callback function
 ******************************************************************************/
exports.get_allOrgnsLstMdl = function(callback) {

    var QRY_TO_EXEC = `SELECT tnt_id,tnt_nm,tnt_dsply_nm FROM tnt_lst_t WHERE clnt_id=201 ORDER BY tnt_id`;

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
 * Function      : get_goToLgnMdl
 * Description   : To get all tenants list
 * Arguments     : callback function
 ******************************************************************************/
exports.get_goToLgnMdl = function(tnt_id,callback) {

    var QRY_TO_EXEC = `SELECT * FROM tnt_lst_t WHERE clnt_id=201 AND tnt_id=${tnt_id} ORDER BY tnt_id`;
    console.log(QRY_TO_EXEC);
    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};