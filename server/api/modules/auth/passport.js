var LocalStrategy = require('passport-local').Strategy;

// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');


/**************************************************************************************
* Functionality  : related to passport
* Parameters     : Cognito Id token and Username
* Description    : To check if idtoken is related to logged in user or not
* Change History : 
* 08/05/2016    - Sony Angel - Initial Function
*
***************************************************************************************/

module.exports = function (passport) {
  // serialize sessions
  passport.serializeUser(function(user, callback) {
    callback(null, user);
  });

  passport.deserializeUser(function(obj, callback) {
    callback(null, obj);
  });
  passport.use('cognito-login',new LocalStrategy({
     passReqToCallback : true 
  }, function(req,username,password, callback) {
      var QRY_TO_EXEC = "select * FROM usr_lst_t as u JOIN dsgns_lst_t as d on d.dsgns_id = u.dsgns_id where u.usr_nm = '"+req.body.username+"' and u.pwd_tx = SHA1('"+req.body.password+"') and u.d_in is null;";

      if(callback && typeof callback == "function")
            dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,resdata){
                // console.log(resdata);
                callback(null,resdata);
                return;
            });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
  }));
}