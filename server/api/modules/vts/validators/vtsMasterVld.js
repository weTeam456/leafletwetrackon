var cmdSch =  require('../../../validators/commonVld');

exports.FenceGrpSch = {
     "body": {
       'email': {
            notEmpty: true,
            isEmail: {
            errorMessage: 'Invalid Email'
            }
        },
        'password': {
            notEmpty: true,
            matches: {
            options: ['example', 'i'] // pass options to the validator with the options property as an array
            // options: [/example/i] // matches also accepts the full expression in the first parameter
            },
            errorMessage: 'Invalid Password' // Error message for the parameter
        },
        'a_in':cmdSch.commonSch.a_in_Rule
              

        },
        "query":{

        },
        "params":{

        },
        "header":cmdSch.commonSch.standardHeader

};

// validations for adding a assert into db
exports.addAssertsVld = {
     "body": {

       'asrt_nm': {
            notEmpty: true,
            errorMessage: 'Vehicle Name Required'
        },
        'asrt_ctgry_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Vehicle Category Id' // Error message for the parameter
            },
            errorMessage: 'Vehicle Category Required'
        },
        // 'asrt_ownr_id': {
        //     notEmpty: false,
        //     isInt:{
        //         errorMessage: 'Invalid Vehicle Owner' // Error message for the parameter
        //     },
        //     errorMessage: 'Vehicle Owner Required'
        // },
        'asrt_type_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Assert type' // Error message for the parameter
            },
            errorMessage: 'Assert type Required'
        },
        'asrt_grp_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Assert Group' // Error message for the parameter
            },
            errorMessage: 'Assert Group Required'
        },
        'dprt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Assert department' // Error message for the parameter
            },
            errorMessage: 'Assert department Required'
        },
        'crnt_dvce_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Asser Device' // Error message for the parameter
            },
            errorMessage: 'Asser Device Required'
        },
        'clnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Client Id' // Error message for the parameter
            },
            errorMessage: 'Client Id Required'
        },
        'tnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            },
            errorMessage: 'Tenant Id Required'
        },
        'a_in':cmdSch.commonSch.a_in_Rule
              
        }
};

// updating Assers Validations for assert Id 
exports.updateAssertsVld = {
    "params":{
        'asrtId': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid asrt Id' // Error message for the parameter
            },
            errorMessage: 'asrt Id Required'
        },
    },
};

// validations for adding a assert type into db
exports.addAssertTypeVld = {
     "body": {

       'asrt_type_nm': {
            notEmpty: true,
            // isAlpha:{
            //     errorMessage: 'Invalid assert Type' // Error message for the parameter
            // },
            errorMessage: 'Vehicle Name Required'
        },
        'a_in': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Indicator' // Error message for the parameter
            },
            errorMessage: 'Indicator Required'
        },
        'clnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Client Id' // Error message for the parameter
            },
            errorMessage: 'Client Id Required'
        },
        'tnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            },
            errorMessage: 'Tenant Id Required'
        }     
    }
};

// validations for adding a Department into db
exports.addDeprtmntVld = {
     "body": {

       'dprt_nm': {
            notEmpty: true,
            // isAlpha:{
            //     errorMessage: 'Invalid department Name' // Error message for the parameter
            // },
            errorMessage: 'department Name Required'
        },
        'a_in': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Indicator' // Error message for the parameter
            },
            errorMessage: 'Indicator Required'
        },
        'clnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Client Id' // Error message for the parameter
            },
            errorMessage: 'Client Id Required'
        },
        'tnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Tenant Id' // Error message for the parameter
            },
            errorMessage: 'Tenant Id Required'
        }     
    }
};

// validations for Updating a Department into db
exports.updDeprtmntVld = {

     "body": {
           'dprt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Department Id' // Error message for the parameter
            },
            errorMessage: 'Department Id Required'
        },
    }
};

// validations for deleting a vehicle in db
exports.delassertVld = {

     "body": {
           'asrt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid assert Id' // Error message for the parameter
            },
            errorMessage: 'assert Id Required'
        },
    }
};

// validations for deleting a asrt type in db
exports.delassertTypeVld = {

     "body": {
           'asrt_type_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid assert Id' // Error message for the parameter
            },
            errorMessage: 'assert Id Required'
        },
    }
};

// validations for deleting a asrt type in db
exports.delDepartmentVld = {

     "body": {
           'dprt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid assert Id' // Error message for the parameter
            },
            errorMessage: 'assert Id Required'
        },
    }
};

// validations for Add asrt Category in db
exports.AddAsertCtgry = {

     "body": {
           'asrt_ctgry_nm': {
            notEmpty: true,
            // isAlpha:{
            //     errorMessage: 'Invalid assert Name' // Error message for the parameter
            // },
            errorMessage: 'assert Name Required'
        },
        'clnt_id': {
            notEmpty: true,
                isInt:{
                    errorMessage: 'Invalid client id' // Error message for the parameter
                },
            errorMessage: 'client id Required'
        },
        'tnt_id': {
            notEmpty: true,
                isInt:{
                    errorMessage: 'Invalid tenent id' // Error message for the parameter
                },
            errorMessage: 'tenant id Required'
        }
    }
};

// validations for Update asrt Category in db
exports.updAsertCtgry = {

     "body": {
           'asrt_ctgry_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid assert Ctgry Id' // Error message for the parameter
            },
            errorMessage: 'assert Ctgry Id Required'
        }
    }
};

// validations for Add asrt Group in db
exports.addAsertGroup = {

     "body": {
           'asrt_grp_nm': {
            notEmpty: true,
            errorMessage: 'assert Group Name Required'
        },
        'a_in': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Indicator' // Error message for the parameter
            },
            errorMessage: 'Indicator Required'
        },
        'clnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Client Id' // Error message for the parameter
            },
            errorMessage: 'Client Id Required'
        },
        'tnt_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid Tenent Id' // Error message for the parameter
            },
            errorMessage: 'Tenent Id Required'
        }
    }
};

// validations for Update asrt Group in db
exports.updAsertGroup = {

     "body": {
           'asrt_grp_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid assert Group Id' // Error message for the parameter
            },
            errorMessage: 'assert Group Id Required'
        }
    }
};

// validations for Update asrt Group in db
exports.addSubscription = {

     "body": {
           'usr_id': {
            notEmpty: true,
            isInt:{
                errorMessage: 'Invalid User Id' // Error message for the parameter
            },
            errorMessage: 'User Id Required'
        },
        'alert_cat_id': {
            notEmpty: true,
            
            errorMessage: 'alert ctgry Id Required'
        }

    }
};
