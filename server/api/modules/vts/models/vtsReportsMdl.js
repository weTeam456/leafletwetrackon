// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');




/*****************************************************************************
* Function      : getAllZonesLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAllZonesLst = function({},callback) {
    
    var QRY_TO_EXEC = `select  * from zones_lst_t`;
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get Fenceinout Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getFnceRptLst = function(data,callback) {
    var condition="WHERE TIME_TO_SEC(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts)) / 60 > fg.cvrg_tm AND fnce_in_ts BETWEEN  '"+data.fromdt+"' and '"+data.todt+"' AND fn.clnt_id = 1 AND fn.tnt_id = 2 AND ft.fnce_type_id = 2 AND fl.a_in=1";

    if (data.vehcat!=0) {
        var condition=condition+" and alt.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and alt.asrt_grp_id in ("+data.vehgrp+")";
    }
    var QRY_TO_EXEC = "SELECT a.asrt_nm,a.asrt_ctgry_nm,a.fnce_nm,a.time_in,a.time_out,a.timdiff,c.crw_nm FROM (SELECT fn.fnce_in_out_ky,alt.asrt_id,alt.asrt_nm,ast.asrt_ctgry_nm,fl.fnce_nm,DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as yr_dt,DATE_FORMAT(fn.fnce_in_ts,'%d-%m-%Y %H:%i') AS time_in,DATE_FORMAT(fn.fnce_out_ts,'%d-%m-%Y %H:%i') AS time_out,TIME_FORMAT(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts),'%H:%i:%s') AS timdiff FROM asrt_lst_t alt LEFT JOIN fnce_in_out_dtl_t fn ON alt.asrt_id=fn.asrt_id JOIN asrt_ctgry_lst_t ast ON alt.asrt_ctgry_id = ast.asrt_ctgry_id JOIN fncs_lst_t fl ON fl.fnce_id = fn.fnce_id JOIN fncs_type_lst_t ft join fncs_grp_lst_t fg ON fl.fnce_grp_id=fg.fnce_grp_id JOIN dte_dmtn_lst_t d ON d.frmtd_dt BETWEEN DATE('"+data.fromdt+"') AND DATE('"+data.todt+"') "+condition+" ORDER BY fn.fnce_in_ts DESC) as a left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id AND DATE_FORMAT(a.yr_dt,'%Y-%m-%d') BETWEEN date('"+data.fromdt+"') AND date('"+data.todt+"') left JOIN crw_lst_t c ON v.crw_id=c.crw_id GROUP BY a.fnce_nm";
    // var QRY_TO_EXEC = "SELECT fn.fnce_in_out_ky,a.asrt_nm,v.crw_id,c.crw_nm,ast.asrt_ctgry_nm,fl.fnce_nm,DATE_FORMAT(fn.fnce_in_ts,'%d-%m-%Y %H:%i') AS time_in,DATE_FORMAT(fn.fnce_out_ts,'%d-%m-%Y %H:%i') AS time_out,TIME_FORMAT(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts),'%H:%i') AS timdiff FROM fnce_in_out_dtl_t fn JOIN asrt_lst_t a ON fn.asrt_id = a.asrt_id JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id JOIN fncs_lst_t fl ON fl.fnce_id = fn.fnce_id JOIN fncs_type_lst_t ft left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id left JOIN crw_lst_t c ON v.crw_id=c.crw_id "+condition+" ORDER BY fn.fnce_in_ts DESC";
    // console.log(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get landmarkinout Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getLndmrkRptLst = function(data,callback) {
    var condition="WHERE TIME_TO_SEC(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts)) / 60 > 3 and fnce_in_ts BETWEEN  '"+data.fromdt+"' and '"+data.todt+"' and fn.clnt_id = 1 AND fn.tnt_id = 2 AND ft.fnce_type_id = 1 AND fl.fnce_grp_id in (1,3) AND fl.a_in=1";

    if (data.vehcat!=0) {
        var condition=condition+" and alt.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and alt.asrt_grp_id in ("+data.vehgrp+")";
    }

    var QRY_TO_EXEC = "SELECT a.asrt_nm,a.asrt_ctgry_nm,a.fnce_nm,a.time_in,a.time_out,a.timdiff,c.crw_nm FROM (SELECT fn.fnce_in_out_ky,alt.asrt_id,alt.asrt_nm,ast.asrt_ctgry_nm,fl.fnce_nm,DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as yr_dt,DATE_FORMAT(fn.fnce_in_ts,'%d-%m-%Y %H:%i') AS time_in,DATE_FORMAT(fn.fnce_out_ts,'%d-%m-%Y %H:%i') AS time_out,TIME_FORMAT(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts),'%H:%i:%s') AS timdiff FROM asrt_lst_t alt LEFT JOIN fnce_in_out_dtl_t fn ON alt.asrt_id=fn.asrt_id JOIN asrt_ctgry_lst_t ast ON alt.asrt_ctgry_id = ast.asrt_ctgry_id JOIN fncs_lst_t fl ON fl.fnce_id = fn.fnce_id JOIN fncs_type_lst_t ft join fncs_grp_lst_t fg ON fl.fnce_grp_id=fg.fnce_grp_id JOIN dte_dmtn_lst_t d ON d.frmtd_dt BETWEEN DATE('"+data.fromdt+"') AND DATE('"+data.todt+"') "+condition+" ORDER BY fn.fnce_in_ts DESC) as a left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id AND DATE_FORMAT(a.yr_dt,'%Y-%m-%d') BETWEEN date('"+data.fromdt+"') AND date('"+data.todt+"') left JOIN crw_lst_t c ON v.crw_id=c.crw_id GROUP BY a.fnce_nm";
    // var QRY_TO_EXEC = "SELECT fn.fnce_in_out_ky,v.crw_id,c.crw_nm,a.asrt_nm,ast.asrt_ctgry_nm,fl.fnce_nm,DATE_FORMAT(fn.fnce_in_ts,'%d-%m-%Y %H:%i') AS time_in,DATE_FORMAT(fn.fnce_out_ts,'%d-%m-%Y %H:%i') AS time_out,TIME_FORMAT(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts),'%H:%i') AS timdiff FROM fnce_in_out_dtl_t fn JOIN asrt_lst_t a ON fn.asrt_id = a.asrt_id JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id JOIN fncs_lst_t fl ON fl.fnce_id = fn.fnce_id JOIN fncs_type_lst_t ft left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id left JOIN crw_lst_t c ON v.crw_id=c.crw_id "+condition+" ORDER BY fn.fnce_in_ts DESC";
    // console.log(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get trips Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTripRptLst = function(data,callback) {
    var condition="WHERE alt.a_in=1 AND t.a_in=1 AND actl_strt_ts is NOT NULL";

    if (data.vehcat!=0) {
        var condition=condition+" and alt.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and alt.asrt_grp_id in ("+data.vehgrp+")";
    }

    var QRY_TO_EXEC = "SELECT DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as dt,alt.asrt_id,asrt_nm,crw_nm,asrt_ctgry_nm,asrt_grp_nm,alt.trips_ct as mn_trp_ct,SUM(actl_trvl_dstne_ct) AS trp_dst,COUNT(trp_rn_id) AS trp_cnt,ROUND(sum(TIME_TO_SEC(TIMEDIFF(t.actl_end_ts,t.actl_strt_ts)) / 60)) AS tvl_tm FROM  asrt_lst_t alt JOIN asrt_sts_lst_t as ast on ast.asrt_sts_id=alt.crnt_ast_sts_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id JOIN asrt_grp_lst_t agp ON alt.asrt_grp_id = agp.asrt_grp_id JOIN dte_dmtn_lst_t d on d.frmtd_dt BETWEEN date('"+data.fromdt+"') AND date('"+data.todt+"') LEFT JOIN trp_rn_dtl_t t  ON t.asrt_id = alt.asrt_id AND actl_strt_ts BETWEEN '"+data.fromdt+"' and '"+data.todt+"' AND date(actl_strt_ts)=d.frmtd_dt LEFT JOIN drvr_asgnt_dtl_t dast on alt.asrt_id=dast.asrt_id and d.frmtd_dt between date(dast.frm_ts) and date(dast.to_ts) LEFT JOIN crw_lst_t vclt ON dast.crw_id=vclt.crw_id "+condition+" GROUP BY alt.asrt_id,DATE(actl_strt_ts) ORDER BY dt DESC";
 
    log.message(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            // console.log(results);
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getTripRptLstbyVehId
* Description   : get trips count for last week
* Arguments     : callback function
******************************************************************************/
exports.getTripRptLstbyVehId = function({veh_id},callback) {
    
    var QRY_TO_EXEC = "SELECT count(trp_rn_id) as trips_ct,DATE_FORMAT(actl_strt_ts,'%Y-%m-%d') as trp_dt,asrt_id,* FROM trp_rn_dtl_t WHERE actl_strt_ts BETWEEN CURDATE() - INTERVAL 6 day  and CURDATE() and asrt_id = '"+veh_id+"' GROUP BY trp_dt order BY trp_dt;";
 
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get lndmrk coverage Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getLndmrkCovRptLst = function(data,callback) {
    var condition="WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm and fnce_in_ts BETWEEN  '"+data.fromdt+"' and '"+data.todt+"' AND flt.a_in=1";

    if (data.vehcat!=0) {
        var condition=condition+" and a.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and a.asrt_grp_id in ("+data.vehgrp+")";
    }
    var QRY_TO_EXEC = "SELECT a.*,c.crw_nm FROM (SELECT fdt.asrt_id,a.asrt_nm,ast.asrt_ctgry_nm,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered,DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as yr_dt FROM fnce_in_out_dtl_t fdt JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id JOIN fncs_lst_t flt on fdt.fnce_id=flt.fnce_id JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id JOIN dte_dmtn_lst_t d ON d.frmtd_dt BETWEEN DATE('"+data.fromdt+"') AND DATE('"+data.todt+"') "+condition+" GROUP BY yr_dt,a.asrt_nm ORDER BY yr_dt,fdt.asrt_id) as a left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id AND DATE_FORMAT(a.yr_dt,'%Y-%m-%d') BETWEEN date('"+data.fromdt+"') AND date('"+data.todt+"') left JOIN crw_lst_t c ON v.crw_id=c.crw_id GROUP BY yr_dt,a.asrt_nm ORDER BY a.yr_dt";
    // var QRY_TO_EXEC = "SELECT fdt.asrt_id,v.crw_id,c.crw_nm,a.asrt_nm,ast.asrt_ctgry_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m-%Y') AS dt,COUNT(fnce_id) AS fnce_covered FROM fnce_in_out_dtl_t fdt JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id left JOIN crw_lst_t c ON v.crw_id=c.crw_id "+condition+" GROUP BY dt,a.asrt_nm ORDER BY dt,fdt.asrt_id";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get Distance Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getDistRptLst = function(data,callback) {  
    
    var condition="WHERE ADDDATE(adt.trvl_dt,INTERVAL adt.trvl_hr HOUR) BETWEEN '"+data.fromdt+"' and '"+data.todt+"' AND trvl_hr > 0";

    if (data.vehcat!=0) {
        var condition=condition+" and a.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and a.asrt_grp_id in ("+data.vehgrp+")";
    }
    var QRY_TO_EXEC = "SELECT a.asrt_id,a.dt,a.asrt_nm,a.asrt_ctgry_nm,a.dst,c.crw_nm FROM (SELECT DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as yr_dt,a.asrt_id,DATE_FORMAT(trvl_dt,'%Y-%m-%d') as dt,a.asrt_nm,alt.asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_lst_t a JOIN asrt_ctgry_lst_t alt ON a.asrt_ctgry_id=alt.asrt_ctgry_id JOIN dte_dmtn_lst_t d ON d.frmtd_dt BETWEEN DATE('"+data.fromdt+"') AND DATE('"+data.todt+"') LEFT JOIN asrt_dstne_dtl_t adt on a.asrt_id=adt.asrt_id "+condition+" GROUP BY trvl_dt,adt.asrt_id HAVING dst is not null ORDER BY trvl_dt desc) as a left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id AND DATE_FORMAT(a.yr_dt,'%Y-%m-%d') BETWEEN date('"+data.fromdt+"') AND date('"+data.todt+"') left JOIN crw_lst_t c ON v.crw_id=c.crw_id GROUP BY a.dt,a.asrt_id ORDER BY a.dt ";
    // var QRY_TO_EXEC = "SELECT a.asrt_id,v.crw_id,c.crw_nm,DATE_FORMAT(trvl_dt,'%Y-%m-%d') as dt,a.asrt_nm,alt.asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t a ON adt.asrt_id = a.asrt_id JOIN asrt_ctgry_lst_t alt ON a.asrt_ctgry_id=alt.asrt_ctgry_id left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id left JOIN crw_lst_t c ON v.crw_id=c.crw_id "+condition+" GROUP BY trvl_dt,adt.asrt_id HAVING dst is not null ORDER BY trvl_dt desc";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get trips Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTripDtlsRptLst = function(data,callback) {
    var condition="WHERE actl_strt_ts BETWEEN  '"+data.fromdt+"' and '"+data.todt+"' AND t.actl_end_ts IS NOT NULL ";

    if (data.vehcat!=0) {
        var condition=condition+" and a.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and asrt_grp_id in ("+data.vehgrp+")";
    }

    var QRY_TO_EXEC = "SELECT t.trp_rn_id,f1.fnce_nm,v.crw_id,c.crw_nm,a.asrt_nm,ac.asrt_ctgry_nm,DATE_FORMAT(actl_strt_ts, '%d-%m-%Y') AS strt_dt,DATE_FORMAT(t.actl_end_ts, '%d-%m-%Y') AS end_dt,t.actl_trvl_tm,round(t.actl_trvl_dstne_ct) AS dst_cov FROM trp_rn_dtl_t AS t JOIN fncs_lst_t AS f1 ON f1.fnce_id = t.strt_fnce_id JOIN fncs_lst_t AS f2 ON f2.fnce_id = t.end_fnce_id JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN rte_lst_t AS r ON r.rte_id = t.rte_id JOIN asrt_ctgry_lst_t ac ON a.asrt_ctgry_id = ac.asrt_ctgry_id left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id left JOIN crw_lst_t c ON v.crw_id=c.crw_id "+condition+" GROUP BY trp_rn_id ORDER BY t.trp_rn_id,asrt_nm";
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get trips Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getallTripDtlsRptLst = function(trip_run_id,callback) {
    
    var QRY_TO_EXEC = "SELECT t.trp_rn_id,a.asrt_nm,f.fnce_id, f.fnce_nm,r.rte_nm,DATE_FORMAT(actl_arvl_ts,'%d-%m-%Y %H:%i') AS arl_ts,DATE_FORMAT(actl_dprte_ts,'%d-%m-%Y %H:%i') AS dep_ts FROM trp_rn_dtl_t as t JOIN trp_rn_fnce_dtl_t as tf on tf.trp_rn_id = t.trp_rn_id JOIN fncs_lst_t as f on f.fnce_id = tf.fnce_id JOIN asrt_lst_t as a on a.asrt_id = t.asrt_id JOIN rte_lst_t as r on r.rte_id = t.rte_id WHERE tf.trp_rn_id = "+trip_run_id+" and (actl_arvl_ts is null OR actl_dprte_ts is null or TIMEDIFF(actl_dprte_ts,actl_arvl_ts)>= '00:03:00') ORDER BY tf.actl_arvl_ts";
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get trips Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getlndMksNtCovLst = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ROW_NUMBER() OVER (PARTITION BY null order BY fnce_id desc) as 'Sno',fnce_id,fnce_nm FROM fncs_lst_t WHERE fnce_id NOT IN (SELECT fnce_id FROM fnce_in_out_dtl_t WHERE DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()) AND fnce_grp_id=1 AND a_in=1";
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });


    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};



/*****************************************************************************
* Function      : get Mileage Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getMileageRptLst = function(data,callback) {

    var condition = "WHERE dist.trvl_dt BETWEEN '"+data.fromdt+"' and '"+data.todt+"'";

    if (data.vehcat!=0) {
        var condition=condition+" and ac.asrt_ctgry_id in ("+data.vehcat+")";
    }
    if (data.vehgrp!=0) {
         var condition=condition+" and ag.asrt_grp_id in ("+data.vehgrp+")";
    }
    
     //var QRY_TO_EXEC = "SELECT dist.asrt_id,al.asrt_nm,MAX(dist.strt_odmtr_ct) - MIN(dist.strt_odmtr_ct) as distance,DATE_FORMAT(dist.trvl_dt,'%d-%m-%Y') as trvl_dt,dist.trvl_hr,dist.strt_odmtr_ct,al.trips_ct,al.asrt_grp_id,al.mileage_ct,ac.asrt_ctgry_nm,((MAX(dist.strt_odmtr_ct) - MIN(dist.strt_odmtr_ct))/al.mileage_ct) as milege_ct FROM asrt_dstne_dtl_t as dist JOIN asrt_lst_t al ON al.asrt_id = dist.asrt_id JOIN asrt_ctgry_lst_t ac ON ac.asrt_ctgry_id = al.asrt_ctgry_id WHERE dist.trvl_dt BETWEEN '"+fromdt+"' AND '"+todt+"' AND dist.strt_odmtr_ct IS NOT NULL GROUP BY dist.asrt_id ORDER BY dist.asrt_id";
    
     var QRY_TO_EXEC = `SELECT ROW_NUMBER() OVER(PARTITION BY null) as 'SNo',dist.asrt_id,al.asrt_nm,MAX(dist.strt_odmtr_ct) - MIN(dist.strt_odmtr_ct) as distance,
                        DATE_FORMAT(dist.trvl_dt,'%d-%m-%Y') as trvl_dt,dist.trvl_hr,
                        al.trips_ct as tot_trips,
                        al.mileage_ct,ac.asrt_ctgry_nm,ag.asrt_grp_nm,
                        ((MAX(dist.strt_odmtr_ct) - MIN(dist.strt_odmtr_ct))/al.mileage_ct) as milege_ct ,
                         COUNT(t.trp_rn_id) as trips_ct
                        FROM asrt_dstne_dtl_t as dist 
                        JOIN asrt_lst_t al ON al.asrt_id = dist.asrt_id
                        JOIN trp_rn_dtl_t t ON t.asrt_id = al.asrt_id 
                        JOIN asrt_ctgry_lst_t ac ON ac.asrt_ctgry_id = al.asrt_ctgry_id
                        JOIN asrt_grp_lst_t ag ON ag.asrt_grp_id = al.asrt_grp_id 
                        `+condition+`
                        AND dist.strt_odmtr_ct IS NOT NULL GROUP BY dist.asrt_id ORDER BY dist.asrt_id`

    // console.log(QRY_TO_EXEC);
    // -- and DATE(fnce_in_ts) BETWEEN '2017-05-11' AND '2017-05-13'
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


exports.fusion_get = function(callback) {
    
    var QRY_TO_EXEC = "SELECT dt FROM fusn_ky_t ORDER BY id";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

exports.fusionKeysByDt = function(dt,callback) {
    
    var QRY_TO_EXEC = "SELECT * from fusn_ky_t WHERE dt="+dt;
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};




/*****************************************************************************
 * Function      : getHeatMapRpt
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getHeatMapRpt = function(data,callback) {
    
 var QRY_TO_EXEC = `SELECT dt,dprt_id,asrt_ctgry_id,asrt_type_id,asrt_grp_id,lat,lng,cvrge_ct 
                    FROM asrt_cvrge_dtl_t 
                    WHERE DATE(dt) 
                    BETWEEN `+data.sdate+` AND `+data.edate+` AND asrt_grp_id=`+data.asrt_grp_id+` AND asrt_ctgry_id=`+data.asrt_ctgry_id+`
                    ORDER BY cvrge_ct desc`;
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};



/*****************************************************************************
 * Function      : getDumpers
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getDumpers_status = function(data,callback) {
    console.log(data.dtFrom+','+data.dtTo +','+ data.fnce_grp_id);

    //var condition = "WHERE DATE(fnce_in_ts) BETWEEN '"+data.dtFrom+"' AND '"+data.dtTo+"' AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 > 3) as fio on fio.fnce_id = f.fnce_id LEFT OUTER JOIN fncs_master_v as v ON v.fnce_id = f.fnce_id WHERE f.fnce_ctgry_id = 1 AND f.fnce_grp_id in ("+data.fnce_grp_id+")"
    var condition = "AND fio.fnce_in_ts BETWEEN '"+data.dtFrom+"' AND '"+data.dtTo+"' AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 >fg.cvrg_tm LEFT OUTER JOIN fncs_master_v as v ON v.fnce_id = f.fnce_id"

     if (data.fnce_grp_id!=0) {
          var condition = condition + " WHERE f.a_in=1 and f.fnce_grp_id in ("+data.fnce_grp_id+")";
    }


 var QRY_TO_EXEC = `SELECT f.*
                    ,(CASE WHEN fio.fnce_id is null then 0 ELSE 1 end) as covr_status
                    ,COUNT(*) as tot_fn
                    ,COUNT(fio.fnce_id) as times_cvrd_ct
                    ,IF (v.zone_fnce_nm IS NULL,'N/A',v.zone_fnce_nm) AS zone_fnce_nm
                    ,IF (v.ward_fnce_nm IS NULL,'N/A',v.ward_fnce_nm) as ward_fnce_nm
                    ,DATE_FORMAT(fnce_in_ts,'%Y-%m-%d %h:%i') as strt_tm
                    ,DATE_FORMAT(fnce_out_ts,'%Y-%m-%d %h:%i') as end_tm
                    ,ROUND(TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60) AS stoppage
                    FROM fncs_lst_t as f 
                    JOIN fncs_grp_lst_t as fg on fg.fnce_grp_id = f.fnce_grp_id 
                    left outer JOIN fnce_in_out_dtl_t as fio on fio.fnce_id = f.fnce_id  
                    `+condition+`
                    GROUP BY f.fnce_id,fio.fnce_id`
      // console.log(QRY_TO_EXEC);
   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};




exports.fenceCate_get = function({}, callback) {

    var QRY_TO_EXEC = `SELECT * FROM fncs_ctgry_lst_t`;


    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};



exports.getCollecType = function({}, callback) {

    var QRY_TO_EXEC = `SELECT * FROM fncs_grp_lst_t where fnce_type_id = 1`;

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};




/*****************************************************************************
 * Function      : getVehRpt_post
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getVehRpt_post = function(data,callback) {
    // console.log(data);
    var QRY_TO_EXEC = "SELECT z.asrt_id,z.asrt_nm,ag.asrt_grp_nm,z.asrt_ctgry_nm,DATE_FORMAT(MIN(z.trp_dt),'%d-%m-%Y') as fm_dt, DATE_FORMAT(MAX(z.trp_dt),'%d-%m-%Y') as to_dt,MIN(z.trps_ct) AS min_trp,MAX(z.trps_ct) as max_trp, ROUND(AVG(z.trps_ct)) as avg_trp FROM(SELECT a.asrt_id,a.asrt_nm,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE(t.actl_strt_ts) as trp_dt,count(*) as trps_ct FROM trp_rn_dtl_t as t JOIN asrt_lst_t as a on a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t as acg on acg.asrt_ctgry_id = a.asrt_ctgry_id WHERE DATE(actl_strt_ts) BETWEEN '"+data.from_date+"' AND '"+data.to_date+"' and a.asrt_id='"+data.asrt_id+"' GROUP BY trp_dt,a.asrt_grp_id,a.asrt_ctgry_id ORDER BY a.asrt_grp_id,a.asrt_ctgry_id) as z JOIN asrt_grp_lst_t as ag on ag.asrt_grp_id = z.asrt_grp_id GROUP BY asrt_grp_nm,asrt_ctgry_nm";
    // console.log(QRY_TO_EXEC);

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
 * Function      : getLastData
 * Description   : 
 * Arguments     : callback function
 ******************************************************************************/
exports.getLastData = function(data,callback) {
    // console.log(data.asrt_id);
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,z.asrt_ctgry_nm,DATE_FORMAT(MIN(z.trp_dt),'%d-%m-%Y') as fm_dt, DATE_FORMAT(MAX(z.trp_dt),'%d-%m-%Y') as to_dt,MIN(z.trps_ct) as min_trp,MAX(z.trps_ct) as max_trp, ROUND(AVG(z.trps_ct)) as avg_trp FROM(SELECT acg.asrt_ctgry_nm,a.asrt_grp_id,DATE(t.actl_strt_ts) as trp_dt,count(*) as trps_ct FROM trp_rn_dtl_t as t JOIN asrt_lst_t as a on a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t as acg on acg.asrt_ctgry_id = a.asrt_ctgry_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 7 day and CURDATE() - INTERVAL 1 day and a.asrt_grp_id = (SELECT asrt_grp_id FROM asrt_lst_t WHERE asrt_id='"+data.asrt_id+"') GROUP BY trp_dt,a.asrt_grp_id,a.asrt_ctgry_id ORDER BY a.asrt_grp_id,a.asrt_ctgry_id) as z JOIN asrt_grp_lst_t as ag on ag.asrt_grp_id = z.asrt_grp_id GROUP BY asrt_grp_nm;";

    // console.log(QRY_TO_EXEC);

   if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};



/*****************************************************************************
* Function      : postCrewPrfmnceRpt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.postCrewPrfmnceRpt = function(data, callback) {
    // console.log(data);
    // var condition = "WHERE da.frm_ts >= '" + data.fromdt + "' and da.to_ts  <= '" + data.todt + "' AND td.actl_strt_ts BETWEEN '" + data.fromdt + "' and '" + data.todt + "'";

    // if (data.vehcat != 0) {
    //     var condition = condition + " and ac.asrt_ctgry_id in (" + data.vehcat + ")";
    // }
    // if (data.vehgrp != 0) {
    //     var condition = condition + " and ag.asrt_grp_id in (" + data.vehgrp + ")";
    // }
    // if (data.vehcrew != 0) {
    //     var condition = condition + " and da.crw_id in (" + data.vehcrew + ")";
    // }
    var condition="";
     if (data.vehgrp!=0) {
         var condition="WHERE a.asrt_grp_id in ("+data.vehgrp+")";
    }
    
    if (data.vehgrp==0 && data.vehcat!=0) {
         var condition=" WHERE a.asrt_ctgry_id in ("+data.vehcat+")";
          }
         else if(data.vehcat!=0){
            var condition=condition + " and a.asrt_ctgry_id in ("+data.vehcat+")";
         }
   
         if (data.vehgrp==0 && data.vehcat==0 && data.vehcrew != 0) {
            var condition=" WHERE a.crw_id in (" + data.vehcrew + ")";
         }
        else if (data.vehcrew != 0) {
        var condition = condition + " and a.crw_id in (" + data.vehcrew + ")";
    }

    // if (data.vehcat!=0) {
    //     var condition="a.asrt_ctgry_id in ("+data.vehcat+")";
    // }
    // if (data.vehgrp!=0) {
    //      var condition=condition+" and a.asrt_grp_id in ("+data.vehgrp+")";
    // }
    // if (data.vehcrew != 0) {
    //     var condition = condition + " and a.crw_id in (" + data.vehcrew + ")";
    // }
     // var QRY_TO_EXEC = `SELECT count(fi.fnce_id) as fnce_ct,count(DISTINCT td.trp_rn_id) as trips_ct,cl.crw_nm,cl.crw_ph,al.asrt_nm,ag.asrt_grp_nm,ac.asrt_ctgry_nm,da.* 
     //                    FROM drvr_asgnt_dtl_t as da
     //                    JOIN crw_lst_t as cl on cl.crw_id = da.crw_id
     //                    JOIN trp_rn_dtl_t as td on td.asrt_id = da.asrt_id
     //                    JOIN asrt_lst_t as al on al.asrt_id = da.asrt_id
     //                    JOIN asrt_grp_lst_t as ag on ag.asrt_grp_id = al.asrt_grp_id
     //                    JOIN asrt_ctgry_lst_t as ac on ac.asrt_ctgry_id = al.asrt_ctgry_id
     //                    left JOIN fnce_in_out_dtl_t as fi on td.trp_rn_id = fi.trp_rn_id 
     //                    ` + condition + `
     //                    and td.a_in=1 and da.clse_in = 0`

     var QRY_TO_EXEC = "SELECT yr_dt,a.asrt_id,a.asrt_nm,a.asrt_ctgry_nm,b.asrt_grp_nm,a.dst,b.trps_cnt,c.fncs,a.fuel_ct,a.ful_ct,a.crw_id,a.crw_nm,a.crw_ph FROM (SELECT DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as yr_dt,a.asrt_id,a.asrt_nm,alt.asrt_ctgry_nm,a.asrt_grp_id,a.asrt_ctgry_id,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst,fuel_ct,v.crw_id,crw_nm,crw_ph,CASE WHEN fldt.fuel_ct IS NULL THEN 0 ELSE fldt.fuel_ct END AS ful_ct FROM asrt_lst_t a JOIN asrt_ctgry_lst_t alt ON a.asrt_ctgry_id=alt.asrt_ctgry_id JOIN dte_dmtn_lst_t d ON d.frmtd_dt BETWEEN DATE('" + data.fromdt + "') AND DATE('" + data.todt + "') LEFT JOIN asrt_dstne_dtl_t adt on a.asrt_id=adt.asrt_id left JOIN drvr_asgnt_dtl_t v ON a.asrt_id=v.asrt_id AND DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') BETWEEN date('" + data.fromdt + "') AND date('" + data.todt + "') AND v.clse_in=0 left outer JOIN fuel_dtl_t fldt ON a.asrt_id=fldt.asrt_id AND DATE_FORMAT(fldt.isse_dt,'%Y-%m-%d') BETWEEN date('" + data.fromdt + "') AND date('" + data.todt + "') AND fldt.cls_in=0 left JOIN crw_lst_t c ON v.crw_id=c.crw_id WHERE ADDDATE(adt.trvl_dt,INTERVAL adt.trvl_hr HOUR) BETWEEN '" + data.fromdt + "' and '" + data.todt + "' AND trvl_hr > 0 GROUP BY adt.asrt_id HAVING dst is not null ORDER BY adt.asrt_id) as a join (SELECT alt.asrt_id,asrt_nm,asrt_ctgry_nm,asrt_grp_nm,COUNT(trp_rn_id) AS trps_cnt,DATE(actl_strt_ts) as actl_dt FROM  asrt_lst_t alt JOIN asrt_sts_lst_t as ast on ast.asrt_sts_id=alt.crnt_ast_sts_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id JOIN asrt_grp_lst_t agp ON alt.asrt_grp_id = agp.asrt_grp_id JOIN dte_dmtn_lst_t d on d.frmtd_dt BETWEEN date('" + data.fromdt + "') AND date('" + data.todt + "') LEFT JOIN trp_rn_dtl_t t  ON t.asrt_id = alt.asrt_id AND actl_strt_ts BETWEEN '" + data.fromdt + "' and '" + data.todt + "' AND date(actl_strt_ts)=d.frmtd_dt WHERE alt.a_in=1 AND t.a_in=1 AND actl_strt_ts is NOT NULL GROUP BY alt.asrt_id ORDER BY dt DESC) as b on a.asrt_id=b.asrt_id JOIN (SELECT asrt_id,COUNT(fdt.fnce_id) as fncs FROM fnce_in_out_dtl_t fdt JOIN fncs_lst_t flt on fdt.fnce_id=flt.fnce_id JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm AND fnce_in_ts BETWEEN '" + data.fromdt + "' AND '" + data.todt + "' AND flt.a_in=1 GROUP BY asrt_id) as c ON a.asrt_id=c.asrt_id "+condition+" GROUP BY a.asrt_id ORDER BY a.asrt_id";
    // console.log(QRY_TO_EXEC);

    if (callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls, function(err, results) {
            callback(err, results);
            return;
        });
    else
        return dbutil.execQuery(sqldb.MySQLConPool, QRY_TO_EXEC, cntxtDtls);
};

/*****************************************************************************
* Function      : getWrdCovgDtlsRpt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getWrdCovgDtlsRpt = function(data,callback) {
     var QRY_TO_EXEC = `SELECT A.*,B.* FROM 
                        (SELECT s.*,sum(s.fnce_covered) as tot_fnc_cvr,MIN(s.fnce_covered) as min_cvr ,MAX(s.fnce_covered) as max_cvr 
                        FROM (SELECT du.fncovd_dt,f.zone_fnce_nm as zn_nm,f.ward_fnce_nm as wr_fn_nm,f.fnce_grp_nm as colpn_nm,COUNT(DISTINCT fio.fnce_id) as fnce_covered 
                        from fncs_master_v as f JOIN (SELECT DISTINCT(DATE(fnce_in_ts)) as fncovd_dt 
                        FROM fnce_in_out_dtl_t 
                        WHERE  DATE(fnce_in_ts) 
                        BETWEEN CURDATE() - INTERVAL 6 day AND CURDATE()) as du 
                        left outer join (SELECT x.fnce_id,DATE(x.fnce_in_ts) as fns_dt 
                        FROM fnce_in_out_dtl_t as x 
                        JOIN fncs_lst_t flt on x.fnce_id=flt.fnce_id
                        JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id 
                        JOIN asrt_lst_t alt on x.asrt_id=alt.asrt_id
                        WHERE DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 day AND CURDATE()
                        and TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 > fg.cvrg_tm AND flt.fnce_grp_id in (1,3) AND flt.fnce_type_id=1 AND asrt_grp_id in (8,14) AND asrt_ctgry_id in (1,4,5) AND flt.a_in=1
                        GROUP BY DATE(fnce_in_ts),x.fnce_id ORDER BY fns_dt) as fio on fio.fnce_id = f.fnce_id and fio.fns_dt = du.fncovd_dt 
                        GROUP BY f.ward_fnce_nm,du.fncovd_dt,f.fnce_grp_nm ORDER BY f.ward_fnce_nm,du.fncovd_dt,f.fnce_grp_nm) as s 
                        GROUP BY wr_fn_nm,colpn_nm ORDER BY s.fncovd_dt,wr_fn_nm) as A 
                        left join(SELECT du.fdate,f.fnce_id,f.zone_fnce_nm,f.ward_fnce_nm,f.fnce_grp_nm,COUNT(f.fnce_id) AS tot_fncs,COUNT(DISTINCT fio.fnce_id) AS fns_crd 
                        FROM fncs_master_v AS f 
                        JOIN (SELECT DISTINCT (DATE(fnce_in_ts)) AS fdate 
                        FROM fnce_in_out_dtl_t 
                        WHERE fnce_in_ts BETWEEN '${data.fromdt}' and '${data.todt}') AS du 
                        LEFT OUTER JOIN 
                        (SELECT x.fnce_id,DATE(x.fnce_in_ts) AS fns_dt 
                        FROM fnce_in_out_dtl_t AS x 
                        JOIN fncs_lst_t flt on x.fnce_id=flt.fnce_id
                        JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id
                        JOIN asrt_lst_t alt on x.asrt_id=alt.asrt_id
                        WHERE fnce_in_ts BETWEEN '${data.fromdt}' and '${data.todt}'
                        AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm
                        AND flt.fnce_grp_id in (1,3) AND flt.fnce_type_id=1 AND asrt_grp_id in (8,14) AND asrt_ctgry_id in (1,4,5) AND flt.a_in=1
                        GROUP BY DATE(fnce_in_ts),x.fnce_id ORDER BY fns_dt) AS fio ON fio.fnce_id = f.fnce_id AND fio.fns_dt = du.fdate 
                        GROUP BY f.ward_fnce_nm,du.fdate,f.fnce_grp_nm ORDER BY f.ward_fnce_nm,du.fdate,f.fnce_grp_nm) as B 
                        on B.zone_fnce_nm=A.zn_nm  and B.ward_fnce_nm=A.wr_fn_nm and B.fnce_grp_nm=A.colpn_nm GROUP BY B.fnce_id`;

    // var QRY_TO_EXEC = "SELECT A.*,B.* FROM (SELECT s.*,sum(s.fnce_covered) as tot_fnc_cvr,MIN(s.fnce_covered) as min_cvr ,MAX(s.fnce_covered) as max_cvr FROM (SELECT du.fncovd_dt,f.zone_fnce_nm as zn_nm,f.ward_fnce_nm as wr_fn_nm,f.fnce_grp_nm as colpn_nm,COUNT(fio.fnce_id) as fnce_covered from fncs_master_v as f JOIN (SELECT DISTINCT(DATE(fnce_in_ts)) as fncovd_dt FROM fnce_in_out_dtl_t WHERE  DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 5 day AND CURDATE()) as du left outer join (SELECT x.fnce_id,DATE(x.fnce_in_ts) as fns_dt FROM fnce_in_out_dtl_t as x WHERE DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 5 day AND CURDATE() and TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 > 3 GROUP BY DATE(fnce_in_ts),fnce_id ORDER BY fns_dt) as fio on fio.fnce_id = f.fnce_id and fio.fns_dt = du.fncovd_dt GROUP BY f.ward_fnce_nm,du.fncovd_dt,f.fnce_grp_nm ORDER BY f.ward_fnce_nm,du.fncovd_dt,f.fnce_grp_nm) as s GROUP BY wr_fn_nm,colpn_nm ORDER BY wr_fn_nm,s.fncovd_dt,colpn_nm  ) as A left join(SELECT du.fdate,f.zone_fnce_nm,f.ward_fnce_nm,f.fnce_grp_nm,COUNT(f.fnce_id) AS tot_fncs,COUNT(fio.fnce_id) AS fns_crd FROM fncs_master_v AS f JOIN (SELECT DISTINCT (DATE(fnce_in_ts)) AS fdate FROM fnce_in_out_dtl_t WHERE fnce_in_ts BETWEEN '"+data.fromdt+"' AND '"+data.todt+"') AS du LEFT OUTER JOIN (SELECT x.fnce_id,DATE(x.fnce_in_ts) AS fns_dt FROM fnce_in_out_dtl_t AS x WHERE fnce_in_ts BETWEEN '"+data.fromdt+"' AND '"+data.todt+"' AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > 3 GROUP BY DATE(fnce_in_ts),fnce_id ORDER BY fns_dt) AS fio ON fio.fnce_id = f.fnce_id AND fio.fns_dt = du.fdate GROUP BY f.ward_fnce_nm,du.fdate,f.fnce_grp_nm ORDER BY f.ward_fnce_nm,du.fdate,f.fnce_grp_nm) as B on B.zone_fnce_nm=A.zn_nm  and B.ward_fnce_nm=A.wr_fn_nm and B.fnce_grp_nm=A.colpn_nm GROUP BY ward_fnce_nm";
    log.message(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            // console.log(results);
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotlVehCnt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotlVehCnt = function(callback) {
    var QRY_TO_EXEC = "SELECT COUNT(DISTINCT asrt_id) as tot_vhs FROM fnce_in_out_dtl_t WHERE fnce_id=278";
     
    log.message(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            // console.log(results);
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehCntErly
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehCntErly = function(callback) {
    var QRY_TO_EXEC = "SELECT COUNT(DISTINCT asrt_id) as ely_cnt FROM fnce_in_out_dtl_t WHERE DATE(fnce_out_ts)=CURDATE() AND TIME(fnce_out_ts)<='06:00:00' AND DATE(fnce_in_ts)<> CURDATE() AND fnce_id=278";
     
    // log.message(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            // console.log(results);
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehCntMidl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehCntMidl = function(callback) {
    var QRY_TO_EXEC = "SELECT COUNT(DISTINCT asrt_id) as mid_cnt FROM fnce_in_out_dtl_t WHERE DATE(fnce_out_ts)=CURDATE() AND TIME(fnce_out_ts) BETWEEN '06:01:00' AND '07:00:00' AND DATE(fnce_in_ts)<> CURDATE() AND fnce_id=278";
     
    log.message(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            // console.log(results);
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehCntDly
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehCntDly = function(callback) {
    var QRY_TO_EXEC = "SELECT COUNT(DISTINCT asrt_id) as dly_cnt FROM fnce_in_out_dtl_t WHERE DATE(fnce_out_ts)=CURDATE() AND TIME(fnce_out_ts) >= '07:01:00' AND DATE(fnce_in_ts)<> CURDATE() AND fnce_id=278";
     
    log.message(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            // console.log(results);
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get Distance Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehdtlRpt = function(data,callback) {  
   
    var QRY_TO_EXEC = "SELECT a.*, b.* FROM (SELECT DATE_FORMAT(actl_strt_ts,'%y-%m-%d') as dt,t.asrt_id,asrt_nm,asrt_ctgry_nm,SUM(actl_trvl_dstne_ct) AS trp_dst,COUNT(trp_rn_id) AS trp_cnt,(count(trp_rn_id) * capacity_ct) AS cpty_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt ON t.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id WHERE actl_strt_ts BETWEEN '"+data.fromdt+"' AND '"+data.todt+"' AND t.asrt_id = "+data.vehid+" GROUP BY DATE(actl_strt_ts)ORDER BY dt) AS a JOIN (SELECT DATE_FORMAT(fnce_in_ts,'%y-%m-%d') as fnc_dt,asrt_id,COUNT(fdt.fnce_id) AS fnc_cvd FROM fnce_in_out_dtl_t fdt JOIN fncs_lst_t flt on fdt.fnce_id=flt.fnce_id WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > 3 AND fnce_in_ts BETWEEN '"+data.fromdt+"' AND '"+data.todt+"' AND asrt_id = "+data.vehid+" AND fnce_grp_id in (1,3) AND flt.a_in=1 GROUP BY DATE(fnce_in_ts) ORDER BY fnc_dt) AS b  on b.fnc_dt=a.dt";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get each trip Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchTripDtl = function(vehid,date,callback) {  
   
    var QRY_TO_EXEC =`SELECT a.*,b.* FROM 
(SELECT ROW_NUMBER() OVER (PARTITION BY null order BY t.trp_rn_id) as 'trp_no',t.asrt_id,asrt_nm,trp_rn_id,DATE_FORMAT(actl_strt_ts,'%h:%i') as strt_tm,
DATE_FORMAT(actl_end_ts,'%h:%i') as end_tm,actl_trvl_dstne_ct,strt_fnce_id,end_fnce_id,flt1.fnce_nm as strt_fnce_nm,flt2.fnce_nm as end_fnce_nm  
FROM trp_rn_dtl_t t 
JOIN asrt_lst_t alt ON t.asrt_id=alt.asrt_id
JOIN fncs_lst_t flt1 on t.strt_fnce_id=flt1.fnce_id 
JOIN fncs_lst_t flt2 on t.end_fnce_id=flt2.fnce_id 
WHERE t.asrt_id=${vehid}
AND DATE(actl_strt_ts)='${date}' and alt.a_in=1 AND t.a_in=1 AND actl_strt_ts is NOT NULL AND actl_end_ts is NOT NULL AND flt1.a_in=1 AND flt2.a_in=1) as a 
LEFT JOIN 
(SELECT fnc.asrt_id,fnc.fnce_id,trp_rn_id,COUNT(fnc.fnce_id) as fnc_cnt 
FROM fnce_in_out_dtl_t fnc 
JOIN fncs_lst_t flt ON fnc.fnce_id=flt.fnce_id
JOIN fncs_grp_lst_t fg ON flt.fnce_grp_id=fg.fnce_grp_id 
WHERE DATE(fnce_in_ts)='${date}'
AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm 
AND flt.fnce_grp_id in (1,3) AND flt.a_in=1
AND asrt_id=${vehid}
GROUP BY trp_rn_id) as b ON a.trp_rn_id=b.trp_rn_id ORDER BY a.trp_no`;
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : get ech trip fnces Lst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchTripFncDtl = function(trp_rn_id,callback) {  
   
    var QRY_TO_EXEC = "SELECT fdt.asrt_id,asrt_nm,asrt_ctgry_nm,DATE_FORMAT(fnce_in_ts,'%h:%i') as fnc_in,DATE_FORMAT(fnce_out_ts,'%h:%i') as fnc_ut,trp_rn_id,flt.fnce_nm FROM fnce_in_out_dtl_t fdt JOIN fncs_lst_t flt ON fdt.fnce_id = flt.fnce_id JOIN fncs_grp_lst_t fg ON flt.fnce_grp_id = fg.fnce_grp_id JOIN asrt_lst_t alt ON fdt.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE (TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > (fg.cvrg_tm)) AND flt.fnce_grp_id IN (1, 3) AND trp_rn_id = "+trp_rn_id+" AND flt.a_in=1 ORDER BY fnce_in_ts";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};



/*****************************************************************************
* Function      : getAllFncsForTripDtl
* Description   : Get All fences for a trip based on trp_rn_id
* Arguments     : callback function
* History
*  06/28/2017     Sunil Mulagada     Initial Code
******************************************************************************/
exports.getAllFncsForTripDtl = function(trp_rn_id,callback) {  
   
    var QRY_TO_EXEC = `SELECT fdt.asrt_id,asrt_nm,asrt_ctgry_nm,DATE_FORMAT(fnce_in_ts,'%Y-%m-%d %h:%i:%s') as fnc_in
                            ,DATE_FORMAT(fnce_out_ts,'%Y-%m-%d %h:%i:%s') as fnc_ut,trp_rn_id,flt.fnce_nm,flt.fnce_id,flt.lat,flt.lng
                            FROM fnce_in_out_dtl_t fdt 
                            JOIN fncs_lst_t flt ON fdt.fnce_id = flt.fnce_id 
                            JOIN fncs_grp_lst_t fg ON flt.fnce_grp_id = fg.fnce_grp_id 
                            JOIN asrt_lst_t alt ON fdt.asrt_id=alt.asrt_id 
                            JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id 
                            WHERE flt.fnce_grp_id IN (1, 3) 
                            AND trp_rn_id = ${trp_rn_id}
                            ORDER BY fnce_in_ts`;
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get hourly fences covered
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethrlyFncsCovd = function(data,callback) {  
    
    if (data.vehgrp==0) {
        var condition=`alt.asrt_grp_id in (8)`;}
         else{
         condition=`alt.asrt_grp_id in (${data.vehgrp})`;
    }
    if (data.vehctgryid==0) {
        var condition=condition + " and alt.asrt_ctgry_id in (4)";}
        else{
        condition=condition + " and alt.asrt_ctgry_id in ("+data.vehctgryid+")";
    }

      var QRY_TO_EXEC = `SELECT day_nm, DATE_FORMAT(fdt.fnce_in_ts, '%d-%m-%Y') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered,HOUR(fnce_in_ts) as hr_ts,fnce_in_ts,fnce_out_ts,day_nm
                         FROM fnce_in_out_dtl_t fdt
                         JOIN asrt_lst_t alt ON fdt.asrt_id=alt.asrt_id 
                         JOIN fncs_lst_t flt on fdt.fnce_id=flt.fnce_id
                         JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id
                         LEFT JOIN dte_dmtn_lst_t dte ON dte.frmtd_dt = DATE(fnce_in_ts)
                         WHERE ${condition} and DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE()
                         AND TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts))/60 > fg.cvrg_tm AND flt.fnce_grp_id in (1,3) AND flt.a_in=1
                         GROUP BY dt,HOUR(fnce_in_ts) ORDER BY dt DESC,HOUR(fnce_in_ts)`;
    console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};