// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');




/*****************************************************************************
* Function      : get asrtlist
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAsrtLst = function({},callback) {
    
    var QRY_TO_EXEC = `select ac.asrt_ctgry_nm,ag.asrt_grp_nm,dl.dvce_nm,a.* 
                        from asrt_lst_t as a
                        JOIN asrt_ctgry_lst_t as ac on ac.asrt_ctgry_id=a.asrt_ctgry_id
                        JOIN asrt_grp_lst_t as ag on ag.asrt_grp_id = a.asrt_grp_id
                        JOIN dvcs_lst_t as dl on dl.dvce_id = a.crnt_dvce_id
                        where a.a_in = 1 and a.d_in <> 1 order by a.asrt_id`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get asrtlist
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAsrtTypeLst = function({},callback) {
    
    var QRY_TO_EXEC = `SELECT * FROM asrt_type_lst_t WHERE a_in = 1 and d_in <> 1`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get asrtlist
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.typeOfVendors = function({},callback) {
    
    var QRY_TO_EXEC = `select ac.asrt_ctgry_nm,ag.asrt_grp_nm,dl.dvce_nm,a.* 
                        from asrt_lst_t as a
                        JOIN asrt_ctgry_lst_t as ac on ac.asrt_ctgry_id=a.asrt_ctgry_id
                        JOIN asrt_grp_lst_t as ag on ag.asrt_grp_id = a.asrt_grp_id
                        JOIN dvcs_lst_t as dl on dl.dvce_id = a.crnt_dvce_id
                        where a.a_in = 1 and a.d_in <> 1 and a.asrt_grp_id = 14 order by a.asrt_id desc;`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : add Asserts Into Db
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.addAssertsIntoDb = function(data,callback) {
    
    var QRY_TO_EXEC = "INSERT INTO asrt_lst_t(asrt_nm,asrt_ctgry_id,asrt_ownr_id,asrt_type_id,asrt_grp_id,dprt_id,crnt_dvce_id,crnt_ast_sts_id,a_in,i_ts,clnt_id,tnt_id) VALUES('"+data.asrt_nm+"','"+data.asrt_ctgry_id+"','"+data.asrt_ownr_id+"','"+data.asrt_type_id+"','"+data.asrt_grp_id+"','"+data.dprt_id+"','"+data.crnt_dvce_id+"','"+data.crnt_ast_sts_id+"','"+data.a_in+"',CURRENT_TIMESTAMP(),'"+data.clnt_id+"','"+data.tnt_id+"')";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : update Asserts In table
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updAssertsIntable = function(data,asrt_id,callback) {

    var QRY_TO_EXEC = "UPDATE asrt_lst_t set asrt_nm='"+data.asrt_nm+"',asrt_ctgry_id='"+data.asrt_ctgry_id+"' ,asrt_ownr_id='"+data.asrt_ownr_id+"' ,asrt_type_id='"+data.asrt_type_id+"' ,asrt_grp_id='"+data.asrt_grp_id+"' ,dprt_id='"+data.dprt_id+"' ,crnt_dvce_id='"+data.crnt_dvce_id+"' ,crnt_ast_sts_id='"+data.crnt_ast_sts_id+"' ,a_in='"+data.a_in+"' ,u_ts=CURRENT_TIMESTAMP() ,clnt_id='"+data.clnt_id+"' ,tnt_id='"+data.tnt_id+"' WHERE asrt_id='"+asrt_id+"' ";
    // console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get Asert List By Id
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.GetAsrtLstById = function(condition,callback) {
    
    var QRY_TO_EXEC = "SELECT * FROM asrt_lst_t where a_in=1 "+condition+" and d_in <> 1";
    
    // console.log(QRY_TO_EXEC);	
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get asrt Category List
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.asrtCtgryLst = function({},callback) {
    
    var QRY_TO_EXEC = `SELECT * FROM asrt_ctgry_lst_t where d_in <> 1 and a_in = 1`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get asert Group List
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.asrtGroupLst = function({},callback) {
    
    var QRY_TO_EXEC = "SELECT * FROM asrt_grp_lst_t WHERE a_in=1 and d_in <> 1";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get device List
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.deviceLst = function({},callback) {
    
    var QRY_TO_EXEC = `SELECT * FROM dvcs_lst_t`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get tenant List
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.tenantLst = function({},callback) {
    
    var QRY_TO_EXEC = `SELECT * FROM tnt_lst_t`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get client List
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.clientLst = function({},callback) {
    
    var QRY_TO_EXEC = `SELECT * FROM clnt_dtl_t`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get owners List
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.ownersLst = function({},callback) {
    
    var QRY_TO_EXEC = `SELECT * FROM asrt_ownr_lst_t`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : get addAsrtType
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.addAsrtType = function(data,callback) {
    
    var QRY_TO_EXEC = "INSERT INTO asrt_type_lst_t(asrt_type_nm,a_in,clnt_id,tnt_id,i_ts) VALUES('"+data.asrt_type_nm+"',1,'"+data.clnt_id+"','"+data.tnt_id+"',CURRENT_TIMESTAMP())";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : updAsrtType
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updAsrtType = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE asrt_type_lst_t SET asrt_type_nm='"+data.asrt_type_nm+"',clnt_id='"+data.clnt_id+"',tnt_id='"+data.tnt_id+"',u_ts=CURRENT_TIMESTAMP() where asrt_type_id = '"+data.asrt_type_id+"'";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : addDepartment
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.addDepartment = function(data,callback) {
    
    var QRY_TO_EXEC = "INSERT INTO dprts_lst_t(dprt_nm,a_in,i_ts,clnt_id,tnt_id) VALUES('"+data.dprt_nm+"',1,CURRENT_TIMESTAMP(),'"+data.clnt_id+"','"+data.tnt_id+"')";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : updDepartment
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updDepartment = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE dprts_lst_t set dprt_nm='"+data.dprt_nm+"',clnt_id='"+data.clnt_id+"',tnt_id='"+data.tnt_id+"',u_ts=CURRENT_TIMESTAMP() where dprt_id='"+data.dprt_id+"'";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : deptmntsLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.deptmntsLst = function(callback) {
    
    var QRY_TO_EXEC = "SELECT * FROM dprts_lst_t where a_in=1 and d_in <> 1";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : deleteAsrts
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.deleteAsrts = function(data,callback) {
    
    var QRY_TO_EXEC = "update asrt_lst_t set d_in=1,d_ts = current_timestamp() where asrt_id = '"+data.asrt_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : delAsrtType
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.delAsrtType = function(data,callback) {
    
    var QRY_TO_EXEC = "update asrt_type_lst_t set d_in=1,d_ts=current_timestamp() where asrt_type_id = '"+data.asrt_type_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : delDepartment
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.delDepartment = function(data,callback) {
    
    var QRY_TO_EXEC = "update dprts_lst_t set d_in=1,d_ts=current_timestamp() where dprt_id = '"+data.dprt_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : AddAsrtCtgry
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.AddAsrtCtgry = function(data,callback) {
    
    var QRY_TO_EXEC = "INSERT INTO asrt_ctgry_lst_t(asrt_ctgry_nm,a_in,i_ts,clnt_id,tnt_id) VALUES('"+data.asrt_ctgry_nm+"',1,CURRENT_TIMESTAMP(),'"+data.clnt_id+"','"+data.tnt_id+"'); ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : UpdAsrtCtgry
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.UpdAsrtCtgry = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE asrt_ctgry_lst_t SET asrt_ctgry_nm = '"+data.asrt_ctgry_nm+"',a_in=1,u_ts=CURRENT_TIMESTAMP(),clnt_id='"+data.clnt_id+"',tnt_id='"+data.tnt_id+"' WHERE asrt_ctgry_id='"+data.asrt_ctgry_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : delAsrtCtgry
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.delAsrtCtgry = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE asrt_ctgry_lst_t SET d_in=1,d_ts=CURRENT_TIMESTAMP() WHERE asrt_ctgry_id='"+data.asrt_ctgry_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : addGroup
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.addGroup = function(data,callback) {
    
    var QRY_TO_EXEC = "INSERT INTO asrt_grp_lst_t(asrt_grp_nm,a_in,clnt_id,tnt_id,i_ts) VALUES('"+data.asrt_grp_nm+"','"+data.a_in+"','"+data.clnt_id+"','"+data.tnt_id+"',CURRENT_TIMESTAMP())";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : addGroup
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updGroup = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE asrt_grp_lst_t SET asrt_grp_nm ='"+data.asrt_grp_nm+"' ,clnt_id='"+data.clnt_id+"',tnt_id='"+data.tnt_id+"',u_ts=CURRENT_TIMESTAMP() WHERE asrt_grp_id='"+data.asrt_grp_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : delGroup
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.delGroup = function(data,callback) {
    
    var QRY_TO_EXEC = "UPDATE asrt_grp_lst_t SET d_in=1,d_ts=CURRENT_TIMESTAMP() WHERE asrt_grp_id='"+data.asrt_grp_id+"' ";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};