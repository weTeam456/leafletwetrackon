// Standard Inclusions
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var sqldb       = require(appRoot+'/config/db.config');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);

var dbutil      = require(appRoot+'/utils/db.utils');




/*****************************************************************************
* Function      : getLndMrkDtlsLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getLndMrkDtlsLst = function({},callback) {
    
    var QRY_TO_EXEC = "SELECT DATE_FORMAT(f1.fnce_in_ts, '%Y-%m-%d') AS fdate,(SELECT COUNT(*) FROM fncs_lst_t where fnce_grp_id in (1,3) AND a_in=1) AS totalfncs,((SELECT COUNT(*) FROM fncs_lst_t where fnce_grp_id in (1,3) AND a_in=1) - (COUNT(DISTINCT f1.fnce_id))) AS fnce_uncovered,COUNT(DISTINCT f1.fnce_id) AS fnce_covered FROM fnce_in_out_dtl_t AS f1 WHERE (DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE()) AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > 3 GROUP BY DATE(fnce_in_ts) ORDER BY fnce_in_ts,fnce_id";
 
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getLndMrkDtlsLstbyVehId
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getLndMrkDtlsLstbyVehId = function({veh_id},callback) {
    
    var QRY_TO_EXEC = "SELECT fio.asrt_id,DATE_FORMAT(fnce_in_ts,'%Y-%m-%d') as fdate,COUNT(DISTINCT fnce_id) as fnce_covered FROM fnce_in_out_dtl_t as fio WHERE asrt_id='"+veh_id+"' and (DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 day and CURDATE()) and TIME_TO_SEC(TIMEDIFF(fnce_out_ts,fnce_in_ts))/60 >3 GROUP BY fdate ORDER BY fdate;";
 
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getDywseDstnsCntDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getDywseDstnsCntDtl = function({},callback) {
    
    //var QRY_TO_EXEC = "SELECT a.asrt_nm,v1.*,v2.* FROM asrt_lst_t as a JOIN (SELECT asrt_id,trvl_dt,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) as dust_ct FROM asrt_dstne_dtl_t WHERE trvl_dt = CURDATE() GROUP BY asrt_id) as v1 on v1.asrt_id = a.asrt_id JOIN (SELECT asrt_id,trvl_dt,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) as dust_ct FROM asrt_dstne_dtl_t WHERE trvl_dt = CURDATE() - INTERVAL 1 day GROUP BY asrt_id) as v2 on v2.asrt_id = a.asrt_id ORDER BY a.asrt_id";

    var QRY_TO_EXEC = "SELECT alt.asrt_ctgry_nm, a.asrt_nm,v1.asrt_id as asrt1,v1.dt as trvl_dt1,v1.dist_ct as dist_ct1,v1.asrt_id as asrt2,v2.ydt as trvl_dt2,v2.dist_ct as dist_ct2 FROM asrt_lst_t as a JOIN (SELECT asrt_id,DATE_FORMAT(trvl_dt, '%Y-%m-%d' )as dt,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) as dist_ct   FROM asrt_dstne_dtl_t   WHERE trvl_dt = CURDATE()  GROUP BY asrt_id) as v1 on v1.asrt_id = a.asrt_id JOIN (SELECT asrt_id,DATE_FORMAT(trvl_dt, '%Y-%m-%d' )as ydt,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) as dist_ct   FROM asrt_dstne_dtl_t  WHERE trvl_dt = CURDATE() - INTERVAL 1 day  GROUP BY asrt_id) as v2 on v2.asrt_id = a.asrt_id JOIN asrt_ctgry_lst_t alt ON a.asrt_ctgry_id = alt.asrt_ctgry_id group by alt.asrt_ctgry_id ORDER BY a.asrt_id;"
        log.message("INFO",QRY_TO_EXEC,100,QRY_TO_EXEC);
        
        if(callback && typeof callback == "function") 
        	dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            	(err,results);
                return;
            });
        else 
    	   return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getDywseDstnsCntDtlbyVehId
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getDywseDstnsCntDtlbyVehId = function({veh_id},callback) {
    
    var QRY_TO_EXEC = "SELECT DATE_FORMAT(trvl_dt,'%Y-%m-%d') as trvl_dt,MAX(strt_odmtr_ct) as max_dist,MIN(strt_odmtr_ct) as min_dist, (MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) as tvl_dist,asrt_id FROM asrt_dstne_dtl_t as d WHERE trvl_dt  BETWEEN CURDATE() - INTERVAL 6 day  and CURDATE() and asrt_id='"+veh_id+"' GROUP BY trvl_dt ORDER BY trvl_dt;"
        log.message("INFO",QRY_TO_EXEC,100,QRY_TO_EXEC);
        
        if(callback && typeof callback == "function") 
            dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                (err,results);
                return;
            });
        else 
           return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getVehStsCntDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getVehStsCntDtl = function({},callback) {
   var QRY_TO_EXEC = `SELECT a.dt,COUNT(DISTINCT asrt_id) AS veh_cnt,
sum(CASE WHEN TIME(fnce_out_ts) > '06:30:00' THEN 1 ELSE 0 END) AS Delay,
sum(CASE WHEN TIME(fnce_out_ts) < '06:30:00' THEN 1 ELSE 0 END) AS Early 
FROM (SELECT asrt_id,DATE_FORMAT(fnce_out_ts,'%m-%d') as dt,MIN(fnce_out_ts) as fnce_out_ts
   FROM fnce_in_out_dtl_t
   WHERE  DATE(fnce_out_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND fnce_id = 278 
   GROUP BY dt,asrt_id 
   ORDER BY dt,asrt_id) as a
GROUP BY dt 
ORDER BY dt`;
    // var QRY_TO_EXEC = "SELECT DATE_FORMAT(fnce_in_ts,'%Y-%m-%d') AS dt,COUNT(DISTINCT fnce_id) AS fn_ct,CASE WHEN TIME(fnce_in_ts) > '07:00' THEN 'delay' END AS Delay,CASE WHEN TIME(fnce_in_ts) < '07:00' THEN 'early' END AS Early FROM fnce_in_out_dtl_t f WHERE fnce_out_ts IS NOT NULL AND i_ts BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() GROUP BY Early,dt ORDER BY dt";
 
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getVehStsCntDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getVehCntDtl = function({},callback) {
   var QRY_TO_EXEC = "SELECT COUNT(asrt_nm) AS veh_cnt FROM asrt_lst_t WHERE asrt_grp_id=8 ORDER BY asrt_id";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getShVehCntDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getShVehCntDtl = function({},callback) {
   var QRY_TO_EXEC = "SELECT COUNT(asrt_nm) AS veh_cnt FROM asrt_lst_t WHERE asrt_grp_id=14 ORDER BY asrt_id";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getVehStsCntDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotVehStsDtl = function({},callback) {
   var QRY_TO_EXEC = `SELECT a.dt,COUNT(DISTINCT asrt_id) AS veh_cnt,
sum(CASE WHEN TIME(fnce_out_ts) > '06:30:00' THEN 1 ELSE 0 END) AS Delay,
sum(CASE WHEN TIME(fnce_out_ts) < '06:30:00' THEN 1 ELSE 0 END) AS Early 
FROM (SELECT asrt_id,DATE_FORMAT(fnce_out_ts,'%m-%d') as dt,MIN(fnce_out_ts) as fnce_out_ts
   FROM fnce_in_out_dtl_t
   WHERE  DATE(fnce_out_ts)=CURDATE() AND fnce_id = 278 
   GROUP BY dt,asrt_id 
   ORDER BY dt,asrt_id) as a`;
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getTotTrpDistDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotTrpDistDtl = function({},callback) {
   var QRY_TO_EXEC = "SELECT DATE_FORMAT(t.actl_strt_ts, '%d-%m-%Y') AS trp_dt,t.asrt_id,ast.asrt_nm,alst.asrt_ctgry_nm,ROUND(sum(TIME_TO_SEC(TIMEDIFF(t.actl_end_ts,t.actl_strt_ts)) / 60)) AS tvl_tm,COUNT(trp_rn_id) trips_ct,SUM((odmtr_end_ct - odmtr_strt_ct)) AS dtst_tvl1 FROM trp_rn_dtl_t t JOIN asrt_lst_t ast ON t.asrt_id = ast.asrt_id JOIN asrt_ctgry_lst_t alst ON ast.asrt_ctgry_id = alst.asrt_ctgry_id WHERE DATE(t.actl_strt_ts)=CURDATE()";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getTotColPntsDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotColPntsDtl = function({},callback) {
   var QRY_TO_EXEC = "SELECT DATE_FORMAT(f1.fnce_in_ts, '%Y-%m-%d') AS fdate,(SELECT COUNT(*)FROM fncs_lst_t WHERE a_in=1) AS totalfncs,((SELECT COUNT(*) FROM fncs_lst_t WHERE a_in=1) - (COUNT(DISTINCT f1.fnce_id))) AS fnce_uncovered,COUNT(DISTINCT f1.fnce_id) AS fnce_covered FROM fnce_in_out_dtl_t AS f1 WHERE(DATE(fnce_in_ts) =CURDATE())AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > 3 GROUP BY DATE(fnce_in_ts) ORDER BY fnce_in_ts,fnce_id";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};



/*****************************************************************************
* Function      : getVehDstChrtDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getVehDstChrtDtl = function(asrt_id,callback) {
    
   var QRY_TO_EXEC = `SELECT DATE_FORMAT(adt.trvl_dt,'%d-%m') as dt,DATE_FORMAT(adt.trvl_dt,'%d-%m-%Y') as Cmp_dt,a.asrt_id,
                        a.asrt_nm,acg.asrt_ctgry_nm,Round((MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct))) as dst
                        FROM asrt_dstne_dtl_t adt 
                        JOIN asrt_lst_t a ON adt.asrt_id = a.asrt_id 
                        JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id
                        where a.asrt_id = `+asrt_id+` and adt.trvl_dt BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY
                        GROUP BY trvl_dt,a.asrt_id
                        order by trvl_dt,a.asrt_id`;
    
    // log.message("INFO",cntxtDtls,100,QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getTotVehDstChrtDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotVehDstChrtDtl = function(cat_id,callback) {
    
   var QRY_TO_EXEC = `SELECT z.dt,z.Cmp_dt,z.asrt_ctgry_nm,MIN(z.dst) as min_dst, MAX(z.dst) as max_dst, SUM(z.dst) as tot_dst, Round(AVG(z.dst)) as avg_dst
                        from (
                        SELECT DATE_FORMAT(adt.trvl_dt,'%d-%m') as dt,DATE_FORMAT(adt.trvl_dt,'%d-%m-%Y') as Cmp_dt,a.asrt_id,
                        a.asrt_nm,acg.asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst 
                        FROM asrt_dstne_dtl_t adt 
                        JOIN asrt_lst_t a ON adt.asrt_id = a.asrt_id 
                        JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id
                        where a.asrt_ctgry_id = `+cat_id+` and adt.trvl_dt BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY
                        GROUP BY trvl_dt,a.asrt_ctgry_id,a.asrt_id
                        order by trvl_dt,adt.asrt_id) as z
                        
                        GROUP BY z.dt,z.asrt_ctgry_nm
                        ORDER BY z.dt,z.asrt_ctgry_nm;`;
    
    // log.message("INFO",cntxtDtls,100,QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getVehTrpCvgChrt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getVehTrpCvgChrt = function(asrt_id,callback) {
    
   var QRY_TO_EXEC = `SELECT
                        DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,
                        DATE_FORMAT(t.actl_strt_ts, '%d-%m-%Y') AS Cmp_trp_dt,
                        t.asrt_id,
                        ast.asrt_nm,
                        alst.asrt_ctgry_nm,ast.asrt_ctgry_id,
                        COUNT(trp_rn_id) trips_ct
                        FROM trp_rn_dtl_t t
                        JOIN asrt_lst_t ast ON t.asrt_id = ast.asrt_id
                        JOIN asrt_ctgry_lst_t alst ON ast.asrt_ctgry_id = alst.asrt_ctgry_id
                        where ast.asrt_id = `+asrt_id+` and t.actl_strt_ts BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY
                        GROUP BY trp_dt, t.asrt_id ORDER BY trp_dt;`;
    
    // log.message("INFO",cntxtDtls,100,QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getTotVehTrpCvgChrt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotVehTrpCvgChrt = function(cat_id,callback) {
    
   var QRY_TO_EXEC = `SELECT z.trp_dt,z.asrt_ctgry_nm,MIN(z.trips_ct) as min_ct, MAX(z.trips_ct) as max_ct, SUM(z.trips_ct) as tot_ct, Round(AVG(z.trips_ct)) as avg_ct
                        from (SELECT
                                DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,
                                DATE_FORMAT(t.actl_strt_ts, '%d-%m-%Y') AS Cmp_trp_dt,a.asrt_id,
                                a.asrt_nm,alst.asrt_ctgry_nm,
                                COUNT(trp_rn_id) as trips_ct
                                FROM trp_rn_dtl_t t
                                JOIN asrt_lst_t a ON t.asrt_id = a.asrt_id
                                JOIN asrt_ctgry_lst_t alst ON a.asrt_ctgry_id = alst.asrt_ctgry_id
                                where a.asrt_ctgry_id = `+cat_id+` and t.actl_strt_ts BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY
                                GROUP BY trp_dt,a.asrt_id ORDER BY trp_dt) as z
                        
                        GROUP BY z.trp_dt,z.asrt_ctgry_nm
                        ORDER BY z.trp_dt,z.asrt_ctgry_nm;`;
    
    // log.message("INFO",cntxtDtls,100,QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getVehBinCvgChrt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getVehBinCvgChrt = function(asrt_id,callback) {
    
   var QRY_TO_EXEC = `select DATE_FORMAT(fio.fnce_in_ts, '%d-%m') AS dt,DATE_FORMAT(fio.fnce_in_ts, '%d-%m-%Y') AS Cmp_dt,
                        a.asrt_id,a.asrt_nm,acg.asrt_ctgry_nm,count(fio.trp_rn_id) as bin_cvg_ct from fnce_in_out_dtl_t fio
                        join asrt_lst_t a on fio.asrt_id=a.asrt_id
                        join asrt_ctgry_lst_t acg on a.asrt_ctgry_id=acg.asrt_ctgry_id
                        where a.asrt_id=`+asrt_id+` and TIME_TO_SEC(TIMEDIFF(fio.fnce_out_ts,fio.fnce_in_ts))/60 > 3 and fio.fnce_in_ts BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY
                        group by dt,a.asrt_id
                        order by dt,a.asrt_id;`;
    
    // log.message("INFO",cntxtDtls,100,QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getTotVehBinCvgChrt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotVehBinCvgChrt = function(cat_id,callback) {
    
   var QRY_TO_EXEC = `SELECT z.dt,z.Cmp_dt,z.asrt_ctgry_nm,MIN(z.bin_cvg_ct) as min_ct, MAX(z.bin_cvg_ct) as max_ct, SUM(z.bin_cvg_ct) as tot_ct, Round(AVG(z.bin_cvg_ct)) as avg_ct
                        from (select DATE_FORMAT(fio.fnce_in_ts, '%d-%m') AS dt,DATE_FORMAT(fio.fnce_in_ts, '%d-%m-%Y') AS Cmp_dt,
                        a.asrt_id,a.asrt_nm,acg.asrt_ctgry_nm,count(fio.trp_rn_id) as bin_cvg_ct from fnce_in_out_dtl_t fio
                        join asrt_lst_t a on fio.asrt_id=a.asrt_id
                        join asrt_ctgry_lst_t acg on a.asrt_ctgry_id=acg.asrt_ctgry_id
                        where a.asrt_ctgry_id=`+cat_id+` and TIME_TO_SEC(TIMEDIFF(fio.fnce_out_ts,fio.fnce_in_ts))/60 > 3 and fio.fnce_in_ts BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY
                        group by dt,a.asrt_id
                        order by dt,a.asrt_id) as z
                        
                        GROUP BY z.dt,z.asrt_ctgry_nm
                        ORDER BY z.dt,z.asrt_ctgry_nm;`;
    
    // log.message("INFO",cntxtDtls,100,QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getVehDshChrtDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehDshStsDtl = function(callback) {
   var QRY_TO_EXEC = "SELECT COUNT(asrt_id) AS asrt_cnt FROM fnce_in_out_dtl_t where DATE(fnce_in_ts)=CURDATE() AND fnce_out_ts is NULL";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehTypeTrpDtlsMdl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehTypeTrpDtlsMdl = function(callback) {
    
    var QRY_TO_EXEC = " select count(distinct fio.fnce_id) as fnce_ct,COUNT(DISTINCT al.asrt_id) as asrt_ct,ag.asrt_grp_nm,ac.asrt_ctgry_nm,fio.fnce_in_ts,fnce_out_ts,fnce_id,trp_rn_id,al.* from asrt_lst_t as al join asrt_grp_lst_t as ag on ag.asrt_grp_id = al.asrt_grp_id join asrt_ctgry_lst_t as ac on ac.asrt_ctgry_id = al.asrt_ctgry_id join fnce_in_out_dtl_t as fio on fio.asrt_id = al.asrt_id where TIME_TO_SEC(TIMEDIFF(fio.fnce_out_ts,fio.fnce_in_ts))/60 > 3 and date(fnce_in_ts) = curdate() group by al.asrt_ctgry_id order by ag.asrt_grp_nm;"
        log.message("INFO",QRY_TO_EXEC,100,QRY_TO_EXEC);
        
        if(callback && typeof callback == "function") 
            dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                (err,results);
                return;
            });
        else 
           return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : gettotLndMrksDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotLndMrksDtl = function(callback) {
   var QRY_TO_EXEC = "SELECT COUNT(fnce_nm) AS fnce_cnt FROM fncs_lst_t WHERE fnce_grp_id=1 AND a_in=1";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotLndMrksCovDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotLndMrksCovDtl = function(callback) {
   var QRY_TO_EXEC = "SELECT COUNT(f.fnce_id) AS fncs_cnt,f.fnce_nm,fn.* FROM fnce_in_out_dtl_t fn LEFT JOIN fncs_lst_t f ON fn.fnce_id=f.fnce_id WHERE DATE(fnce_in_ts)=CURDATE() AND TIME_TO_SEC(TIMEDIFF(fn.fnce_out_ts,fn.fnce_in_ts)) / 60 > 3 AND fnce_grp_id=1 AND f.a_in=1";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getlndMrksNtCovDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getlndMrksNtCovDtl = function(callback) {
   var QRY_TO_EXEC = "SELECT fnce_id,fnce_nm,COUNT(fnce_id) AS notcovered_cnt FROM fncs_lst_t WHERE fnce_id NOT IN (SELECT fnce_id FROM fnce_in_out_dtl_t WHERE DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()) AND fnce_grp_id=1 AND a_in=1";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getalllndMrksDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getalllndMrksDtl = function(callback) {
   var QRY_TO_EXEC = "SELECT fnce_id,fnce_nm FROM fncs_lst_t WHERE fnce_grp_id=1 AND a_in=1";
    
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getLndMrkDshChrtDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getLndMrkDshChrtDtl = function(fnce_id,callback) {
   var QRY_TO_EXEC = "select ac.asrt_ctgry_nm,asrt_nm,f.fnce_id,fnce_nm,TIME_FORMAT(fnce_in_ts,'%h:%m') as fncein_time,DATE_FORMAT(fnce_in_ts,'%m-%d') AS fncecov_dt FROM fnce_in_out_dtl_t f JOIN fncs_lst_t flt ON f.fnce_id=flt.fnce_id JOIN asrt_lst_t a ON f.asrt_id=a.asrt_id JOIN asrt_ctgry_lst_t ac ON a.asrt_ctgry_id=ac.asrt_ctgry_id WHERE f.fnce_id="+fnce_id+" AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() - INTERVAL 1 DAY AND flt.fnce_grp_id=1 AND flt.a_in=1 GROUP BY DATE(fnce_in_ts) ORDER BY DATE(fnce_in_ts)";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotTrctsDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotTrctsDstTrvld = function(callback) {
   var QRY_TO_EXEC = "SELECT b.asrt_grp_id,b.asrt_grp_nm,b.asrt_ctgry_id,b.asrt_ctgry_nm,ROUND(MAX(b.dist)) AS max_dst,ROUND(MIN(b.dist)) AS min_dst,ROUND(AVG(b.dist)) AS avg_dst FROM (SELECT ag.asrt_grp_id,a.asrt_ctgry_id,ag.asrt_grp_nm,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,SUM(a.dist_trvl) AS dist FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.strt_odmtr_ct IS NOT NULL GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm) AS b GROUP BY asrt_grp_nm,asrt_ctgry_nm ORDER BY asrt_grp_nm,asrt_ctgry_nm";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotDstCrDyTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotDstCrDyTrvld = function(callback) {
   var QRY_TO_EXEC = "SELECT a.asrt_grp_id,ag.asrt_grp_nm,a.asrt_ctgry_id,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,ROUND(SUM(a.dist_trvl)) AS dst FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt =CURDATE() AND adt.strt_odmtr_ct IS NOT NULL GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotFnceCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotFnceCovd = function(callback) {
   var QRY_TO_EXEC = `SELECT
    a1.asrt_grp_nm,
    MAX(a1.fnce_covered) AS max_fncovd,
    MIN(a1.fnce_covered) AS min_fncovd,
    ROUND(AVG(a1.fnce_covered)) AS avg_fncovd
FROM
    (
        SELECT
            asrt_grp_nm,
            DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,
            COUNT(DISTINCT fdt.fnce_id) AS fnce_covered
        FROM
            fnce_in_out_dtl_t fdt
        JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id
        JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id
        JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id
        JOIN fncs_lst_t AS f ON fdt.fnce_id = f.fnce_id
        JOIN fncs_grp_lst_t as fg on f.fnce_grp_id=fg.fnce_grp_id
        WHERE
            TIME_TO_SEC(
                TIMEDIFF(
                    fdt.fnce_out_ts,
                    fdt.fnce_in_ts
                )
            ) / 60 > fg.cvrg_tm
        AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY
        AND CURDATE()
        AND a.asrt_grp_id IN (8, 14)
        AND f.fnce_grp_id = 1 AND f.a_in=1
        GROUP BY
            dt,
            ag.asrt_grp_nm
        ORDER BY
            dt,
            fdt.asrt_id
    ) AS a1
GROUP BY
    asrt_grp_nm`;
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : totFnceDyCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.totFnceDyCovd = function(callback) {
   var QRY_TO_EXEC = `SELECT asrt_grp_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered 
FROM fnce_in_out_dtl_t fdt 
JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id 
JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id 
JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id 
JOIN fncs_lst_t AS f ON fdt.fnce_id = f.fnce_id
JOIN fncs_grp_lst_t as fg ON f.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_in_ts) =CURDATE() 
AND a.asrt_grp_id IN (8, 14) AND f.fnce_grp_id = 1 AND f.a_in=1
GROUP BY dt,ag.asrt_grp_nm ORDER BY dt,fdt.asrt_id`;

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : totFnceBnsDyCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.totFnceBnsDyCovd = function(callback) {
   var QRY_TO_EXEC = `SELECT asrt_grp_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered 
FROM fnce_in_out_dtl_t fdt 
JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id 
JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id 
JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id 
JOIN fncs_lst_t AS f ON fdt.fnce_id = f.fnce_id 
JOIN fncs_grp_lst_t as fg ON f.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm  
AND DATE(fnce_in_ts) =CURDATE() AND a.asrt_grp_id IN (8) 
AND f.fnce_grp_id = 3 and f.a_in=1 GROUP BY dt,ag.asrt_grp_nm ORDER BY dt,fdt.asrt_id`;
    // log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : gettotFnceBnsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotFnceBnsCovd = function(callback) {
   var QRY_TO_EXEC = `SELECT a1.asrt_grp_nm,MAX(a1.fnce_covered) AS max_fncovd,MIN(a1.fnce_covered) AS min_fncovd,ROUND(AVG(a1.fnce_covered)) AS avg_fncovd 
FROM 
(SELECT asrt_grp_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered 
FROM fnce_in_out_dtl_t fdt 
JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id 
JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id 
JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id 
JOIN fncs_lst_t AS f ON fdt.fnce_id = f.fnce_id 
JOIN fncs_grp_lst_t as fg ON f.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY and CURDATE() 
AND a.asrt_grp_id IN (8) AND f.fnce_grp_id = 3 AND f.a_in=1
GROUP BY dt,ag.asrt_grp_nm ORDER BY dt,fdt.asrt_id) AS a1 GROUP BY asrt_grp_nm`;
    // log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getmcrEchVehCntDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchVehCntDtls = function({},callback) {
   var QRY_TO_EXEC = "SELECT a.asrt_ctgry_id,ac.asrt_ctgry_nm,COUNT(a.asrt_ctgry_id) AS veh_cnt FROM asrt_lst_t a JOIN asrt_ctgry_lst_t ac ON a.asrt_ctgry_id = ac.asrt_ctgry_id WHERE asrt_grp_id = 8 AND a.a_in=1 GROUP BY a.asrt_ctgry_id ORDER BY asrt_ctgry_nm";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshEchVehCntDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshEchVehCntDtls = function({},callback) {
   var QRY_TO_EXEC = "SELECT ac.asrt_ctgry_nm,COUNT(a.asrt_ctgry_id) AS veh_cnt,a.* FROM asrt_lst_t a JOIN asrt_ctgry_lst_t ac ON a.asrt_ctgry_id=ac.asrt_ctgry_id WHERE asrt_grp_id=14 GROUP BY a.asrt_ctgry_id ORDER BY asrt_id";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotTripsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotTripsCovd = function({},callback) {
   var QRY_TO_EXEC = "SELECT a1.asrt_grp_id,a1.asrt_grp_nm,a1.asrt_ctgry_id,a1.asrt_ctgry_nm,MAX(trps_ct) AS max_trps,MIN(trps_ct) AS min_trps,ROUND(AVG(trps_ct)) AS avg_trps FROM (SELECT asrt_grp_nm,a.asrt_ctgry_id,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm) AS a1 GROUP BY a1.asrt_grp_nm,a1.asrt_ctgry_nm ORDER BY a1.asrt_grp_nm,a1.asrt_ctgry_nm";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotTripsDyCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotTripsDyCovd = function({},callback) {
   var QRY_TO_EXEC = "SELECT asrt_grp_nm,a.asrt_ctgry_id,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) =CURDATE() AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getSubscriptions
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getSubscriptions = function(usr_id,callback) {
   var QRY_TO_EXEC = "SELECT ul.usr_nm,ac.alert_cat_nm,al.* FROM alert_subscn_dtl_t as al JOIN usr_lst_t as ul on ul.usr_id=al.usr_id JOIN alert_cat_lst_t as ac on ac.alert_cat_id=al.alert_cat_id where al.usr_id = '"+usr_id+"' ";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : getAlertCtgry
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAlertCtgry = function(callback) {

   var QRY_TO_EXEC = "SELECT * FROM alert_cat_lst_t";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : newSubscriber
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.addNewSubscriber = function(data,callback) {
  //console.log(newSubDtls);
     var QRY_TO_EXEC = "INSERT INTO alert_subscn_dtl_t(usr_id,alert_cat_id,sms_alert_in,app_alert_in,email_alert_in,active_in,i_ts) values('"+data.usr_id+"','"+data.alert_cat_id+"','"+data.sms_alert_in+"','"+data.app_alert_in+"','"+data.email_alert_in+"',1,CURRENT_TIMESTAMP());";
     //console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAlertList
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAlertList = function(data,ctgry_id,callback) {
  
     var QRY_TO_EXEC = "select ul.usr_nm,ac.alert_cat_nm,ac.alert_cat_cd,date_format(an.alert_ts,'%y-%m-%d %h:%m') as alert_tm,an.*,ad.* from alert_notify_dtl_t as an join alert_dtl_t as ad on ad.alert_id=an.alert_id join usr_lst_t as ul on ul.usr_id=an.usr_id join alert_cat_lst_t as ac on ac.alert_cat_id = an.alert_cat_id where an.sms_ct = 1 and an.usr_id='"+data.usr_id+"' and an.alert_cat_id in ("+ctgry_id+") and an.alert_ts between '"+data.from_date+"' and '"+data.to_date+"'";

     //console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getDriverAsigDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getDriverAsigDtls = function(data,callback) {
  
     // var QRY_TO_EXEC = "SELECT * FROM veh_assignment_dtl_t WHERE crew_id='"+data.crew_id+"' and asrt_id='"+data.asrt_id+"'  ";

     var QRY_TO_EXEC = `SELECT date_format(vs.frm_ts,'%Y-%m-%d %H:%i') as fromDate,date_format(vs.to_ts,'%Y-%m-%d %H:%i') as toDate,cl.crw_nm,al.asrt_nm,vs.*
                        FROM drvr_asgnt_dtl_t as vs
                        join crw_lst_t as cl on cl.crw_id = vs.crw_id
                        JOIN asrt_lst_t as al on al.asrt_id = vs.asrt_id
                        WHERE vs.crw_id=${data.crw_id} and vs.asrt_id=${data.asrt_id} 
                        AND (DATE_FORMAT(frm_ts,'%Y-%m-%d %h:%m') >= "${data.frm_ts}" and DATE_FORMAT(to_ts,'%Y-%m-%d %h:%m') <= "${data.to_ts}") `;

        // console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : checkExistinRcds
* Description   : Checking if the requested record is aleready in db or not
* Arguments     : callback function
******************************************************************************/
exports.checkExistinRcds = function(data,callback) {
  
     var QRY_TO_EXEC = `SELECT * FROM fuel_dtl_t where asrt_id=${data.asrt_id} and isse_dt = CURDATE() ;`;

        // console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : updatingFuelDetails
* Description   : updating the requested record is aleready in db 
* Arguments     : callback function
******************************************************************************/
exports.updatingFuelDetails = function(data,callback) {
  
     var QRY_TO_EXEC = `UPDATE fuel_dtl_t SET fuel_ct=${data.new_fuel_ct},u_ts=CURRENT_TIMESTAMP() WHERE asrt_id=${data.asrt_id} and isse_dt=CURDATE() ;`;

        // console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : addFuelDetails
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.addFuelDetails = function(data,callback) {
  
     var QRY_TO_EXEC = `INSERT INTO fuel_dtl_t(asrt_id,fuel_ct,isse_dt,i_ts) VALUES(${data.asrt_id},${data.new_fuel_ct},CURDATE(),CURRENT_TIMESTAMP()); `;

        // console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : closeExpiredRcrds
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.closeExpiredRcrds = function(data,callback) {
  
     var QRY_TO_EXEC = `update fuel_dtl_t set cls_in = 1 where asrt_id =${data.asrt_id} and isse_dt < curdate();`;

        // console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getAsrtsForFuel
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAsrtsForFuel = function(data,callback) {
        
     // var QRY_TO_EXEC = "SELECT * FROM asrt_lst_t WHERE asrt_ctgry_id in ('"+data.asrt_ctgry_id+"') and asrt_grp_id in ('"+data.asrt_grp_id+"') ";

     var QRY_TO_EXEC = `SELECT ac.asrt_ctgry_nm,ag.asrt_grp_nm,DATE_FORMAT(fl.isse_dt,'%Y-%m-%d') as date,fl.fuel_ct,al.*
                        FROM asrt_lst_t AS al
                        left outer JOIN fuel_dtl_t as fl on fl.asrt_id = al.asrt_id and fl.cls_in <> 1
                        JOIN asrt_ctgry_lst_t as ac on ac.asrt_ctgry_id = al.asrt_ctgry_id
                        JOIN asrt_grp_lst_t as ag on ag.asrt_grp_id = al.asrt_grp_id
                        WHERE al.asrt_ctgry_id in (${data.asrt_ctgry_id}) and al.asrt_grp_id in (${data.asrt_grp_id})
                        and al.a_in <> 0 order by asrt_id `;

        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getFuelHstory
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getFuelHstory = function(data,callback) {
        
     var QRY_TO_EXEC = `SELECT (max(ad.strt_odmtr_ct) - MIN(ad.strt_odmtr_ct) ) as dist,al.asrt_nm,al.mileage_ct,DATE_FORMAT(fd.isse_dt,'%d-%m-%Y') as issed_dt,fd.* 
                        FROM fuel_dtl_t as fd
                        JOIN asrt_lst_t as al on al.asrt_id = fd.asrt_id
                        JOIN asrt_dstne_dtl_t as ad on ad.asrt_id = fd.asrt_id and fd.isse_dt = ad.trvl_dt
                        WHERE fd.asrt_id = ${data.asrt_id} GROUP BY trvl_dt ORDER BY fd.isse_dt DESC; `;

        //console.log(QRY_TO_EXEC);
        if(callback && typeof callback == "function")
                dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
                    callback(err,results);
                    return;
                });
        else
          return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : updSubscription
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.updSubscription = function(data,callback) {

    var QRY_TO_EXEC = '';
    // console.log(data);
    if(data.sms_alert_in == 1 || data.sms_alert_in == 0) {
        QRY_TO_EXEC = "UPDATE alert_subscn_dtl_t SET sms_alert_in='"+data.sms_alert_in+"',u_ts=CURRENT_TIMESTAMP() WHERE usr_id='"+data.usr_id+"' and alert_cat_id='"+data.alert_cat_id+"' ";
    } else if(data.app_alert_in == 1 || data.app_alert_in == 0) {
        QRY_TO_EXEC = "UPDATE alert_subscn_dtl_t SET app_alert_in='"+data.app_alert_in+"',u_ts=CURRENT_TIMESTAMP() WHERE usr_id='"+data.usr_id+"' and alert_cat_id='"+data.alert_cat_id+"' ";
    } else if(data.email_alert_in == 1 || data.email_alert_in == 0) {
        QRY_TO_EXEC = "UPDATE alert_subscn_dtl_t SET email_alert_in='"+data.email_alert_in+"' ,u_ts=CURRENT_TIMESTAMP() WHERE usr_id='"+data.usr_id+"' and alert_cat_id='"+data.alert_cat_id+"' ";
    }
   // var QRY_TO_EXEC = "UPDATE alert_subscn_dtl_t SET sms_alert_in='"+data.sms_alert_in+"' ,app_alert_in='"+data.app_alert_in+"' ,email_alert_in='"+data.email_alert_in+"' ,u_ts=CURRENT_TIMESTAMP() WHERE usr_id='"+data.usr_id+"' and alert_cat_id='"+data.alert_cat_id+"' ";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : getAllCrewList
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getAllCrewList = function(data,callback) {

   var QRY_TO_EXEC = "SELECT * FROM crw_lst_t";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : getAllCrewList
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.driverAssignmentDtls = function(data,callback) {

   var QRY_TO_EXEC = "INSERT INTO drvr_asgnt_dtl_t(crw_id,frm_ts,to_ts,asrt_id,i_ts) values('"+data.crw_id+"','"+data.frm_ts+"','"+data.to_ts+"','"+data.asrt_id+"',CURRENT_TIMESTAMP() )";
    log.message("Inserting :: "+QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : checkingCrewExistance
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.checkingCrewExistance = function(data,callback) {

   // var QRY_TO_EXEC = "SELECT * FROM veh_assignment_dtl_t where close_in <> 1 and crew_id='"+data.crew_id+"' and asrt_id='"+data.asrt_id+"' and ('"+data.to_date+"' BETWEEN from_date and to_date )";

   // var QRY_TO_EXEC = "SELECT * FROM drvr_asgnt_dtl_t where clse_in <> 1 and ((crw_id <> '"+data.crw_id+"' and asrt_id='"+data.asrt_id+"') or (crw_id = '"+data.crw_id+"' and asrt_id <> '"+data.asrt_id+"')) and (('"+data.frm_ts+"' BETWEEN frm_ts and to_ts or frm_ts > '"+data.frm_ts+"') or ('"+data.to_ts+"' BETWEEN frm_ts and to_ts and  to_ts < '"+data.to_ts+"'))";

   var QRY_TO_EXEC = `SELECT c.crw_nm,a.asrt_nm,d.* 
                      FROM drvr_asgnt_dtl_t as d
                      join asrt_lst_t as a on a.asrt_id = d.asrt_id
                      join crw_lst_t as c on c.crw_id = d.crw_id
                      where clse_in <> 1 and ((d.crw_id <> ${data.crw_id} and d.asrt_id=${data.asrt_id}) or (d.crw_id = ${data.crw_id} and d.asrt_id <> ${data.asrt_id}))
                      and (("${data.frm_ts}" BETWEEN frm_ts and to_ts or frm_ts > "${data.frm_ts}") or 
                      ("${data.to_ts}" BETWEEN frm_ts and to_ts and  to_ts < "${data.to_ts}"));`;
    

    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : checkDriverToDt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.checkDriverToDt = function(data,callback) {

   var QRY_TO_EXEC = "SELECT * FROM drvr_asgnt_dtl_t where clse_in <> 1 and asrt_id='"+data.asrt_id+"' and ('"+data.to_ts+"' BETWEEN frm_ts and to_ts )";
    log.message("To Date Checking :: "+QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : checkDriverFromDt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.checkDriverFromDt = function(data,callback) {

   // var QRY_TO_EXEC = "SELECT * FROM drvr_asgnt_dtl_t where clse_in <> 1 and crw_id='"+data.crw_id+"' and asrt_id='"+data.asrt_id+"' and ('"+data.frm_ts+"' BETWEEN frm_ts and to_ts or frm_ts > CURRENT_TIMESTAMP() )";
    
  // var QRY_TO_EXEC = "SELECT * FROM drvr_asgnt_dtl_t where clse_in <> 1 and asrt_id='"+data.asrt_id+"' and ('"+data.frm_ts+"' BETWEEN frm_ts and to_ts or frm_ts > CURRENT_TIMESTAMP() )";

  var QRY_TO_EXEC = `SELECT c.crw_nm,a.asrt_nm,d.*
                      FROM drvr_asgnt_dtl_t as d
                      join asrt_lst_t as a on a.asrt_id = d.asrt_id
                      join crw_lst_t as c on c.crw_id = d.crw_id
                      where clse_in <> 1 and d.asrt_id=${data.asrt_id} and 
                      ("${data.frm_ts}" BETWEEN frm_ts and to_ts or frm_ts > CURRENT_TIMESTAMP() )`;

    log.message("check from date :: "+QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : closeExistingRecord
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.closeExistingRecord = function(data,callback) {

   // var QRY_TO_EXEC = "UPDATE drvr_asgnt_dtl_t SET to_ts=CURRENT_TIMESTAMP() ,clse_in=1 , u_ts=CURRENT_TIMESTAMP() WHERE clse_in <> 1 and crw_id='"+data.crw_id+"' and asrt_id='"+data.asrt_id+"' ";
    
  var QRY_TO_EXEC = "UPDATE drvr_asgnt_dtl_t SET to_ts=CURRENT_TIMESTAMP() ,clse_in=1 , u_ts=CURRENT_TIMESTAMP() WHERE clse_in <> 1 and asrt_id='"+data.asrt_id+"' ";


    log.message("closing :: "+QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : closenotInRangeRecord
* Description   : close Existing Record when from and to timings are not in the range
* Arguments     : callback function
******************************************************************************/
exports.closenotInRangeRecord = function(data,callback) {

   // var QRY_TO_EXEC = "UPDATE drvr_asgnt_dtl_t SET to_ts=CURRENT_TIMESTAMP() ,clse_in=1 , u_ts=CURRENT_TIMESTAMP() WHERE clse_in <> 1 and crw_id='"+data.crw_id+"' and asrt_id='"+data.asrt_id+"' ";
    
  var QRY_TO_EXEC = "UPDATE drvr_asgnt_dtl_t SET clse_in=1 , u_ts=CURRENT_TIMESTAMP() WHERE clse_in <> 1 and asrt_id='"+data.asrt_id+"' ";

    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : closeExpiredRecords
* Description   : closing all the records which are having todate grater than current date
* Arguments     : callback function
******************************************************************************/
exports.closeExpiredRecords = function(callback) {

   var QRY_TO_EXEC = "UPDATE drvr_asgnt_dtl_t SET clse_in=1 , u_ts=CURRENT_TIMESTAMP() WHERE to_ts < CURRENT_TIMESTAMP() AND clse_in <> 1;";
    log.message(QRY_TO_EXEC);
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getTotVehCnt
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getTotVehCnt = function({},callback) {
   var QRY_TO_EXEC = "SELECT COUNT(DISTINCT asrt_nm) as tot_vehs FROM asrt_lst_t";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : gettotCapctyLftd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotCapctyLftd = function(callback) {
    
    var QRY_TO_EXEC = "SELECT z.asrt_grp_id,ag.asrt_grp_nm,asrt_ctgry_id,z.asrt_ctgry_nm,MIN(cp_cnt) AS min_cpty,MAX(cp_cnt) AS max_cpty,ROUND(AVG(cp_cnt)) AS avg_cpty FROM (SELECT a.asrt_ctgry_id,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE(t.actl_strt_ts) AS trp_dt,count(*) AS trps_ct,a.capacity_ct,(count(*) * capacity_ct) AS cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() GROUP BY trp_dt,asrt_grp_id,a.asrt_ctgry_id ORDER BY a.asrt_grp_id,a.asrt_ctgry_id,trp_dt) AS z JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = z.asrt_grp_id GROUP BY asrt_grp_nm,asrt_ctgry_nm ORDER BY asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotCapctyDyLftd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotCapctyDyLftd = function(callback) {
    
    var QRY_TO_EXEC = "SELECT a.asrt_grp_id,ag.asrt_grp_nm,a.asrt_ctgry_id,acg.asrt_ctgry_nm,DATE(t.actl_strt_ts) AS trp_dt,count(*) AS trps_ct,a.capacity_ct,(count(*) * capacity_ct) as cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) = CURDATE() GROUP BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm ORDER BY ag.asrt_grp_nm,acg.asrt_ctgry_nm,trp_dt";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getmcrVehsDistTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrVehsDistTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,ROUND(SUM(a.dist_trvl)) AS dst FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.strt_odmtr_ct IS NOT NULL AND alt.asrt_grp_id = 8 AND alt.asrt_ctgry_id = 1 GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrVehsTiprDistTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrVehsTiprDistTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,ROUND(SUM(a.dist_trvl)) AS dst FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.strt_odmtr_ct IS NOT NULL AND alt.asrt_grp_id = 8 AND alt.asrt_ctgry_id = 5 GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrVehsDmprsDistTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrVehsDmprsDistTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,ROUND(SUM(a.dist_trvl)) AS dst FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.strt_odmtr_ct IS NOT NULL AND alt.asrt_grp_id = 8 AND alt.asrt_ctgry_id = 4 GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};
/*****************************************************************************
* Function      : gethirdTrctsDistTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethirdTrctsDistTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,ROUND(SUM(a.dist_trvl)) AS dst FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.strt_odmtr_ct IS NOT NULL AND alt.asrt_grp_id = 14 AND alt.asrt_ctgry_id = 1 GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gethirdTipprsDistTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethirdTipprsDistTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,a.asrt_ctgry_nm,DATE_FORMAT(a.trvl_dt, '%d-%m') AS trvl_dt,ROUND(SUM(a.dist_trvl)) AS dst FROM (SELECT alt.asrt_nm,alt.asrt_grp_id,adt.trvl_dt,alt.asrt_ctgry_id,acg.asrt_ctgry_nm,(MAX(adt.strt_odmtr_ct) - MIN(adt.strt_odmtr_ct)) AS dist_trvl FROM asrt_lst_t AS alt JOIN asrt_dstne_dtl_t AS adt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = alt.asrt_ctgry_id WHERE adt.trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.strt_odmtr_ct IS NOT NULL AND alt.asrt_grp_id = 14 AND alt.asrt_ctgry_id = 5 GROUP BY alt.asrt_nm,adt.trvl_dt ORDER BY alt.asrt_nm,adt.trvl_dt) AS a JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id GROUP BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trvl_dt,asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrTrctsTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrTrctsTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT asrt_grp_nm,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts,'%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=8 AND a.asrt_ctgry_id=1 AND actl_strt_ts is NOT NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrTipprsTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrTipprsTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT asrt_grp_nm,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts,'%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=8 AND a.asrt_ctgry_id=5 AND actl_strt_ts is NOT NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrDmprsTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrDmprsTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT asrt_grp_nm,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts,'%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=8 AND a.asrt_ctgry_id=4 AND actl_strt_ts is NOT NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gethirdTrctsTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethirdTrctsTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT asrt_grp_nm,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts,'%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=14 AND a.asrt_ctgry_id=1 AND actl_strt_ts is NOT NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gethirdTipprsTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethirdTipprsTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT asrt_grp_nm,acg.asrt_ctgry_nm,a.asrt_grp_id,DATE_FORMAT(t.actl_strt_ts,'%d-%m') AS trp_dt,count(*) AS trps_ct FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=14 AND a.asrt_ctgry_id=5 AND actl_strt_ts is NOT NULL GROUP BY trp_dt,asrt_grp_nm,asrt_ctgry_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrColPntsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrColPntsCovd = function(callback) {
    
    var QRY_TO_EXEC = `SELECT asrt_grp_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered 
FROM fnce_in_out_dtl_t fdt 
JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id 
JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id 
JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id 
JOIN fncs_lst_t AS f ON fdt.fnce_id=f.fnce_id
JOIN fncs_grp_lst_t as fg ON f.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() 
AND a.asrt_grp_id=8 AND f.fnce_grp_id=1 AND f.a_in=1 GROUP BY dt,ag.asrt_grp_nm ORDER BY dt,fdt.asrt_id`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gethrdColPntsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethrdColPntsCovd = function(callback) {
    
    var QRY_TO_EXEC = `SELECT asrt_grp_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered 
FROM fnce_in_out_dtl_t fdt 
JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id 
JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id 
JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id 
JOIN fncs_lst_t AS f ON fdt.fnce_id=f.fnce_id
JOIN fncs_grp_lst_t as fg ON f.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() 
AND a.asrt_grp_id=14 AND f.fnce_grp_id=1 AND f.a_in=1 GROUP BY dt,ag.asrt_grp_nm ORDER BY dt,fdt.asrt_id`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrDmprBnsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrDmprBnsCovd = function(callback) {
    
    var QRY_TO_EXEC = `SELECT asrt_grp_nm,DATE_FORMAT(fdt.fnce_in_ts, '%d-%m') AS dt,COUNT(DISTINCT fdt.fnce_id) AS fnce_covered 
FROM fnce_in_out_dtl_t fdt JOIN asrt_lst_t a ON fdt.asrt_id = a.asrt_id 
JOIN asrt_ctgry_lst_t ast ON a.asrt_ctgry_id = ast.asrt_ctgry_id 
JOIN asrt_grp_lst_t AS ag ON a.asrt_grp_id = ag.asrt_grp_id 
JOIN fncs_lst_t AS f ON fdt.fnce_id=f.fnce_id 
JOIN fncs_grp_lst_t as fg ON f.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() 
AND a.asrt_grp_id=8 AND f.fnce_grp_id=3 AND f.a_in=1 GROUP BY dt,ag.asrt_grp_nm ORDER BY dt,fdt.asrt_id`;
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrTrctsCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrTrctsCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,acg.asrt_ctgry_nm,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,(count(*) * capacity_ct) AS cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=8 AND a.asrt_ctgry_id=1 GROUP BY DATE(t.actl_strt_ts),ag.asrt_grp_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrTipprsCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrTipprsCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,acg.asrt_ctgry_nm,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,(count(*) * capacity_ct) AS cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=8 AND a.asrt_ctgry_id=5 GROUP BY DATE(t.actl_strt_ts),ag.asrt_grp_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrDmprsCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrDmprsCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,acg.asrt_ctgry_nm,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,(count(*) * capacity_ct) AS cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=8 AND a.asrt_ctgry_id=4 GROUP BY DATE(t.actl_strt_ts),ag.asrt_grp_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gethirdTrctsCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethirdTrctsCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,acg.asrt_ctgry_nm,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,(count(*) * capacity_ct) AS cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=14 AND a.asrt_ctgry_id=1 GROUP BY DATE(t.actl_strt_ts),ag.asrt_grp_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gethirdTipprsCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gethirdTipprsCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT ag.asrt_grp_nm,acg.asrt_ctgry_nm,DATE_FORMAT(t.actl_strt_ts, '%d-%m') AS trp_dt,(count(*) * capacity_ct) AS cp_cnt FROM trp_rn_dtl_t AS t JOIN asrt_lst_t AS a ON a.asrt_id = t.asrt_id JOIN asrt_ctgry_lst_t AS acg ON acg.asrt_ctgry_id = a.asrt_ctgry_id JOIN asrt_grp_lst_t AS ag ON ag.asrt_grp_id = a.asrt_grp_id WHERE DATE(actl_strt_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND a.asrt_grp_id=14 AND a.asrt_ctgry_id=5 GROUP BY DATE(t.actl_strt_ts),ag.asrt_grp_nm ORDER BY trp_dt,ag.asrt_grp_nm,acg.asrt_ctgry_nm";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchTrctDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchTrctDstTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT trvl_dt,adt.asrt_id,asrt_nm,asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t AS alt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t as act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE trvl_dt = CURDATE() AND asrt_grp_id = 8 AND alt.asrt_ctgry_id = 1 GROUP BY adt.asrt_id HAVING dst > 0 ORDER BY adt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchTipprDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchTipprDstTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT trvl_dt,adt.asrt_id,asrt_nm,asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t AS alt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t as act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE trvl_dt = CURDATE() AND asrt_grp_id = 8 AND alt.asrt_ctgry_id = 5 GROUP BY adt.asrt_id HAVING dst > 0 ORDER BY adt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchDmprDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchDmprDstTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT trvl_dt,adt.asrt_id,asrt_nm,asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t AS alt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t as act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE trvl_dt = CURDATE() AND asrt_grp_id = 8 AND alt.asrt_ctgry_id = 4 GROUP BY adt.asrt_id HAVING dst > 0 ORDER BY adt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchTractsDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchTractsDstTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT trvl_dt,adt.asrt_id,asrt_nm,asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t AS alt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t as act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE trvl_dt = CURDATE() AND asrt_grp_id = 14 AND alt.asrt_ctgry_id = 1 GROUP BY adt.asrt_id HAVING dst > 0 ORDER BY adt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchTipprDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchTipprDstTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT trvl_dt,adt.asrt_id,asrt_nm,asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t AS alt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t as act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE trvl_dt = CURDATE() AND asrt_grp_id = 14 AND alt.asrt_ctgry_id = 5 GROUP BY adt.asrt_id HAVING dst > 0 ORDER BY adt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchTrctTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchTrctTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,act.asrt_ctgry_nm,COUNT(trp_rn_id) as trps_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND alt.asrt_grp_id=8 AND alt.asrt_ctgry_id=1 AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL  GROUP BY asrt_nm ORDER BY alt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchTipprTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchTipprTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,act.asrt_ctgry_nm,COUNT(trp_rn_id) as trps_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND alt.asrt_grp_id=8 AND alt.asrt_ctgry_id=5 AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL  GROUP BY asrt_nm ORDER BY alt.asrt_id";
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchDmprTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchDmprTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,act.asrt_ctgry_nm,COUNT(trp_rn_id) as trps_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND alt.asrt_grp_id=8 AND alt.asrt_ctgry_id=4 AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL  GROUP BY asrt_nm ORDER BY alt.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchTractTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchTractTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,act.asrt_ctgry_nm,COUNT(trp_rn_id) as trps_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND alt.asrt_grp_id=14 AND alt.asrt_ctgry_id=1 AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL  GROUP BY asrt_nm ORDER BY alt.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchTipprTripsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchTipprTripsTrvld = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,act.asrt_ctgry_nm,COUNT(trp_rn_id) as trps_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act on alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND alt.asrt_grp_id=14 AND alt.asrt_ctgry_id=5 AND t.a_in=1 AND t.actl_strt_ts is NOT NULL AND t.actl_end_ts is not NULL  GROUP BY asrt_nm ORDER BY alt.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchColPntCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchColPntCovd = function(callback) {
    
    var QRY_TO_EXEC = `SELECT fnc.asrt_id,asrt_nm,asrt_ctgry_nm,COUNT(DISTINCT fnc.fnce_id) AS fnc_cnt 
FROM fnce_in_out_dtl_t fnc 
JOIN asrt_lst_t alt ON fnc.asrt_id = alt.asrt_id 
JOIN fncs_lst_t flt ON fnc.fnce_id = flt.fnce_id 
JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id=act.asrt_ctgry_id
JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id
WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_out_ts) = CURDATE() AND asrt_grp_id = 8 
AND flt.fnce_grp_id = 1 AND flt.a_in=1 GROUP BY fnc.asrt_id ORDER BY fnc.asrt_id`;
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchColPntCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchColPntCovd = function(callback) {
    
    var QRY_TO_EXEC = `SELECT fnc.asrt_id,asrt_nm,asrt_ctgry_nm,COUNT(DISTINCT fnc.fnce_id) AS fnc_cnt 
FROM fnce_in_out_dtl_t fnc 
JOIN asrt_lst_t alt ON fnc.asrt_id = alt.asrt_id 
JOIN fncs_lst_t flt ON fnc.fnce_id = flt.fnce_id 
JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id=act.asrt_ctgry_id 
JOIN fncs_grp_lst_t as fg ON flt.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_out_ts) = CURDATE() 
AND asrt_grp_id = 14 AND fg.fnce_grp_id = 1 AND flt.a_in=1
GROUP BY fnc.asrt_id ORDER BY fnc.asrt_id`;
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchTrctCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchTrctCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,asrt_ctgry_nm,COUNT(*) as trps,capacity_ct,(count(*) * capacity_ct) as cpty_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND asrt_grp_id=8 AND alt.asrt_ctgry_id=1 GROUP BY t.asrt_id ORDER BY t.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchTipprCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchTipprCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,asrt_ctgry_nm,COUNT(*) as trps,capacity_ct,(count(*) * capacity_ct) as cpty_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id=act.asrt_ctgry_id WHERE DATE(actl_strt_ts)=CURDATE() AND asrt_grp_id=8 AND alt.asrt_ctgry_id=5 GROUP BY t.asrt_id ORDER BY t.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchDmprCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchDmprCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,asrt_ctgry_nm,COUNT(*) AS trps,capacity_ct,(count(*) * capacity_ct) AS cpty_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt ON t.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id WHERE DATE(actl_strt_ts) = CURDATE() AND asrt_grp_id = 8 AND alt.asrt_ctgry_id = 4 GROUP BY t.asrt_id ORDER BY t.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchTrctCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchTrctCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,asrt_ctgry_nm,COUNT(*) AS trps,capacity_ct,(count(*) * capacity_ct) AS cpty_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt ON t.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id WHERE DATE(actl_strt_ts) = CURDATE() AND asrt_grp_id = 14 AND alt.asrt_ctgry_id = 1 GROUP BY t.asrt_id ORDER BY t.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getshahEchTipprCpty
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getshahEchTipprCpty = function(callback) {
    
    var QRY_TO_EXEC = "SELECT t.asrt_id,alt.asrt_nm,asrt_ctgry_nm,COUNT(*) AS trps,capacity_ct,(count(*) * capacity_ct) AS cpty_cnt FROM trp_rn_dtl_t t JOIN asrt_lst_t alt ON t.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id WHERE DATE(actl_strt_ts) = CURDATE() AND asrt_grp_id = 14 AND alt.asrt_ctgry_id = 5 GROUP BY t.asrt_id ORDER BY t.asrt_id";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehEchdyDstTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchdyDstTrvld = function(vehid,callback) {
    
    var QRY_TO_EXEC = "SELECT DATE_FORMAT(trvl_dt,'%m-%d') as trvl_dt,adt.asrt_id,asrt_nm,asrt_ctgry_nm,(MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct)) AS dst FROM asrt_dstne_dtl_t adt JOIN asrt_lst_t AS alt ON adt.asrt_id = alt.asrt_id JOIN asrt_ctgry_lst_t AS act ON alt.asrt_ctgry_id = act.asrt_ctgry_id WHERE trvl_dt BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() AND adt.asrt_id="+vehid+" GROUP BY trvl_dt ORDER BY trvl_dt";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};


/*****************************************************************************
* Function      : getvehEchdyTrpsTrvld
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchdyTrpsTrvld = function(vehid,callback) {
    
    var QRY_TO_EXEC = "SELECT DATE_FORMAT(yr.frmtd_dt,'%m-%d') as yr_dt,t.asrt_id,alt.asrt_nm,act.asrt_ctgry_nm,COUNT(trp_rn_id) AS trps_cnt FROM dte_dmtn_lst_t yr LEFT OUTER JOIN trp_rn_dtl_t as t ON DATE(t.actl_strt_ts)=yr.frmtd_dt and t.asrt_id = "+vehid+" left OUTER JOIN asrt_lst_t alt ON t.asrt_id = alt.asrt_id left OUTER JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id WHERE DATE(yr.frmtd_dt) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() GROUP BY yr_dt ORDER BY yr_dt";
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehEchdyColPntsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchdyColPntsCovd = function(vehid,callback) {
    
    var QRY_TO_EXEC = `SELECT DATE_FORMAT(fnce_in_ts, '%m-%d') AS dt,fnc.asrt_id,asrt_nm,asrt_ctgry_nm,COUNT(DISTINCT fnc.fnce_id) AS fnc_cnt 
FROM fnce_in_out_dtl_t fnc 
JOIN asrt_lst_t alt ON fnc.asrt_id = alt.asrt_id 
JOIN fncs_lst_t flt ON fnc.fnce_id = flt.fnce_id 
JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id
JOIN fncs_grp_lst_t as fg ON flt.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_in_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() 
AND fnc.asrt_id = ${vehid} AND flt.fnce_grp_id = 1 AND flt.a_in=1 GROUP BY dt,fnc.asrt_id ORDER BY dt`;
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehEchdyDmprBnsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchdyDmprBnsCovd = function(vehid,callback) {
    
    var QRY_TO_EXEC = `SELECT DATE_FORMAT(fnce_in_ts, '%m-%d') AS dt,fnc.asrt_id,asrt_nm,asrt_ctgry_nm,COUNT(DISTINCT fnc.fnce_id) AS fnc_cnt 
FROM fnce_in_out_dtl_t fnc 
JOIN asrt_lst_t alt ON fnc.asrt_id = alt.asrt_id 
JOIN fncs_lst_t flt ON fnc.fnce_id = flt.fnce_id 
JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id
JOIN fncs_grp_lst_t as fg ON flt.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_out_ts) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE()
AND fnc.asrt_id = ${vehid} AND flt.fnce_grp_id = 3 AND flt.a_in=1 GROUP BY dt,fnc.asrt_id ORDER BY dt`;
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getmcrEchDmprBnsCovd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getmcrEchDmprBnsCovd = function(callback) {
    
    var QRY_TO_EXEC = `SELECT fnc.asrt_id,asrt_nm,asrt_ctgry_nm,COUNT(DISTINCT fnc.fnce_id) AS fnc_cnt 
FROM fnce_in_out_dtl_t fnc 
JOIN asrt_lst_t alt ON fnc.asrt_id = alt.asrt_id 
JOIN fncs_lst_t flt ON fnc.fnce_id = flt.fnce_id 
JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id
JOIN fncs_grp_lst_t as fg ON flt.fnce_grp_id=fg.fnce_grp_id 
WHERE TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > fg.cvrg_tm 
AND DATE(fnce_out_ts) = CURDATE() 
AND asrt_grp_id = 8 AND flt.fnce_grp_id = 3 AND flt.a_in=1 GROUP BY fnc.asrt_id ORDER BY fnc.asrt_id`;
    //console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getvehEchdyCptyDmpd
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getvehEchdyCptyDmpd = function(vehid,callback) {
    
    var QRY_TO_EXEC = `SELECT DATE_FORMAT(yr.frmtd_dt, '%m-%d') AS yr_dt,DATE_FORMAT(actl_strt_ts, '%m-%d') AS dt,
t.asrt_id,alt.asrt_nm,asrt_ctgry_nm,COUNT(*) AS trps,capacity_ct,(count(*) * capacity_ct) AS cpty_cnt 
FROM dte_dmtn_lst_t yr LEFT OUTER JOIN trp_rn_dtl_t t ON DATE(t.actl_strt_ts)=yr.frmtd_dt 
AND t.asrt_id = ${vehid} LEFT OUTER JOIN asrt_lst_t alt ON t.asrt_id = alt.asrt_id 
LEFT OUTER JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id 
WHERE DATE(yr.frmtd_dt) BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() GROUP BY yr.frmtd_dt ORDER BY yr.frmtd_dt`;
    ////console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getallCropVehsDtl
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getallCropVehsDtl = function(data,callback) {

    if (data.vehgrp==0) {
        var condition=`alt.asrt_grp_id in (8,14)`;}
        else{
         condition=`alt.asrt_grp_id in (${data.vehgrp})`;
    }
    if (data.vehtypeid==0) {
        var condition=condition + " and alt.asrt_type_id in (1,4)";}
        else{
         condition=condition + " and alt.asrt_type_id in ("+data.vehtypeid+")";
    }
    if (data.vehctgryid==0) {
        var condition=condition + " and alt.asrt_ctgry_id in (1,4,5)";}
        else{
         condition=condition + " and alt.asrt_ctgry_id in ("+data.vehctgryid+")";
    }
    
    var QRY_TO_EXEC = `SELECT a.asrt_id as veh_id,a.*, b.*,dast.crw_id,crw_nm,crw_ph FROM 
(
        SELECT DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as dt
        ,alt.asrt_id,asrt_nm,asrt_ctgry_nm,asrt_grp_nm,ast.asrt_sts_nm,alt.trips_ct as mn_trp_ct
        ,SUM(actl_trvl_dstne_ct) AS trp_dst,COUNT(trp_rn_id) AS trp_cnt
        ,trips_ct,(count(trp_rn_id) * capacity_ct) AS cpty_cnt 
        FROM  asrt_lst_t alt
        JOIN asrt_sts_lst_t as ast on ast.asrt_sts_id=alt.crnt_ast_sts_id
        JOIN asrt_ctgry_lst_t act ON alt.asrt_ctgry_id = act.asrt_ctgry_id
        JOIN asrt_grp_lst_t agp ON alt.asrt_grp_id = agp.asrt_grp_id
        JOIN dte_dmtn_lst_t d on d.frmtd_dt BETWEEN date('${data.fromdt}') AND date('${data.todt}')
        LEFT JOIN trp_rn_dtl_t t  ON t.asrt_id = alt.asrt_id 
                                    AND actl_strt_ts BETWEEN '${data.fromdt}' 
                                    AND '${data.todt}'
                                    AND date(actl_strt_ts)=d.frmtd_dt
        WHERE ${condition} and alt.a_in=1 AND t.a_in=1 AND actl_strt_ts is NOT NULL AND actl_end_ts is NOT NULL
        GROUP BY alt.asrt_id,DATE(actl_strt_ts) ORDER BY dt DESC
) AS a 
LEFT JOIN (
        SELECT DATE_FORMAT(d.frmtd_dt,'%Y-%m-%d') as fnc_dt,alt.asrt_id,COUNT(fdt.fnce_id) AS fnc_cvd
        FROM asrt_lst_t alt 
        JOIN dte_dmtn_lst_t d on d.frmtd_dt BETWEEN date('${data.fromdt}') AND date('${data.todt}')
        LEFT JOIN 
        (
        select fio.* from fnce_in_out_dtl_t fio join fncs_lst_t f on fio.fnce_id=f.fnce_id
                                                join fncs_grp_lst_t fg on f.fnce_grp_id=fg.fnce_grp_id
                                                    AND TIME_TO_SEC(TIMEDIFF(fnce_out_ts, fnce_in_ts)) / 60 > cvrg_tm AND f.a_in=1
                                                    AND fnce_in_ts BETWEEN '${data.fromdt}' AND '${data.todt}'                
        )fdt on  fdt.asrt_id=alt.asrt_id AND date(fdt.fnce_in_ts)=d.frmtd_dt
        LEFT JOIN fncs_lst_t flt on fdt.fnce_id=flt.fnce_id 
        WHERE ${condition} and flt.fnce_grp_id in (1,3) 
        and alt.a_in=1 AND flt.a_in=1
        GROUP BY alt.asrt_id,DATE(fnce_in_ts) ORDER BY fnc_dt DESC
) AS b  
on b.fnc_dt=a.dt AND b.asrt_id=a.asrt_id
LEFT JOIN drvr_asgnt_dtl_t dast on a.asrt_id=dast.asrt_id and a.dt between date(dast.frm_ts) and date(dast.to_ts)
LEFT JOIN crw_lst_t vclt ON dast.crw_id=vclt.crw_id
GROUP BY a.asrt_id,a.dt
order by a.asrt_grp_nm,a.asrt_ctgry_nm,a.asrt_id,dt`;
console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getasrtGrpLstbyType
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getasrtGrpLstbyType = function(vehtypeid,callback) {
   var QRY_TO_EXEC = "SELECT a.asrt_grp_id,asrt_grp_nm FROM asrt_lst_t a JOIN asrt_grp_lst_t agt on a.asrt_grp_id=agt.asrt_grp_id WHERE asrt_type_id="+vehtypeid+" GROUP BY a.asrt_grp_id";
     
    
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : getasrtDshbrdCtgryLst
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.getasrtDshbrdCtgryLst = function(vehtypeid,callback) {
   var QRY_TO_EXEC = "SELECT * FROM asrt_ctgry_lst_t WHERE asrt_ctgry_id in (1,4,5) ORDER BY asrt_ctgry_id";
      
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};

/*****************************************************************************
* Function      : gettotalVehDtls
* Description   : 
* Arguments     : callback function
******************************************************************************/
exports.gettotalVehDtls = function(data,callback) {

    if (data.vehgrp==0) {
        var condition=`alt.asrt_grp_id in (8,14)`;}
        else{
         condition=`alt.asrt_grp_id in (${data.vehgrp})`;
    }
    if (data.vehtypeid==0) {
        var condition=condition + " and alt.asrt_type_id in (1,4)";}
        else{
         condition=condition + " and alt.asrt_type_id in ("+data.vehtypeid+")";
    }
    if (data.vehctgryid==0) {
        var condition=condition + " and alt.asrt_ctgry_id in (1,4,5)";}
        else{
         condition=condition + " and alt.asrt_ctgry_id in ("+data.vehctgryid+")";
    }
    
    var QRY_TO_EXEC = `SELECT a.*,b.*,c.*,d.*,e.*,f.* FROM
(SELECT COUNT(trp_rn_id) as trips_cnt FROM trp_rn_dtl_t t
JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id 
WHERE ${condition} and actl_strt_ts BETWEEN '${data.fromdt}' and '${data.todt}' AND actl_strt_ts is NOT NULL and alt.a_in=1 AND t.a_in=1 AND actl_end_ts is NOT NULL) as a

JOIN
(SELECT ROUND((MAX(strt_odmtr_ct) - MIN(strt_odmtr_ct))) AS dist_trvl FROM asrt_dstne_dtl_t adt
JOIN asrt_lst_t alt on adt.asrt_id=alt.asrt_id
WHERE ADDDATE(adt.trvl_dt,INTERVAL adt.trvl_hr HOUR) BETWEEN '${data.fromdt}' and '${data.todt}' AND trvl_hr > 0
AND alt.asrt_grp_id in (8,14) and alt.asrt_type_id in (1,4) and alt.asrt_ctgry_id in (1,4,5)) as b
JOIN
(SELECT COUNT(DISTINCT fdt.fnce_id) as fncs_cvd FROM fnce_in_out_dtl_t fdt
JOIN asrt_lst_t alt ON fdt.asrt_id=alt.asrt_id
JOIN fncs_lst_t flt ON fdt.fnce_id=flt.fnce_id
JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id
WHERE ${condition} and TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm AND flt.a_in=1
AND fnce_in_ts BETWEEN '${data.fromdt}' and '${data.todt}' AND flt.fnce_grp_id=1) as c
JOIN
(SELECT COUNT(DISTINCT fdt.fnce_id) as bns_cvd FROM fnce_in_out_dtl_t fdt
JOIN asrt_lst_t alt ON fdt.asrt_id=alt.asrt_id
JOIN fncs_lst_t flt ON fdt.fnce_id=flt.fnce_id
JOIN fncs_grp_lst_t fg on flt.fnce_grp_id=fg.fnce_grp_id
WHERE ${condition} and TIME_TO_SEC(TIMEDIFF(fdt.fnce_out_ts,fdt.fnce_in_ts)) / 60 > fg.cvrg_tm AND flt.a_in=1
AND fnce_in_ts BETWEEN '${data.fromdt}' and '${data.todt}' AND flt.fnce_grp_id=3) as d
JOIN
(SELECT (COUNT(trp_rn_id) * capacity_ct) as cpty_cnt FROM trp_rn_dtl_t t
JOIN asrt_lst_t alt on t.asrt_id=alt.asrt_id 
WHERE ${condition} and actl_strt_ts BETWEEN '${data.fromdt}' and '${data.todt}' AND actl_strt_ts is NOT NULL and alt.a_in=1 AND t.a_in=1) as e
JOIN
(SELECT SUM(fuel_ct) as ful_ct FROM fuel_dtl_t fl
JOIN asrt_lst_t alt on fl.asrt_id=alt.asrt_id 
WHERE ${condition} and date BETWEEN '${data.fromdt}' and '${data.todt}') as f`;
console.log(QRY_TO_EXEC);
    if(callback && typeof callback == "function")
        dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls,function(err,results){
            callback(err,results);
            return;
        });
    else
       return dbutil.execQuery(sqldb.MySQLConPool,QRY_TO_EXEC,cntxtDtls);
};