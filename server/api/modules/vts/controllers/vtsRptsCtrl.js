// Standard Inclusions
var cognitoConfig = require('./../../../../../config/aws-cognito.config.json');
var AWS = require('aws-sdk');
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cognitoUtils= require(appRoot+'/utils/cognito.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);
var usrmngtVld = require(appRoot+'/server/api/modules/vts/validators/vtsMasterVld');

// Model Inclusions
var vtsrptsmdl = require('../models/vtsReportsMdl');


/**************************************************************************************
* Controller     : fence_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.fnceRpt_post = function(req, res) {
    var fnm="fnceRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${fnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getFnceRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,fnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,fnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : fence_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.lndmrkRpt_post = function(req, res) {
    var lnm="lndmrkRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${lnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getLndmrkRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lnm,{}); 
                });
        }
    });

}


/**************************************************************************************
* Controller     : trip_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.tripRpt_post = function(req, res) {
    var tnm="tripRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${tnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getTripRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : tripRptbyVehId_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - Chaitanya Krishna Balaga - Initial Function
*
***************************************************************************************/
exports.tripRptbyVehId_get = function(req, res) {
    var tnm="tripRptbyVehId_get";
    log.message("INFO",cntxtDtls,100,`In ${tnm}`);
    var veh_id = req.params.vehId;
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getTripRptLstbyVehId({veh_id})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tnm,{}); 
                });
        }
    });

}


/**************************************************************************************
* Controller     : lndmrkcov_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.lndmrkcovRpt_post = function(req, res) {
    var lndmrkcovnm="lndmrkcovRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkcovnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkcovnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getLndmrkCovRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkcovnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkcovnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : Distance_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.DistRpt_post = function(req, res) {
    var distnm="DistRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${distnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,distnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getDistRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,distnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,distnm,{}); 
                });
        }
    });
}


/**************************************************************************************
* Controller     : MileageRpt_get
* Parameters     : None
* Description    : 
* Change History :
* 06/06/2017    - Surya Teja Tanuku - Initial Function
*
***************************************************************************************/
exports.MileageRpt_get = function(req, res) {
    var mileageNm="MileageRpt_get";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${mileageNm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,mileageNm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getMileageRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,mileageNm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,mileageNm,{}); 
                });
        }
    });
}



/**************************************************************************************
* Controller     : heatMapRptWeekly_get
* Parameters     : None
* Description    : 
* Change History :
*12/08/2017    - Surya Teja Tanuku - Initial Function
*
***************************************************************************************/
exports.heatMapRptWeekly_get = function(req, res) {
        log.message("INFO", cntxtDtls, 100, "test message");
        var data = req.body;
        console.log(data);
    vtsrptsmdl.getHeatMapRpt(data)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'heatMapRptWeekly_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'heatMapRptWeekly_get', {});
        });
}




exports.fusionKeys_get = function(req, res) {

    log.message("INFO", cntxtDtls, 100, "test message");

    vtsrptsmdl.fusion_get({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'fusionKeys_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'fusionKeys_get', {});
        });
}


exports.fusionKeysByDt_get = function(req, res) {

    var dt =  req.params.dt;
    console.log(dt);

    log.message("INFO", cntxtDtls, 100, "test message");

    vtsrptsmdl.fusionKeysByDt(dt)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'fusionKeysByDt_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'fusionKeysByDt_get', {});
        });
}




/**************************************************************************************
 * Controller     : cvrdDumpersStatus_get
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.cvrdDumpersStatus_get = function(req, res) {
    console.log("covered dumping status report");
    var dumpCvrdNm="dumpCoveredRpt_get";
    var data = req.body;
    // console.log(req.body);

    log.message("INFO", cntxtDtls, 100, 'In ${dumpCoveredRpt_get}');
    vtsrptsmdl.getDumpers_status(req.body)
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'cvrdDumpersStatus_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'cvrdDumpersStatus_get', {});
        });
}

/**************************************************************************************
 * Controller     : crewPrfmnceData_post
 * Parameters     : None
 * Description    : 
 * Change History :
 * 01/05/2017    - Surya Teja Tanuku
 *
 ***************************************************************************************/
exports.crewPrfmnceData_post = function(req, res) {
    console.log("driver performance status report");
    var crewPrfmnceRpt="driverPerformanceRpt_get";
    var data = req.body;
    // console.log(req.body);

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,crewPrfmnceRpt,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.postCrewPrfmnceRpt(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,crewPrfmnceRpt,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,crewPrfmnceRpt,{}); 
                });
        }
    });
}


exports.getCollecType_get = function(req, res) {

    log.message("INFO", cntxtDtls, 100, "test message");

    vtsrptsmdl.getCollecType({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'getCollecType_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'getCollecType_get', {});
        });

}





/**************************************************************************************
* Controller     : trip_details_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.TripDtlsRpt_post = function(req, res) {
    var tripdtlsnm="TripDtlsRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${tripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getTripDtlsRptLst(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tripdtlsnm,{}); 
                });
        }
    });
}


/**************************************************************************************
* Controller     : all_trip_details_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.allTripDtlsRpt_get = function(req, res) {
    var alltripdtlsnm="allTripDtlsRpt_get";
    var trip_run_id=req.params.trp_rn_id;
    log.message("INFO",cntxtDtls,100,`In ${alltripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,alltripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getallTripDtlsRptLst(trip_run_id)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,alltripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,alltripdtlsnm,{}); 
                });
        }
    });
}


/**************************************************************************************
* Controller     : all_trip_details_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.LndMrkNtCovLst_get = function(req, res) {
    var lndmkntcovnm="LndMrkNtCovLst_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmkntcovnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmkntcovnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getlndMksNtCovLst()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmkntcovnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmkntcovnm,{}); 
                });
        }
    });
}




exports.fenceCategories_get = function(req, res) {

    log.message("INFO", cntxtDtls, 100, "test message");

    vtsrptsmdl.fenceCate_get({})
        .then(function(results) {
            df.formatSucessRes(res, results, cntxtDtls, 'fenceCategories_get', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'fenceCategories_get', {});
        });

}



exports.vehRptgetData_post = function(req, res) {
    var vehRptgetDataNm="vehRptgetData_post";
    var data = req.body;

    log.message("INFO", cntxtDtls, 100, 'In ${vehRptgetData_post}');
    vtsrptsmdl.getVehRpt_post(data)
        .then(function(singleAsrtDtl) {
            // console.log(results);
                vtsrptsmdl.getLastData(data)
                   .then(function(groupAsrtDtl){
                    df.formatSucessRes(res,
                        {
                            "singleAsrtDtl":singleAsrtDtl,
                            "groupAsrtDtl":groupAsrtDtl},
                            cntxtDtls,'vehRptgetDataNm',{});  
                        },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,'vehRptgetDataNm',{}); 
                });
            //df.formatSucessRes(res, results, cntxtDtls, 'vehRptgetData_post', {});
        }, function(error) {
            df.formatErrorRes(res, error, cntxtDtls, 'vehRptgetData_post', {});
        });
}


/**************************************************************************************
* Controller     : WrdCovgDtlsRpt_post
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.wrdCovgDtlsRpt_post = function(req, res) {
    var tripdtlsnm="wrdCovgDtlsRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${tripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getWrdCovgDtlsRpt(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tripdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : totlVehCnt_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totlVehCnt_get = function(req, res) {
    var tripdtlsnm="totlVehCnt_get";
    log.message("INFO",cntxtDtls,100,`In ${tripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.gettotlVehCnt()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tripdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehCntErly_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehCntErly_get = function(req, res) {
    var tripdtlsnm="vehCntErly_get";
    log.message("INFO",cntxtDtls,100,`In ${tripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getvehCntErly()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tripdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehCntMidl_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehCntMidl_get = function(req, res) {
    var tripdtlsnm="vehCntMidl_get";
    log.message("INFO",cntxtDtls,100,`In ${tripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getvehCntMidl()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tripdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehCntDly_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehCntDly_get = function(req, res) {
    var tripdtlsnm="vehCntDly_get";
    log.message("INFO",cntxtDtls,100,`In ${tripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getvehCntDly()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tripdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehdtls_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehdtlRpt_post = function(req, res) {
    var distnm="vehdtlRpt_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${distnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,distnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getvehdtlRpt(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,distnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,distnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchTripDtl_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchTripDtl_get = function(req, res) {
    var alltripdtlsnm="vehEchTripDtl_get";
    var vehid=req.params.asrt_id;
    var date=req.params.dt;
    log.message("INFO",cntxtDtls,100,`In ${alltripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,alltripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getvehEchTripDtl(vehid,date)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,alltripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,alltripdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchTripFncDtl_get
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchTripFncDtl_get = function(req, res) {
    var alltripdtlsnm="vehEchTripFncDtl_get";
    var trp_rn_id=req.params.trp_rn_id;
    log.message("INFO",cntxtDtls,100,`In ${alltripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,alltripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getvehEchTripFncDtl(trp_rn_id)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,alltripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,alltripdtlsnm,{}); 
                });
        }
    });
}


/**************************************************************************************
* Controller     : AllFncsForTripDtl_get
* Parameters     : None
* Description    : Get all fences for a Trip run Id
* Change History :
* 06/28/2017    - Sunil Mulagada - Initial Function
*
***************************************************************************************/
exports.AllFncsForTripDtl_get = function(req, res) {
    var alltripdtlsnm="vehEchTripFncDtl_get";
    var trp_rn_id=req.params.trp_rn_id;
    log.message("INFO",cntxtDtls,100,`In ${alltripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,alltripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.getAllFncsForTripDtl(trp_rn_id)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,alltripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,alltripdtlsnm,{}); 
                });
        }
    });
}

// /**************************************************************************************
// * Controller     : hrlyFncsCovd_get
// * Parameters     : None
// * Description    : 
// * Change History :
// * 11/05/2017    - SrujanaM - Initial Function
// *
// ***************************************************************************************/
// exports.hrlyFncsCovd_get = function(req, res) {
//     var alltripdtlsnm="hrlyFncsCovd_get";
//     log.message("INFO",cntxtDtls,100,`In ${alltripdtlsnm}`);
//     req.getValidationResult().then(function(result) {
//         if (!result.isEmpty()) {
//                 df.forParamErrorRes(res,result,cntxtDtls,alltripdtlsnm,util.inspect(result.array()),{}); 
//                 return;
//         } else {  // Model gets called Here 
//                 vtsrptsmdl.gethrlyFncsCovd()
//                 .then(function(results){
//                     df.formatSucessRes(res,results,cntxtDtls,alltripdtlsnm,{});  
//                 },function(error){
//                     df.formatErrorRes(res,error,cntxtDtls,alltripdtlsnm,{}); 
//                 });
//         }
//     });
// }

/**************************************************************************************
* Controller     : hrlyFncsCovd_post
* Parameters     : None
* Description    : 
* Change History :
* 11/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hrlyFncsCovd_post = function(req, res) {
    var alltripdtlsnm="hrlyFncsCovd_post";
    var data=req.body;
    console.log(data);
    log.message("INFO",cntxtDtls,100,`In ${alltripdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,alltripdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsrptsmdl.gethrlyFncsCovd(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,alltripdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,alltripdtlsnm,{}); 
                });
        }
    });
}
