// Standard Inclusions
var cognitoConfig = require('./../../../../../config/aws-cognito.config.json');
var AWS = require('aws-sdk');
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cognitoUtils= require(appRoot+'/utils/cognito.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);
var vtsMasterVld = require(appRoot+'/server/api/modules/vts/validators/vtsMasterVld');

// Model Inclusions
var vtsasrtmdl = require('../models/vtsAssetsMdl');


/**************************************************************************************
* Controller     : fence_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.asrtLst_get = function(req, res) {
    var anm="asrtLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.getAsrtLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : fence_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.asrtTypeLst_get = function(req, res) {
    var anm="asrtTypeLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.getAsrtTypeLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : typeOfVendors_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.typeOfVendors_get = function(req, res) {
    var anm="typeOfVendors_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.typeOfVendors({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : addAsrts_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.addAsrts_post = function(req, res) {
    var anm="addAsrts_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;

    req.checkBody(vtsMasterVld.addAssertsVld.body);

    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.addAssertsIntoDb(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : updateAsrts_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.updateAsrts_post = function(req, res) {
    var anm="updateAsrts_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    var asrt_id = req.params;

    req.checkParams(vtsMasterVld.updateAssertsVld.params);

    req.checkBody(vtsMasterVld.addAssertsVld.body);

    req.getValidationResult(req.body,req.params).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.updAssertsIntable(data,asrt_id.asrtId)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : GetAsrtLstById_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.GetAsrtLstById_get = function(req, res) {
    var anm="GetAsrtLstById_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var reqObject=[];
    var condition= '';
    var string = '';
    var data = req.body;

    for(var i=0;i<data.length;i++) {
        string = data[i].cname +"=" + data[i].cvalue;
        // condition = string;
        condition = condition+ " and " +string;
    }
    console.log(condition);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {    
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.GetAsrtLstById(condition)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : asrtCtgryLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.asrtCtgryLst_get = function(req, res) {
    var anm="asrtCtgryLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.asrtCtgryLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : asrtGroupLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.asrtGroupLst_get = function(req, res) {
    var anm="asrtGroupLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.asrtGroupLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : deviceLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.deviceLst_get = function(req, res) {
    var anm="deviceLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.deviceLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : tenantLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.tenantLst_get = function(req, res) {
    var anm="tenantLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.tenantLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : clientLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.clientLst_get = function(req, res) {
    var anm="clientLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.clientLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : ownersLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.ownersLst_get = function(req, res) {
    var anm="ownersLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,fnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.ownersLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : addAsrtType_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.addAsrtType_post = function(req, res) {
    var anm="addAsrtType_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);

    req.checkBody(vtsMasterVld.addAssertTypeVld.body);

    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.addAsrtType(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : updAsrtType_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.updAsrtType_post = function(req, res) {
    var anm="updAsrtType_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);

    req.checkBody(vtsMasterVld.addAssertTypeVld.body);
    
    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.updAsrtType(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : addDepartment_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.addDepartment_post = function(req, res) {
    var anm="addDepartment_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);

    req.checkBody(vtsMasterVld.addDeprtmntVld.body);
    
    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.addDepartment(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : updDepartment_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.updDepartment_post = function(req, res) {
    var anm="updDepartment_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);

    req.checkBody(vtsMasterVld.addDeprtmntVld.body);
    req.checkBody(vtsMasterVld.updDeprtmntVld.body);
    
    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.updDepartment(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : deptmntsLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.deptmntsLst_get = function(req, res) {
    var anm="deptmntsLst_get";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.deptmntsLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : deptmntsLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.deleteAsrts_post = function(req, res) {
    var anm="deleteAsrts_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;

    req.checkBody(vtsMasterVld.delassertVld.body);        // validating assert_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.deleteAsrts(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : delAsrtType_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.delAsrtType_post = function(req, res) {
    var anm="delAsrtType_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;

    req.checkBody(vtsMasterVld.delassertTypeVld.body);        // validating assert_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.delAsrtType(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : delDepartment_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.delDepartment_post = function(req, res) {
    var anm="delDepartment_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    
    req.checkBody(vtsMasterVld.delDepartmentVld.body);        // validating dprt_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.delDepartment(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : delDepartment_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.delDepartment_post = function(req, res) {
    var anm="delDepartment_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    
    req.checkBody(vtsMasterVld.delDepartmentVld.body);        // validating assert_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.delDepartment(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : AddAsrtCtgry_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.AddAsrtCtgry_post = function(req, res) {
    var anm="AddAsrtCtgry_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);
    
    req.checkBody(vtsMasterVld.AddAsertCtgry.body);        // validating assert_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.AddAsrtCtgry(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : UpdAsrtCtgry_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.UpdAsrtCtgry_post = function(req, res) {
    var anm="UpdAsrtCtgry_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);
    
    req.checkBody(vtsMasterVld.AddAsertCtgry.body);        // validating assert_id
    req.checkBody(vtsMasterVld.updAsertCtgry.body);        // validating assert_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.UpdAsrtCtgry(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : delAsrtCtgry_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.delAsrtCtgry_post = function(req, res) {
    var anm="delAsrtCtgry_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;
    console.log(data);
    
    // req.checkBody(vtsMasterVld.AddAsertCtgry.body);        // validating assert_id
    // req.checkBody(vtsMasterVld.updAsertCtgry.body);        // validating assert_id

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.delAsrtCtgry(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : addGroup_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.addGroup_post = function(req, res) {
    var anm="addGroup_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;

    req.checkBody(vtsMasterVld.addAsertGroup.body);

    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.addGroup(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : updGroup_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.updGroup_post = function(req, res) {
    var anm="updGroup_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;

    req.checkBody(vtsMasterVld.addAsertGroup.body);
    req.checkBody(vtsMasterVld.updAsertGroup.body);

    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.updGroup(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : delGroup_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna balaga - Initial Function
*
***************************************************************************************/
exports.delGroup_post = function(req, res) {
    var anm="delGroup_post";
    log.message("INFO",cntxtDtls,100,`In ${anm}`);

    var data = req.body;

    req.checkBody(vtsMasterVld.updAsertGroup.body);

    req.getValidationResult(req.body).then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,anm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsasrtmdl.delGroup(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,anm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,anm,{}); 
                });
        }
    });
}