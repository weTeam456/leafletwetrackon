// Standard Inclusions
var cognitoConfig = require('./../../../../../config/aws-cognito.config.json');
var AWS = require('aws-sdk');
var log         = require(appRoot+'/utils/logmessages');
var std         = require(appRoot+'/utils/standardMessages');
var df          = require(appRoot+'/utils/dflower.utils');
var cognitoUtils= require(appRoot+'/utils/cognito.utils');
var cntxtDtls   = df.getModuleMetaData(__dirname,__filename);
var usrmngtVld = require(appRoot+'/server/api/modules/vts/validators/vtsMasterVld');

// Model Inclusions
var vtsdshbrdchrtsmdl = require('../models/vtsDshBrdChrtsMdl');


/**************************************************************************************
* Controller     : lndmrkscov_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.LndMrksCovDtls_get = function(req, res) {
    var lndmrkdtlsnm="LndMrksCovDtls_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getLndMrkDtlsLst({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : LndMrksCovDtlsbyVehId_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - Chaitanya Krishna Balaga- Initial Function
*
***************************************************************************************/
exports.LndMrksCovDtlsbyVehId_get = function(req, res) {
    var lndmrkdtlsnm="LndMrksCovDtlsbyVehId_get";
    var veh_id = req.params.vehId;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getLndMrkDtlsLstbyVehId({veh_id})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : Daywise Distance Count
* Parameters     : None
* Description    : 
* Change History :
* 18/05/2017    - RamyaM - Initial Function
*
***************************************************************************************/
exports.dywseDstnsCnt_get = function(req, res) {
    var dywseDstnsCntDtl="dywseDstnsCnt_get";
    log.message("INFO",cntxtDtls,100,`In ${dywseDstnsCntDtl}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,dywseDstnsCntDtl,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            // console.log("RamsSri");
                vtsdshbrdchrtsmdl.getDywseDstnsCntDtl({})
                .then(function(results){
                    // console.log(results);
                    df.formatSucessRes(res,results,cntxtDtls,dywseDstnsCntDtl,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,dywseDstnsCntDtl,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : dywseDstnsCntbyVehId_get
* Parameters     : None
* Description    : Daywise Distance Count
* Change History :
* 18/05/2017    - RamyaM - Initial Function
*
***************************************************************************************/
exports.dywseDstnsCntbyVehId_get = function(req, res) {
    var dywseDstnsCntDtl="Daywise Distance Count";

    var veh_id = req.params.vehId;

    log.message("INFO",cntxtDtls,100,`In ${dywseDstnsCntDtl}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,dywseDstnsCntDtl,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            console.log("RamsSri");
                vtsdshbrdchrtsmdl.getDywseDstnsCntDtlbyVehId({veh_id})
                .then(function(results){
                    // console.log(results);
                    df.formatSucessRes(res,results,cntxtDtls,dywseDstnsCntDtl,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,dywseDstnsCntDtl,{}); 
                });
        }
    });
}


/**************************************************************************************
* Controller     : vehicle_sts_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehStsCntsDtls_get = function(req, res) {
    var vehstsdtlnm="vehStsCntsDtls_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getVehStsCntDtl({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });

}


/**************************************************************************************
* Controller     : vehicle_cnts_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totVehCntDtls_get = function(req, res) {
    var vehcntdtlnm="totVehCntDtls_get";
    log.message("INFO",cntxtDtls,100,`In ${vehcntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehcntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getVehCntDtl({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehcntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehcntdtlnm,{}); 
                });
        }
    });

}


/**************************************************************************************
* Controller     : vehicle_cnts_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totShVehCntDtls_get = function(req, res) {
    var vehcntdtlnm="totShVehCntDtls_get";
    log.message("INFO",cntxtDtls,100,`In ${vehcntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehcntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getShVehCntDtl({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehcntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehcntdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehicle_cnts_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totVehStsDtl_get = function(req, res) {
    var totvehstsdtlnm="totVehStsDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${totvehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,totvehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getTotVehStsDtl({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,totvehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,totvehstsdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehicle_cnts_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totTrpDistCntDtl_get = function(req, res) {
    var tottrpdstdtlnm="totTrpDistCntDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${tottrpdstdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,tottrpdstdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getTotTrpDistDtl({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,tottrpdstdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,tottrpdstdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehicle_cnts_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totColPntsCntDtl_get = function(req, res) {
    var totcolpntdtlnm="totColPntsCntDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${totcolpntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,totcolpntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getTotColPntsDtl({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,totcolpntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,totcolpntdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehDshChrtDtl_get
* Parameters     : None
* Description    : 
* Change History :
* 09/06/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.vehDshChrtDtl_get = function(req, res) {
    var vehdtlsnm="vehDshChrtDtl_get";
    var asrt_id=req.params.asrt_id;
    var cat_id=req.params.cat_id;

    log.message("INFO",cntxtDtls,100,`In ${vehdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            var results = [];
                vtsdshbrdchrtsmdl.getVehDstChrtDtl(asrt_id)
                .then(function(data1){
                    results.push(data1);
                    vtsdshbrdchrtsmdl.getTotVehDstChrtDtl(cat_id)
                    .then(function(data2){
                        results.push(data2);
                        // console.log(results)
                        df.formatSucessRes(res,results,cntxtDtls,vehdtlsnm,{});  
                    },function(error){
                        df.formatErrorRes(res,error,cntxtDtls,vehdtlsnm,{}); 
                    }); 
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehTrpCvgChrt_get
* Parameters     : None
* Description    : 
* Change History :
* 09/06/2017    - Sony Angel - Initial Function
*
***************************************************************************************/
exports.vehTrpCvgChrt_get = function(req, res) {
    var vehdtlsnm="vehTrpCvgChrt_get";
    var asrt_id=req.params.asrt_id;
    var cat_id=req.params.cat_id;

    log.message("INFO",cntxtDtls,100,`In ${vehdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            var results = [];
                vtsdshbrdchrtsmdl.getVehTrpCvgChrt(asrt_id)
                .then(function(data1){
                    results.push(data1);
                    vtsdshbrdchrtsmdl.getTotVehTrpCvgChrt(cat_id)
                    .then(function(data2){
                        results.push(data2);
                        // console.log(results)
                        df.formatSucessRes(res,results,cntxtDtls,vehdtlsnm,{});  
                    },function(error){
                        df.formatErrorRes(res,error,cntxtDtls,vehdtlsnm,{}); 
                    });   
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehBinCvgChrt_get
* Parameters     : None
* Description    : 
* Change History :
* 09/06/2017   - Sony Angel - Initial Function
*
***************************************************************************************/
exports.vehBinCvgChrt_get = function(req, res) {
    var vehdtlsnm="vehBinCvgChrt_get";
    var asrt_id=req.params.asrt_id;
    var cat_id=req.params.cat_id;

    log.message("INFO",cntxtDtls,100,`In ${vehdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            var results = [];
                vtsdshbrdchrtsmdl.getVehBinCvgChrt(asrt_id)
                .then(function(data1){
                    results.push(data1);
                    vtsdshbrdchrtsmdl.getTotVehBinCvgChrt(cat_id)
                    .then(function(data2){
                        results.push(data2);
                        // console.log(results)
                        df.formatSucessRes(res,results,cntxtDtls,vehdtlsnm,{});  
                    },function(error){
                        df.formatErrorRes(res,error,cntxtDtls,vehdtlsnm,{}); 
                    });     
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : vehicle_active_dtls_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehDshStsDtl_get = function(req, res) {
    var vehstsnm="vehDshStsDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getvehDshStsDtl()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totLndMrksCntDtl_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totLndMrksCntDtl_get = function(req, res) {
    var totlndmrknm="totLndMrksCntDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${totlndmrknm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,totlndmrknm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotLndMrksDtl()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,totlndmrknm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,totlndmrknm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : totLndMrksCovDtl_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totLndMrksCovDtl_get = function(req, res) {
    var totlndmrkcovnm="totLndMrksCovDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${totlndmrkcovnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,totlndmrkcovnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotLndMrksCovDtl()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,totlndmrkcovnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,totlndmrkcovnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : lndMrksNtCovDtl_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.lndMrksNtCovDtl_get = function(req, res) {
    var lndmrkntcovnm="lndMrksNtCovDtl_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkntcovnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkntcovnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getlndMrksNtCovDtl()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkntcovnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkntcovnm,{}); 
                });
        }
    });
}


/**************************************************************************************
* Controller     : lndMrksLst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.lndMrksLst_get = function(req, res) {
    var alllndmrksnm="lndMrksLst_get";
    log.message("INFO",cntxtDtls,100,`In ${alllndmrksnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,alllndmrksnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getalllndMrksDtl()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,alllndmrksnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,alllndmrksnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : Landmrk_dtls_lst_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.lndMrkDshChrtLst_get = function(req, res) {
    var lndmrkdtlsnm="lndMrkDshChrtLst_get";
    var fnce_id=req.params.fnce_id;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getLndMrkDshChrtDtl(fnce_id)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}
/**************************************************************************************
* Controller     : vehTypeTrpDtls_get
* Parameters     : None
* Description    : Daywise Distance Count
* Change History :
* 18/05/2017    - Chaitanya Krishna Balaga - Initial Function
*
***************************************************************************************/
exports.vehTypeTrpDtls_get = function(req, res) {
    var vehTypeTrpDtls="veh Type TrpDtls";

    log.message("INFO",cntxtDtls,100,`In ${vehTypeTrpDtls}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehTypeTrpDtls,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
            console.log("RamsSri");
                vtsdshbrdchrtsmdl.getvehTypeTrpDtlsMdl()
                .then(function(results){
                    // console.log(results);
                    df.formatSucessRes(res,results,cntxtDtls,vehTypeTrpDtls,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehTypeTrpDtls,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : totTrctsDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totTrctsDstTrvld_get = function(req, res) {
    var lndmrkdtlsnm="totTrctsDstTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotTrctsDstTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totDstCrDyTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totDstCrDyTrvld_get = function(req, res) {
    var lndmrkdtlsnm="totDstCrDyTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotDstCrDyTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totFnceCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totFnceCovd_get = function(req, res) {
    var lndmrkdtlsnm="totFnceCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotFnceCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totFnceDyCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totFnceDyCovd_get = function(req, res) {
    var lndmrkdtlsnm="totFnceDyCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.totFnceDyCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totFnceBnsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totFnceBnsCovd_get = function(req, res) {
    var lndmrkdtlsnm="totFnceBnsCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotFnceBnsCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totFnceBnsDyCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totFnceBnsDyCovd_get = function(req, res) {
    var lndmrkdtlsnm="totFnceBnsDyCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.totFnceBnsDyCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });

}
/**************************************************************************************
* Controller     : mcrEchVehCntDtls_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchVehCntDtls_get = function(req, res) {
    var vehcntdtlnm="mcrEchVehCntDtls_get";
    log.message("INFO",cntxtDtls,100,`In ${vehcntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehcntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchVehCntDtls({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehcntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehcntdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : shEchVehCntDtls_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shEchVehCntDtls_get = function(req, res) {
    var vehcntdtlnm="shEchVehCntDtls_get";
    log.message("INFO",cntxtDtls,100,`In ${vehcntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehcntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshEchVehCntDtls({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehcntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehcntdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totTripsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totTripsCovd_get = function(req, res) {
    var vehcntdtlnm="totTripsCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehcntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehcntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotTripsCovd({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehcntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehcntdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totTripsDyCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totTripsDyCovd_get = function(req, res) {
    var vehcntdtlnm="totTripsDyCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehcntdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehcntdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotTripsDyCovd({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehcntdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehcntdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : subscriptions_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.subscriptions_get = function(req, res) {
    var lndmrkdtlsnm="subscriptions_get";
    var usr_id=req.params.usr_id;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getSubscriptions(usr_id)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : alertCtgry_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.alertCtgry_get = function(req, res) {
    var lndmrkdtlsnm="alertCtgry_get";
    // var usr_id=req.params.usr_id;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getAlertCtgry()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : newSubscriber_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
// exports.newSubscriber_post = function(req, res) {
//     var lndmrkdtlsnm="newSubscriber_post";
//     var data=req.body;
//     log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
//     req.getValidationResult().then(function(result) {
//         if (!result.isEmpty()) {
//                 df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
//                 return;
//         } else {  // Model gets called Here 
//             // console.log(data)
//             var count=0;
//             for(var i=0;i<data.alert_cat_id.length;i++){
//                 vtsdshbrdchrtsmdl.newSubscriber(data,data.alert_cat_id[i])
//                 .then(function(results){
//                     count++;
//                     if(count==data.alert_cat_id.length)
//                         df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
//                 },function(error){
//                     df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
//                 });
//             }
//         }
//     });
// }

exports.newSubscriber_post = function(req, res) {
    var ERR_SEND=false;
    var t_params = req.body;
    var error_log=0;
    var success_log=0;
    var count=t_params.alert_cat_id.length;
    var check=0;

    console.log(t_params);
      
      for (var j = 0; j < t_params.alert_cat_id.length; j++) {
           
           var data={
            "usr_id":t_params.usr_id,
            "alert_cat_id":t_params.alert_cat_id[j],
            "sms_alert_in":t_params.sms_alert_in_top,
            "app_alert_in":t_params.app_alert_in_top,
            "email_alert_in":t_params.email_alert_in_top
           };
           
           vtsdshbrdchrtsmdl.addNewSubscriber(data,function(err, results) {
                if(err){error_log++;check++;}
                else{success_log++;check++;}
                
                if(check==count){
                    res.send({"status":std.message.SUCCESS.code,"message":"updated Successfully","data":{"success":success_log,"error":error_log}});return;
                } 
            });
      }
}

/**************************************************************************************
* Controller     : updSubscription_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.updSubscription_post = function(req, res) {
    var lndmrkdtlsnm="updSubscription_post";
    var data=req.body;
    // console.log(data);
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.updSubscription(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });
}
/**************************************************************************************
* Controller     : allCrewList_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.allCrewList_get = function(req, res) {
    var lndmrkdtlsnm="allCrewList_get";
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getAllCrewList()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : AlertList_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.AlertList_get = function(req, res) {
    var lndmrkdtlsnm="AlertList_get";
    var data=req.body;
    var ctgry_id;

    for(var i=0;i<data.alert_cat_id.length;i++) {
        (ctgry_id) ? ctgry_id = ctgry_id+ ' , '+data.alert_cat_id[i] : ctgry_id = data.alert_cat_id[i];
    }
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here
                vtsdshbrdchrtsmdl.getAlertList(data,ctgry_id)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : driverAsigDtls_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.driverAsigDtls_get = function(req, res) {
    var lndmrkdtlsnm="driverAsigDtls_get";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here
                

                vtsdshbrdchrtsmdl.closeExpiredRecords(function(err, fromCloseDtls) {
                    if(err){ console.log(err); }
                    if(fromCloseDtls) {
                        console.log("succesfully closed all outdated records");
                        // res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":{"success":"success_log","error":"no"}});return;

                        vtsdshbrdchrtsmdl.getDriverAsigDtls(data)
                        .then(function(results){
                            df.formatSucessRes(res,results,cntxtDtls,lndmrkdtlsnm,{});  
                        },function(error){
                            df.formatErrorRes(res,error,cntxtDtls,lndmrkdtlsnm,{}); 
                        });
                    }
                });
        }
    });
}

/**************************************************************************************
* Controller     : addFuelDtls_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.addFuelDtls_get = function(req, res) {
    var lndmrkdtlsnm="addFuelDtls_get";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here
                
                vtsdshbrdchrtsmdl.checkExistinRcds(data,function(err, checkDtls) {
                    if(err){ console.log(err); }
                    if(checkDtls.length>0) {
                        // updating old record with new fuel details
                        vtsdshbrdchrtsmdl.updatingFuelDetails(data,function(err, updFuelDtls) {
                            if(err){ console.log(err); }
                            if(updFuelDtls) {
                                console.log("succesfully Updated The old Record");
                                res.send({"status":331,"message":"Closed and Added Successfully","data":{"success":"success_log","error":"no"}});return;
                            }
                        });
                    } else {
                        vtsdshbrdchrtsmdl.addFuelDetails(data,function(err, fuelDtls) {
                            if(err){ console.log(err); }
                            if(fuelDtls) {
                                console.log("succesfully Added Record");
                                res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":{"success":"success_log","error":"no"}});return;
                            }
                        });
                    }
                });
                
                vtsdshbrdchrtsmdl.closeExpiredRcrds(data,function(err, fuelDtls) {
                    if(err){ console.log(err); }
                    if(fuelDtls) {
                        console.log("succesfully Closed Record");
                        // res.send({"status":std.message.SUCCESS.code,"message":"Closed Successfully","data":{"success":"success_log","error":"no"}});return;
                    }
                });
        }
    });
}

/**************************************************************************************
* Controller     : getAsrtsForFuel_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.getAsrtsForFuel_get = function(req, res) {
    var lndmrkdtlsnm="getAsrtsForFuel_get";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here
            vtsdshbrdchrtsmdl.getAsrtsForFuel(data,function(err, fuelDtls) {
                if(err){ console.log(err); }
                if(fuelDtls) {
                    // df.formatErrorRes(res,err,cntxtDtls,lndmrkdtlsnm); 
                    res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":fuelDtls});return;
                }
            });
        }
    });
}

/**************************************************************************************
* Controller     : asrtFuelHistory_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.asrtFuelHistory_get = function(req, res) {
    var lndmrkdtlsnm="asrtFuelHistory_get";
    var data=req.params;
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here
            vtsdshbrdchrtsmdl.getFuelHstory(data,function(err, fuelDtls) {
                if(err){ console.log(err); }
                if(fuelDtls) {
                    // df.formatErrorRes(res,err,cntxtDtls,lndmrkdtlsnm); 
                    res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":fuelDtls});return;
                }
            });
        }
    });
}

/**************************************************************************************
* Controller     : driverAssignmentDtls_post
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - chaitanya - Initial Function
*
***************************************************************************************/
exports.driverAssignmentDtls_post = function(req, res) {
    var lndmrkdtlsnm="driverAssignmentDtls_post";
    var finalData = [];
    var closedData = [];
    var data=req.body;
    // console.log(data);
    log.message("INFO",cntxtDtls,100,`In ${lndmrkdtlsnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,lndmrkdtlsnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 

            vtsdshbrdchrtsmdl.checkDriverFromDt(data,function(err, fromDtls) {
                if(err){ console.log(err); }
                if(fromDtls.length>0) {
                    var object = {
                        "asrt_nm" : fromDtls[0].asrt_nm,
                        "crw_nm" : fromDtls[0].crw_nm,
                        "asrt_id" : fromDtls[0].asrt_id,
                        "crw_id" : fromDtls[0].crw_id,
                        "frm_ts" : moment(fromDtls[0].frm_ts).format("YYYY-MM-DD HH:mm"),
                        "to_ts" : moment(fromDtls[0].to_ts).format("YYYY-MM-DD HH:mm"),
                        "close_entry_in" : 1
                    }
                    closedData.push(object);
                    vtsdshbrdchrtsmdl.closeExistingRecord(object,function(err, fromCloseDtls) {
                        if(err){ console.log(err); }
                        if(fromCloseDtls) {

                            vtsdshbrdchrtsmdl.checkingCrewExistance(data,function(err, crewDtls) {
                                if(err){ console.log(err); }
                                if(crewDtls.length > 0) {
                                    //Duplicate Driver Entered
                                    // Need to close already assigned driver record andinform them it was closed.

                                    var OldDrvrData = {
                                        "asrt_nm" : crewDtls[0].asrt_nm,
                                        "crw_nm" : crewDtls[0].crw_nm,
                                        "asrt_id" : crewDtls[0].asrt_id,
                                        "crw_id" : crewDtls[0].crw_id,
                                        "frm_ts" : moment(crewDtls[0].frm_ts).format("YYYY-MM-DD HH:mm"),
                                        "to_ts" : moment(crewDtls[0].to_ts).format("YYYY-MM-DD HH:mm"),
                                        "close_drvr_in" : 1
                                    }

                                    

                                    vtsdshbrdchrtsmdl.closeExistingRecord(OldDrvrData,function(err, closeDtls) {
                                        if(err){ console.log(err); }
                                        else if(closeDtls) {
                                            finalData.push(OldDrvrData,object);
                                            vtsdshbrdchrtsmdl.driverAssignmentDtls(data,function(err, addedDtls) {
                                                if(err){ console.log(err); }
                                                else if(addedDtls) {
                                                    // Need to send callback here
                                                    res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":finalData});return;
                                                }
                                            });
                                        }
                                    });

                                } else {
                                    vtsdshbrdchrtsmdl.driverAssignmentDtls(data,function(err, addDtls) {
                                        if(err){ console.log(err); }
                                        else if(addDtls) {
                                            // Need to send callback here
                                            res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":closedData});return;
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    vtsdshbrdchrtsmdl.checkDriverToDt(data,function(err, results) {
                        if(err){ }
                        if(results.length>0) {
                            var obj = {
                                "asrt_id" : results[0].asrt_id,
                                "crw_id" : results[0].crw_id,
                                "frm_ts" : moment(results[0].frm_ts).format("YYYY-MM-DD HH:mm"),
                                "to_ts" : moment(results[0].to_ts).format("YYYY-MM-DD HH:mm")
                            }

                            vtsdshbrdchrtsmdl.closeExistingRecord(obj,function(err, closeDtls) {
                                if(err){ console.log(err); }
                                else if(closeDtls) {
                                    res.send({"status":std.message.SUCCESS.code,"message":"Closed Successfully","data":{"success":"success_log","error":"error_log"}});return;
                                    // Need to send call back for closed entry successfully
                                }
                            });
                        } else {
                            
                            // checking the req record driver variation in the already having dates

                            vtsdshbrdchrtsmdl.closenotInRangeRecord(data,function(err, clsdDtls) {
                                    if(err){ console.log(err); }
                                    else if(clsdDtls) {
                                        vtsdshbrdchrtsmdl.driverAssignmentDtls(data,function(err, addDtls) {
                                            if(err){ console.log(err); }
                                            else if(addDtls) {
                                                // Need to send callback here
                                                res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":{"success":"success_log","error":"no"}});return;
                                            }
                                        });
                                    }
                                });
                        }
                    });
                }
            });
            
            vtsdshbrdchrtsmdl.closeExpiredRecords(function(err, fromCloseDtls) {
                if(err){ console.log(err); }
                if(fromCloseDtls) {
                    console.log("succesfully closed all outdated records");
                    // res.send({"status":std.message.SUCCESS.code,"message":"Closed and Added Successfully","data":{"success":"success_log","error":"no"}});return;
                }
            });

        }
    });
}


/**************************************************************************************
* Controller     : totVehCnt_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totVehCnt_get = function(req, res) {
    var totvehstsdtlnm="totVehCnt_get";
    log.message("INFO",cntxtDtls,100,`In ${totvehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,totvehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getTotVehCnt({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,totvehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,totvehstsdtlnm,{}); 
                });
        }
    });

}

/**************************************************************************************
* Controller     : totCapctyLftd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totCapctyLftd_get = function(req, res) {
    var vehstsdtlnm="totCapctyLftd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotCapctyLftd({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : totCapctyDyLftd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totCapctyDyLftd_get = function(req, res) {
    var vehstsdtlnm="totCapctyDyLftd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotCapctyDyLftd({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrVehsDistTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrVehsDistTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrVehsDistTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrVehsDistTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrVehsTiprDistTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrVehsTiprDistTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrVehsTiprDistTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrVehsTiprDistTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrVehsDmprsDistTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrVehsDmprsDistTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrVehsDmprsDistTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrVehsDmprsDistTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : hirdTrctsDistTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hirdTrctsDistTrvld_get = function(req, res) {
    var vehstsdtlnm="hirdTrctsDistTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethirdTrctsDistTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : hirdTipprsDistTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hirdTipprsDistTrvld_get = function(req, res) {
    var vehstsdtlnm="hirdTipprsDistTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethirdTipprsDistTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrTrctsTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrTrctsTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrTrctsTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrTrctsTripsTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrTipprsTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrTipprsTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrTipprsTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrTipprsTripsTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrDmprsTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrDmprsTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrDmprsTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrDmprsTripsTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}
/**************************************************************************************
* Controller     : hirdTrctsTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hirdTrctsTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="hirdTrctsTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethirdTrctsTripsTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : hirdTipprsTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hirdTipprsTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="hirdTipprsTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethirdTipprsTripsTrvld({})
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrColPntsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrColPntsCovd_get = function(req, res) {
    var vehstsdtlnm="mcrColPntsCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrColPntsCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : hrdColPntsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hrdColPntsCovd_get = function(req, res) {
    var vehstsdtlnm="hrdColPntsCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethrdColPntsCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrDmprBnsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrDmprBnsCovd_get = function(req, res) {
    var vehstsdtlnm="mcrDmprBnsCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrDmprBnsCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrTrctsCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrTrctsCpty_get = function(req, res) {
    var vehstsdtlnm="mcrTrctsCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrTrctsCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrTipprsCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrTipprsCpty_get = function(req, res) {
    var vehstsdtlnm="mcrTipprsCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrTipprsCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrDmprsCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrDmprsCpty_get = function(req, res) {
    var vehstsdtlnm="mcrDmprsCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrDmprsCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}
/**************************************************************************************
* Controller     : hirdTrctsCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hirdTrctsCpty_get = function(req, res) {
    var vehstsdtlnm="hirdTrctsCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethirdTrctsCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : hirdTipprsCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 10/05/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.hirdTipprsCpty_get = function(req, res) {
    var vehstsdtlnm="hirdTipprsCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gethirdTipprsCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchTrctDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchTrctDstTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrEchTrctDstTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchTrctDstTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchTipprDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchTipprDstTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrEchTipprDstTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchTipprDstTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchDmprDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchDmprDstTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrEchTipprDstTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchDmprDstTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchTractsDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchTractsDstTrvld_get = function(req, res) {
    var vehstsdtlnm="shahEchTractsDstTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchTractsDstTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchTipprDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchTipprDstTrvld_get = function(req, res) {
    var vehstsdtlnm="shahEchTipprDstTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchTipprDstTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchTrctTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchTrctTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrEchTrctTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchTrctTripsTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchTipprTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchTipprTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrEchTipprTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchTipprTripsTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchDmprTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchDmprTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="mcrEchDmprTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchDmprTripsTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchTractTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchTractTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="shahEchTractTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchTractTripsTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchTipprTripsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchTipprTripsTrvld_get = function(req, res) {
    var vehstsdtlnm="shahEchTipprTripsTrvld_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchTipprTripsTrvld()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchColPntCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchColPntCovd_get = function(req, res) {
    var vehstsdtlnm="mcrEchColPntCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchColPntCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchColPntCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchColPntCovd_get = function(req, res) {
    var vehstsdtlnm="shahEchColPntCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchColPntCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchTrctCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchTrctCpty_get = function(req, res) {
    var vehstsdtlnm="mcrEchTrctCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchTrctCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchTipprCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchTipprCpty_get = function(req, res) {
    var vehstsdtlnm="mcrEchTipprCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchTipprCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchDmprCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchDmprCpty_get = function(req, res) {
    var vehstsdtlnm="mcrEchDmprCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchDmprCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchTrctCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchTrctCpty_get = function(req, res) {
    var vehstsdtlnm="shahEchTrctCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchTrctCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : shahEchTipprCpty_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.shahEchTipprCpty_get = function(req, res) {
    var vehstsdtlnm="shahEchTipprCpty_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getshahEchTipprCpty()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchdyDstTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchdyDstTrvld_get = function(req, res) {
    var vehstsdtlnm="vehEchdyDstTrvld_get";
    var vehid=req.params.asrt_id;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getvehEchdyDstTrvld(vehid)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchdyTrpsTrvld_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchdyTrpsTrvld_get = function(req, res) {
    var vehstsdtlnm="vehEchdyTrpsTrvld_get";
    var vehid=req.params.asrt_id;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getvehEchdyTrpsTrvld(vehid)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchdyColPntsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchdyColPntsCovd_get = function(req, res) {
    var vehstsdtlnm="vehEchdyColPntsCovd_get";
    var vehid=req.params.asrt_id;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getvehEchdyColPntsCovd(vehid)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchdyDmprBnsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchdyDmprBnsCovd_get = function(req, res) {
    var vehstsdtlnm="vehEchdyDmprBnsCovd_get";
    var vehid=req.params.asrt_id;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getvehEchdyDmprBnsCovd(vehid)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : mcrEchDmprBnsCovd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.mcrEchDmprBnsCovd_get = function(req, res) {
    var vehstsdtlnm="mcrEchDmprBnsCovd_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getmcrEchDmprBnsCovd()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchdyCptyDmpd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.vehEchdyCptyDmpd_get = function(req, res) {
    var vehstsdtlnm="vehEchdyCptyDmpd_get";
    var vehid=req.params.asrt_id;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getvehEchdyCptyDmpd(vehid)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : vehEchdyCptyDmpd_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.allCropVehsDtl_post = function(req, res) {
    var vehstsdtlnm="allCropVehsDtl_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getallCropVehsDtl(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : asrtGrpLstbyType_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.asrtGrpLstbyType_get = function(req, res) {
    var vehstsdtlnm="asrtGrpLstbyType_get";
    var vehtypeid=req.params.asrt_type_id;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getasrtGrpLstbyType(vehtypeid)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : asrtDshbrdCtgryLst_get
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.asrtDshbrdCtgryLst_get = function(req, res) {
    var vehstsdtlnm="asrtDshbrdCtgryLst_get";
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.getasrtDshbrdCtgryLst()
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}

/**************************************************************************************
* Controller     : totalVehDtls_post
* Parameters     : None
* Description    : 
* Change History :
* 16/06/2017    - SrujanaM - Initial Function
*
***************************************************************************************/
exports.totalVehDtls_post = function(req, res) {
    var vehstsdtlnm="totalVehDtls_post";
    var data=req.body;
    log.message("INFO",cntxtDtls,100,`In ${vehstsdtlnm}`);
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
                df.forParamErrorRes(res,result,cntxtDtls,vehstsdtlnm,util.inspect(result.array()),{}); 
                return;
        } else {  // Model gets called Here 
                vtsdshbrdchrtsmdl.gettotalVehDtls(data)
                .then(function(results){
                    df.formatSucessRes(res,results,cntxtDtls,vehstsdtlnm,{});  
                },function(error){
                    df.formatErrorRes(res,error,cntxtDtls,vehstsdtlnm,{}); 
                });
        }
    });
}