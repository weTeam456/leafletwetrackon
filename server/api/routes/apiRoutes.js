var express = require('express');
var passport = require('passport');
var router = express.Router();

//Include Controller

var authCtrl     		= require('../modules/auth/authCtrl');
var adminCtrl     		= require('../modules/admin/controllers/adminCtrl');
var liveTrackCtrl		= require('../modules/livetracking/controllers/liveTrackingCtrl');
var playBackCtrl		= require('../modules/livetracking/controllers/playBackCtrl');
var geoCodeCtrl         = require('../modules/geocoding/controllers/geoCodeCtrl');
var umCtrl     		    = require('../modules/um/controllers/UserManagementCtrl');
var supportCtrl		    = require('../modules/support/controllers/supportCtrl');
var vtsRptsCtrl			= require('../modules/vts/controllers/vtsRptsCtrl');
var vtsAssetsCtrl		= require('../modules/vts/controllers/vtsAssetsCtrl');
var vtsDshBrdChrtsCtrl	= require('../modules/vts/controllers/vtsDshBrdChrtsCtrl');
var sanitationCtrl	= require('../modules/sanitation/controllers/drvrSntnCtrl');

//var appCtrl     		= require('../../controllers/apiv1/application/appCtrl');


/****************************************************************
 Authentication Routes
 *****************************************************************/

function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    // if (req.isAuthenticated())
        return next();
    // res.sendStatus(257);
}

/**** login ****/
router.post("/login", passport.authenticate('cognito-login', {}), authCtrl.login_sess);

/**** Select Organization ****/
router.get("/getOrgnsLst", authCtrl.getOrgnsLst);

/**** Go to login page ****/
router.get("/goToLgn/:tnt_id", authCtrl.get_goToLgn);

/**** forgot password ****/
router.post("/forgetPwd",authCtrl.forgot_pwd);

/**** change password ****/
router.post("/changePwd",authCtrl.change_pwd);

/**** Organisational Levels ****/
router.get("/logout", authCtrl.logout_sess);

/****************************************************************
					Admin related Routess
*****************************************************************/

// router.get('/application/app',isLoggedIn,adminCtrl.oprsHomeRpt_get);

/**** Organisational Levels ****/
router.get('/organisational/level',isLoggedIn,adminCtrl.allOrgLvls_get);
router.get('/organisational/level/:clientid',isLoggedIn,adminCtrl.allClntOrgLvls_get);
router.get('/organisational/level/:clientid/:tenantid',isLoggedIn,adminCtrl.tntOrgLvlsByClnt_get);

router.get('/client',isLoggedIn,adminCtrl.allClients_get);
// router.get('/client/:clientid',isLoggedIn,adminCtrl.oprsHomeRpt_get);
router.get('/client/:clientid/tenants',isLoggedIn,adminCtrl.allTntsByClnt_get);
// router.get('/client/:clientid/tenants/:tenantid',isLoggedIn,adminCtrl.oprsHomeRpt_get);

/**** Get Users - All Users, Client Users, Tenant Users  ****/
router.get('/users',isLoggedIn,adminCtrl.allUsersList_get);
router.get('/users/:clientid',isLoggedIn,adminCtrl.usersClntLst_get);
router.get('/users/:clientid/:tenantid',isLoggedIn,adminCtrl.usersTntLst_get);

/**** User Url Based Access Control ****/
router.get('/acl/usrAccess/:usrid',isLoggedIn,umCtrl.userUrlAccess_get);

/****************************************************************
					User Management (um) Module
*****************************************************************/
router.get('/user/appsLst/:usrid',isLoggedIn,umCtrl.userappsLst_get);

/**** User Menus ****/
router.get('/user/menuLst/:usrid/:appid',isLoggedIn,umCtrl.usermenu_get);


router.get('/designations/:clntId/:tntId',isLoggedIn,umCtrl.designation_get);
router.get('/allProfiles/:clntId/:tntId',isLoggedIn,umCtrl.allPrfl_get);

router.get('/getApps/:clntId/:tntId',isLoggedIn,umCtrl.apps_get);

/**** My Profile - update profile and change password ****/
router.post('/user/updateMyProfile',isLoggedIn,umCtrl.UserProfile_update);

/**** Users - create, delete, update ****/
router.post('/user/create',isLoggedIn,umCtrl.createUser_post);
router.post('/user/assignMenu/:usrid/:clntId/:tntId',isLoggedIn,umCtrl.assignMnuToUsr_get);
router.post('/user/delete',isLoggedIn,umCtrl.deleteUser_post);
router.post('/user/update',isLoggedIn,umCtrl.updateUser_post);

/**** Password Resets ****/
router.get('/getPwdResetUsr/:clntId/:tntId',isLoggedIn,umCtrl.PwdResetUsr_get);

/**** App Profile Routes ****/
router.get('/users/getappPrfl/:clntId/:tntId',isLoggedIn,umCtrl.usersAppPrfl_get);
router.post('/create/appPrf/:clntId/:tntId/:appPrflNm',isLoggedIn,umCtrl.createAppPrf_post);
router.post('/appPrf/assignusers/:appPrflNm',isLoggedIn,umCtrl.assignAppPrfToUsr_post);

/**** Menu Profile Routes ****/
router.get('/getmenuPrfl/:clntId/:tntId',isLoggedIn,umCtrl.usersMnuPrfl_get);
router.post('/create/menu/:clntId/:tntId/:appId/:mnuNm',isLoggedIn,umCtrl.createMnu_post);
router.post('/menu/assignusers/:appId/:mnuNm',isLoggedIn,umCtrl.assignMnuToUsr_post);
router.get('/getMnuItmsbyappID/:clntId/:tntId/:appId',isLoggedIn,umCtrl.mnuItms_get);
router.get('/getSubMnuItmsbyappID/:clntId/:tntId/:appId',isLoggedIn,umCtrl.submnuItms_get);

/**** Designations ****/
router.get('/getUsrsWthDsgns/:clntId/:tntId',isLoggedIn,umCtrl.UsrsWthDsgns_get);

/**** Heirarchy ****/
router.get('/getUserHeirarchy/:clntId/:tntId',isLoggedIn,umCtrl.usrsHirchy_get);
router.post('/Heirarchy/level',isLoggedIn,umCtrl.updHirchy_post);

router.post('/updtDsgn',isLoggedIn,umCtrl.dsgn_update);

/****************************************************************
			         Livetracking Module
*****************************************************************/

// router.get('/getAsrtLst',isLoggedIn,liveTrackCtrl.getAsrtLst);
// router.get('/vehStat/:asrt_type_id',isLoggedIn,liveTrackCtrl.asrtLstId_get);


//In Plyback --- fixed startFences and endFences 
router.get('/getDumpingAreas',liveTrackCtrl.dumpAreas_get);

//Interval 1 Hr -- Only currently running vehicles in playback vehilce dropdown
router.get('/getCurRunnVeh',liveTrackCtrl.currRunnVehicles_get);

//common route for garbage/collection points in liveTracking
router.get('/getDumpers',isLoggedIn,liveTrackCtrl.getDumpers_get);

//masterRoute in liveMap
router.get('/vehicles',isLoggedIn,liveTrackCtrl.vehicles_get);

//getting count of cleared dustbins
router.get('/getCollectionPointsCt/:landmrkId',isLoggedIn,liveTrackCtrl.clearedCollectionPointsCount_get);	

/*Routes For Drop Down Filters In Livemap*/
router.get('/getAsrtType',liveTrackCtrl.asserType_get);
router.get('/getAsrtGrp/:asrt_type_id',liveTrackCtrl.asrtGrpList_get);
router.get('/getAsrtCat/:asrt_grp_id',liveTrackCtrl.asrtCatList_get);
router.get('/getAsrtList/:asrt_ctgry_id',liveTrackCtrl.asrtList_get);

//In Playback, By Selecting vehicles it has to show current day running trips for that id
router.get('/getTripsByAsrtId/:asrt_id',liveTrackCtrl.tripsByAsrtId_get);

//masterRoute in playBack
router.post('/playback/playBackByVeh',playBackCtrl.playBackByVeh_get);
/****************************************************************
			         GEO Fencing Module
*****************************************************************/


router.get('/getCategories',isLoggedIn,geoCodeCtrl.category_get);
router.get('/getCategories/:fnce_ctgry_id',isLoggedIn,geoCodeCtrl.categories_get);
router.get('/getLandMarkGrps',geoCodeCtrl.landMarksGrps_get);

//In Geo Fence Module, by clicking on group name showing wards
router.get('/getFences/:fnce_grp_id',geoCodeCtrl.fenceLandMark_get);
//ds#/liveTracking/livemap
router.get('/getFences',geoCodeCtrl.fencesGeo);
//ds#/liveTracking/playback
router.get('/getDumpingAreas',geoCodeCtrl.muncipalDumpAreas_get);

router.post('/postLandMarks',isLoggedIn,geoCodeCtrl.landMarks_post);
router.post('/postGeoFences',isLoggedIn,geoCodeCtrl.postGeoFences_post);
router.post('/postLandMarkGrp',geoCodeCtrl.LandMarkGrps_post);
router.post('/postLandMarkCat',geoCodeCtrl.LandMarkCate_post);
router.post('/updateLandMarkCate',geoCodeCtrl.LandMarkCate_update);
router.post('/updateLandMarkGrps',geoCodeCtrl.LandMarkGrps_update);

//ds#/geoCoding/landmark/Groups
router.post('/delLandMarkGrpNme', geoCodeCtrl.delLandMarkGrpNme_delete);
///ds#/geoCoding/landmark/Category
router.post('/delLandMarkCateNme',geoCodeCtrl.delLandMarkCateNme_delete);

//ds#/geoCoding
router.get('/dashBoards',geoCodeCtrl.dashBoards_get);


/****************************************************************
			         Vehicle Tracking Module
*****************************************************************/

// Vehicle Tracking System Module(Asrtlst)
router.get('/asrtLst',isLoggedIn,vtsAssetsCtrl.asrtLst_get);


// Vehicle Tracking System Module(Fence Report)
router.post('/fnceRptLst',isLoggedIn,vtsRptsCtrl.fnceRpt_post);

// Vehicle Tracking System Module(LandMark Report)
router.post('/lndmrkRptLst',isLoggedIn,vtsRptsCtrl.lndmrkRpt_post);

// Vehicle Tracking System Module(Trips Report)
router.post('/tripRptLst',vtsRptsCtrl.tripRpt_post);
router.get('/tripRptLst/:vehId',isLoggedIn,vtsRptsCtrl.tripRptbyVehId_get);				// Getting Lst week trips count day by day for selected vehicle

/// Vehicle Tracking System Module(Coverage Report)
router.post('/lndMrkCovRptLst',isLoggedIn,vtsRptsCtrl.lndmrkcovRpt_post);

// Vehicle Tracking System Module(Distance Report)
router.post('/distRptLst',vtsRptsCtrl.DistRpt_post);


// Vehicle Tracking System Module(Fuel Consumption Report)
router.post('/mileageRptLst',vtsRptsCtrl.MileageRpt_get);

//heatMap weeklyRpt
router.post('/weeklyHeatMapRpt',vtsRptsCtrl.heatMapRptWeekly_get);

// Vehicle Tracking System Module(Dumpers Date wise Report)
router.post('/getCvrdDumpersStatus',vtsRptsCtrl.cvrdDumpersStatus_get);
router.get('/getFenceCategories',vtsRptsCtrl.fenceCategories_get);

router.post('/postVehRptgetData',vtsRptsCtrl.vehRptgetData_post);

router.post('/crewPerformanceRptLst',vtsRptsCtrl.crewPrfmnceData_post);

router.get('/getGarbageType',vtsRptsCtrl.getCollecType_get);


// Vehicle Tracking System Module(Tripdetails Report)
router.post('/tripDtlsRptLst',vtsRptsCtrl.TripDtlsRpt_post);

// Vehicle Tracking System Module(LandMarks Not covered Report)
router.get('/lndMrkNtCovLst',vtsRptsCtrl.LndMrkNtCovLst_get);

// Vehicle Tracking System Module(Tripdetails Report)
router.get('/alltripslst/:trp_rn_id',vtsRptsCtrl.allTripDtlsRpt_get);

// Vehicle Tracking System Module(LandMark Details Chart(vts_home))
router.get('/lndmrkCovChrtLst',vtsDshBrdChrtsCtrl.LndMrksCovDtls_get);
router.get('/lndmrkCovChrtLst/:vehId',vtsDshBrdChrtsCtrl.LndMrksCovDtlsbyVehId_get);	// Getting Lst week covered landmarks count day by day for selected vehicle

// Vehicle Tracking System Module(Daywise Distance Count(vts_home))
router.get('/dywseDstnsCnt',vtsDshBrdChrtsCtrl.dywseDstnsCnt_get);
router.get('/dywseDstnsCnt/:vehId',vtsDshBrdChrtsCtrl.dywseDstnsCntbyVehId_get);		// Getting Lst week distance count day by day for selected vehicle

// Vehicle Tracking System Module(Vehicle status Chart(vts_home))
router.get('/vehStsCnts',vtsDshBrdChrtsCtrl.vehStsCntsDtls_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/mcrtotVehCnt',vtsDshBrdChrtsCtrl.totVehCntDtls_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/mcrEchVehCnt',vtsDshBrdChrtsCtrl.mcrEchVehCntDtls_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/shEchVehCnt',vtsDshBrdChrtsCtrl.shEchVehCntDtls_get);

// Vehicle Tracking System Module(Total Shah Vehicle count scoreboard(vts_home))
router.get('/shtotVehCnt',vtsDshBrdChrtsCtrl.totShVehCntDtls_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/totVehStsCnt',vtsDshBrdChrtsCtrl.totVehStsDtl_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/totTrpDistCnt',vtsDshBrdChrtsCtrl.totTrpDistCntDtl_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/totColPntsCnt',vtsDshBrdChrtsCtrl.totColPntsCntDtl_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/asrtGrpLstbyType/:asrt_type_id',vtsDshBrdChrtsCtrl.asrtGrpLstbyType_get);

// Vehicle Tracking System Module(Total Vehicle count scoreboard(vts_home))
router.get('/asrtDshbrdCtgryLst',vtsDshBrdChrtsCtrl.asrtDshbrdCtgryLst_get);

/****Vehicle details (VehDashboard)****/
router.get('/vehDshCvgChrt/:asrt_id/:cat_id',vtsDshBrdChrtsCtrl.vehDshChrtDtl_get);
router.get('/vehTrpCvgChrt/:asrt_id/:cat_id',vtsDshBrdChrtsCtrl.vehTrpCvgChrt_get);
router.get('/vehBinCvgChrt/:asrt_id/:cat_id',vtsDshBrdChrtsCtrl.vehBinCvgChrt_get);

// Vehicle Tracking System Module(Vehicle active count (VehDashboard))
router.get('/vehStsCnt',vtsDshBrdChrtsCtrl.vehDshStsDtl_get);

router.get('/vehTypeTrpDtls',vtsDshBrdChrtsCtrl.vehTypeTrpDtls_get);		// getting Vehicles and their trips by asert group for chart

// Vehicle Tracking System Module(Total collection points count scoreboard(collectionpointsDashboard))
router.get('/totlndmrksCnt',vtsDshBrdChrtsCtrl.totLndMrksCntDtl_get);

// Vehicle Tracking System Module(Total collection points covered scoreboard(collectionpointsDashboard))
router.get('/totLndMrksCov',vtsDshBrdChrtsCtrl.totLndMrksCovDtl_get);

// Vehicle Tracking System Module(Collection points not covered scoreboard(collectionpointsDashboard))
router.get('/lndMrksNtCov',vtsDshBrdChrtsCtrl.lndMrksNtCovDtl_get);

// Vehicle Tracking System Module(Collection points not covered scoreboard(collectionpointsDashboard))
router.get('/lndMrksLst',vtsDshBrdChrtsCtrl.lndMrksLst_get);

// Vehicle Tracking System Module(Collection points not covered scoreboard(collectionpointsDashboard))
router.get('/lndMrkDshChrtLst/:fnce_id',vtsDshBrdChrtsCtrl.lndMrkDshChrtLst_get);

// Vehicle Tracking System Module(vts dst max,min,avg)
router.get('/totTrctsDstTrvld',vtsDshBrdChrtsCtrl.totTrctsDstTrvld_get);

// Vehicle Tracking System Module(vts dst today)
router.get('/totDstCrDyTrvld',vtsDshBrdChrtsCtrl.totDstCrDyTrvld_get);

// Vehicle Tracking System Module(vts fncs max,min,avg)
router.get('/totFnceCovd',vtsDshBrdChrtsCtrl.totFnceCovd_get);

// Vehicle Tracking System Module(vts fncs daily)
router.get('/totFnceDyCovd',vtsDshBrdChrtsCtrl.totFnceDyCovd_get);

// Vehicle Tracking System Module(vts fncs max,min,avg)
router.get('/totFnceBnsCovd',vtsDshBrdChrtsCtrl.totFnceBnsCovd_get);

// Vehicle Tracking System Module(vts fncs daily)
router.get('/totFnceBnsDyCovd',vtsDshBrdChrtsCtrl.totFnceBnsDyCovd_get);

// Vehicle Tracking System Module(vts trips)
router.get('/totTripsCovd',vtsDshBrdChrtsCtrl.totTripsCovd_get);

// Vehicle Tracking System Module(vts trips)
router.get('/totTripsDyCovd',vtsDshBrdChrtsCtrl.totTripsDyCovd_get);

// Vehicle Tracking System Module(Tripdetails Report)
router.post('/wrdcovgRptLst',vtsRptsCtrl.wrdCovgDtlsRpt_post);

// Vehicle Tracking System Module(vts total vehs)
router.get('/totVehCnt',vtsDshBrdChrtsCtrl.totVehCnt_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/totCapctyLftd',vtsDshBrdChrtsCtrl.totCapctyLftd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/totCapctyDyLftd',vtsDshBrdChrtsCtrl.totCapctyDyLftd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrVehsDistTrvld',vtsDshBrdChrtsCtrl.mcrVehsDistTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrVehsTiprDistTrvld',vtsDshBrdChrtsCtrl.mcrVehsTiprDistTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrVehsDmprsDistTrvld',vtsDshBrdChrtsCtrl.mcrVehsDmprsDistTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hirdTrctsDistTrvld',vtsDshBrdChrtsCtrl.hirdTrctsDistTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hirdTipprsDistTrvld',vtsDshBrdChrtsCtrl.hirdTipprsDistTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrTrctsTripsTrvld',vtsDshBrdChrtsCtrl.mcrTrctsTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrTipprsTripsTrvld',vtsDshBrdChrtsCtrl.mcrTipprsTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrDmprsTripsTrvld',vtsDshBrdChrtsCtrl.mcrDmprsTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hirdTrctsTripsTrvld',vtsDshBrdChrtsCtrl.hirdTrctsTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hirdTipprsTripsTrvld',vtsDshBrdChrtsCtrl.hirdTipprsTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrColPntsCovd',vtsDshBrdChrtsCtrl.mcrColPntsCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hrdColPntsCovd',vtsDshBrdChrtsCtrl.hrdColPntsCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrDmprBnsCovd',vtsDshBrdChrtsCtrl.mcrDmprBnsCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrTrctsCpty',vtsDshBrdChrtsCtrl.mcrTrctsCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrTipprsCpty',vtsDshBrdChrtsCtrl.mcrTipprsCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrDmprsCpty',vtsDshBrdChrtsCtrl.mcrDmprsCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hirdTrctsCpty',vtsDshBrdChrtsCtrl.hirdTrctsCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/hirdTipprsCpty',vtsDshBrdChrtsCtrl.hirdTipprsCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchTrctDstTrvld',vtsDshBrdChrtsCtrl.mcrEchTrctDstTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchTipprDstTrvld',vtsDshBrdChrtsCtrl.mcrEchTipprDstTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchTractsDstTrvld',vtsDshBrdChrtsCtrl.shahEchTractsDstTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchTipprDstTrvld',vtsDshBrdChrtsCtrl.shahEchTipprDstTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchTrctTripsTrvld',vtsDshBrdChrtsCtrl.mcrEchTrctTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchTipprTripsTrvld',vtsDshBrdChrtsCtrl.mcrEchTipprTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchDmprTripsTrvld',vtsDshBrdChrtsCtrl.mcrEchDmprTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchTractTripsTrvld',vtsDshBrdChrtsCtrl.shahEchTractTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchTipprTripsTrvld',vtsDshBrdChrtsCtrl.shahEchTipprTripsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchColPntCovd',vtsDshBrdChrtsCtrl.mcrEchColPntCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchColPntCovd',vtsDshBrdChrtsCtrl.shahEchColPntCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchTrctCpty',vtsDshBrdChrtsCtrl.mcrEchTrctCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchTipprCpty',vtsDshBrdChrtsCtrl.mcrEchTipprCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchDmprCpty',vtsDshBrdChrtsCtrl.mcrEchDmprCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchTrctCpty',vtsDshBrdChrtsCtrl.shahEchTrctCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/shahEchTipprCpty',vtsDshBrdChrtsCtrl.shahEchTipprCpty_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchDmprDstTrvld',vtsDshBrdChrtsCtrl.mcrEchDmprDstTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchdyDstTrvld/:asrt_id',vtsDshBrdChrtsCtrl.vehEchdyDstTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchdyTrpsTrvld/:asrt_id',vtsDshBrdChrtsCtrl.vehEchdyTrpsTrvld_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchdyColPntsCovd/:asrt_id',vtsDshBrdChrtsCtrl.vehEchdyColPntsCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchdyDmprBnsCovd/:asrt_id',vtsDshBrdChrtsCtrl.vehEchdyDmprBnsCovd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchdyCptyDmpd/:asrt_id',vtsDshBrdChrtsCtrl.vehEchdyCptyDmpd_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/mcrEchDmprBnsCovd',vtsDshBrdChrtsCtrl.mcrEchDmprBnsCovd_get);

// Vehicle Tracking System Module(Vehicle details Report)
router.get('/totlVehCnt',vtsRptsCtrl.totlVehCnt_get);

// Vehicle Tracking System Module(Vehicle details Report)
router.get('/vehCntErly',vtsRptsCtrl.vehCntErly_get);

// Vehicle Tracking System Module(Vehicle details Report)
router.get('/vehCntMidl',vtsRptsCtrl.vehCntMidl_get);

// Vehicle Tracking System Module(Vehicle details Report)
router.get('/vehCntDly',vtsRptsCtrl.vehCntDly_get);

// Vehicle Tracking System Module(Distance Report)
router.post('/vehdtlRptLst',vtsRptsCtrl.vehdtlRpt_post);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchTripDtl/:asrt_id',vtsRptsCtrl.vehEchTripDtl_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchTripDtl/:asrt_id/:dt',vtsRptsCtrl.vehEchTripDtl_get);

// Vehicle Tracking System Module(vts total vehs)
router.get('/vehEchTripFncDtl/:trp_rn_id',vtsRptsCtrl.vehEchTripFncDtl_get);

// Vehicle Tracking System Module(Distance Report)
router.post('/allCropVehsDtl',vtsDshBrdChrtsCtrl.allCropVehsDtl_post);

// Get all the Fences based on trip run Id
router.get('/vehEchTripFncDtlAll/:trp_rn_id',vtsRptsCtrl.AllFncsForTripDtl_get);

// Vehicle Tracking System Module(Total counts cards)
router.post('/totalVehDtls',vtsDshBrdChrtsCtrl.totalVehDtls_post);

// // Vehicle Tracking System Module(vts reports)
router.post('/hrlyFncsCovd',vtsRptsCtrl.hrlyFncsCovd_post);
// router.get('/hrlyFncsCovd',vtsRptsCtrl.hrlyFncsCovd_get);
/**** Master Data Routes ****/

router.get('/getAllasrtLst',isLoggedIn,vtsAssetsCtrl.asrtLst_get);			// Vehicle Tracking System Module(Asrtlst)
// getting asserts by asrt_id,asrt_ctgry_id,asrt_ownr_id,asrt_type_id,asrt_grp_id,dprt_id,crnt_dvce_id,crnt_ast_sts_id,clnt_id,tnt_id
router.post('/asrtLstbyId',isLoggedIn,vtsAssetsCtrl.GetAsrtLstById_get);
router.post('/AddVehicle',isLoggedIn,vtsAssetsCtrl.addAsrts_post);		// Inserting Vehicles
router.post('/UpdateVehicle/:asrtId',isLoggedIn,vtsAssetsCtrl.updateAsrts_post);		// Updating Vehicles
router.post('/DeleteVehicle',isLoggedIn,vtsAssetsCtrl.deleteAsrts_post);		// Deleting Vehicles

router.get('/asrtTypeLst',isLoggedIn,vtsAssetsCtrl.asrtTypeLst_get);	// Getting Assert Type List
router.post('/AddVehicleType',isLoggedIn,vtsAssetsCtrl.addAsrtType_post);		// Inserting assert Types
router.post('/UpdateVehicleType',isLoggedIn,vtsAssetsCtrl.updAsrtType_post);		// Updating assert Types
router.post('/DeleteVehicleType',isLoggedIn,vtsAssetsCtrl.delAsrtType_post);		// Deleting assert Types

router.get('/asrtCtgryLst',vtsAssetsCtrl.asrtCtgryLst_get);		// Getting Assert Category List
router.post('/addVehicleCtgry',isLoggedIn,vtsAssetsCtrl.AddAsrtCtgry_post);		// Inserting assert Category
router.post('/updVehicleCtgry',isLoggedIn,vtsAssetsCtrl.UpdAsrtCtgry_post);		// Updating assert Category
router.post('/delVehicleCtgry',isLoggedIn,vtsAssetsCtrl.delAsrtCtgry_post);		// Deleting assert Category

router.get('/asrtGroupLst',isLoggedIn,vtsAssetsCtrl.asrtGroupLst_get);	// Getting Assert Group List
router.post('/addGroup',isLoggedIn,vtsAssetsCtrl.addGroup_post);		// Inserting assert Group
router.post('/updGroup',isLoggedIn,vtsAssetsCtrl.updGroup_post);		// Updating assert Group
router.post('/delGroup',isLoggedIn,vtsAssetsCtrl.delGroup_post);		// dELETING assert Group

router.get('/deviceList',isLoggedIn,vtsAssetsCtrl.deviceLst_get);		// Getting Device List
router.get('/tenantList',isLoggedIn,vtsAssetsCtrl.tenantLst_get);		// Getting Tenant List
router.get('/clientList',isLoggedIn,vtsAssetsCtrl.clientLst_get);		// Getting Client List
router.get('/ownersList',isLoggedIn,vtsAssetsCtrl.ownersLst_get);		// Getting Owners List

router.get('/DepartmentList',isLoggedIn,vtsAssetsCtrl.deptmntsLst_get);		// Getting departments List
router.post('/addDepartment',isLoggedIn,vtsAssetsCtrl.addDepartment_post);		// Inserting assert Types
router.post('/updateDepartment',isLoggedIn,vtsAssetsCtrl.updDepartment_post);		// Updating department Types
router.post('/delDepartment',isLoggedIn,vtsAssetsCtrl.delDepartment_post);		// deleting assert Types

router.get('/vendors',isLoggedIn,vtsAssetsCtrl.typeOfVendors_get);		// getting vendors for side menu in master data

/**** Alerts Subscribtion ****/
router.get('/subscriptions/:usr_id',vtsDshBrdChrtsCtrl.subscriptions_get);
router.get('/alertCtgry',vtsDshBrdChrtsCtrl.alertCtgry_get);
router.post('/addNewSubscription',vtsDshBrdChrtsCtrl.newSubscriber_post);
router.post('/updSubscription',vtsDshBrdChrtsCtrl.updSubscription_post);
router.get('/driversList',vtsDshBrdChrtsCtrl.allCrewList_get);
router.post('/driverAssignment',vtsDshBrdChrtsCtrl.driverAssignmentDtls_post);
router.post('/AlertList',vtsDshBrdChrtsCtrl.AlertList_get);			// getting alert details between two dates
router.post('/DriverAsigList',vtsDshBrdChrtsCtrl.driverAsigDtls_get);	// getting driver details between two dates
router.post('/addFuelDtls',vtsDshBrdChrtsCtrl.addFuelDtls_get);	// Inserting Fuel Details into db

router.post('/getAsrtsForFuel',vtsDshBrdChrtsCtrl.getAsrtsForFuel_get);	// Getting asrt_lst by asrt_ctgryId,AsrtGrpId
router.get('/asrtFuelHistory/:asrt_id',vtsDshBrdChrtsCtrl.asrtFuelHistory_get);		// getting asrt fuel history

/****************************************************************
			         Support Module
*****************************************************************/

router.get('/support/ticketDtl/:status_id',supportCtrl.getTicketDtlsByStsId_get);   //get the ticket details based on status

router.get('/support/grpticketDtl/:status_id',supportCtrl.getGrpOpenTicketDtls_get);

router.get('/support/openticketDtl/:usr_id',supportCtrl.getOpenTicketDtls_get);


router.get('/support/getticketStatusList',supportCtrl.getticketStatusList_get);

router.get('/support/ticketDtlById/:tkt_id',supportCtrl.getTicketDtlById_get);

router.get('/support/getTicketCmnts/:tkt_id',supportCtrl.getTicketCmnts_get);

router.post('/support/updtTicketStatus/:tkt_id/:tkt_status_id',supportCtrl.updateTicketDtls_post);
router.get('/support/getUsrGrpList',supportCtrl.getUsrGrpList_get);
router.get('/support/getUsrList',supportCtrl.getUsrList_get);


router.post('/support/assignusr',supportCtrl.assignusr_post);

router.post('/support/submtCmnts',supportCtrl.submtCmnts_post);

router.get('/support/acceptedtktDtl/:usr_id',supportCtrl.getacceptedtktDtls_get);

router.post('/support/acptTktStatusChange',supportCtrl.acptTktStatusChange_post);

 router.get('/support/tktPriorityDtls',supportCtrl.getTktPriorityDtls_get);

 // ********** app routes****************

 router.post('/sanitation/drvrImgDtlsPost',sanitationCtrl.drvrImgDtls_post);
 router.get('/sanitation/drvrAsgndtlsGet/:drvrId',sanitationCtrl.drvrAsgndtls_get);
 router.post('/sanitation/getTrpDtls/:asrtId',sanitationCtrl.TrpDtls_post);
 router.get('/totalvehicles',liveTrackCtrl.vehicles_get);
 // router.post('/sanitation/updtTrpDtls',sanitationCtrl.updtTrpDtls_post);
 router.get('/sanitation/getRecentImgs/:crw_id',sanitationCtrl.recentImgs_get);
 router.post('/mbleDtls',sanitationCtrl.mbleDtls_post);
module.exports = router;