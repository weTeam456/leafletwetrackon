var util = require('util'),
    bodyParser = require('body-parser'),
    express = require('express'),
    expressValidator = require('express-validator'),
    app = express();

geoFenceSch = require('./server/api/modules/geocoding/validators/geoFence');

app.use(bodyParser.json());
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      fld_nm : formParam,
      msg_tx   : msg,
      spld_vl : value
    };
  }
}));

console.log("sdfsdf"+JSON.stringify(geoFenceSch.FenceGrpSch));
app.post('/:urlparam', function(req, res) {

  // VALIDATION
  // checkBody only checks req.body; none of the other req parameters
  // Similarly checkParams only checks in req.params (URL params) and
  // checkQuery only checks req.query (GET params).
  req.checkBody(geoFenceSch.FenceGrpSch.body);
//   req.checkParams('urlparam', 'Invalid urlparam').isAlpha();
//   req.checkQuery('getparam', 'Invalid getparam').isInt();
 req.checkHeaders(geoFenceSch.FenceGrpSch.header);


  // Alternatively use `var result = yield req.getValidationResult();`
  // when using generators e.g. with co-express
  req.getValidationResult().then(function(result) {
    if (!result.isEmpty()) {
      res.status(400).send({"status":200,"data":[],"errors":util.inspect(result.array())});
      return;
    }
    res.json({
      urlparam: req.params.urlparam,
      getparam: req.params.getparam,
      postparam: req.params.postparam
    });
  });
});
app.listen(8888);