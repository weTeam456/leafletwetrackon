var express             = require('express');
var cookieParser        = require('cookie-parser');
var expressSession      = require('express-session');
var passport            = require('passport');

var app                 = express();

var bodyParser          = require('body-parser');
var errorhandler        = require('errorhandler');
var notifier            = require('node-notifier');

var fs                  = require('fs');
var compression         = require('compression')
    moment              = require('moment');
    path                = require('path');
    nconf               = require('nconf');
    expressValidator    = require('express-validator');
    util                = require('util');

appRoot                 = __dirname;
var settings            = require('./utils/settings.utils');
appSettings             = settings.getSettings();
var auditRequest        = require('./utils/audit.requests');

var banner              = require('./utils/banner');
var osDtls              = require('./server/ds-services/modules/monitor/utils/osdetails');
var repDsEvents         = require('./server/ds-services/modules/monitor/exposures/reportEvents');



var logDirectory        = appRoot + '/' + appSettings.app.log_dir;
var sch = require(appRoot+'/utils/schedule.utils');


app.use('/client', express.static(__dirname + '/client'));
app.use(express.static(appRoot + '/api/swagger/swagger.json'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      fld_nm : formParam,
      msg_tx   : msg,
      spld_vl : value
    };
  }
}));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended:true})); // support encoded bodies

app.use(expressSession({
  secret:'dream1234',
  resave:true,
  saveUninitialised: false,
  cookie: {
    maxAge:3600000
  },
  rolling: true,
  unset: 'destroy'
}));

// required for passport
require('./server/api/modules/auth/passport')(passport); // pass passport for configuration

// Initialization of passport
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());

app.use(compression());

// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

// setup the logger

app.use(errorhandler({log: errorNotification}));

function errorNotification(err, str, req) {
  var title = 'Error in ' + req.method + ' ' + req.url

  notifier.notify({
    title: title,
    message: str
  })
}

app.use(logErrors);
function logErrors(err, req, res, next) {
    console.error(err.stack);
    next(err);
}

process.on('uncaughtException', (err) => {
  console.error(err.stack);
});


app.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});


app.all('*',function(req,res,next){
  auditRequest.auditAllUrls(req,res,'audit','');
  next();
});

app.get('/api/swagger/swagger.json', function(req,res){
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.sendFile(appRoot+'/api/swagger/swagger.json');
});

app.use('/', require('./server/app/routes/appRoutes'));
app.use('/apiv1', require('./server/api/routes/apiRoutes'));
app.use('/ds', require('./server/ds-services/routes/dsRoutes'));



if(appSettings.dsUtils.scheduler.start_on_launch===true)
    sch.scheduleScripts(function(err){
        if(err){ 
            console.log("Issues with Scheculing :: "+err);
        }
    });
if(appSettings.dsServices.monitor.REGISTER_ON_START===true)
      osDtls.registerApp();

var server = app.listen(appSettings.app.app_port, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log(appSettings.app.app_name+' Started. listening at http://%s:%s', host, port);
    if(appSettings.dsServices.monitor.MONITOR_APP_START===true)
      repDsEvents.reportNodeStart();
    if(appSettings.app.banner==true)
      banner.startPrint("http://"+server.address().address+":"+server.address().port);
});


process.on('exit', (code) => {
  banner.endPrint("http://"+server.address().address+":"+server.address().port);
});