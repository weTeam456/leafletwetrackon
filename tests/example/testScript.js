describe('Example Tests', function () {
        it('has a dummy spec to test 2 + 2', function() {
            // An intentionally failing test. No code within expect() will never equal 4.
            expect(2+2).toEqual(4);
        });

        // it('test1', function() {
        //     $().destroyWin('window'); // jquery 1.3.2 
        //     expect($j('#window')).not.toBeInDOM(); // $j = jquery 2.1.1 
        // });
        
});